package com.fertiletech.fieldcodata.backend.comments;

import java.util.Collections;
import java.util.List;

import com.google.appengine.api.datastore.Text;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

public class CommentsDAO {
	//comment array indicates auto-generation
	public static AcitivityComment createComment(String[] commentList, boolean showPublic, Key<? extends Object> leadKey, String user)
	{
		StringBuilder joiner = new StringBuilder();
		for(String comment : commentList)
			joiner.append("<p>").append(comment).append("</p>");
		return createComment(new Text(joiner.toString()), showPublic, leadKey, user);
	}
	
	
	
	public static AcitivityComment createComment(Text text,
			boolean showPublic, Key<? extends Object> leadKey, String user) {
		return createComment(null, text, showPublic, leadKey, user);
	}



	public static AcitivityComment createComment(Objectify ofy, Text comment, boolean showPublic, Key<? extends Object> leadKey, String user)
	{
		if(ofy == null)
			ofy = ObjectifyService.beginTransaction();
		AcitivityComment loanComment = new AcitivityComment(comment, leadKey, showPublic, user);			
		try
		{
			ofy.put(loanComment);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return loanComment;
	}

	public static RatingComment createRating(int rating, Text comment, boolean showPublic, Key<? extends Object> leadKey, String user)
	{
		Objectify ofy = ObjectifyService.beginTransaction();
		RatingComment loanComment = new RatingComment(comment, leadKey, showPublic, user, rating);			
		try
		{
			ofy.put(loanComment);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return loanComment;
	}	
	
	public static List<AcitivityComment> getActivityComments(Objectify ofy, Key<? extends Object> leadKey)
	{
		if(ofy == null)
			ofy = ObjectifyService.begin();
		Key<AcitivityComment> parentKey = AcitivityComment.getParentKey(leadKey);
		List<AcitivityComment> list = ofy.query(AcitivityComment.class).ancestor(parentKey).list();
		Collections.sort(list, Collections.reverseOrder()); //sort list in descending order
		return list;
	}
	
	public static List<RatingComment> getRatings(Objectify ofy, Key<? extends Object> leadKey)
	{
		if(ofy == null)
			ofy = ObjectifyService.begin();
		Key<AcitivityComment> parentKey = AcitivityComment.getParentKey(leadKey);
		List<RatingComment> list = ofy.query(RatingComment.class).ancestor(parentKey).list();
		Collections.sort(list, Collections.reverseOrder()); //sort list in descending order
		return list;
	}	
}
