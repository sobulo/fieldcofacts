package com.fertiletech.fieldcodata.backend.entities;

import java.util.Date;
import java.util.HashSet;

import com.google.appengine.api.datastore.Text;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Parent;

public class RelatedContact extends Contact{
	
	String relationship;
	

	//this comes with assumption that same contact can not be related
	//to multiple residents 
	@Parent Key<? extends Contact> parentContact;
	
	@Override
	public Key<RelatedContact> getKey() {
		return new Key<RelatedContact>(parentContact, RelatedContact.class, key);
	}

	RelatedContact(){}
	
	RelatedContact(Key<? extends Contact> parent, String firstName, String lastName, Date dob, String primaryEmail, String primaryNumber,
			HashSet<String> secondaryNumbers, HashSet<String> secondaryEmails,
			String companyName, Text address, Boolean isMale, String title, String relationship) 
	{
		super(firstName, lastName, dob, primaryEmail, primaryNumber, secondaryNumbers, secondaryEmails, companyName, address, isMale, title);
		this.relationship = relationship;
		this.parentContact = parent;
	}
	
	public String getRelationship()
	{
		return relationship;
	}
	
	void setRelationship(String relationship)
	{
		this.relationship = relationship;
	}
	
	protected RelatedContact clone() throws CloneNotSupportedException
	{
		RelatedContact r = (RelatedContact) super.clone();
		r.parentContact = null;
		return r;
	}
}
