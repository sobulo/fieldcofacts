package com.fertiletech.fieldcodata.backend.entities;

import com.googlecode.objectify.Key;


public class Facility extends Apartment{
	
	public Facility() {
		super();
	}

	public Facility(Key<Building> buildingKey, String displayName) {
		super(buildingKey, displayName);
	}

	@Override
	public Key<Facility> getKey()
	{
		return new Key<Facility>(buildingKey, Facility.class, displayName);
	}
}
