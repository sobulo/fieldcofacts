package com.fertiletech.fieldcodata.backend.entities;

import java.util.Date;
import java.util.HashSet;

import com.google.appengine.api.datastore.Text;
import com.googlecode.objectify.Key;

public class Representative extends Contact{
	boolean accessAllCompany;

	Representative(){}
	
	Representative(String firstName, String lastName, Date dob, String primaryEmail, String primaryNumber,
			HashSet<String> secondaryNumbers, HashSet<String> secondaryEmails,
			String companyName, Text address, Boolean isMale, String title, boolean accessAllCompany) 
	{
		super(firstName, lastName, dob, primaryEmail, primaryNumber, secondaryNumbers, secondaryEmails, companyName, address, isMale, title);
		this.accessAllCompany = accessAllCompany;
	}

	@Override
	public Key<Representative> getKey() {
		return new Key<Representative>(Representative.class, key);
	}

	public boolean isAccessAllCompany() {
		return accessAllCompany;
	}

	void setAccessAllCompany(boolean accessAllCompany) {
		this.accessAllCompany = accessAllCompany;
	}
}
