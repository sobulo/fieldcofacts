package com.fertiletech.fieldcodata.backend.entities;

import javax.persistence.Id;

import com.google.appengine.api.datastore.Text;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Unindexed;

@Cached
@Unindexed
public class Building implements Location{
	@Id
	private String key;
	@Indexed private String displayName;
	private Text buildingAddress;
	
	Building(){}
	
	Building(String key, String displayName, Text buildingAddress) {
		this.displayName = displayName;
		this.buildingAddress = buildingAddress;
		this.key = key;
	}
	
	public Key<Building> getKey()
	{
		return new Key<Building>(Building.class, key);
	}

	public String getDisplayName() {
		return displayName;
	}
	
	public Text getBuildingAddress() {
		return buildingAddress;
	}

	void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	void setBuildingAddress(Text buildingAddress) {
		this.buildingAddress = buildingAddress;
	}

	@Override
	public String getLocationName() {
		return key;
	}
}
