package com.fertiletech.fieldcodata.backend.entities;

import java.util.Date;
import java.util.HashSet;

import javax.persistence.PrePersist;

import com.google.appengine.api.datastore.Text;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Indexed;

public class ProspectiveTenant extends Contact{
	@Indexed Boolean convertedLead;
	@Indexed Date lastModified;

	ProspectiveTenant(){}
	
	ProspectiveTenant(String firstName, String lastName, Date dob, String primaryEmail, String primaryNumber,
			HashSet<String> secondaryNumbers, HashSet<String> secondaryEmails,
			String companyName, Text address, Boolean isMale, String title) 
	{
		super(firstName, lastName, dob, primaryEmail, primaryNumber, secondaryNumbers, secondaryEmails, companyName, address, isMale, title);
	}
	
	@Override
	public Key<ProspectiveTenant> getKey() {
		return new Key<ProspectiveTenant>(ProspectiveTenant.class, key);
	}
	
	public Boolean isConvertedLead()
	{
		return convertedLead;
	}
	
	public void setConvertedLead(boolean converted)
	{
		this.convertedLead = converted;
	}
	
	@PrePersist
	void stampModificationDate()
	{
		this.lastModified = new Date();
	}
	
	public Date getLastModified()
	{
		return lastModified;
	}
}
