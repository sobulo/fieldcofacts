package com.fertiletech.fieldcodata.backend.entities;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;

/**
 * @author sobulo@fertiletech.com
 */
public class EntityConstants {
	public final static String[][] EMPLOYEE_NAMES = {
		// this is a comment, below are non technician employees
	{ "Oluyemi", "Adeosun", "oluyemi.adeosun@fieldcolimited.com", "123" }, // ok
			{ "Ajibola", "Abudu", "ajibola.abudu@fieldcolimited.com", "123" }, // ok
		{ "Funmi", "Adisa", "funmi.adisa@fieldcolimited.com", "123" },
		{ "Yewande", "Llewellyn", "yewande.llewellyn@fieldcolimited.com",
				"123" }, //2
		{ "Ayodeji", "Bajomo", "ayodeji.bajomo@fieldcolimited.com", "123" },
		{ "Theresa", "Obuye", "theresa.obuye@fieldcolimited.com", "123" },
		{ "Ibukun", "Paulissen", "Ibukun.Paulissen@fieldcolimited.com",
				"123" },
		{ "Kayode", "Ogunbunmi", "kayode.ogunbunmi@fieldcolimited.com",
				"123" },
		{ "Blessing", "Ejiogu", "blessing.ejiogu@fieldcolimited.com", "123" },
		{ "Oladipupo", "Bakare", "oladipupo.bakare@fieldcolimited.com",
				"123" },
		{ "Temitope", "Shoga", "temitope.shoga@fieldcolimited.com", "123" },
		{ "Mojisola", "Bakare", "mojisola.bakare@fieldcolimited.com", "123" }, //10
		{ "Micheal", "Gbadamosi", "michael.gbadamosi@fieldcolimited.com",
				"123" },
		{ "Wakwe", "Osita", "osita.wakwe@fieldcolimited.com", "123" },
		{ "Lilian", "Eleghasim", "Lilian.Eleghasim@fieldcolimited.com",
				"123" },
		{ "Lolia", "Deal", "lolia.deal@fieldcolimited.com", "123" },
		{ "Rachel", "Onyedi", "rachel.onyedi@fieldcolimited.com", "123" },
		{ "Samuel", "Sylvanus", "samuel.sylvanus@fieldcolimited.com", "123" },
		{ "Bashiru", "Ojulari", "bashiru.ojulari@fieldcolimited.com", "123" },
		{ "Olamilekan", "Igbayilola",
				"olamilekan.igbayilola@fieldcolimited.com", "123" },
		{ "Olakunle", "Egbaiyelo", "olukunle.egbaiyelo@fieldcolimited.com",
				"123" },
		{ "Deji", "Opeloyeru", "Deji.Opeloyeru@fieldcolimited.com", "123" },
		{ "Funmilola", "Oyerogba", "Funmilola.Oyerogba@fieldcolimited.com",
				"123" },
		{ "Edward", "Unuane", "Edward.Unuane@fieldcolimited.com", "123" },
		{ "Adedeji", "Adio", "adedeji.adio", "123" },
		// technicians -- Mak start here
		{ "Vincent", "Amaechi", "vincent.amaechi", "4034" },
		{ "Waheed", "Oladapo", "waheed.oladapo", "999" },
		{ "Fanu", "Kehinde", "fanu.kehinde", "999" },

		{ "Festus", "Samuel", "festus.samuel", "1817" },
		{ "James", "Onoja", "james.onoja", "3496" },
		{ "Sylvester", "Ndimele", "sylvester.ndimele", "4354" },

		{ "Alabi", "Abduljeleel", "alabi.abduljeleel", "3453" },
		{ "Ajisafe", "Kazeem", "ajisafe.kazeem", "999" },
		{ "Amen", "Ekpe", "amen.ekpe", "999" },
		{ "Daniel", "Nwakwo", "daniel.nwakwo", "999" },
		{ "Akorede Mojeed", "Kolawole", "akorede.kolawole", "999" },
		{ "Ramon", "Salami", "ramon.salami", "999" }, //35

		{ "Taiwo", "Moses", "taiwo.moses", "999" },
		{ "John", "Ogunwole", "john.ogunwole", "999" },
		{ "Segun", "Sule", "segun.sule", "999" }, //38
		{ "Akintola", "Aliu", "akintola.aliu", "999" },
		{ "Yinka", "Otegbade", "yinka.otegbade", "999" },
		{ "Ogundare", "Sunday", "ogundare.sunday", "999" },
		{ "Adesina", "Babatunde", "adeshina.babatunde", "999" },

		{ "Onasanya", "Moshood", "onasanya.moshood", "1904" },
		{ "Rasheed", "Oguntade", "rasheed.oguntade", "999" },
		{ "Wasiu", "Balogun", "balogun.wasiu", "1289" },
		{ "Akinyemi", "A", "akinyemi.a", "999" },
		{ "Stephen", "Ifemadu", "stephen.ifemadu", "999" },
		{ "Cosmos", "A", "cosmos.a", "999" },//48
		{ "Kunle", "Adegbite", "kunle.adegbite", "999" },//49
		{ "Moses", "Onyelor", "moses.onyelor", "999" },
		{ "Alex", "Ohari", "alex.ohari", "999" },
		{ "Badmus", "Kehinde", "badmus.kehinde", "999" },//52
		{ "Chuka", "M", "chuka.m", "999" }, //53
		{ "Dare", "Olalude", "dare.olalude", "999" },
		{ "Hassan", "Olanrewaju", "hassan.olanrewaju", "999" },
		{ "Idowu", "Segun", "idowu.segun", "999" },
		{ "Shopeyin", "Jamiu", "shopeyin.jamiu", "999" },//57

		{ "Bawa-Allah", "Nurudeen", "bawa-allah.nurudeen", "999" },
		{ "Ajibola", "Sola", "ajibola.sola", "999" },
		{ "Oshun", "Alimi", "oshun.alimi", "999" },
		{ "Augustine", "Oare", "augustine.oare", "999" },

		{ "Oriku", "Benjamin", "oriku.benjamin", "999" },
		};
	
	static String[] ACCTS_ENERGY_CHARGES = {"Electricity/Diesel(Energy)", "Admn. Fee"};
	static String[] ACCTS_SERVICE_CHARGES = 
	{
		"Security ",
		"Garden Maintenance",
		"Lift maintenance ",
		"Gym Instructor & Maintenance of Gym equipments ",
		"Cost of Personnel",
		"Cleaning of common areas",
		"Refuse disposal",
		"Fumigation",
		"Depreciation on Gym Equipments",
		"Depreciation on Water Treatment Plant",
		"Depreciation on Sewage ",
		"Depreciation Air conditioners",
		"Depreciation on the 500KVA Generator  ",
		"Depreciation on the 500KVA Generator  ",
		"Supply of plumbing consumables",
		"Supply of electrical consumables",
		"Supply of R & A consumables",
		"Supply of water treatment chemicals",
		"Supply of swimming pool chemicals & water treatment chemicals",
		"Repainting",
		"Carpentry work ",
		"Replacement of compressor ",
		"Servicing of water treatment plant",
		"Servicing of A C",
		"Reprogramming of DSTV decoder ",
		"Servicing of gym equipment ",
		"Renovation work ",
		"Production of Stewards ID cards",
		"Aluminium work ",
		"Replacement of swimming pool media",
		"Servicing of fire extinguisher ",
		"Purchase of sewage treatment chemicals",
		"Evacuation of sewage transfer pump basket",
		"Deep cleaning ",
		"Cost of stationaries and Internet services",
		"Plumbing work ",
		"Other Admn. Charges" 
	};
	
	static String[] ACCTS_TRANSACTION_CHARGES = {
		"Rent", 
		"Legal Fees",			
		"Post Vacation",
		"Agency"			
	};
	
	
	static String[] ACCT_TYPES = {
		"Diesel/ Electricity(Energy)",
		"Service Charge",
		"Transactions"
	};
	
	static String[] ACCT_DESCRIPTIONS = 
	{
		"Account of energy deposits paid by resident",
		"Account of service charge deposits paid by resident to maintain the apartment"	,		
		"Account of transaction fees such as rent, agency, post vacation and legal fees"
	};
	
	static String[] PROD_CAT = {"CMB-Electrical", "CMB-Plumbing", "FA-Electronics", "FA-Air Conditioner"};
	
	public static HashMap<String, String> BILL_MAPPINGS = new HashMap<String, String>();
	public static HashMap<String, String> ACCOUNT_MAPPINGS = new HashMap<String, String>();
	public static HashMap<String, String> PROD_CAT_MAPPINGS = new HashMap<String, String>();
	static
	{
		for(String type : ACCTS_ENERGY_CHARGES)
			BILL_MAPPINGS.put(type, ACCT_TYPES[0]);
		for(String type : ACCTS_SERVICE_CHARGES)
			BILL_MAPPINGS.put(type, ACCT_TYPES[1]);
		for(String type : ACCTS_TRANSACTION_CHARGES)
			BILL_MAPPINGS.put(type, ACCT_TYPES[2]);

		for(int i = 0; i < ACCT_TYPES.length;i++)
			ACCOUNT_MAPPINGS.put(ACCT_TYPES[i], ACCT_DESCRIPTIONS[i]);
		
		for(int i = 0; i < PROD_CAT.length; i++)
		{
			String key = PROD_CAT[i];
			String desc = "It is recommended that you prefix fixed assets (e.g. Furniture, Air Conditioner, TVs etc) with FA-, this will help with future data-migration post ICESTONE enhancements.";
			if(key.startsWith("CMB-"))
				desc = "It is recommended that you prefix consumables (e.g. light bulbs, duct tape, diesel) with CMB-, this will help with future data-migration post ICESTONE enhancements." +
						"Using this prefix also provides a work-around to current constraint of serial numbers built into the system, in particular convention of Date/Quantity/Unit (e.g." +
						" Serial as [20thMar2014/25/Boxes], Category as [CMB-Plumbing] and Product as [Duct Tape] will allow you to get consumables into the system and later filter using CMB- as prefix when generating reports";
			PROD_CAT_MAPPINGS.put(key, desc);
		}
	}
	
	
	//public final static String APP_PARAM_VENDOR_CATEGORY_LIST_KEY = "vendor-category-names
	
    public final static String EMAIL_CONTROLLER_ID = "EMAIL Controller";    
    public final static String SMS_CONTROLLER_ID = "SMS Controller";
    public final static String[] FACILITY_TYPES = {"Maintenance Bin", "Trash Bin", "Repairs Bin"};
    
	//number and date formats
	public final static NumberFormat NUMBER_FORMAT = NumberFormat.getInstance();
	public final static DateFormat DATE_FORMAT = new SimpleDateFormat("d MMM yyyy");
	static
	{
		NUMBER_FORMAT.setMaximumFractionDigits(2);
		NUMBER_FORMAT.setMinimumIntegerDigits(1);
	}
}
