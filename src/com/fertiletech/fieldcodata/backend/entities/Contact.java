package com.fertiletech.fieldcodata.backend.entities;

import java.util.Date;
import java.util.HashSet;

import javax.persistence.Id;

import com.google.appengine.api.datastore.Text;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Unindexed;

@Unindexed
public abstract class Contact implements Cloneable
{
	@Id
	protected Long key;
	
	@Indexed private String primaryEmail;
	private String firstName;
	private String lastName;
	private String primaryNumber;
	private HashSet<String> secondaryNumbers;
	private HashSet<String> secondaryEmails;
	@Indexed private String companyName;
	private Date dob;
	private Text address;
	String title;
	boolean isMale;	
	
	Contact(){}
	
	Contact(String firstName, String lastName, Date dateOfBirth, String primaryEmail, String primaryNumber,
			HashSet<String> secondaryNumbers, HashSet<String> secondaryEmails,
			String companyName, Text address, Boolean isMale, String title) {
		setContactValues(firstName, lastName, dateOfBirth, primaryEmail, primaryNumber, secondaryNumbers, secondaryEmails, companyName, address, isMale, title);
	}
	
	private void setContactValues(String firstName, String lastName, Date dateOfBirth, String primaryEmail, String primaryNumber,
			HashSet<String> secondaryNumbers, HashSet<String> secondaryEmails,
			String companyName, Text address, String title) {
		this.primaryEmail = primaryEmail == null? null : primaryEmail.toLowerCase();
		this.primaryNumber = primaryNumber;
		this.secondaryNumbers = secondaryNumbers;
		this.secondaryEmails = secondaryEmails;
		this.companyName = companyName == null? null : companyName.toUpperCase();
		this.address = address;
		this.title = title;
		this.firstName = firstName;
		this.lastName = lastName;
		this.dob = dateOfBirth;
	}	
	
	void setContactValues(String firstName, String lastName, Date dob, String primaryEmail, String primaryNumber,
			HashSet<String> secondaryNumbers, HashSet<String> secondaryEmails,
			String companyName, Text address, Boolean isMale, String title) {
		setContactValues(firstName, lastName, dob, primaryEmail, primaryNumber, secondaryNumbers, secondaryEmails, companyName, address, title);
		if(isMale != null)
			this.isMale = isMale;
	}
	

	void setTitle(String title)
	{
		this.title = title;
	}

	public String getTitle() {
		return title;
	}
	
	public Date getDateOfBirth()
	{
		return dob;
	}
	
	void setPrimaryEmail(String primaryEmail) {
		this.primaryEmail = primaryEmail;
	}

	void setPrimaryNumber(String primaryNumber) {
		this.primaryNumber = primaryNumber;
	}

	void setSecondaryNumbers(HashSet<String> secondaryNumbers) {
		this.secondaryNumbers = secondaryNumbers;
	}

	void setSecondaryEmails(HashSet<String> secondaryEmails) {
		this.secondaryEmails = secondaryEmails;
	}

	void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	void setAddress(Text address) {
		this.address = address;
	}

	void setMale(boolean isMale) {
		this.isMale = isMale;
	}

	public String getPrimaryEmail() {
		return primaryEmail;
	}

	public String getPrimaryNumber() {
		return primaryNumber;
	}

	public HashSet<String> getSecondaryNumbers() {
		return secondaryNumbers;
	}

	public HashSet<String> getSecondaryEmails() {
		return secondaryEmails;
	}

	public String getCompanyName() {
		return companyName;
	}

	public Text getAddress() {
		return address;
	}

	public Boolean isMale() {
		return isMale;
	}
	
	public abstract <T extends Contact> Key<T> getKey();

	public String getFirstName() {
		return firstName;
	}

	void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	@Override
	protected Contact clone() throws CloneNotSupportedException
	{
		Contact c = (Contact) super.clone();
		c.dob = this.dob==null?null:new Date(this.dob.getTime());
		c.address = this.address==null?null:new Text(this.address.getValue());
		c.key = null;
		return c;
	}
}
