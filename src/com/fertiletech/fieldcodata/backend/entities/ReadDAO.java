package com.fertiletech.fieldcodata.backend.entities;



import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import com.fertiletech.fieldcodata.ui.server.GeneralFuncs;
import com.fertiletech.fieldcodata.ui.shared.dto.DTOConstants;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessageHeader;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessageHeader.TableMessageContent;
import com.fertiletech.fieldcodata.ui.shared.exceptions.MissingEntitiesException;
import com.google.appengine.api.datastore.QueryResultIterable;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.Query;

public class ReadDAO {
    private static final Logger log =
            Logger.getLogger(ReadDAO.class.getName());
	static
	{
		WriteDAO.registerClassesWithObjectify();
	}
	
	public static List<TableMessage> getLeaseDTOs(Key<Tenant> tk, Objectify ofy, TableMessageHeader h)
	{
		List<LeaseNegotiation> leases = ofy.query(LeaseNegotiation.class).ancestor(tk).list();
		ArrayList<TableMessage> result = new ArrayList<TableMessage>();
		if(h != null)
			result.add(h);
		for(LeaseNegotiation l : leases)
			result.add(l.getDTO());
		return result;
	}

	public static List<TableMessage> getLeaseDTOs(Date start, Date end, Objectify ofy, TableMessageHeader h) throws MissingEntitiesException
	{
		Query<LeaseNegotiation> leaseQuery1 = ofy.query(LeaseNegotiation.class);
		Query<LeaseNegotiation> leaseQuery2 = ofy.query(LeaseNegotiation.class);

		if(start != null)
		{
			leaseQuery1.filter("startDate >=", start);
			leaseQuery2.filter("endDate >=", start);
		}
		
		if(end != null)
		{
			leaseQuery1.filter("startDate <=", end);
			leaseQuery2.filter("endDate <=", end);
		}

		HashSet<Key<Tenant>> tenantKeys = new HashSet<Key<Tenant>>();
		ArrayList<LeaseNegotiation> leases = new ArrayList<LeaseNegotiation>();
		HashSet<Key<LeaseNegotiation>> leaseUniqueHelper = new HashSet<Key<LeaseNegotiation>>();
		leases.addAll(leaseQuery1.list());
		for(LeaseNegotiation l : leases)
			leaseUniqueHelper.add(l.getKey());
		for(LeaseNegotiation l : leaseQuery2.list())
			if(!leaseUniqueHelper.contains(l.getKey()))
				leases.add(l);
		
		for(LeaseNegotiation l : leases)
			tenantKeys.add((Key<Tenant>) l.getTenant());
		Map<Key<Tenant>, Tenant> tenantKeyMap = getEntities(tenantKeys, ofy, false);
		ArrayList<TableMessage> result = new ArrayList<TableMessage>();
		if(h != null)
			result.add(h);
		for(LeaseNegotiation l : leases)
		{
			TableMessage m = l.getDTO();
			Tenant t = tenantKeyMap.get(l.getTenant());
			String fullName = "";
			if(t != null)
			{
				fullName = t.getFirstName() + " " + t.getLastName() + " - " + t.getCompanyName(); 
				if(t.isArchived())
					fullName += " [A]";
				if(t.isOwnerOccupier())
					fullName += " [O]";
				if(l.isOwner != t.isOwnerOccupier())
					fullName += "*";	
			}
			m.setText(DTOConstants.LEG_OWNR_IDX, fullName);
			result.add(m);
		}
		return result;
	}	
	
	public static List<Key<Building>> getAllBuildings(Objectify ofy)
	{
		return ofy.query(Building.class).listKeys();
	}
	
	public static HashMap<String, String> getBuildingNameToDescriptionMap(Objectify ofy)
	{
		List<Building> buildings = ofy.query(Building.class).list();
		HashMap<String, String> result = new HashMap<String, String>(buildings.size());
		for(Building bld : buildings)
			result.put(bld.getKey().getName(), bld.getDisplayName());
		return result;
	}
	
	
	public static List<Key<Apartment>> getApartments(Objectify ofy, Key<Building> buildingKey)
	{
		return ofy.query(Apartment.class).ancestor(buildingKey).listKeys();
	}
	
	public static List<Vendor> getAllVendors(Objectify ofy)
	{
		return ofy.query(Vendor.class).list();
	}
	
	public static List<Key<Tenant>> getTenant(Objectify ofy, Key<? extends Apartment> aptUnit, boolean archived)
	{
		List<Key<Tenant>> result = addArchiveFlagToTenantQuery(archived, ofy.query(Tenant.class).ancestor(aptUnit)).listKeys();
		return result;
	}
	
	public static Key<Tenant> getCurrentTenant(Objectify ofy, Key<? extends Apartment> aptUnit)
	{
		List<Key<Tenant>> result = getTenant(ofy, aptUnit, false);
		if(result.size() > 1)
			throw new RuntimeException("Found more than 1 primary tenant in the same apartment, please list other tenants as secondary tenants");
		return result.size() == 0 ? null : result.get(0);
	}	
	
	public static List<Key<Tenant>> getTenants(Objectify ofy, Key<Building> bldKey, Boolean archive)
	{
		Query<Tenant> q = addArchiveFlagToTenantQuery(archive, ofy.query(Tenant.class).ancestor(bldKey));
		return q.listKeys();
	}
	
	public static Query<Tenant> addArchiveFlagToTenantQuery(Boolean archive, Query<Tenant> q)
	{
		if(archive == null)
			return q;
		return q.filter("archived", archive);
	}
	
	public static List<Tenant> getTenantObjects(Objectify ofy, Key<Building> bldKey, Boolean archive)
	{
		return addArchiveFlagToTenantQuery(archive, ofy.query(Tenant.class).ancestor(bldKey)).list();
	}	
	
    public static <T> void printKeyDifferences(Collection<Key<T>> requestedCollection, Collection<Key<T>> retrievedCollection)
    {
    	Key<T>[] requestedKeys = new Key[requestedCollection.size()];
    	requestedKeys = requestedCollection.toArray(requestedKeys);
    	Key<T>[] retrievedKeys = new Key[retrievedCollection.size()];
    	retrievedKeys = retrievedCollection.toArray(retrievedKeys);
    	ArrayList<Key<T>> retrievedOnly, requestedOnly, intersect;
    	retrievedOnly = new ArrayList<Key<T>>();
    	requestedOnly = new ArrayList<Key<T>>();
    	intersect = new ArrayList<Key<T>>();
    	GeneralFuncs.arrayDiff(requestedKeys, retrievedKeys, intersect, requestedOnly, retrievedOnly);
    	
    	log.severe("Unable to load some entites from database ....");
    	for(Key<T> k : requestedOnly)
    		log.severe("Unable to retrieve: " + k);
        log.severe("Keys initially requested are ...... ");
        for(Key<T> errKey : requestedCollection)
            log.severe("Requested Key: " + errKey);    	
    }    
    
    public static <T> Map<Key<T>, T> getEntities(Collection<Key<T>> entityKeys, Objectify ofy, boolean throwException) throws MissingEntitiesException
    {
    	if(entityKeys == null || entityKeys.size() == 0) return new HashMap<Key<T>, T>(1);
        Map<Key<T>, T> entityData = ofy.get(entityKeys);
        if(throwException && entityData.size() != entityKeys.size())
        {
        	printKeyDifferences(entityKeys, entityData.keySet());
    		throw new MissingEntitiesException("Batch get failed, see server log for details");
        }
        return entityData;
    }

    //used by billing modules, necessary? or just a carryover from ported code off another project (grep)
    public static String keyToPrettyString(Key<?> k)
    {
        return k.getName().replace('~', '-').toUpperCase();
    }

    public static String getNameFromLastKeyPart(Key<?> k)
    {
        String[] keyParts = k.getName().split("~");
        return(keyParts[keyParts.length - 1]);
    }

    public static String getNameFromLastKeyPart(Key<?> k, int n)
    {
        String[] keyParts = k.getName().split("~");
        StringBuilder result = new StringBuilder();
        for(int i= n; i > 0; i--)
        	result.append(keyParts[keyParts.length - i]);
        return result.toString();
    }
    
    public static HashMap<String, String>[] getApartments(Key<Building> buildingKey)
    {
    	Objectify ofy = ObjectifyService.begin();
    	QueryResultIterable<Key<Apartment>> apartments = ofy.query(Apartment.class).ancestor(buildingKey).fetchKeys();
    	Set<Key<Object>> occupiedApartments = ofy.query(Tenant.class).ancestor(buildingKey).filter("archived", false).fetchParentKeys();
    	HashMap<String, String>[] result = new HashMap[2];
    	result[DTOConstants.APT_OCCUPIED_IDX] = new HashMap<String, String>(occupiedApartments.size());
    	result[DTOConstants.APT_VACANT_IDX] = new HashMap<String, String>();
    	for(Key<Apartment> apt : apartments)
    	{
    		String aptDisplay = buildingKey.getName() + "-" + apt.getName();
    		if(occupiedApartments.contains(apt))
    			result[DTOConstants.APT_OCCUPIED_IDX].put(apt.getString(), aptDisplay);
    		else
    			result[DTOConstants.APT_VACANT_IDX].put(apt.getString(), aptDisplay);
    	}
    	return result;
    }
    
    public static List<TableMessage> getApartmentsTable(Key<Building> buildingKey, Objectify ofy)
    {
    	TableMessageHeader h = new TableMessageHeader(3);
		h.setText(DTOConstants.TNT_APT_IDX, "Apartment",
				TableMessageContent.TEXT);
		h.setText(DTOConstants.TNT_FNAME_IDX, "Name",
				TableMessageContent.TEXT);
		h.setText(DTOConstants.TNT_BUILDING_IDX, "Building",
				TableMessageContent.TEXT);
		h.setMessageId(buildingKey.getString());
		List<TableMessage> result = new ArrayList<TableMessage>();
		result.add(h);
    	QueryResultIterable<Key<Apartment>> apartments = ofy.query(Apartment.class).ancestor(buildingKey).fetchKeys();
    	List<Tenant> tenants = getTenantObjects(ofy, buildingKey, false);
    	HashMap<Key<? extends Apartment>, String> tenantAptTable= new HashMap<Key<? extends Apartment>, String>();
    	for(Tenant t : tenants)
    		tenantAptTable.put(t.getApartmentUnit(), t.getLastName() + ", " + t.getFirstName());
    	for(Key<Apartment> apt : apartments)
    	{
    		TableMessage m = new TableMessage(3, 0, 0);
    		m.setText(DTOConstants.TNT_BUILDING_IDX, buildingKey.getName());
    		m.setText(DTOConstants.TNT_APT_IDX, apt.getName());
    		m.setText(DTOConstants.TNT_FNAME_IDX, tenantAptTable.get(apt));
    		m.setMessageId(apt.getString());
    		result.add(m);
    	}
    	return result;
    }
    
    public static ProspectiveData getProspectiveData(Key<ProspectiveTenant> ptKey)
    {
    	Objectify ofy = ObjectifyService.begin();
    	List<ProspectiveData> dataWrapper = ofy.query(ProspectiveData.class).ancestor(ptKey).list();
    	if(dataWrapper.size() == 0)
    		return null;
    	else if(dataWrapper.size() == 1)
    		return dataWrapper.get(0);
    	else
    		throw new RuntimeException("Multiple data objects per propspective tenant not supported");
    }
    
	public static String getSiteName(Key<? extends Location> locKey)
	{
		return Apartment.getLocationName(locKey);
	}  
	
	public static List<Key<Tenant>> getCompanyTenants(Key<Representative> repKey, Objectify ofy)
	{
		return ofy.query(Tenant.class).filter("companyRep", repKey).listKeys();
	}
    
}
