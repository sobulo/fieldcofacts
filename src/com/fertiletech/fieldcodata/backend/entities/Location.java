package com.fertiletech.fieldcodata.backend.entities;

import com.googlecode.objectify.Key;

public interface Location {
	String getLocationName();
	Key<? extends Location> getKey();
}
