package com.fertiletech.fieldcodata.backend.entities;

import java.util.Date;
import java.util.HashMap;

import javax.persistence.Id;
import javax.persistence.PrePersist;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Parent;
import com.googlecode.objectify.annotation.Serialized;
import com.googlecode.objectify.annotation.Unindexed;

@Unindexed
public class ProspectiveData {
	
	@Id Long id;
	
	@Parent Key<ProspectiveTenant> user;
	
	@Indexed Date lastModified;

	@Serialized
	HashMap<String, String> data;
	
	public ProspectiveData()
	{
		data = new HashMap<String, String>();
	}
	
	public ProspectiveData(Key<ProspectiveTenant> userKey, HashMap<String, String> formInfo)
	{
		this.user = userKey;
		this.data = formInfo;
	}
	
	public HashMap<String, String> getData()
	{
		return data;
	}
	
	public Key<ProspectiveData> getKey()
	{
		return new Key<ProspectiveData>(user, ProspectiveData.class, id);
	}
	
	public void setData(HashMap<String, String> formInfo)
	{
		this.data = formInfo;
	}
	
	@PrePersist void setTimeStamp()
	{
		this.lastModified = new Date();
	}
}
