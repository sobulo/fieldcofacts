package com.fertiletech.fieldcodata.backend.entities;

import java.util.Date;
import java.util.HashSet;

import com.google.appengine.api.datastore.Text;
import com.googlecode.objectify.Key;

public class Vendor extends Contact{

	String category;
	
	Vendor(){}
	
	Vendor(String category, String firstName, String lastName, Date dob, String primaryEmail, String primaryNumber,
			HashSet<String> secondaryNumbers, HashSet<String> secondaryEmails,
			String companyName, Text address, Boolean isMale, String title)
	{
		super(firstName, lastName, dob, primaryEmail, primaryNumber, secondaryNumbers, 
				secondaryEmails, companyName, address, isMale, title);
		this.category = category;
		if(companyName == null || companyName.trim().length() == 0)
			throw new NullPointerException("Can not create a vendor with a blank/null company name");
	}
	
	@Override
	public Key<Vendor> getKey() {
		return new Key<Vendor>(Vendor.class, key);
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

}
