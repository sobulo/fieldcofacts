package com.fertiletech.fieldcodata.backend.entities;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Logger;

import com.fertiletech.fieldcodata.backend.accounting.InetBill;
import com.fertiletech.fieldcodata.backend.accounting.InetCompanyAccount;
import com.fertiletech.fieldcodata.backend.accounting.InetDeposit;
import com.fertiletech.fieldcodata.backend.accounting.InetResidentAccount;
import com.fertiletech.fieldcodata.backend.accounting.InetWithdrawal;
import com.fertiletech.fieldcodata.backend.comments.AcitivityComment;
import com.fertiletech.fieldcodata.backend.comments.RatingComment;
import com.fertiletech.fieldcodata.backend.inventory.InventoryAllocation;
import com.fertiletech.fieldcodata.backend.inventory.InventoryItem;
import com.fertiletech.fieldcodata.backend.inventory.ProductDetails;
import com.fertiletech.fieldcodata.backend.tickets.RequestTicket;
import com.fertiletech.fieldcodata.scripts.pdfreports.InetImageBlob;
import com.fertiletech.fieldcodata.scripts.tasks.TaskQueueHelper;
import com.fertiletech.fieldcodata.ui.server.TenantDAOManagerImpl;
import com.fertiletech.fieldcodata.ui.shared.dto.DTOConstants;
import com.fertiletech.fieldcodata.ui.shared.dto.FormConstants;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.fertiletech.fieldcodata.ui.shared.exceptions.DuplicateEntitiesException;
import com.fertiletech.fieldcodata.ui.shared.exceptions.MissingEntitiesException;
import com.google.appengine.api.datastore.Text;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

public class WriteDAO {
	public static void logAndThrowDuplicateException(Logger log,
			String msgPrefix, Key<?> objectifyKey)
			throws DuplicateEntitiesException {
		String msg = msgPrefix
				+ ". Attempt to create a duplicate object with Key: ";
		log.warning(msg + objectifyKey);
		throw new DuplicateEntitiesException(msg + objectifyKey);
	}

	public static void logAndThrowMissingEntityException(Logger log,
			String msgPrefix, Key<?> objectifyKey)
			throws MissingEntitiesException {
		String msg = msgPrefix + ".  object not found - Key is: ";
		log.warning(msg + objectifyKey);
		throw new MissingEntitiesException(msg + objectifyKey);
	}

	private static final Logger log = Logger
			.getLogger(WriteDAO.class.getName());

	private static boolean isOfyRegistered = false;

	static {
		registerClassesWithObjectify();
	}

	// objectify requires we register all classes whose object instances
	// will/can be
	// persisted in appengine's native datastore

	public static void registerClassesWithObjectify() {
		if (isOfyRegistered)
			return;

		ObjectifyService.register(ApplicationParameters.class);
		ObjectifyService.register(Apartment.class);
		ObjectifyService.register(AcitivityComment.class);
		ObjectifyService.register(Building.class);
		ObjectifyService.register(Tenant.class);
		ObjectifyService.register(ProspectiveTenant.class);
		ObjectifyService.register(ProspectiveData.class);
		ObjectifyService.register(Representative.class);
		ObjectifyService.register(InetImageBlob.class);
		ObjectifyService.register(InetBill.class);
		ObjectifyService.register(InetWithdrawal.class);
		ObjectifyService.register(InetCompanyAccount.class);
		ObjectifyService.register(InetDeposit.class);
		ObjectifyService.register(LeaseNegotiation.class);
		ObjectifyService.register(Facility.class);
		ObjectifyService.register(Vendor.class);
		ObjectifyService.register(RatingComment.class);
		ObjectifyService.register(RequestTicket.class);
		ObjectifyService.register(InventoryAllocation.class);
		ObjectifyService.register(InventoryItem.class);
		ObjectifyService.register(ProductDetails.class);
		ObjectifyService.register(RelatedContact.class);
		ObjectifyService.register(InetResidentAccount.class);

		isOfyRegistered = true;
	}

	public static ApplicationParameters createApplicationParameters(String id,
			HashMap<String, String> parameters, String type)
			throws DuplicateEntitiesException {
		ApplicationParameters paramsObject = new ApplicationParameters(id,
				parameters, type);
		Objectify ofy = ObjectifyService.beginTransaction();
		try {
			Key<ApplicationParameters> existingKey = paramsObject.getKey();
			ApplicationParameters temp = ofy.find(existingKey);
			if (temp != null)
				logAndThrowDuplicateException(log,
						"Application parameter already exists", existingKey);
			else {
				ofy.put(paramsObject);
				ofy.getTxn().commit();
			}
		} finally {
			if (ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return paramsObject;
	}
	
	//exists because spreadsheet upload doesn't have access to apartment constructor
	public static Apartment getApartmentObject(Key<Building> bk,String displayName)
	{
		return new Apartment(bk, displayName);
	}
	
	public static LeaseNegotiation createLease(Key<Tenant> tk, TableMessage m, String userComments)
	{
		Objectify ofy = ObjectifyService.beginTransaction();
		LeaseNegotiation newLease = null;
		try
		{
			//query for all existing leases
			ArrayList<LeaseNegotiation> leases = new ArrayList<LeaseNegotiation>();			
			leases.addAll(ofy.query(LeaseNegotiation.class).ancestor(tk).list());
			newLease = LeaseNegotiation.getLeaseFromDTO(tk, m);
			leases.add(newLease);
			ofy.put(newLease);
			Tenant t = updateLeaseOwnershipFlag(tk, leases, ofy); //unnecessary sort, but minimizes bugs by being consistent with remove use case
			ofy.put(t);
			ofy.getTxn().commit();
			String[] comments = {newLease.getHTML("<font color='green'><b>ADDED LEASE DETAILS LISTED BELOW</b></font><br/>"), userComments};
			TaskQueueHelper.scheduleCreateComment(comments, tk.getString(), m.getText(DTOConstants.LEG_UPDT_USER_USR));
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return newLease; 
	}
	
	private static Comparator<LeaseNegotiation> ownerComparator = new Comparator<LeaseNegotiation>() {

		@Override
		public int compare(LeaseNegotiation o1, LeaseNegotiation o2) {
			if(o1.getEndDate() == null && o2.getEndDate() == null)
				return o1.createDate.compareTo(o2.createDate);
			else if(o1.getEndDate() == null) return -1;
			else if(o2.getEndDate() == null) return 1;
			
			int comp = o1.getEndDate().compareTo(o2.getEndDate());
			if(comp == 0)
				comp = o1.createDate.compareTo(o2.createDate);
			return comp;
		}
	};
	public static Tenant updateLeaseOwnershipFlag(Key<Tenant> tk, List<LeaseNegotiation> leases, Objectify ofy)
	{
		Collections.sort(leases, ownerComparator);
		Tenant t = ofy.get(tk);
		if(leases.size() == 0)
			t.owner = false;
		else
			t.owner = leases.get(leases.size() - 1).isOwner;
		return t;
	}
	
	public static Tenant updateArchiveFlag(Key<Tenant> tk, String updateUser, String userComments) throws MissingEntitiesException, DuplicateEntitiesException
	{
		Objectify ofy = ObjectifyService.beginTransaction();
		Tenant t = ofy.find(tk);
		try
		{
			if(t == null)
				throw new MissingEntitiesException("Unable to find tenant object");
			
			if(t.isArchived())
				throw new DuplicateEntitiesException("This resident is already archived");
			if(t.isOwnerOccupier())
				throw new DuplicateEntitiesException("An owner/occupier may not be archived");
			
			t.archived = true;
			ofy.put(t);
			String[] comments = {"<font color='red'><b>Archived Tenant</b></font><br/>" + t.getFirstName() + " " + t.getLastName(), userComments};
			TaskQueueHelper.scheduleCreateComment(comments, tk.getString(), updateUser);	
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return t;
	}

	public static Building createBuilding(String id, String displayName,
			String address) throws DuplicateEntitiesException {
		Building b = new Building(id, displayName, address == null ? null
				: new Text(address));
		Objectify ofy = ObjectifyService.beginTransaction();
		try {
			Key<Building> existingBuilding = b.getKey();
			Building temp = ofy.find(existingBuilding);
			if (temp != null)
				logAndThrowDuplicateException(log,
						"Application parameter already exists",
						existingBuilding);
			else {
				ofy.put(b);
				ofy.getTxn().commit();
			}
		} finally {
			if (ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return b;
	}

	public static Apartment createApartment(Key<Building> bk, String name) throws DuplicateEntitiesException
	{
		Apartment a = new Apartment(bk, name);
		return createApartment(a);
	}
	private static Apartment createApartment(Apartment a) throws DuplicateEntitiesException {
		Objectify ofy = ObjectifyService.beginTransaction();
		try {
			Apartment exists = ofy.find(a.getKey());
			if(exists != null)
				throw new DuplicateEntitiesException("Location exists already " + exists.getKey());
			ofy.put(a);
			ofy.getTxn().commit();
		} finally {
			if (ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return a;
	}
	
	public static Facility createFacility(Key<Building> buildingKey,
			String displayName) throws DuplicateEntitiesException {
		Facility a = new Facility(buildingKey, displayName);
		return (Facility) createApartment(a);
	}	

	public static Representative createRepresentative(String firstName,
			String lastName, Date dob, String primaryEmail, String primaryNumber,
			HashSet<String> secondaryNumbers, HashSet<String> secondaryEmails,
			String companyName, String tenantAddress, Boolean isMale,
			String title, boolean accessAll) throws DuplicateEntitiesException {
		Text address = tenantAddress == null ? null : new Text(tenantAddress);
		Representative rep = new Representative(firstName, lastName, dob,
				primaryEmail, primaryNumber, secondaryNumbers, secondaryEmails,
				companyName, address, isMale, title, accessAll);
		Objectify ofy = ObjectifyService.beginTransaction();
		try {
			ofy.put(rep);
			ofy.getTxn().commit();
		} finally {
			if (ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return rep;
	}
	
	public static List<Tenant> updateRepresentative(HashSet<Key<Tenant>> tenantKeys, Key<Representative> repKey)
	{
		HashMap<Key<Building>, List<Key<Tenant>>> tenantGrouping = new HashMap<Key<Building>, List<Key<Tenant>>>();
		for(Key<Tenant> k : tenantKeys)
		{
			Key<Building> bk = k.getParent().getParent();
			if(!tenantGrouping.containsKey(bk))
				tenantGrouping.put(bk, new ArrayList<Key<Tenant>>());
			tenantGrouping.get(bk).add(k);
		}
				
		ArrayList<Tenant> resultAdd = new ArrayList<Tenant>();
		
		for(List<Key<Tenant>> tKeys : tenantGrouping.values())
		{
			Objectify ofy = ObjectifyService.beginTransaction();
			try
			{
				Collection<Tenant> tenants = ofy.get(tKeys).values();
				for(Tenant t : tenants)
					t.setCompanyRep(repKey);
				ofy.put(tenants);
				ofy.getTxn().commit();
				resultAdd.addAll(tenants);
			}
			finally
			{
				if(ofy.getTxn().isActive())
					ofy.getTxn().rollback();
			}
		}
		return resultAdd;
	}
	
	public static List<Tenant> removeRepresentative(HashSet<Key<Tenant>> tenantKeys, Key<Representative> repKey)
	{
		HashMap<Key<Building>, List<Key<Tenant>>> tenantGrouping = new HashMap<Key<Building>, List<Key<Tenant>>>();
		for(Key<Tenant> k : tenantKeys)
		{
			Key<Building> bk = k.getParent().getParent();
			if(!tenantGrouping.containsKey(bk))
				tenantGrouping.put(bk, new ArrayList<Key<Tenant>>());
			tenantGrouping.get(bk).add(k);
		}
				
		ArrayList<Tenant> resultRemove = new ArrayList<Tenant>();
					
		for(List<Key<Tenant>> tKeys : tenantGrouping.values())
		{
			Objectify ofy = ObjectifyService.beginTransaction();
			try
			{
				Collection<Tenant> tenants = ofy.get(tKeys).values();
				for(Tenant t : tenants)
				{
					if(repKey.equals(t.getCompanyRep()))
						t.setCompanyRep(null);
					else
						throw new RuntimeException("Attempted to remove a company rep not associated with tenant " + t.getKey());
				}
				ofy.put(tenants);
				ofy.getTxn().commit();
				resultRemove.addAll(tenants);
			}
			finally
			{
				if(ofy.getTxn().isActive())
					ofy.getTxn().rollback();
			}
		}		
		return resultRemove;
	}	

	public static Tenant createTenant(String firstName, String lastName,
			Date dob, String primaryEmail, String primaryNumber,
			HashSet<String> secondaryNumbers, HashSet<String> secondaryEmails,
			String companyName, String tenantAddress, Boolean isMale,
			String title, Key<Apartment> aptUnit, Key<Representative> companyRep)
			throws DuplicateEntitiesException {
		return createTenant(null, firstName, lastName, dob, primaryEmail,
				primaryNumber, secondaryNumbers, secondaryEmails, companyName,
				tenantAddress, isMale, title, aptUnit, companyRep, null);
	}

	public static Tenant createTenant(HashMap<String, String> data,
			String firstName, String lastName, Date dob, String primaryEmail,
			String primaryNumber, HashSet<String> secondaryNumbers,
			HashSet<String> secondaryEmails, String companyName,
			String tenantAddress, Boolean isMale, String title,
			Key<Apartment> aptUnit, Key<Representative> companyRep, String updateUser)
			throws DuplicateEntitiesException {
		Tenant t = getTenantObject(firstName, lastName, dob, primaryEmail,
				primaryNumber, secondaryNumbers, secondaryEmails, companyName,
				tenantAddress, isMale, title, aptUnit, companyRep);
		Double legalFee, agencyFee, rent;
		String staffOps, leaseFolderUrl, rentCcy, agencyCcy, legalCcy;
		Date rentDate, agencyDate, startDate, endDate, legalDate;
		legalFee = agencyFee = rent = null;
		rentDate = agencyDate = startDate = endDate = legalDate = null;
		staffOps = leaseFolderUrl = rentCcy = agencyCcy = legalCcy = null;
		try {
			if (data != null) {
				legalFee = Double.valueOf(data.get(FormConstants.PD_LEGAL_FEE));
				agencyFee = Double.valueOf(data
						.get(FormConstants.PD_AGENCY_FEE));
				rent = Double.valueOf(data.get(FormConstants.PD_RENT_FEE));
				rentDate = DateFormat.getDateInstance(DateFormat.MEDIUM).parse(
						data.get(FormConstants.PD_RENT_DATE));
				agencyDate = DateFormat.getDateInstance(DateFormat.MEDIUM)
						.parse(data.get(FormConstants.PD_AGENCY_DATE));
				legalDate = DateFormat.getDateInstance(DateFormat.MEDIUM)
						.parse(data.get(FormConstants.PD_LEGAL_DATE));
				startDate = DateFormat.getDateInstance(DateFormat.MEDIUM)
						.parse(data.get(FormConstants.PD_TENANCY_START));
				endDate = DateFormat.getDateInstance(DateFormat.MEDIUM)
						.parse(data.get(FormConstants.PD_TENANCY_END));				
				staffOps = data.get(FormConstants.PD_STAFF_NAME);
				leaseFolderUrl = data.get(FormConstants.PD_DOCS_FOLDER);
				rentCcy = data.get(FormConstants.PD_RENT_CCY);
				legalCcy = data.get(FormConstants.PD_LEGAL_CCY);
				agencyCcy = data.get(FormConstants.PD_AGENCY_CCY);
			}
		} catch (ParseException ex) {
			log.severe("Skipping creation of lease negotiation due to exception "
					+ ex.getMessage());
			data = null;
		}

		Objectify ofy = ObjectifyService.beginTransaction();
		try {
			Key<Tenant> existingTenant = ReadDAO.getCurrentTenant(ofy, aptUnit);
			if (existingTenant != null)
				logAndThrowDuplicateException(log,
						"There is already a tenant in the apartment",
						existingTenant);
			ofy.put(t);
			TenantDAOManagerImpl.scheduleTenantBioComment(t, t.getKey(), ofy, "New Resident Details", updateUser);
			if (data != null) {
				LeaseNegotiation ng = new LeaseNegotiation(t.getKey(),
						legalFee, legalCcy, legalDate, rent, rentCcy, rentDate, agencyFee,
						agencyCcy, agencyDate, startDate, endDate, leaseFolderUrl,
						staffOps, updateUser, false);
				ofy.put(ng);
				String[] comments = {ng.getHTML("<font color='green'><b>ADDED LEASE DETAILS LISTED BELOW</b></font><br/>"), "Prospect->Tenant auto-trigger"};
				TaskQueueHelper.scheduleCreateComment(comments, t.getKey().getString(), updateUser);				
			}
			ofy.getTxn().commit();			
		} finally {
			if (ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return t;
	}

	public static ProspectiveData saveProspectiveData(
			Key<ProspectiveTenant> ptKey,
			HashMap<String, String> prospectiveData) {
		ProspectiveData dataObject = ReadDAO.getProspectiveData(ptKey);
		
		if (dataObject == null)
			dataObject = new ProspectiveData(ptKey, prospectiveData);
		else
			dataObject.setData(prospectiveData);
		Objectify ofy = ObjectifyService.beginTransaction();
		ProspectiveTenant pt = ofy.get(ptKey);
		try {
			pt.setConvertedLead(Boolean.valueOf(dataObject.getData().get(FormConstants.PD_TRANSACTION_CONCL)));
			ofy.put(dataObject,pt);
			ofy.getTxn().commit();
		} finally {
			if (ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return dataObject;
	}

	public static ProspectiveTenant createProspectiveTenant(String firstName,
			String lastName, Date dob, String primaryEmail, String primaryNumber,
			HashSet<String> secondaryNumbers, HashSet<String> secondaryEmails,
			String companyName, String tenantAddress, Boolean isMale,
			String title) {
		Text textAddy = tenantAddress == null ? null : new Text(tenantAddress);
		ProspectiveTenant t = new ProspectiveTenant(firstName, lastName,
				dob, primaryEmail, primaryNumber, secondaryNumbers, secondaryEmails,
				companyName, textAddy, isMale, title);
		Objectify ofy = ObjectifyService.beginTransaction();
		try {
			ofy.put(t);
			ofy.getTxn().commit();
		} finally {
			if (ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return t;
	}
	
	public static RelatedContact createRelatedContact(String firstName,
			String lastName, Date dob, String primaryEmail, String primaryNumber,
			HashSet<String> secondaryNumbers, HashSet<String> secondaryEmails,
			String companyName, String tenantAddress, Boolean isMale,
			String title, Key<Tenant> tenantKey, String relationship) {
		Text textAddy = tenantAddress == null ? null : new Text(tenantAddress);
		RelatedContact c = new RelatedContact(tenantKey, firstName, lastName,
				dob, primaryEmail, primaryNumber, secondaryNumbers, secondaryEmails,
				companyName, textAddy, isMale, title, relationship);
		Objectify ofy = ObjectifyService.beginTransaction();
		try {
			ofy.put(c);
			ofy.getTxn().commit();
		} finally {
			if (ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return c;
	}	

	public static Vendor createVendor(String category, String firstName,
			String lastName, Date dob, String primaryEmail, String primaryNumber,
			HashSet<String> secondaryNumbers, HashSet<String> secondaryEmails,
			String companyName, String address, Boolean isMale, String title, boolean throwDuplicateException)
			throws DuplicateEntitiesException {
		Text textAddy = address == null ? null : new Text(address);
		Vendor v = new Vendor(category, firstName, lastName, dob, primaryEmail,
				primaryNumber, secondaryNumbers, secondaryEmails, companyName,
				textAddy, isMale, title);
		Objectify ofy = ObjectifyService.begin();
		List<Vendor> existingVendorCompany = ofy.query(Vendor.class)
				.filter("companyName", companyName.toUpperCase()).list();
		if (throwDuplicateException && existingVendorCompany.size() > 0) {
			logAndThrowDuplicateException(log,
					"Found " + existingVendorCompany.size()
							+ " company with that name [" + companyName
							+ "] already exists: ", existingVendorCompany
							.get(0).getKey());
		}
		ofy.put(v);
		return v;
	}

	public static Tenant getTenantObject(String firstName, String lastName,
			Date dob, String primaryEmail, String primaryNumber,
			HashSet<String> secondaryNumbers, HashSet<String> secondaryEmails,
			String companyName, String tenantAddress, Boolean isMale,
			String title, Key<? extends Apartment> aptUnit, Key<Representative> companyRep) {
		Text address = tenantAddress == null ? null : new Text(tenantAddress);
		Tenant t = new Tenant(firstName, lastName, dob, primaryEmail, primaryNumber,
				secondaryNumbers, secondaryEmails, companyName, address,
				isMale, title, aptUnit, companyRep);
		return t;
	}

	public static void updateApplicationParameters(String userID,
			Key<ApplicationParameters> key, HashMap<String, String> parameters)
			throws MissingEntitiesException {
		Objectify ofy = ObjectifyService.beginTransaction();
		try {
			ApplicationParameters paramsObject = ofy.find(key);
			if (paramsObject == null)
				logAndThrowMissingEntityException(log,
						"Parameter does not exist, unable to update", key);

			HashMap<String, String> oldParameters = paramsObject.getParams();
			paramsObject.setParams(parameters);
			ofy.put(paramsObject);
			ArrayList<String> diffs = new ArrayList<String>();
			for(String o : oldParameters.keySet())
			{
				if(parameters.containsKey(o))
				{
					if(!parameters.get(o).equalsIgnoreCase(oldParameters.get(o)))
						diffs.add("Value for " + o + " changed from: [" + oldParameters.get(o) + "] to: [" + parameters.get(o) + "]");
				}
				else
					diffs.add("Entry for " + o +" REMOVED. Its description at time of removal was: [" + oldParameters.get(o) + "]");
					
			}
			for(String o : parameters.keySet())
			{
				if(!oldParameters.containsKey(o))
					diffs.add("Entry for " + o + " ADDED. Its description is: [" + parameters.get(o) + "]");
			}
			String[] comments = diffs.toArray(new String[diffs.size()]);
			TaskQueueHelper.scheduleCreateComment(comments, key.getString(), userID);
			ofy.getTxn().commit();

		} finally {
			if (ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
	}

	public static void updateBuilding(Key<Building> key, String displayName,
			String address) throws MissingEntitiesException {
		Objectify ofy = ObjectifyService.beginTransaction();
		try {
			Building b = ofy.find(key);
			if (b == null)
				logAndThrowMissingEntityException(log,
						"Building does not exist, unable to update", key);

			b.setDisplayName(displayName);
			b.setBuildingAddress(address == null ? null : new Text(address));

			ofy.put(b);
			ofy.getTxn().commit();

		} finally {
			if (ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
	}

	public static void updateApartment(Key<Apartment> key, String displayName)
			throws MissingEntitiesException {
		Objectify ofy = ObjectifyService.beginTransaction();
		try {
			Apartment b = ofy.find(key);
			if (b == null)
				logAndThrowMissingEntityException(log,
						"Apartment does not exist, unable to update", key);

			b.setDisplayName(displayName);

			ofy.put(b);
			ofy.getTxn().commit();
		} finally {
			if (ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
	}

	public static Tenant updateTenant(Key<Tenant> key, String firstName,
			Date dob, String lastName, String primaryEmail, String primaryNumber,
			HashSet<String> secondaryNumbers, HashSet<String> secondaryEmails,
			String companyName, String tenantAddress, Boolean isMale,
			String title, Key<Representative> companyRep, String documentsFolder)
			throws MissingEntitiesException {
		Tenant t = null;
		Objectify ofy = ObjectifyService.beginTransaction();
		try {
			t = ofy.find(key);
			if (t == null)
				logAndThrowMissingEntityException(log,
						"Tenant does not exist, unable to update", key);
			Text address = tenantAddress == null ? null : new Text(
					tenantAddress);
			t.setContactValues(firstName, lastName, dob, primaryEmail,
					primaryNumber, secondaryNumbers, secondaryEmails,
					companyName, address, isMale, title);
			t.setCompanyRep(companyRep);
			ofy.put(t);
			ofy.getTxn().commit();
		} finally {
			if (ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return t;
	}

	public static Contact updateContact(Key<? extends Contact> key,
			String firstName, String lastName, Date dob, String primaryEmail,
			String primaryNumber, HashSet<String> secondaryNumbers,
			HashSet<String> secondaryEmails, String companyName,
			String tenantAddress, Boolean isMale, String title, String[] args)
			throws MissingEntitiesException {
		Contact t = null;
		Objectify ofy = ObjectifyService.beginTransaction();
		try {
			t = ofy.find(key);
			if (t == null)
				logAndThrowMissingEntityException(log,
						"Contact does not exist, unable to update", key);
			Text address = tenantAddress == null ? null : new Text(
					tenantAddress);
			t.setContactValues(firstName, lastName, dob, primaryEmail,
					primaryNumber, secondaryNumbers, secondaryEmails,
					companyName, address, isMale, title);
			if(t instanceof RelatedContact)
			{
				RelatedContact c = (RelatedContact) t;
				c.setRelationship(args[1]);
			}
			//TODO use args to updated additional fields based on instance type
			ofy.put(t);
			ofy.getTxn().commit();
			log.warning("Title value after save: " + title);
		} finally {
			if (ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return t;
	}

	public static Tenant updateApartmentTenant(Key<Apartment> aptKey,
			Tenant t) throws DuplicateEntitiesException,
			MissingEntitiesException {
		Objectify ofy = ObjectifyService.beginTransaction();
		try 
		{

			if(!t.archived)
				throw new MissingEntitiesException("This resident must first be marked as archived before you can move" +
						" resident to a new apartment");			

			Key<Tenant> existingTenant = ReadDAO.getCurrentTenant(ofy, aptKey);
			if (existingTenant != null)
				logAndThrowDuplicateException(log,
						"Apartment " + aptKey.getName() + " " + aptKey.getParent().getName() + " "
								+ " already has a tenant", existingTenant);

			Tenant copyOfTenant = t;
			if(aptKey.equals(t.getApartmentUnit())) //simply a restore, no need to change apt keys
				copyOfTenant.archived = false;
			else
			{				
				copyOfTenant = t.clone();
				copyOfTenant.apartmentUnit = aptKey;
			}
			
			Apartment apt = ofy.find(aptKey);
			if (apt == null)
				logAndThrowMissingEntityException(log,
						"Apartment does not exist", aptKey);

			ofy.put(copyOfTenant);
			ofy.getTxn().commit();
			return copyOfTenant;
		}
		catch(CloneNotSupportedException ex)
		{
			throw new RuntimeException(ex);
		}
		finally {
			if (ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
	}
	
	public static List<RelatedContact> cloneContacts(Tenant newTenant, Tenant oldTenant) throws MissingEntitiesException
	{
		Objectify ofy = ObjectifyService.beginTransaction();
		List<RelatedContact> relatives = null;
		try
		{
			if(!oldTenant.archived)
				throw new MissingEntitiesException("This resident must first be marked as archived before you can" +
						" copy the contacts");
			relatives = ofy.query(RelatedContact.class).ancestor(oldTenant.getKey()).list();
			if(relatives.size() == 0)
				return relatives;
			
			ArrayList<RelatedContact> temp = new ArrayList<RelatedContact>();
			for(RelatedContact r : relatives)
			{
				RelatedContact contactCopy = r.clone();
				contactCopy.parentContact = newTenant.getKey();
				temp.add(contactCopy);
			}
			ofy.put(temp);
			ofy.getTxn().commit();
			relatives = temp;
		}
		catch(CloneNotSupportedException ex)
		{
			throw new RuntimeException(ex);
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return relatives;
	}
}
