package com.fertiletech.fieldcodata.backend.entities;

import javax.persistence.Id;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Parent;

public class Apartment implements Location{
	@Id
	protected String displayName;
	
	@Parent
	protected Key<Building> buildingKey;
	
	Apartment(){}
	
	Apartment(Key<Building> buildingKey, String displayName)
	{
		this.buildingKey = buildingKey;
		this.displayName = displayName;
	}
	
	public String getDisplayName()
	{
		return displayName;
	}
	
	void setDisplayName(String displayName)
	{
		this.displayName = displayName;
	}
	
	public Key<? extends Apartment> getKey()
	{
		return new Key<Apartment>(buildingKey, Apartment.class, displayName);
	}

	@Override
	public String getLocationName() {
		return buildingKey.getName() + " " + displayName;
	}
	
	public static String getLocationName(Key<? extends Location> ak)
	{
		if(ak.getParent() == null)
			return "**" + ak.getName();
		
		return ak.getParent().getName() + " " + ak.getName();
	}
}
