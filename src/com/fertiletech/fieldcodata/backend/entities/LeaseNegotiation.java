package com.fertiletech.fieldcodata.backend.entities;

import java.util.Date;

import javax.persistence.Id;

import com.fertiletech.fieldcodata.ui.shared.dto.DTOConstants;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Parent;
import com.googlecode.objectify.annotation.Unindexed;

@Unindexed
public class LeaseNegotiation {
	@Id 
	Long id;
	double legalFee;
	Date legalFeeDate;
	Date rentDate;
	Date agencyDate;
	String rentCCy;
	String legalCcy;
	String agencyCcy;
	double rent;
	double agencyFee;
	@Indexed Date startDate;
	@Indexed Date endDate;
	@Parent Key<? extends Contact> tenant;
	String leaseFolderUrl;
	String staffOps;
	Date createDate;
	String updateUser;
	boolean isOwner;
	
	public LeaseNegotiation() {createDate = new Date();}

	LeaseNegotiation(Key<? extends Contact> tenant, double legalFee, String legalCcy, Date legalFeeDate, double rent, String rentCcy, Date rentDate,
			double agencyFee, String agencyCcy, Date agencyDate, Date startDate, Date endDate, String leaseFolderUrl, 
			String staffOps, String updateUser, boolean isOwner) {
		this();
		this.legalFee = legalFee;
		this.legalFeeDate = legalFeeDate;
		this.rentDate = rentDate;
		this.agencyDate = agencyDate;
		this.rent = rent;
		this.agencyFee = agencyFee;
		this.startDate = startDate;
		this.endDate = endDate;
		this.tenant = tenant;
		this.leaseFolderUrl = leaseFolderUrl;
		this.staffOps = staffOps;
		this.updateUser = updateUser;
		this.isOwner = isOwner;
		this.rentCCy = rentCcy;
		this.agencyCcy = agencyCcy;
		this.legalCcy = legalCcy;
	}
	
	public TableMessage getDTO()
	{
		TableMessage m = new TableMessage(9,3,6);
		m.setNumber(DTOConstants.LEG_FEE_LG_IDX, this.legalFee);
		m.setNumber(DTOConstants.LEG_FEE_RT_IDX, this.rent);
		m.setNumber(DTOConstants.LEG_FEE_AG_IDX, this.agencyFee);
		m.setDate(DTOConstants.LEG_DT_LG_IDX, this.legalFeeDate);
		m.setDate(DTOConstants.LEG_DT_RT_IDX, this.rentDate);
		m.setDate(DTOConstants.LEG_DT_AG_IDX, this.agencyDate);		
		m.setDate(DTOConstants.LEG_DT_STR_IDX, this.startDate);
		m.setDate(DTOConstants.LEG_DT_END_IDX, this.endDate);
		m.setDate(DTOConstants.LEG_DT_CRT_IDX, this.createDate);
		m.setText(DTOConstants.LEG_CLNT_FLDR_IDX, this.leaseFolderUrl);
		m.setText(DTOConstants.LEG_STAFF_IDX, this.staffOps);
		m.setText(DTOConstants.LEG_UPDT_USER_USR, this.updateUser);
		m.setText(DTOConstants.LEG_BLD_IDX, tenant.getParent().getParent().getName());
		m.setText(DTOConstants.LEG_APT_IDX, tenant.getParent().getName());
		m.setText(DTOConstants.LEG_OWNR_IDX, Boolean.toString(isOwner));
		m.setText(DTOConstants.LEG_CCY_RT_IDX, rentCCy);
		m.setText(DTOConstants.LEG_CCY_AG_IDX, agencyCcy);
		m.setText(DTOConstants.LEG_CCY_LG_IDX, legalCcy);
		if(id != null)
			m.setMessageId(getKey().getString());
		return m;
	}
	
	public static LeaseNegotiation getLeaseFromDTO(Key<Tenant> tk, TableMessage m)
	{
		
		double legalFee=m.getNumber(DTOConstants.LEG_FEE_LG_IDX);
		double rent=m.getNumber(DTOConstants.LEG_FEE_RT_IDX);
		double agencyFee=m.getNumber(DTOConstants.LEG_FEE_AG_IDX);
		Date legalFeeDate=m.getDate(DTOConstants.LEG_DT_LG_IDX); 
		Date rentDate=m.getDate(DTOConstants.LEG_DT_RT_IDX);
		Date agencyDate=m.getDate(DTOConstants.LEG_DT_AG_IDX); 		
		Date startDate=m.getDate(DTOConstants.LEG_DT_STR_IDX); 
		Date endDate=m.getDate(DTOConstants.LEG_DT_END_IDX); 
		String leaseFolderUrl=m.getText(DTOConstants.LEG_CLNT_FLDR_IDX);
		String staffOps=m.getText(DTOConstants.LEG_STAFF_IDX); 
		String updateUser=m.getText(DTOConstants.LEG_UPDT_USER_USR);
		String rentCurrency = m.getText(DTOConstants.LEG_CCY_RT_IDX);
		String agencyCurrency = m.getText(DTOConstants.LEG_CCY_AG_IDX);
		String legalCurrency = m.getText(DTOConstants.LEG_CCY_LG_IDX);

		boolean isOwner = Boolean.valueOf(m.getText(DTOConstants.LEG_OWNR_IDX));
		return new LeaseNegotiation(tk, legalFee, legalCurrency, legalFeeDate, rent, rentCurrency, rentDate, agencyFee, agencyCurrency, agencyDate, 
				startDate, endDate, leaseFolderUrl, staffOps, updateUser, isOwner);
	}
	
	public Key<LeaseNegotiation> getKey()
	{
		return new Key<LeaseNegotiation>(tenant, LeaseNegotiation.class, id);
	}

	public double getLegalFee() {
		return legalFee;
	}

	public Date getLegalFeeDate() {
		return legalFeeDate;
	}

	public Date getRentDate() {
		return rentDate;
	}

	public Date getAgencyDate() {
		return agencyDate;
	}

	public double getRent() {
		return rent;
	}

	public double getAgencyFee() {
		return agencyFee;
	}

	public Date getStartDate() {
		return startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public Key<? extends Contact> getTenant() {
		return tenant;
	}

	public String getLeaseFolderUrl() {
		return leaseFolderUrl;
	}

	public String getStaffOps() {
		return staffOps;
	}

	public String getUpdateUser() {
		return updateUser;
	}
	
	public String getHTML(String header)
	{
		StringBuilder sb = new StringBuilder(header);
		sb.append("<b>Legal Ccy: </b>").append(this.legalCcy);		
		sb.append(" <b>Rent Ccy: </b>").append(this.rentCCy);		
		sb.append(" <b>Agency Ccy: </b>").append(this.agencyCcy).append("<br/>");		
		sb.append("<b>Legal Fee: </b>").append(EntityConstants.NUMBER_FORMAT.format(this.legalFee));
		sb.append(" <b>Rent Fee</b>").append(EntityConstants.NUMBER_FORMAT.format(this.rent));
		sb.append(" <b>Agency Fee</b>").append(EntityConstants.NUMBER_FORMAT.format(this.agencyFee)).append("<br/>");
		sb.append("<b>Legal Fee Date: </b>").append(EntityConstants.DATE_FORMAT.format(this.legalFeeDate));
		sb.append(" <b>Rent Date: </b>").append(EntityConstants.DATE_FORMAT.format(this.rentDate));
		sb.append(" <b>Agency Date: </b>").append(EntityConstants.DATE_FORMAT.format(this.agencyDate)).append("<br/>");
		sb.append(" <b>Create Date: </b>").append(EntityConstants.DATE_FORMAT.format(this.createDate));
		sb.append(" <b>Start Date: </b>").append(EntityConstants.DATE_FORMAT.format(this.startDate));
		sb.append(" <b>End Date: </b>").append(EntityConstants.DATE_FORMAT.format(this.endDate)).append("<br/>");
		sb.append("<b>Documents Folder:</b> <a href='").append(this.leaseFolderUrl).append("'>").append(this.leaseFolderUrl).append("</a></br>");
		sb.append("<b>Lease prepared by: </b>").append(this.staffOps);
		sb.append(" <b>System entry by: </b>").append(this.updateUser).append("<br/>");
		sb.append("<b>Resident Type: </b>").append(isOwner?" Owner/Occupier":" Tenant");		
		return sb.toString();
	}
}
