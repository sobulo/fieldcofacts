package com.fertiletech.fieldcodata.backend.entities;

import java.util.Date;
import java.util.HashSet;

import com.google.appengine.api.datastore.Text;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Parent;
import com.googlecode.objectify.condition.IfNotNull;

public class Tenant extends Contact implements Cloneable{
	@Parent Key<? extends Apartment> apartmentUnit;
	@Indexed(IfNotNull.class) Key<Representative> companyRep;
	@Indexed boolean archived;
	boolean owner;
	
	Tenant(){}
	
	Tenant(String firstName, String lastName, Date dob, String primaryEmail, String primaryNumber,
			HashSet<String> secondaryNumbers, HashSet<String> secondaryEmails,
			String companyName, Text address, Boolean isMale, String title, Key<? extends Apartment> apartmentUnit, Key<Representative> companyRep) 
	{
		super(firstName, lastName, dob, primaryEmail, primaryNumber, secondaryNumbers, secondaryEmails, companyName, address, isMale, title);
		this.apartmentUnit = apartmentUnit;
		this.companyRep = companyRep;
	}

	Tenant(Tenant t, Key<Apartment> aptKey)
	{
		super(t.getFirstName(), t.getLastName(), t.getDateOfBirth(), t.getPrimaryEmail(), t.getPrimaryNumber(), 
				t.getSecondaryNumbers(), t.getSecondaryEmails(), t.getCompanyName(), t.getAddress(), t.isMale(), t.getTitle());
		this.companyRep = t.companyRep;
	}

	Tenant(ProspectiveTenant t, Key<Apartment> aptKey)
	{
		super(t.getFirstName(), t.getLastName(), t.getDateOfBirth(), t.getPrimaryEmail(), t.getPrimaryNumber(), 
				t.getSecondaryNumbers(), t.getSecondaryEmails(), t.getCompanyName(), t.getAddress(), t.isMale(), t.getTitle());
	}	
	
	@Override
	public Key<Tenant> getKey(){
		return new Key<Tenant>(apartmentUnit, Tenant.class, key);
	}

	public Key<? extends Apartment> getApartmentUnit() {
		return apartmentUnit;
	}

	public Key<Representative> getCompanyRep() {
		return companyRep;
	}

	protected void setApartmentUnit(Key<Apartment> apartmentUnit) {
		this.apartmentUnit = apartmentUnit;
	}

	protected void setCompanyRep(Key<Representative> companyRep) {
		this.companyRep = companyRep;
	}
	
	public boolean isArchived()
	{
		return archived;
	}
	
	public boolean isOwnerOccupier()
	{
		return owner;
	}
	
	@Override
	protected Tenant clone() throws CloneNotSupportedException
	{
		Tenant t = (Tenant) super.clone();
		t.apartmentUnit = null;
		if(!this.archived)
			throw new CloneNotSupportedException("Only Archived Resident Information May be Cloned");
		t.archived = false;
		return t;
	}
}
