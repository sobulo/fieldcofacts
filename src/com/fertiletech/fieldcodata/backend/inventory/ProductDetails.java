package com.fertiletech.fieldcodata.backend.inventory;

import java.util.HashSet;

import javax.persistence.Id;

import com.fertiletech.fieldcodata.backend.entities.Vendor;
import com.google.appengine.api.datastore.Text;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Indexed;

/**
 * manufacturer + model number should be unique
 * 
 * open question as to whether to add a tonne of other fields or just have it all as free form text in the description 
 * bit
 * 
 * @author sobulo@fertiletech.com
 *
 */
public class ProductDetails {
	@Id
	String key;
	String manufacturer;
	String modelNumber;
	@Indexed String classification; //air conditioner, tv, etc
	Text description;
	HashSet<Key<Vendor>> vendors;
	ProductDetails(){}
		
	ProductDetails(String manufacturer, String modelNumber,
			String classification, Text description) {
		super();
		this.manufacturer = manufacturer;
		this.modelNumber = modelNumber;
		this.classification = classification;
		this.description = description;
		this.key = getKeyString(modelNumber, manufacturer);
	}
	
	public Key<ProductDetails> getKey()
	{
		return getKey(modelNumber, manufacturer);
	}

	private static String getKeyString(String modelNumber, String manufacturer)
	{
		return modelNumber.trim().toLowerCase() + " | " + manufacturer.trim().toLowerCase(); 		
	}
	
	public static Key<ProductDetails> getKey(String modelNumber, String manufacturer)
	{
		return new Key<ProductDetails>(ProductDetails.class, getKeyString(modelNumber, manufacturer));
	}
	
	public String getManufacturer() {
		return manufacturer;
	}
	
	void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	
	public String getModelNumber() {
		return modelNumber;
	}
	
	void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}
	
	public String getClassification() {
		return classification;
	}
	
	void setClassification(String classification) {
		this.classification = classification;
	}
	
	public Text getDescription() {
		return description;
	}
	
	void setDescription(Text description) {
		this.description = description;
	}

	public HashSet<Key<Vendor>> getVendors() {
		return vendors;
	}

	void setVendors(HashSet<Key<Vendor>> vendors) {
		this.vendors = vendors;
	}
}
