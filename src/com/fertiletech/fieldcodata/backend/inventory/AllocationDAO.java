package com.fertiletech.fieldcodata.backend.inventory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.fertiletech.fieldcodata.backend.entities.Location;
import com.fertiletech.fieldcodata.backend.entities.WriteDAO;
import com.fertiletech.fieldcodata.backend.tickets.RequestTicket;
import com.google.appengine.api.datastore.Text;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

public class AllocationDAO {
	
	static
	{
		WriteDAO.registerClassesWithObjectify();
	}
	
	private final static int ADDED_SERIAL_IDX = 0;
	private final static int REMOVED_SERIAL_IDX = 1;
	private final static int SAME_SERIAL_IDX = 2;
	
	public static List<InventoryAllocation> getAllocations(Key<InventoryItem> itemKey, Objectify ofy)
	{
		return ofy.query(InventoryAllocation.class).ancestor(itemKey).list();
	}
	
	public static Set<Key<InventoryItem>> getInventoryKeys(Key<? extends Location> locationKey, Objectify ofy)
	{
		List<Key<InventoryItem>> pks = ofy.query(InventoryItem.class).filter("locationKey",locationKey).listKeys();
		HashSet<Key<InventoryItem>> parentKeys = new HashSet<Key<InventoryItem>>();
		parentKeys.addAll(pks);
		return parentKeys;
	}
	
	public static Collection<InventoryAllocation> getAllocations(Key<? extends Location> locationKey, Date startDate, Date endDate, Objectify ofy)
	{
		List<Key<InventoryAllocation>> requestedKeys = new ArrayList<Key<InventoryAllocation>>();
		
		//get inventory for specified location, could allow null for this and just filter on dates but slightly worried about
		//how this will perform memory wise in terms of number of allocations. is this any worse than fetching all comments
		//for a tenant that's been around for years? TODO revisit making this an option. Will also need to remove filtering of
		//nulls on client end
		List<Key<InventoryItem>> pks = ofy.query(InventoryItem.class).filter("locationKey",locationKey).listKeys();
		Set<Key<InventoryItem>> parentKeys = getInventoryKeys(locationKey, ofy);
		
		//get allocation keys
		List<Key<InventoryAllocation>> allocationKeys = ofy.query(InventoryAllocation.class).filter("creationDate >=", startDate).filter("creationDate <=", endDate).order("creationDate").listKeys();
		
		//join inventory with allocations
		for(Key<InventoryAllocation> ak : allocationKeys)
			if(parentKeys.contains(ak.getParent()))
				requestedKeys.add(ak);
		
		//fetch result
		Map<Key<InventoryAllocation>, InventoryAllocation>  x = ofy.get(requestedKeys);
		return x.values();
	}	 
	
	/**
	 * excludeMoved flag determines whether to have history of items moved out of the inventory. When true only the serial numbers of
	 * items still in the inventory are included. All elements in the array are asumed to be from the same inventory 
	 * but this check is not enforced
	 * 
	 * @param parentIventory
	 * @param includeAll
	 * @return
	 */
	public static HashMap<String, ArrayList<String>> getInventorySerialNumbers(List<InventoryAllocation> allocations, boolean excludeMoved)
	{
		HashMap<String, ArrayList<String>> allAllocMap = new HashMap<String, ArrayList<String>>();
		for(InventoryAllocation a : allocations)
		{
			String serailNumber = a.getSerialNumber();
			if(!allAllocMap.containsKey(serailNumber))
			{
				allAllocMap.put(serailNumber, new ArrayList<String>());
			}
			allAllocMap.get(serailNumber).add(a.toString());
		}
		
		if(excludeMoved)
		{
			HashMap<String, ArrayList<String>> movedExcludedAllocMap = new HashMap<String, ArrayList<String>>();
			for(String serial : allAllocMap.keySet())
			{
				if(allAllocMap.get(serial).size() % 2 == 1) //even means last allocation was a move in
					movedExcludedAllocMap.put(serial, allAllocMap.get(serial));
			}
			return movedExcludedAllocMap;
		}
		else
			return allAllocMap;
	}
	
	public static HashMap<String, ArrayList<String>> getInventorySerialNumbers(Key<InventoryItem> itemKey, boolean excludeMoved, Objectify ofy)
	{
		return getInventorySerialNumbers(getAllocations(itemKey, ofy), excludeMoved);
	}
	
	public static ProductDetails getProductInfo(String manufacturer, String modelNumber, Objectify ofy)
	{
		Key<ProductDetails> pk = ProductDetails.getKey(modelNumber, manufacturer);
		return ofy.find(pk);
	}
	
	public static ProductDetails saveProductInfo(Key<ProductDetails> pk, String manufacturer, String model, 
			String classification, String description)
	{
		ProductDetails p;
		Objectify ofy = ObjectifyService.beginTransaction();
		if(pk != null)
		{
			p = ofy.get(pk);
			p.setClassification(classification);
			p.setDescription(new Text(description));
		}
		else
		{
			p = new ProductDetails(manufacturer, model, classification, new Text(description));
		}
		
		try
		{
			ofy.put(p);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return p;
	}
	
	private static Set<String>[] getSerialDiffs(Key<InventoryItem> itemKey, Set<String> requestedSerials, Objectify ofy)
	{
		Set<String> existingSerials = getInventorySerialNumbers(itemKey, true, ofy).keySet();
		Set<String>[] result = new HashSet[3];
		HashSet<String> added = new HashSet<String>();
		HashSet<String> removed = new HashSet<String>();
		
		for(String serial : requestedSerials)
		{
			if(!existingSerials.contains(serial))
				added.add(serial);
		}
		
		/**
		 * Note this is to know whether to move a product to a bin, i.e. mark null. Not sure if this can or should be used
		 * for cases where allocations are moving across inventories, that probably has it own special cases.
		 * TODO confirm once allocation panels code base stable
		 * 
		 * Also note that you can always tell later what serials were moved by querying allocation objects since we don't
		 * delete allocations. Though as database size grows we'll want to truncate history periodically with a cron job
		 */
		for(String serial : existingSerials)
		{
			if(!requestedSerials.contains(serial))
				removed.add(serial);
		}
		
		result[ADDED_SERIAL_IDX] = added;
		result[REMOVED_SERIAL_IDX] = removed;
		result[SAME_SERIAL_IDX] = new HashSet<String>(); //can't simply assign value as existingSerials becuase existingserial is a set but not necessarily a hashset
		result[SAME_SERIAL_IDX].addAll(existingSerials);
		return result;
	}
	
	public static InventoryItem saveInventoryItem(Key<InventoryItem> invKey, Key<Location> lk, Key<ProductDetails> pk,
			Double suggestedPrice, Set<String> serialNumbers, String user, Key<RequestTicket> ticketKey)
	{
		ProductDetails p;
		List<InventoryAllocation> allocations = new ArrayList<InventoryAllocation>();
		InventoryItem invItem = null;		
		Objectify ofy = ObjectifyService.beginTransaction();
		try
		{
			if(invKey == null)
			{
				invItem = new InventoryItem(pk, lk, serialNumbers.size(), suggestedPrice);
				ofy.put(invItem);
				for(String serial : serialNumbers)
				{
					//TODO fromInventory currently marked as null but should later be refactored to take in icestone
					//ability to take in an allocation date also part of icestone enhancements which means prior to icestone
					//data-migration requirement is that building inventories don't support back dating when stocked, same way
					//they can't tell where they were sourced from (i.e. null val for from)
					InventoryAllocation a = new InventoryAllocation(true, serial, invItem.getKey(), null, suggestedPrice, new Date(), user, ticketKey);
					allocations.add(a);
				}
			}
			else
			{
				Set<String>[] diffs = getSerialDiffs(invKey, serialNumbers, ofy);
				invItem = ofy.get(invKey);
				invItem.setSuggestedPrice(suggestedPrice);
				for(String serial : diffs[ADDED_SERIAL_IDX]) //add items to inventory
				{
					//same icestone issue described above needs to be addressed
					InventoryAllocation a = new InventoryAllocation(true, serial, invItem.getKey(), null, suggestedPrice, new Date(), user, ticketKey);
					allocations.add(a);
				}
				
				for(String serial : diffs[REMOVED_SERIAL_IDX]) //remove items from inventory
				{
					//this is interesting, when icestone exists, what happens when you delete from building inventory?
					//trigger something on gui side to restock icestone? umm allocation panel exists for that already
					InventoryAllocation a = new InventoryAllocation(false, serial, null, invItem.getKey(), suggestedPrice, new Date(), user, ticketKey);
					allocations.add(a);
				}
				invItem.setQuantity(diffs[SAME_SERIAL_IDX].size() + 
						diffs[ADDED_SERIAL_IDX].size() - diffs[REMOVED_SERIAL_IDX].size());
				ofy.put(invItem);
			}
			ofy.put(allocations);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return invItem;
	}
	
	public static List<InventoryAllocation> moveInventory(Key<Location> destination, Key<InventoryItem> fromInv, Set<String> serial, Double price, Date allocationDate, String user, Key<RequestTicket> ticketKey)
	{
		List<Object> toResult = new ArrayList<Object>();
		List<Object> fromResult = new ArrayList<Object>();
		List<InventoryAllocation> result = new ArrayList<InventoryAllocation>();
		
		//calculate to inventory key
		Key<ProductDetails> productKey = fromInv.getParent();
		Key<InventoryItem> toInv = destination == null ? null : InventoryItem.getKey(productKey, destination);
		
		//setup child allocation objects for to and from
		for(String s : serial)
		{
			InventoryAllocation from = new InventoryAllocation(false, s, toInv, fromInv, price, allocationDate, user, ticketKey);
			fromResult.add(from);
			result.add(from);
			if(toInv != null)
			{
				InventoryAllocation to = new InventoryAllocation(true, s, toInv, fromInv, price, allocationDate, user, ticketKey);
				toResult.add(to);
				result.add(to);
			}
		}
		Objectify ofy = ObjectifyService.beginTransaction();
		try
		{
			//modify quantity on parent inventory item
			InventoryItem fromItem = ofy.get(fromInv);
			int quantity = fromItem.getQuantity() - fromResult.size();
			if(quantity < 0)
				throw new RuntimeException("Sanity check failed!!Quantity less than 0 for inventory move from: " + fromInv + " destination: " + destination);
			fromItem.setQuantity(quantity);
			fromResult.add(fromItem);
			InventoryItem toItem = null;
			if(toInv != null)
			{
				toItem = ofy.find(toInv);
				if(toItem == null)
				{
					toItem = new InventoryItem(fromItem.getProductKey(), destination, toResult.size(), price);
				}
				else
				{
					toItem.setSuggestedPrice(price);
					toItem.setQuantity(toItem.getQuantity() + toResult.size());
				}
				toResult.add(toItem);
			}
			//save changes
			ofy.put(fromResult);
			ofy.put(toResult);
			ofy.getTxn().commit();			
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return result;
	}
	
	public static List<InventoryItem> getInventoryByProduct(Key<ProductDetails> pk)
	{
		Objectify ofy = ObjectifyService.begin();
		return ofy.query(InventoryItem.class).ancestor(pk).list();
	}
	
	public static List<InventoryItem> getInventoryByLocation(Key<? extends Location> lk)
	{
		Objectify ofy = ObjectifyService.begin();
		return ofy.query(InventoryItem.class).filter("locationKey", lk).list();
	}	
	
	public static List<Key<ProductDetails>> getAllProductDefinitions()
	{
		Objectify ofy = ObjectifyService.begin();
		return ofy.query(ProductDetails.class).listKeys();
	}
}
