package com.fertiletech.fieldcodata.backend.inventory;

import java.util.Date;

import javax.persistence.Id;

import com.fertiletech.fieldcodata.backend.tickets.RequestTicket;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Parent;
import com.googlecode.objectify.annotation.Unindexed;

/**
 * An allocation knows which inventoryitem it belongs to
 * this essentially allows us to take an inventory item and see
 * a historical record of how it was built up
 * 
 * inventoryitem = sum of allocations with it as it's toDestination MINUS sum of allocations with it as it's fromDestination
 * 
 * Also note that inventoryitem keys set on the object are immutable. If an allocation is being made from inventoryitem A to inventory
 * item B, two entries will need to be made showing A-->B, one with A marked as parent and another with B marked as parent.
 * This need to be taken into account when querying for inventoryallocation objects to prevent double counting
 * 
 * @author sobulo@fertiletech.com
 *
 */
@Unindexed
public class InventoryAllocation {
	@Id
	Long key;
	@Parent Key<InventoryItem> inventoryOwner;
	@Indexed Key<RequestTicket> ticketKey;
	String serialNumber;
	
	Key<InventoryItem> toInventory;
	Key<InventoryItem> fromInventory;
	Double price;
	Date allocationDate;
	
	String user;
	@Indexed Date creationDate;
	
	InventoryAllocation()
	{
		creationDate = new Date();
	}
		
	InventoryAllocation(boolean markToAsParent, String serialNumber, Key<InventoryItem> toInventory,
			Key<InventoryItem> fromInventory, Double price, Date allocationDate, String user, Key<RequestTicket> ticketKey) 
	{
		this();
		this.serialNumber = serialNumber;
		this.toInventory = toInventory;
		this.fromInventory = fromInventory;
		this.price = price;
		this.allocationDate = allocationDate;
		this.user = user;
		this.inventoryOwner = markToAsParent?toInventory:fromInventory;
		this.ticketKey = ticketKey;
	}
	
	public Key<RequestTicket> getTicketKey() {
		return ticketKey;
	}

	public void setTicketKey(Key<RequestTicket> ticketKey) {
		this.ticketKey = ticketKey;
	}

	public Key<InventoryAllocation> getKey()
	{
		return new Key<InventoryAllocation>(inventoryOwner, InventoryAllocation.class, key);
	}

	public Key<InventoryItem> getToInventory() {
		return toInventory;
	}
		
	public Key<InventoryItem> getFromInventory() {
		return fromInventory;
	}
		
	public Double getPrice() {
		return price;
	}
	
	void setPrice(Double price) {
		this.price = price;
	}
	
	public Date getAllocationDate() {
		return allocationDate;
	}
	
	void setAllocationDate(Date allocationDate) {
		this.allocationDate = allocationDate;
	}
	
	public String getSerialNumber() {
		return serialNumber;
	}
	
	public Key<InventoryItem> getInventoryOwner() {
		return inventoryOwner;
	}

	public String getUser() {
		return user;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	@Override
	public String toString()
	{
		String from = fromInventory == null ? "Unspecified Stock?" : fromInventory.getName();
		String to = toInventory == null ? "Delete Bin?" : toInventory.getName();
		StringBuilder result = new StringBuilder("<b>Product:</b> ").append(inventoryOwner.getParent().getName()).append("<br/>");
		result.append("<b>Inventory:</b> ").append(inventoryOwner.getName()).append("<br/>");
		result.append("<b>Moved From:</b> ").append(from).append(" <b>To:</b> ").append(to).append("<br/>");
		result.append("<b>Allocation Date:</b> ").append(allocationDate).append("<br/>");
		result.append("<b>Entry Date:</b> ").append(creationDate).append(" <b>User:</b> ").append(user).append("<hr/>");
		return result.toString();
	}
}
