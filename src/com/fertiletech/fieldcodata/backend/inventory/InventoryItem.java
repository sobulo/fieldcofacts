package com.fertiletech.fieldcodata.backend.inventory;

import javax.persistence.Id;

import com.fertiletech.fieldcodata.backend.entities.Location;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Parent;
import com.googlecode.objectify.annotation.Unindexed;

/**
 * Location should map to icestone, building inventory, or an apartment?
 * 
 * @author sobulo@fertiletech.com
 *
 */
@Unindexed
public class InventoryItem 
{
	@Id
	String key;
	@Parent Key<ProductDetails> productKey;
	@Indexed Key<Location> locationKey;
	int quantity;
	Double suggestedPrice;
	
	public InventoryItem() {}
	
	InventoryItem(Key<ProductDetails> productKey, Key<Location> location,
			int quantity, Double suggestedPrice) 
	{
		this.productKey = productKey;
		this.locationKey = location;
		this.quantity = quantity;
		this.suggestedPrice = suggestedPrice;
		key = getLocationString(location); 
	}
	
	public static String getLocationString(Key<Location> location)
	{
		//base case
		if(location == null)
			return "";
		
		//recursive case
		Key<Location> locParent = location.getParent();
		String parentStr = getLocationString(locParent);
		String locStr = (location.getName() == null ? String.valueOf(location.getId()) : location.getName());
		return parentStr + " " + locStr; 
	}
	
	public static Key<InventoryItem> getKey(Key<ProductDetails> pk, Key<Location> lk)
	{
		return new Key<InventoryItem>(pk, InventoryItem.class, getLocationString(lk));		
	}
	
	public Key<InventoryItem> getKey()
	{
		return getKey(productKey, locationKey);
	}

	public Key<ProductDetails> getProductKey() {
		return productKey;
	}
	
	public Key<Location> getLocation() {
		return locationKey;
	}
	
	public int getQuantity() {
		return quantity;
	}
	
	void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Double getSuggestedPrice() {
		return suggestedPrice;
	}

	void setSuggestedPrice(Double suggestedPrice) {
		this.suggestedPrice = suggestedPrice;
	}
}
