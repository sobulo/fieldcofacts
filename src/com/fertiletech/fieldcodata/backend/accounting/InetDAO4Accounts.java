/**
 * 
 */
package com.fertiletech.fieldcodata.backend.accounting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.logging.Logger;

import com.fertiletech.fieldcodata.backend.entities.Apartment;
import com.fertiletech.fieldcodata.backend.entities.Building;
import com.fertiletech.fieldcodata.backend.entities.Contact;
import com.fertiletech.fieldcodata.backend.entities.EntityConstants;
import com.fertiletech.fieldcodata.backend.entities.ReadDAO;
import com.fertiletech.fieldcodata.backend.entities.Representative;
import com.fertiletech.fieldcodata.backend.entities.Tenant;
import com.fertiletech.fieldcodata.backend.tickets.RequestTicket;
import com.fertiletech.fieldcodata.scripts.tasks.TaskQueueHelper;
import com.fertiletech.fieldcodata.ui.server.TenantDAOManagerImpl;
import com.fertiletech.fieldcodata.ui.shared.dto.BillDescriptionItem;
import com.fertiletech.fieldcodata.ui.shared.dto.DTOConstants;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessageHeader;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessageHeader.TableMessageContent;
import com.fertiletech.fieldcodata.ui.shared.exceptions.DuplicateEntitiesException;
import com.fertiletech.fieldcodata.ui.shared.exceptions.ManualVerificationException;
import com.fertiletech.fieldcodata.ui.shared.exceptions.MissingEntitiesException;
import com.google.appengine.api.datastore.QueryResultIterable;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.Query;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class InetDAO4Accounts {
	private static final Logger log = Logger.getLogger(InetDAO4Accounts.class.getName());
	

    public static InetCompanyAccount createAccount(String bank, String sortCode, String accountName, String accountNumber, String currency, String updater) throws DuplicateEntitiesException
    {
    	InetCompanyAccount acct = new InetCompanyAccount(accountNumber, accountName, bank, sortCode, currency);
    	Objectify ofy = ObjectifyService.beginTransaction();
    	try
    	{
    		ofy.put(acct);
    		String[] comments = getBankComment(true, bank, sortCode, accountNumber, accountName, currency);
    		TaskQueueHelper.scheduleCreateComment(comments, acct.getKey().getString(), updater);
    		ofy.getTxn().commit();
    	}
    	finally
    	{
    		if(ofy.getTxn().isActive())
    			ofy.getTxn().rollback();
    	}
    	return acct;
    }
    
    private static String[] getBankComment(boolean isCreate, String bank, String sortCode, String accountNumber, String accountName, String currency)
    {
		String[] comments = {"COMPANY BANK ACCOUNT " + (isCreate? "CREATED" : "UPDATED"), "Bank: " + bank, "Sort Code: " + sortCode, 
				 "Account Name: " + accountName, " Account Number: " + accountNumber, "Currency: " + currency};
		return comments;
    }

    public static InetCompanyAccount updateAccount(Key<InetCompanyAccount> ak, String bank, String sortCode, String accountName, String accountNumber, String currency, String updater) throws MissingEntitiesException
    {
    	Objectify ofy = ObjectifyService.beginTransaction();
    	InetCompanyAccount acct = null;
    	try
    	{
    		acct = ofy.find(ak);
    		if(acct == null)
    		{
            	String msgPrefix = "Account not found. Details: ";
                MissingEntitiesException ex = new MissingEntitiesException(msgPrefix + ak);
                log.severe(msgPrefix + ak);
                throw ex;    			
    		}
    		acct.setAccountName(accountName);
    		acct.setBank(bank);
    		acct.setSortCode(sortCode);
    		acct.setAccountNumber(accountNumber);
    		acct.setCurrency(currency);
    		ofy.put(acct);
    		String[] comments = getBankComment(true, bank, sortCode, accountNumber, accountName, currency);
    		TaskQueueHelper.scheduleCreateComment(comments, acct.getKey().getString(), updater);    		
    		ofy.getTxn().commit();
    	}
    	finally
    	{
    		if(ofy.getTxn().isActive())
    			ofy.getTxn().rollback();
    	}
    	return acct;
    }    
    
    public static InetDeposit createDeposit(Key<InetCompanyAccount> companyAccount,
			Date depositDate, String referenceNumber, String comments,
			Key<InetResidentAccount> depositOwner, double amount, String user)
    {
    	InetDeposit deposit = new InetDeposit(companyAccount, depositDate, referenceNumber, 
    			comments, depositOwner, amount, user);
    	Objectify ofy = ObjectifyService.beginTransaction();
    	try
    	{
    		InetResidentAccount account = ofy.get(depositOwner);
    		account.processLedger(deposit);
    		ofy.put(deposit);
    		ofy.put(account);
    		ofy.getTxn().commit();
    		String[] msg = {"Deposit [" + EntityConstants.NUMBER_FORMAT.format(amount) + "] " + " made. ", comments};
    		TaskQueueHelper.scheduleCreateComment(msg, InetResidentAccount.getResident(depositOwner, false).getString(), user);
    	}
    	finally
    	{
    		if(ofy.getTxn().isActive())
    			ofy.getTxn().rollback();
    	}
    	return deposit;
    }    
    
    public static InetBill createBill(Key<? extends Contact> billedUserKey, Key<RequestTicket> ticketKey, LinkedHashSet<BillDescriptionItem> billDescription, String category, Date start, Date end, Date due, String ccy) throws DuplicateEntitiesException
    {
    	InetBill bill = new InetBill(billedUserKey, ticketKey, billDescription, category, start, end, due, ccy);
    	Objectify ofy = ObjectifyService.beginTransaction();
    	try
    	{
    		InetBill billCheck = ofy.find(bill.getKey());
    		if(billCheck != null)
    		{
            	String msgPrefix = "Bill already exists. Details: ";
                DuplicateEntitiesException ex = new DuplicateEntitiesException(msgPrefix + bill.getKey());
                log.severe(msgPrefix + bill.getKey());
                throw ex;    			
    		}
    		ofy.put(bill);
    		ofy.getTxn().commit();
    	}
    	finally
    	{
    		if(ofy.getTxn().isActive())
    			ofy.getTxn().rollback();
    	}
    	return bill;
    }
    
    public static InetBill createBill(Key<? extends Contact> billedUserKey, Key<InetBill> modelBillKey) throws DuplicateEntitiesException
    {
    	InetBill modelBill = ObjectifyService.begin().get(modelBillKey);
    	return createBill(billedUserKey, modelBill.getTicketKey(), (LinkedHashSet<BillDescriptionItem>) modelBill.getItemizedBill(), 
    			modelBill.getTitle(), modelBill.getServicePeriodStartDate(), modelBill.getServicePeriodEndDate(), modelBill.getDueDate(), modelBill.getCurrency());
    }
    
    
    public static InetWithdrawal createPayment(final Key<InetBill> billKey, final Key<InetResidentAccount> accountKey, final double amount, String comments, final Date paymentDate, String user) 
    	throws MissingEntitiesException, ManualVerificationException
    {	
    	InetWithdrawal withdrawal = null;
    	Objectify ofy2 = ObjectifyService.beginTransaction();
    	Objectify ofy = ObjectifyService.beginTransaction();
    	InetResidentAccount account = ofy.get(accountKey);
    	InetBill bill = ofy2.get(billKey);
    	boolean txnSucceeded = false;
    	if(bill == null)
    		throw new MissingEntitiesException("Unable to locate bill: " + billKey);
    	if(account == null)
    		throw new MissingEntitiesException("Unable to locate resident account: " + accountKey);
    	double unpaidBillAmount = bill.getTotalAmount() - bill.getSettledAmount();
    	double remainingBalance = account.getCurrentAmount();
    	
    	if(!account.getCurrency().equals(bill.currency))
    		throw new ManualVerificationException("Currency on bill/invoice does not match currency denomination of the resident account");
    	
    	if(unpaidBillAmount - amount < (0 - InetBill.THRESHOLD))
    	{
    		String msgFmt = "Payment amount %f exceeds unpaid bill amount %f." +
    				" Use the view bill option on the navigation panel to see details of this bill (Bill ID: %s)."; 
    		log.warning(String.format(msgFmt, amount, unpaidBillAmount, bill.getKey().getName()));
    		throw new ManualVerificationException(String.format(msgFmt, amount, unpaidBillAmount, bill.getKey().getName()));
    	}
    	
    	if(!account.isAllowOverDraft() && remainingBalance - amount < (0 - InetBill.THRESHOLD))
    	{
    		String msgFmt = "Current balance on the resident account %f is not sufficient to fund payment amount %f." +
    				" trigger a smaller payment or use the view deposits option on the navigation panel to see details of the deposit (Deposit ID: %s).";
    		log.warning(String.format(msgFmt, remainingBalance, amount, account.getFormattedTitle()));
    		throw new ManualVerificationException(String.format(msgFmt, remainingBalance, amount, bill.getKey().getName()));
    	}
    	
    	try
    	{
    		bill.setSettledAmount(amount + bill.getSettledAmount());
    		if(comments == null || comments.trim().length() == 0)
    			comments = "Account deduction for payment of " + bill.getFormatedTitle();

    		String referenceId = bill.getTitle() + "  [id: " + String.valueOf(bill.getInvoiceID() + "]");
    		withdrawal = new InetWithdrawal(billKey, accountKey, amount, referenceId, comments, paymentDate, user);
    		account.processLedger(withdrawal);
    		ofy.put(withdrawal, account);        
    		ofy.getTxn().commit();
    		txnSucceeded = true;
    	}
    	finally
    	{
    		if(ofy.getTxn().isActive())
    			ofy.getTxn().rollback();
    	}
    	
    	if(txnSucceeded)
    	{
    		try
    		{
    			ofy2.put(bill);
    			ofy2.getTxn().commit();
    		}
    		finally
    		{
    			if(ofy2.getTxn().isActive())
    			{
    				log.severe("***MANUAL ATTN NEEDED, PARTIAL TXN COMMIT ***" + bill.getKey() + " and " + withdrawal.getKey() + " and " + account.getKey());
    				ofy2.getTxn().rollback();
    			}
    		}
    	}

    	return withdrawal;
    }
    
    public static List<InetBill> getStudentBills(Key<? extends Contact> userKey, Objectify ofy)
    {
    	return ofy.query(InetBill.class).ancestor(userKey).list();
    }    
    
    public static List<InetBill> getAllBillsByDate(Date startDate, Date endDate, Objectify ofy)
    {
    	return ofy.query(InetBill.class).filter("dueDate >=", startDate).filter("dueDate <=", endDate).list();
    }
    
    public static HashMap<String, String> getStudentBillKeys(Key<? extends Contact> userKey, Objectify ofy)
    {
    	HashMap<String, String> result = new HashMap<String, String>();
    	QueryResultIterable<Key<InetBill>> billKeys = ofy.query(InetBill.class).ancestor(userKey).fetchKeys();
    	for(Key<InetBill> bk : billKeys)
    		result.put(ofy.getFactory().keyToString(bk), ReadDAO.keyToPrettyString(bk));
    	return result;
    }    
    
    public static List<InetWithdrawal> getPayments(Key<InetBill> bk, Objectify ofy)
    {
    	return ofy.query(InetWithdrawal.class).filter("allocatedBill", bk).list();
    }
    
    public static List<InetDeposit> getDeposits(Key<? extends Contact> depositOwner, Objectify ofy)
    {
    	Query<InetDeposit> q = ofy.query(InetDeposit.class).ancestor(InetResidentAccount.getParentAccountKey((Key<Tenant>) depositOwner));
    	return q.list();
    }
    
    public static List<InetDeposit> getWithdrawals(Key<? extends Contact> withdrawalOwner, Objectify ofy)
    {
    	Query<InetDeposit> q = ofy.query(InetDeposit.class).ancestor(InetResidentAccount.getParentAccountKey((Key<Tenant>) withdrawalOwner));
    	return q.list();
    }
    
    public static List<LedgerEntry> getLedgerEntries(Key<InetResidentAccount> raKey, Date startDate, Date endDate, Objectify ofy)
    {
    	List<LedgerEntry> result = new ArrayList<LedgerEntry>();
    	Query<InetDeposit> dq = ofy.query(InetDeposit.class).ancestor(raKey);
    	Query<InetWithdrawal> wq = ofy.query(InetWithdrawal.class).ancestor(raKey);
    	if(startDate != null)
    	{
    		dq = dq.filter("createDate >=", startDate);
    		wq = wq.filter("createDate >=", startDate);    		
    	}
    	if(endDate != null)
    	{
    		dq = dq.filter("createDate <=", endDate);
    		wq = wq.filter("createDate <=", endDate);
    	}
    	result.addAll(dq.list());
    	result.addAll(wq.list());
    	return result;
    }
    
    public static List<InetDeposit> getDeposits(Date startDate, Date endDate, Objectify ofy)
    {
    	Query<InetDeposit> q = ofy.query(InetDeposit.class);
    	if(startDate != null)
    		q = q.filter("createDate >=", startDate);
    	if(endDate != null) 
    		q = q.filter("createDate <=", endDate);
    	return q.list();
    }
    
    public static List<InetCompanyAccount> getAllAccounts(Objectify ofy)
    {
    	return ofy.query(InetCompanyAccount.class).list();
    }
    
    public static HashMap<Key<InetCompanyAccount>, InetCompanyAccount> getCompanyAcctsMap(Objectify ofy)
    {
		HashMap<Key<InetCompanyAccount>, InetCompanyAccount> bankAccts = new HashMap<Key<InetCompanyAccount>, InetCompanyAccount>();
		List<InetCompanyAccount> temp = InetDAO4Accounts.getAllAccounts(ofy);
		for(InetCompanyAccount ca : temp)
			bankAccts.put(ca.getKey(), ca);
		return bankAccts;
    }
    
	public static List<TableMessage> getAllReps() 
	{
		List<Representative> reps = ObjectifyService.begin().query(Representative.class).list();
		List<TableMessage> result = new ArrayList<TableMessage>();
		//result.add(getRepHeader());
		for(Representative r : reps)
			result.add(TenantDAOManagerImpl.getContactDTO(r, null));
		
		return result;
	}
	
	public static List<TableMessage> getCompanyTenants(Key<Representative> repKey, Objectify ofy)
	{
		List<Key<Tenant>> tenantKeys = ReadDAO.getCompanyTenants(repKey, ofy);
		List<TableMessage> result = TenantDAOManagerImpl.getTenantDTOList(tenantKeys, ofy);
		result.get(0).setMessageId(repKey.getString());
		return result;
	}
	 
	public static TableMessageHeader getRepHeader()
	{
		//TODO, code below is broken. offset won't work on biopanels or any other instance that relies on the explicit
		//value of the constants, which is most cases. You'll need to ditch the offset and find values to put in the first
		//two columns or apply offset specifically in instances where data is just being dumped in a table
		TableMessageHeader h = new TableMessageHeader(6);
		int offset = DTOConstants.REP_OFFSET;
		h.setText(DTOConstants.TNT_FNAME_IDX - offset, "First Name", TableMessageContent.TEXT);
		h.setText(DTOConstants.TNT_LNAME_IDX - offset, "Surname", TableMessageContent.TEXT);
		h.setText(DTOConstants.TNT_COMPANY_IDX - offset, "Company", TableMessageContent.TEXT);
		h.setText(DTOConstants.TNT_PRIMARY_PHONE_IDX - offset, "Phone", TableMessageContent.TEXT);
		h.setText(DTOConstants.TNT_PRIMARY_EMAIL_IDX - offset, "Email", TableMessageContent.TEXT);
		h.setText(DTOConstants.TNT_ADDRESS_IDX - offset, "Address", TableMessageContent.TEXT);
		return h;
	}
	
	public static List<InetResidentAccount> createResidentAccount(Key<Tenant> tk, String[] types, String user, boolean allowOverDraft, String currency, int depositWarning) throws DuplicateEntitiesException
	{
		List<InetResidentAccount> accountsToSave = new ArrayList<InetResidentAccount>(types.length);
		for(String t : types)
		{
			InetResidentAccount ra = new InetResidentAccount(tk, t, allowOverDraft, currency, depositWarning);
			accountsToSave.add(ra);
		}
		Objectify ofy = ObjectifyService.beginTransaction();
		try
		{
			for(InetResidentAccount ra : accountsToSave)
			{
				InetResidentAccount tmp = ofy.find(ra.getKey());
				if(tmp != null)
					throw new DuplicateEntitiesException("Account already exists for " + ra.getKey());
			}
			ofy.put(accountsToSave);
			String typeStr = Arrays.toString(types);
			String[] comments = {"Created account(s): " + typeStr};
			TaskQueueHelper.scheduleCreateComment(comments, tk.getString(), user);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return accountsToSave;
	}

	public static InetResidentAccount updateResidentAccount(Key<InetResidentAccount> raKey, String user,
			boolean allowOverDraft, int depositWarning) throws MissingEntitiesException
	{
		Objectify ofy = ObjectifyService.beginTransaction();
		InetResidentAccount ra= ofy.find(raKey);
		try
		{
			if(ra == null)
				throw new MissingEntitiesException("Unable to find account for: " + raKey);
			ra.allowOverDraft = allowOverDraft;
			ra.depositWarningLevel = depositWarning;
			ofy.put(ra);
			
			String[] comments = {"Modified account: " + ra.getKey().getName()};
			TaskQueueHelper.scheduleCreateComment(comments, ra.getResident().getString(), user);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return ra;
	}	
	
	public static TableMessage getResidentAcctDTO(InetResidentAccount ra)
	{
		TableMessage m = new TableMessage(5, 3, 2);
		m.setText(DTOConstants.RSD_ACCT_TYPE_IDX, ra.getDepositType());
		Key<Apartment> ak = ra.getResident().getParent();
		m.setText(DTOConstants.RSD_ACCT_CURR_IDX, ra.getCurrency());		
		m.setText(DTOConstants.RSD_ACCT_OVER_IDX, String.valueOf(ra.allowOverDraft));
		m.setNumber(DTOConstants.RSD_ACCT_AMT_IDX, ra.getCurrentAmount());
		m.setNumber(DTOConstants.RSD_ACCT_LEV_IDX, ra.depositWarningLevel);
		m.setNumber(DTOConstants.RSD_ACCT_TXN_IDX, ra.getNumberOfTxns());
		m.setDate(DTOConstants.RSD_ACCT_MOD_DT_IDX, ra.getLastModifiedDate());
		m.setDate(DTOConstants.RSD_ACCT_CRT_DT_IDX, ra.getCreateDate());
		m.setText(DTOConstants.RSD_ACCT_BLD_IDX, ak.getParent().getName());		
		m.setText(DTOConstants.RSD_ACCT_APT_IDX, ak.getName());		
		m.setMessageId(ra.getKey().getString());
		return m;
	}
	
	public static TableMessageHeader getResidentAcctDTOHeader()
	{
		TableMessageHeader m = new TableMessageHeader(10);
		m.setText(0, "Type", TableMessageContent.TEXT);
		m.setText(1, "Ccy", TableMessageContent.TEXT);
		m.setText(2, "-ve ok?", TableMessageContent.TEXT);
		m.setText(3, "Balance", TableMessageContent.NUMBER);
		m.setText(4, "Warning Level", TableMessageContent.NUMBER);
		m.setText(5, "Txn Count", TableMessageContent.NUMBER);
		m.setFormatOption(5, "###");
		m.setText(6, "Modified", TableMessageContent.DATE);
		m.setText(7, "Created", TableMessageContent.DATE);
		m.setText(8, "Bld", TableMessageContent.TEXT);
		m.setText(9, "Apt", TableMessageContent.TEXT);		
		return m;
	}
	
	public static List<InetResidentAccount> getResidentAccountForBuilding(Key<Building> bk, Objectify ofy)
	{
		Key<InetResidentAccount> pk = new Key<InetResidentAccount>(InetResidentAccount.class, bk.getName());
		return ofy.query(InetResidentAccount.class).ancestor(pk).list();
	}
}
