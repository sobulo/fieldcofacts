package com.fertiletech.fieldcodata.backend.accounting;

import java.util.Date;

import com.googlecode.objectify.Key;

public interface LedgerEntry{
	boolean isCredit();
	double getAmount();
	double getAccountBalance();
	void setAccountBalance(double balance);
	String getDescription();
	Date getEntryDate();
	Date getEffectiveDate();
	long getEntryCount();
	void setEntryCount(long count);
	Key<? extends LedgerEntry> getKey();
	String getReferenceId();
	String getEntryUser();
}
