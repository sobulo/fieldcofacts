package com.fertiletech.fieldcodata.backend.accounting;

import java.util.Comparator;
import java.util.Date;

import javax.persistence.Id;
import javax.persistence.PrePersist;

import com.fertiletech.fieldcodata.backend.entities.Apartment;
import com.fertiletech.fieldcodata.backend.entities.Building;
import com.fertiletech.fieldcodata.backend.entities.Tenant;
import com.fertiletech.fieldcodata.ui.shared.dto.DTOConstants;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Parent;
import com.googlecode.objectify.annotation.Unindexed;

@Unindexed
public class InetResidentAccount implements Comparator<LedgerEntry> {
	@Id
	private String keyName;

	@Parent
	Key<InetResidentAccount> parentAccountKey;
	@Indexed
	double currentAmount;
	private int entryCount;
	private Date createDate;
	private Date lastModified;
	boolean allowOverDraft;

	int depositWarningLevel;
	private final static char KEY_SEPERATOR = '-';

	public InetResidentAccount() {
		createDate = new Date();
	}

	public InetResidentAccount(Key<Tenant> tenantKey, String depositType,
			boolean allowOverDraft, String currency, int depositWarningLevel) {
		this();
		if (depositType == null || currency == null
				|| depositType.length() == 0
				|| currency.length() != DTOConstants.CURRENCY_LENGTH)
			throw new RuntimeException("Invalid type or currency. Type: "
					+ depositType + " currency: " + currency);

		this.entryCount = 0;
		this.currentAmount = 0;
		this.parentAccountKey = getParentAccountKey(tenantKey);
		this.allowOverDraft = allowOverDraft;
		this.keyName = getKeyName(depositType, currency);
		this.depositWarningLevel = depositWarningLevel;
	}

	static String getKeyName(String depositType, String currency)
	{
		return currency + KEY_SEPERATOR + depositType;
	}
	public Key<InetResidentAccount> getParentAccountKey() {
		return parentAccountKey;
	}

	public Date getLastModified() {
		return lastModified;
	}

	public boolean isAllowOverDraft() {
		return allowOverDraft;
	}

	public int getDepositWarningLevel() {
		return depositWarningLevel;
	}

	public Key<InetResidentAccount> getKey() {
		return getKeyHelper(parentAccountKey, keyName);
	}

	public double getCurrentAmount() {
		return currentAmount;
	}

	void processLedger(LedgerEntry entry) {
		currentAmount += (entry.getAmount() * (entry.isCredit() ? 1 : -1));
		entry.setAccountBalance(currentAmount);
		entry.setEntryCount(++entryCount);
	}

	public int getNumberOfTxns() {
		return entryCount;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public Date getLastModifiedDate() {
		return lastModified;
	}

	@PrePersist
	void changeModificationDate() {
		lastModified = new Date();
	}

	public static Key<InetResidentAccount> getKey(Key<Tenant> tenantKey,
			String type, String ccy) {
		return getKeyHelper(getParentAccountKey(tenantKey), getKeyName(type, ccy));
	}

	private static Key<InetResidentAccount> getKeyHelper(
			Key<InetResidentAccount> pk, String typeAndCCy) {
		return new Key<InetResidentAccount>(pk, InetResidentAccount.class, typeAndCCy);
	}

	public static Key<InetResidentAccount> getParentAccountKey(
			Key<Tenant> tenantKey) {
		long tenantID = tenantKey.getId();
		String aptID = tenantKey.getParent().getName();
		String buildingID = tenantKey.getParent().getParent().getName();
		Key<InetResidentAccount> tempKey = new Key<InetResidentAccount>(
				InetResidentAccount.class, buildingID);
		tempKey = new Key<InetResidentAccount>(tempKey,
				InetResidentAccount.class, aptID);
		tempKey = new Key<InetResidentAccount>(tempKey,
				InetResidentAccount.class, String.valueOf(tenantID));
		return tempKey;
	}

	public Key<Tenant> getResident() {
		return getResident(parentAccountKey, true);
	}

	public static Key<Tenant> getResident(
			Key<InetResidentAccount> parentAccountKey, boolean isAccountParent) {
		if (!isAccountParent)
			parentAccountKey = parentAccountKey.getParent();

		long tenantID = Long.valueOf(parentAccountKey.getName());
		String aptID = parentAccountKey.getParent().getName();
		String buildingID = parentAccountKey.getParent().getParent().getName();

		Key<Building> bk = new Key<Building>(Building.class, buildingID);
		Key<Apartment> ak = new Key<Apartment>(bk, Apartment.class, aptID);
		Key<Tenant> tk = new Key<Tenant>(ak, Tenant.class, tenantID);
		return tk;
	}



	public String getFormattedTitle() 
	{
		StringBuilder result = new StringBuilder();
		Key<InetResidentAccount> cursor = getKey();
		while(cursor != null)
		{
			result.append(cursor.getName());
			cursor = cursor.getParent();
		}
		return result.toString();
	}

	@Override
	public int compare(LedgerEntry o1, LedgerEntry o2) {
		return compareFunction(o1, o2);
	}

	private static int compareFunction(LedgerEntry o1, LedgerEntry o2) {
		if(o1 == o2 || o1.getKey().equals(o2.getKey())) return 0;
		if (o1.getKey().getParent().compareTo(o2.getKey().getParent()) == 0) {
			if (o1.getEntryCount() == o2.getEntryCount())
				return 0;
			if (o1.getEntryCount() > o2.getEntryCount())
				return 1;
			else
				return -1;
		} else
			return o1.getKey().getParent().compareTo(o2.getKey().getParent());

	}

	public static Comparator<LedgerEntry> getComparator() {
		return new Comparator<LedgerEntry>() {

			@Override
			public int compare(LedgerEntry o1, LedgerEntry o2) {
				return compareFunction(o1, o2);
			}
		};
	}

	public String getDepositType() {
		return keyName.substring(DTOConstants.CURRENCY_LENGTH + 1);
	}

	public String getCurrency() {
		return keyName.substring(0, DTOConstants.CURRENCY_LENGTH);
	}
}
