/**
 * 
 */
package com.fertiletech.fieldcodata.backend.accounting;


import java.text.DateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;

import javax.persistence.Id;
import javax.persistence.PrePersist;

import com.fertiletech.fieldcodata.backend.entities.Contact;
import com.fertiletech.fieldcodata.backend.tickets.RequestTicket;
import com.fertiletech.fieldcodata.ui.shared.dto.BillDescriptionItem;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Parent;
import com.googlecode.objectify.annotation.Serialized;
import com.googlecode.objectify.annotation.Unindexed;

/**
 * @author Segun Razaq Sobulo
 *
 */
@Indexed
@Cached
public class InetBill{
	@Id private String key;
	@Unindexed private double settledAmount;
	@Parent private Key<? extends Contact> userKey;
	private boolean settled;  //indexed field for finding which bills are fully paid already
	
	//billing period
	@Unindexed private Date servicePeriodStartDate;
	@Unindexed private Date servicePeriodEndDate;
	@Unindexed Date createDate;
	@Unindexed String currency;
	Date dueDate;
	@Indexed Long invoiceId;
	
	@Indexed Key<RequestTicket> ticketKey;
	
	//title - possibly use it for categories /i.e. no free-form stuff
	private String title;
	
    @Serialized
    private LinkedHashSet<BillDescriptionItem> itemizedBill;	
	
	final static double THRESHOLD = 0.01;
	final static DateFormat DF = DateFormat.getDateInstance(DateFormat.MEDIUM);
	
	InetBill(){ createDate = new Date();}
	
	InetBill(Key<? extends Contact> billedUserKey, Key<RequestTicket> ticketKey, LinkedHashSet<BillDescriptionItem> billDescription, 
			String category, Date start, Date end, Date due, String ccy)
	{
		this();
		this.itemizedBill = billDescription;
		this.title = category;
		this.servicePeriodStartDate = start;
		this.servicePeriodEndDate = end;
		this.dueDate = due;
		this.settled = false;
		this.userKey = billedUserKey;
		this.key = getKeyString();
		this.ticketKey = ticketKey;
		this.currency = ccy;
	}
	
    private String getKeyString()
    {
    	return getKeyString(userKey, title, servicePeriodStartDate, servicePeriodEndDate);
    }
    
    public static String getKeyString(Key<? extends Contact> uKey, String category, Date start, Date end)
    {
        String format = "%s~%s";
        return String.format(format, getFormatedTitle(category, start, end), getUserKeyString(uKey));    	
    }
    
    public String getFormatedTitle()
    {
    	return getFormatedTitle(this.title, this.servicePeriodStartDate, this.servicePeriodEndDate);
    }
    
    private static String getFormatedTitle(String category, Date start, Date end)
    {
    	return category + " " + DF.format(start) + " - " + DF.format(end);    	
    }
    
    private static String getUserKeyString(Key<? extends Contact> userKey)
    {
    	String format = "%s.%s.%s";
    	String buildingName = userKey.getParent().getParent().getName();
    	String aptID = String.valueOf(userKey.getParent().getName());
    	String TenantID = String.valueOf(userKey.getId());
    	return String.format(format, buildingName, aptID, TenantID);
    }
    
	public Key<InetBill> getKey() {
		return new Key<InetBill>(userKey, InetBill.class, key);
	}
	
	public Date getServicePeriodStartDate() {
		return servicePeriodStartDate;
	}

	public Date getServicePeriodEndDate() {
		return servicePeriodEndDate;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public String getTitle() {
		return title;
	}

	public boolean isSettled()
	{
		return settled;
	}
	
	public double getSettledAmount()
	{
		return settledAmount;
	}
	
	public void setSettledAmount(double amount)
	{
		settledAmount = amount;
	}
	
	public Key<? extends Contact> getUserKey()
	{
		return userKey;
	}
	
	public HashSet<BillDescriptionItem> getItemizedBill() {
		return itemizedBill;
	}
	
	public void setItemizedBill(LinkedHashSet<BillDescriptionItem> itemizedBill)
	{
		this.itemizedBill = itemizedBill;
	}
	
	public double getTotalAmount()
	{
		double total = 0;
		for(BillDescriptionItem bdi : itemizedBill)
			total += bdi.getAmount();
		return total;
	}
	
	public long getInvoiceID()
	{
		return invoiceId;
	}
	
	public Key<RequestTicket> getTicketKey() {
		return ticketKey;
	}

	public void setTicketKey(Key<RequestTicket> ticketKey) {
		this.ticketKey = ticketKey;
	}

	@PrePersist void updateInvoiceId()
	{
		if(invoiceId == null)
		{
			invoiceId = ObjectifyService.factory().allocateId(InetWithdrawal.class);
		}
	}
	@PrePersist void updateSettled() { settled = Math.abs(getTotalAmount() - settledAmount) < THRESHOLD; }

	public String getCurrency() {
		return currency;
	}; 
}
