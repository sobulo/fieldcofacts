package com.fertiletech.fieldcodata.scripts.bulkaction;

import com.fertiletech.utils.ColumnAccessor;
import com.fertiletech.utils.ExcelManager;

public class BulkConstants {
    public final static ColumnAccessor[] TENANT_UPLOAD_ACCESSORS =
            new ColumnAccessor[7];
    public final static ColumnAccessor[] VENDOR_UPLOAD_ACCESSORS =
            new ColumnAccessor[6];
    
    public final static String APT_HEADER = "APARTMENT";
    public final static String NAME_HEADER = "NAME";
    public final static String COMPANY_HEADER = "COMPANY";
    public final static String PHONE_HEADER = "PHONE 1";
    public final static String PHONE2_HEADER = "PHONE 2";
    public final static String EMAIL_HEADER = "E-MAIL 1";
    public final static String EMAIL2_HEADER = "E-MAIL 2";
    public final static String ADDRESS_HEADER = "ADDRESS";
    public final static String CATEGORY_HEADER = "TYPE";

    static{
        TENANT_UPLOAD_ACCESSORS[0] = new ExcelManager.DefaultAccessor(APT_HEADER, true);
        TENANT_UPLOAD_ACCESSORS[1] = new ExcelManager.DefaultAccessor(NAME_HEADER, false);     
        TENANT_UPLOAD_ACCESSORS[2] = new ExcelManager.DefaultAccessor(COMPANY_HEADER, false);
        TENANT_UPLOAD_ACCESSORS[3] = new ExcelManager.DefaultAccessor(PHONE_HEADER, false);
        TENANT_UPLOAD_ACCESSORS[4] = new ExcelManager.DefaultAccessor(PHONE2_HEADER, false);
        TENANT_UPLOAD_ACCESSORS[5] = new ExcelManager.DefaultAccessor(EMAIL_HEADER, false);
        TENANT_UPLOAD_ACCESSORS[6] = new ExcelManager.DefaultAccessor(EMAIL2_HEADER, false);
        
        VENDOR_UPLOAD_ACCESSORS[0] = new ExcelManager.DefaultAccessor(CATEGORY_HEADER, true);
        VENDOR_UPLOAD_ACCESSORS[1] = new ExcelManager.DefaultAccessor(NAME_HEADER, true);        
        VENDOR_UPLOAD_ACCESSORS[2] = new ExcelManager.DefaultAccessor(COMPANY_HEADER, true);
        VENDOR_UPLOAD_ACCESSORS[3] = new ExcelManager.DefaultAccessor(ADDRESS_HEADER, false);
        VENDOR_UPLOAD_ACCESSORS[4] = new ExcelManager.DefaultAccessor(PHONE2_HEADER, false);
        VENDOR_UPLOAD_ACCESSORS[5] = new ExcelManager.DefaultAccessor(EMAIL2_HEADER, false);
    }
    
    
}
