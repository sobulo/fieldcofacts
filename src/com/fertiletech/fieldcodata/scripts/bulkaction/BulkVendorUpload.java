package com.fertiletech.fieldcodata.scripts.bulkaction;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.fertiletech.fieldcodata.backend.entities.ReadDAO;
import com.fertiletech.fieldcodata.backend.entities.Vendor;
import com.fertiletech.fieldcodata.backend.entities.WriteDAO;
import com.fertiletech.fieldcodata.ui.server.GeneralFuncs;
import com.fertiletech.fieldcodata.ui.server.TenantDAOManagerImpl;
import com.fertiletech.fieldcodata.ui.server.login.LoginHelper;
import com.fertiletech.utils.ColumnAccessor;
import com.fertiletech.utils.EmptyColumnValueException;
import com.fertiletech.utils.ExcelManager;
import com.fertiletech.utils.InvalidColumnValueException;
import com.googlecode.objectify.ObjectifyService;


public class BulkVendorUpload extends HttpServlet{
	// TODO review classes of j9educationaction package and ensure no instance
	// of services being created. instead create static versions of such fns
	private static final Logger log = Logger.getLogger(BulkVendorUpload.class
			.getName());
	private final static String VENDOR_UPLD_SESSION_NAME = "vendorUploadData";
	private final static String VENDOR_FILE_NAME = "vendorUploadName";
	private final static String LOAD_PARAM_NAME = "loadData";
	private final static String GOOD_DATA_PREFIX = "<i></i>";

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		// TODO add validation

		res.setContentType("text/html");
		ServletOutputStream out = res.getOutputStream();
		if (req.getParameter(LOAD_PARAM_NAME) != null) {
			Object upldSessObj = req.getSession().getAttribute(
					VENDOR_UPLD_SESSION_NAME);
			if (upldSessObj != null) {
				// retrive data from session object
				ArrayList<VendorStruct> vendorDataList = (ArrayList<VendorStruct>) upldSessObj;
				StringBuilder badMsg = new StringBuilder();
				StringBuilder okVendors = new StringBuilder();
				int createCount = 0;
				String user = LoginHelper.getLoggedInUser(req);
				String fileName = (String) req.getSession().getAttribute(VENDOR_FILE_NAME);
				try {											
						//second pass, create tenants
						for (VendorStruct elem : vendorDataList) 
						{
							if(elem.name.equals(""))
								continue; //skip line, no tenant to create
							
							String[] names = elem.name.split(" ");
							String firstName = "";
							String lastName = "";
							if(names.length == 1)
								lastName = names[0];
							else if(names.length == 2)
							{
								lastName = names[1];
								firstName = names[0];
							}
							else
							{
								lastName = names[names.length - 1];
								firstName = elem.name.substring(0, elem.name.lastIndexOf(" "));
							}
							
							String[] numbers = elem.phone2.split(",");
							HashSet<String> secondaryNumbers = new HashSet<String>();
							for(int i = 1; i < numbers.length; i++)
								secondaryNumbers.add(numbers[i]);
							
							String[] emails = elem.email2.split(",");
							HashSet<String> secondaryEmails = new HashSet<String>();
							for(int i = 1; i < emails.length; i++)
								secondaryEmails.add(emails[i]);

							String primaryEmail = emails.length > 0? emails[0] : null;
							String primaryNumber = numbers.length > 0 ? numbers[0] : null;
							String companyName = elem.company;
							String vendorAddress = elem.address;
							Boolean isMale = null;
							String title = null;
							
							Vendor v = WriteDAO.createVendor(elem.category, firstName, lastName, null, primaryEmail, primaryNumber, secondaryNumbers, secondaryEmails,
									companyName, vendorAddress, isMale, title, true);
							okVendors.append(++createCount).append(": ").append(v.getCompanyName()).append("<br/>");
							String commentTitle = "Bulk Vendor Upload of Vendor Contact for Company: " + companyName + ". " + fileName + " Entry " + createCount + " of " + vendorDataList.size();
							TenantDAOManagerImpl.scheduleTenantBioComment(v, v.getKey(), null, commentTitle, user);							
					} 
				}
				catch(Exception ex)
				{
					out.println(badMsg.toString());
					out.println("<b>" + ex.getMessage() + "</b>");
				}
				
				out.println("<br/><hr/><br/>Created the following vendors<br/>");
				out.println(okVendors.toString());
				out.flush();
				// set session data to null as we've saved the data
				req.setAttribute(VENDOR_UPLD_SESSION_NAME, null);
			} else {
				out.println("<b>unable to retrieve session info. Try "
						+ "enabling cookies in your browser.</b>");
			}
		} else {
			/**
			 * First pass: this is where we read excel file and save contents in a session object
			 * In the 2nd pass above, we create tenant objects in the database based on this
			 */
			ServletFileUpload upload = new ServletFileUpload();
			upload.setHeaderEncoding("ISO-8858-2");
			try {
				FileItemIterator iterator = upload.getItemIterator(req);
				while (iterator.hasNext()) {
					FileItemStream item = iterator.next();
					InputStream stream = item.openStream();
						log.warning("Bulk VENDOR upload attempt: "
								+ item.getFieldName() + ", name = "
								+ item.getName());
						ColumnAccessor[] headerAccessors = BulkConstants.VENDOR_UPLOAD_ACCESSORS;
						String[] expectedHeaders = ColumnAccessor
								.getAccessorNames(headerAccessors);
						// TODO, return error page if count greater than 200
						ExcelManager sheetManager = new ExcelManager(stream);
						// confirm headers in ssheet match what we expect
						String[] sheetHeaders;
						try 
						{
							sheetHeaders = sheetManager.getHeaders();

							// expectedheaders are uppercase so convert sheet
							// prior to compare
							for (int s = 0; s < sheetHeaders.length; s++) {
								sheetHeaders[s] = sheetHeaders[s].toUpperCase();
							}
						} catch (InvalidColumnValueException ex) {
							log.severe(ex.getMessage());
							out.println("<b>Unable to read excel sheet headers</b><br/> Error was: "
									+ ex.getMessage());
							return;
						}
						ArrayList<String> sheetOnly = new ArrayList<String>();
						ArrayList<String> intersect = new ArrayList<String>();
						ArrayList<String> expectedOnly = new ArrayList<String>();
						GeneralFuncs.arrayDiff(expectedHeaders, sheetHeaders,
								intersect, expectedOnly, sheetOnly);
						// handle errors in column headers
						if (expectedOnly.size() > 0 || sheetOnly.size() > 0) {
							out.println("<b>Uploaded sheet should contain only these headers:</b>");
							printHtmlList(expectedHeaders, out);
							if (expectedOnly.size() > 0) {
								out.println("<b>These headers are missing from ssheet:</b>");
								printHtmlList(expectedOnly, out);
							}
							if (sheetOnly.size() > 0) {
								out.println("<b>These headers are not expected but were found in ssheet</b>");
								printHtmlList(sheetOnly, out);
							}
							return;
						}

						sheetManager.initializeAccessorList(headerAccessors);
						// todo, replace these with string builders
						String goodDataHTML = "";
						String badDataHTML = "";
						String tempVal = "";
						String[] htmlHeaders = new String[headerAccessors.length];
						ArrayList<VendorStruct> sessionData = new ArrayList<VendorStruct>();

						for (int i = 0; i < headerAccessors.length; i++) {
							htmlHeaders[i] = headerAccessors[i].getHeaderName();
						}

						int numRows = sheetManager.totalRows();
						HashMap<String, String> companyNameTable = new HashMap<String, String>(
								numRows);
						
						//we load all vendors and build a map of existing companies in the database
						List<Vendor> allVendors = ReadDAO.getAllVendors(ObjectifyService.begin());
						HashMap<String, String> vendorsInDatabase = new HashMap<String, String>();
						for(Vendor v : allVendors)
							vendorsInDatabase.put(v.getCompanyName(), v.getLastName());
						
						for (int i = 1; i < numRows; i++) {
							try {
								String row = "<tr>";
								VendorStruct tempVendorData = new VendorStruct();
								for (int j = 0; j < headerAccessors.length; j++) {
									tempVal = getValue(i, headerAccessors[j],
											sheetManager, tempVendorData);
									row += "<td>" + tempVal + "</td>";
								}

								if (tempVendorData.isValid) {
									if (companyNameTable.containsKey(tempVendorData.company)) {
										String errMsg = String
												.format("Contact [%s] can't have company [%s]. An earlier spreadsheet row (contact: [%s]) in the file maps to that company",
														tempVendorData.name, tempVendorData.company, companyNameTable.get(tempVendorData.company));
										row = new StringBuilder(
												"<tr><td colspan=")
												.append(htmlHeaders.length)
												.append(">").append(errMsg)
												.append("</td>").toString();
										tempVendorData.isValid = false;
									} 
									else if(vendorsInDatabase.containsKey(tempVendorData.company)) {
										String errMsg = String
												.format("Contact [%s] is listed under company [%s]. However the vendor company is already listed in the database, contact person is [%s]",
														tempVendorData.name, tempVendorData.company, vendorsInDatabase.get(tempVendorData.company));
										row = new StringBuilder(
												"<tr><td colspan=")
												.append(htmlHeaders.length)
												.append(">").append(errMsg)
												.append("</td>").toString();
										tempVendorData.isValid = false;
									}									
									else {		
										companyNameTable.put(tempVendorData.company, tempVendorData.name);
										row += "<td>" + tempVendorData.company + "</td>";
										row += "<td>" + tempVendorData.name + "</td></tr>";
									}
								}

								if (tempVendorData.isValid) {
									// append to display result
									goodDataHTML += row;
									sessionData.add(tempVendorData);
								} else {
									badDataHTML += (row + "</tr>");
								}
							} catch (EmptyColumnValueException ex) {
							} // logic to help skip blank lines
						}

						String htmlTableStart = "<TABLE border='1' class='themePaddedBorder'>";
						String htmlHeader = "";
						for (String s : htmlHeaders) {
							htmlHeader += "<TH>" + s + "</TH>";
						}
						String htmlTableEnd = "</TABLE>";

						if (goodDataHTML.length() > 0) {
							out.println(GOOD_DATA_PREFIX);
							String goodHeaderSuffix = "<TH>Apartment OK</TH><TH>Name OK</TH>";
							goodDataHTML = "<b>Below shows data that passed prelim checks. Hit save to store the data:</b><br/><hr/>"
									+ htmlTableStart
									+ "<TR>"
									+ htmlHeader
									+ goodHeaderSuffix
									+ "</TR>"
									+ goodDataHTML + htmlTableEnd;
						}

						// TODO, possibly fixed but check
						if (badDataHTML.length() > 0) {
							out.println("<b>Below shows records with errors</b>");
							out.println(htmlTableStart);
							out.println("<TR>");
							out.println(htmlHeader);
							out.println("</TR>");
							out.print(badDataHTML);
							out.println(htmlTableEnd);
						}

						out.println(goodDataHTML);

						// TODO, test what happens with sheet containing only
						// headers
						req.getSession().setAttribute(VENDOR_UPLD_SESSION_NAME, sessionData);
						req.getSession().setAttribute(VENDOR_FILE_NAME, item.getName());
					}
				}
			 catch (FileUploadException ex) {
				out.println("<b>File upload failed:</b>" + ex.getMessage());
			} catch (InvalidColumnValueException ex) {
				log.severe(ex.getMessage());
				out.println("<b>Unable to read excel sheet</b><br/> Error was: "
						+ ex.getMessage());
			}
		}
	}

	private String getValue(int row, ColumnAccessor accessor,
			ExcelManager sheetManager, VendorStruct tenantData)
			throws InvalidColumnValueException {
		Object temp;
		String val;
		String headerName = accessor.getHeaderName();
		try {
			temp = sheetManager.getData(row, accessor);

			if (temp == null)
				val = "";
			else
				val = ((String) temp).trim();

			if (headerName.equals(BulkConstants.NAME_HEADER)) {
				tenantData.name = val;
			} else if (headerName.equals(BulkConstants.CATEGORY_HEADER)) {
				tenantData.category = val;
			} else if (headerName.equals(BulkConstants.COMPANY_HEADER)) {
				tenantData.company = val.toUpperCase();
			} else if (headerName.equals(BulkConstants.ADDRESS_HEADER)) {
				tenantData.address = val;	
			} else if (headerName.equals(BulkConstants.EMAIL2_HEADER)) {
				tenantData.email2 = val.toLowerCase();
			} else if (headerName.equals(BulkConstants.PHONE2_HEADER)) {
				tenantData.phone2 = val;
			}
		} catch (EmptyColumnValueException ex) {
			if (sheetManager.isBlankRow(row))
				throw ex;
			val = ex.getMessage();
			tenantData.isValid = false;
		} catch (InvalidColumnValueException ex) {
			val = ex.getMessage();
			tenantData.isValid = false;
		}
		return val;
	}

	private void printHtmlList(String[] list, ServletOutputStream out)
			throws IOException {
		out.println("<ul>");
		for (String s : list) {
			out.println("<li>" + s + "</li>");
		}
		out.println("</ul>");
	}

	private void printHtmlList(List<String> list, ServletOutputStream out)
			throws IOException {
		out.println("<ul>");
		for (String s : list) {
			out.println("<li>" + s + "</li>");
		}
		out.println("</ul>");
	}	
}

class VendorStruct implements Serializable {
	String category;
	String name;
	String address;
	String company;
	String email2;
	String phone2;
	boolean isValid = true;
}