/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fertiletech.fieldcodata.scripts.bulkaction;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;

import com.fertiletech.fieldcodata.backend.entities.Apartment;
import com.fertiletech.fieldcodata.backend.entities.Building;
import com.fertiletech.fieldcodata.backend.entities.ReadDAO;
import com.fertiletech.fieldcodata.backend.entities.Representative;
import com.fertiletech.fieldcodata.backend.entities.Tenant;
import com.fertiletech.fieldcodata.backend.entities.WriteDAO;
import com.fertiletech.fieldcodata.ui.server.GeneralFuncs;
import com.fertiletech.fieldcodata.ui.server.TenantDAOManagerImpl;
import com.fertiletech.fieldcodata.ui.server.login.LoginHelper;
import com.fertiletech.utils.ColumnAccessor;
import com.fertiletech.utils.DateColumnAccessor;
import com.fertiletech.utils.EmptyColumnValueException;
import com.fertiletech.utils.ExcelManager;
import com.fertiletech.utils.InvalidColumnValueException;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

public class BulkTenantAndApartmentUpload extends HttpServlet {
	// TODO review classes of j9educationaction package and ensure no instance
	// of services being created. instead create static versions of such fns
	private static final Logger log = Logger.getLogger(BulkTenantAndApartmentUpload.class
			.getName());
	private final static String TENANT_UPLD_SESSION_NAME = "tenantUploadData";
	private final static String BUILDING_KEY_SESSION_NAME = "buildingkey";
	private final static String FILE_SESSION_NAME = "tenantUploadName";
	private final static String LOAD_PARAM_NAME = "loadData";
	private final static String GOOD_DATA_PREFIX = "<i></i>";
	private final static DateFormat DATE_FORMATTER = DateColumnAccessor
			.getDateFormatter();

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		// TODO add validation
		/*
		 * PanelServiceLoginRoles[] allowedRoles =
		 * {PanelServiceLoginRoles.ROLE_ADMIN}; try {
		 * LoginPortal.verifyRole(allowedRoles, req); } catch
		 * (LoginValidationException e) {
		 * res.getOutputStream().println("<html><body><b>Illegal Access: " +
		 * e.getMessage() + "</b></body></html>"); return; }
		 */

		res.setContentType("text/html");
		String user = LoginHelper.getLoggedInUser(req);
		ServletOutputStream out = res.getOutputStream();
		if (req.getParameter(LOAD_PARAM_NAME) != null) {
			Object upldSessObj = req.getSession().getAttribute(
					TENANT_UPLD_SESSION_NAME);
			Object bldSessObj = req.getSession().getAttribute(
					BUILDING_KEY_SESSION_NAME);
			if (upldSessObj != null && bldSessObj != null) {
				// retrive data from session object
				ArrayList<TenantStruct> tenantDataList = (ArrayList<TenantStruct>) upldSessObj;
				String bldKey = (String) bldSessObj;
				Key<Building> buildingKey = ObjectifyService.factory().stringToKey(bldKey);
				String okMsg = "<b>Apartment,Name,Company,Phone1, Phone2, Email1, Email2</b><br/>";
				StringBuilder badMsg = new StringBuilder();
				String fileName = (String) req.getSession().getAttribute(FILE_SESSION_NAME);

				log.warning("Creating apartments and tenants for: " + buildingKey);
				Objectify ofy = ObjectifyService.beginTransaction();
				List<Key<Apartment>> apartmentKeys = ReadDAO.getApartments(ofy, buildingKey);
				HashMap<String, String> nameIndex = new HashMap<String, String>(
						tenantDataList.size());

				ArrayList<Tenant> newTenantList = new ArrayList<Tenant>();
				ArrayList<Apartment> newApartmentList = new ArrayList<Apartment>();
				try {
					Map<Key<Apartment>, Apartment> existingApartments = ofy.get(apartmentKeys);

					ArrayList<String> foundExistingApartments = new ArrayList<String>();
					HashMap<String, Apartment> apartmentKeyMap = new HashMap<String, Apartment>();
					if (existingApartments.size() != 0) //hmm let's check if there's a display name that matches
					{
						HashMap<String, Apartment> existingApartmentNameMaps = new HashMap<String, Apartment>();
						for(Apartment a : existingApartments.values())
							existingApartmentNameMaps.put(a.getDisplayName(), a);
				
						for(TenantStruct tenantInfo : tenantDataList)
						{
							//TODO, highly unecessary, changed apt keys to strings
							Apartment a = existingApartmentNameMaps.get(tenantInfo.apt);
							if(a != null)
							{
								foundExistingApartments.add(a.getDisplayName());
								apartmentKeyMap.put(tenantInfo.apt, a);
								nameIndex.put(tenantInfo.apt, tenantInfo.name);
							}
						}
					}
					
					if(foundExistingApartments.size() == 0)
					{
						//first pass, create apartments
						for (TenantStruct elem : tenantDataList)
						{
							if(apartmentKeyMap.containsKey(elem.apt))
								throw new RuntimeException("Apartment already marked for creation");
							
							Apartment a = WriteDAO.getApartmentObject(buildingKey, elem.apt);
							apartmentKeyMap.put(elem.apt, a);
							newApartmentList.add(a);
						}
						
						ofy.put(newApartmentList);
						
						//second pass, create tenants
						int tenantCount = 0;
						for (TenantStruct elem : tenantDataList) 
						{
							if(elem.name.equals(""))
								continue; //skip line, no tenant to create
							
							String[] names = elem.name.split(" ");
							String firstName = "";
							String lastName = "";
							if(names.length == 1)
								lastName = names[0];
							else if(names.length == 2)
							{
								lastName = names[1];
								firstName = names[0];
							}
							else
							{
								lastName = names[names.length - 1];
								firstName = elem.name.substring(0, elem.name.lastIndexOf(" "));
							}
							
							String[] numbers = elem.phone2.split(",");
							HashSet<String> secondaryNumbers = new HashSet<String>();
							for(String n : numbers)
								secondaryNumbers.add(n);
							
							String[] emails = elem.email2.split(",");
							HashSet<String> secondaryEmails = new HashSet<String>();
							for(String s : emails)
								secondaryEmails.add(s);

							String primaryEmail = elem.email1;
							String primaryNumber = elem.phone1;
							String companyName = elem.company;
							String tenantAddress = null;
							Boolean isMale = null;
							String title = null;
							Key<? extends Apartment> aptUnit = apartmentKeyMap.get(elem.apt).getKey(); 
							Key<Representative> companyRep = null;
							Tenant t = WriteDAO.getTenantObject(firstName, lastName, null, primaryEmail, primaryNumber, secondaryNumbers, secondaryEmails, companyName, tenantAddress, isMale, title, aptUnit,
									companyRep);
							newTenantList.add(t);
						}
						ofy.put(newTenantList);
						log.warning("Persisted tenant objects: " + newTenantList.size());
						ofy.getTxn().commit();
						log.warning("Committed transaction");
						for(Tenant t : newTenantList)
						{
							String commentTitle = "Bulk Upload for " + t.getApartmentUnit().getParent().getName().toUpperCase() + ". Entry " + (++tenantCount) + " of " + tenantDataList.size();
							TenantDAOManagerImpl.scheduleTenantBioComment(t, t.getKey(), ObjectifyService.begin(), commentTitle, user);
						}
					} else {
						badMsg.append("<font color='red'>Upload request ignored because the following apartment IDs already esist: </font><ul>");
						for (String s : foundExistingApartments) {
							Apartment a = apartmentKeyMap.get(s); 
							badMsg.append("<li>").append("Apartment ID: ")
									.append(a.getKey().toString())
									.append(" Apartment Name: ").append(a.getDisplayName())
									.append(", Tenant Name").append(nameIndex.get(s)).append("</li>");
						}
						badMsg.append("</ul>please examine your spreadsheet upload");
						out.println(badMsg.toString());
						return;
					}
				}
				catch(Exception ex)
				{
					log.warning("Exception caught: " + ex.getMessage());
					out.println(badMsg.toString());
					out.println("<b>" + ex.getMessage() + "</b>");
				}
				finally
				{
					if(ofy.getTxn().isActive())
						ofy.getTxn().rollback();
				}
				log.warning("Printing success message as thus");
				String msg = "Created " + newApartmentList.size() + " apartments and then created " + newTenantList.size() + " tenants";
				log.warning(msg);
				out.println(msg);
				// set session data to null as we've saved the data
				req.setAttribute(TENANT_UPLD_SESSION_NAME, null);
				req.setAttribute(BUILDING_KEY_SESSION_NAME, null);
			} else {
				out.println("<b>unable to retrieve session info. Try "
						+ "enabling cookies in your browser.</b>");
			}
		} else {
			/**
			 * First pass: this is where we read excel file and save contents in a session object
			 * In the 2nd pass above, we create tenant objects in the database based on this
			 */
			ServletFileUpload upload = new ServletFileUpload();
			upload.setHeaderEncoding("ISO-8858-2");
			try {
				FileItemIterator iterator = upload.getItemIterator(req);
				String buildingID = "";
				while (iterator.hasNext()) {
					FileItemStream item = iterator.next();
					InputStream stream = item.openStream();
					if (item.isFormField()
							&& item.getFieldName().equals(
									BUILDING_KEY_SESSION_NAME)) {
						buildingID = Streams.asString(stream);
						req.getSession().setAttribute(BUILDING_KEY_SESSION_NAME,
								buildingID);
					} else {
						log.warning("Bulk tenant upload attempt: "
								+ item.getFieldName() + ", name = "
								+ item.getName());
						ColumnAccessor[] headerAccessors = BulkConstants.TENANT_UPLOAD_ACCESSORS;
						String[] expectedHeaders = ColumnAccessor
								.getAccessorNames(headerAccessors);
						// TODO, return error page if count greater than 200
						// rows
						ExcelManager sheetManager = new ExcelManager(stream);
						// confirm headers in ssheet match what we expect
						String[] sheetHeaders;
						try {
							sheetHeaders = sheetManager.getHeaders();

							// expectedheaders are uppercase so convert sheet
							// prior to compare
							for (int s = 0; s < sheetHeaders.length; s++) {
								sheetHeaders[s] = sheetHeaders[s].toUpperCase();
							}
						} catch (InvalidColumnValueException ex) {
							log.severe(ex.getMessage());
							out.println("<b>Unable to read excel sheet headers</b><br/> Error was: "
									+ ex.getMessage());
							return;
						}
						ArrayList<String> sheetOnly = new ArrayList<String>();
						ArrayList<String> intersect = new ArrayList<String>();
						ArrayList<String> expectedOnly = new ArrayList<String>();
						GeneralFuncs.arrayDiff(expectedHeaders, sheetHeaders,
								intersect, expectedOnly, sheetOnly);
						// handle errors in column headers
						if (expectedOnly.size() > 0 || sheetOnly.size() > 0) {
							out.println("<b>Uploaded sheet should contain only these headers:</b>");
							printHtmlList(expectedHeaders, out);
							if (expectedOnly.size() > 0) {
								out.println("<b>These headers are missing from ssheet:</b>");
								printHtmlList(expectedOnly, out);
							}
							if (sheetOnly.size() > 0) {
								out.println("<b>These headers are not expected but were found in ssheet</b>");
								printHtmlList(sheetOnly, out);
							}
							return;
						}

						sheetManager.initializeAccessorList(headerAccessors);
						// todo, replace these with string builders
						String goodDataHTML = "";
						String badDataHTML = "";
						String tempVal = "";
						String[] htmlHeaders = new String[headerAccessors.length];
						ArrayList<TenantStruct> sessionData = new ArrayList<TenantStruct>();
						String[] loginNameComponents = new String[2];

						for (int i = 0; i < headerAccessors.length; i++) {
							htmlHeaders[i] = headerAccessors[i].getHeaderName();
						}

						int numRows = sheetManager.totalRows();
						HashMap<String, String> apartmentTable = new HashMap<String, String>(
								numRows);
						
						for (int i = 1; i < numRows; i++) {
							try {
								String row = "<tr>";
								TenantStruct tempTenantData = new TenantStruct();
								for (int j = 0; j < headerAccessors.length; j++) {
									tempVal = getValue(i, headerAccessors[j],
											sheetManager, tempTenantData);
									row += "<td>" + tempVal + "</td>";
								}

								if (tempTenantData.isValid) {
									if (apartmentTable.containsKey(tempTenantData.apt)) {
										String errMsg = String
												.format("Apartment [%s] can't have tenant [%s]. An earlier record (tenant: [%s]) in the file maps to that apartment",
														tempTenantData.apt, tempTenantData.name, apartmentTable.get(tempTenantData.apt));
										row = new StringBuilder(
												"<tr><td colspan=")
												.append(htmlHeaders.length)
												.append(">").append(errMsg)
												.append("</td>").toString();
										tempTenantData.isValid = false;
									} else {
										apartmentTable.put(tempTenantData.apt, tempTenantData.name);
										row += "<td>" + tempTenantData.apt + "</td>";
										row += "<td>" + tempTenantData.name + "</td></tr>";
									}
								}

								if (tempTenantData.isValid) {
									// append to display result
									goodDataHTML += row;
									sessionData.add(tempTenantData);
								} else {
									badDataHTML += (row + "</tr>");
								}
							} catch (EmptyColumnValueException ex) {
							} // logic to help skip blank lines
						}

						String htmlTableStart = "<TABLE border='1' class='themePaddedBorder'>";
						String htmlHeader = "";
						for (String s : htmlHeaders) {
							htmlHeader += "<TH>" + s + "</TH>";
						}
						String htmlTableEnd = "</TABLE>";

						if (goodDataHTML.length() > 0) {
							Key<Building> bk = ObjectifyService.begin()
									.getFactory().stringToKey(buildingID);
							out.println(GOOD_DATA_PREFIX);
							String goodHeaderSuffix = "<TH>Apartment OK</TH><TH>Name OK</TH>";
							goodDataHTML = "<b>Below shows data that passed prelim checks. Hit save to store the data for: "
									+ bk.getName()
									+ "</b>"
									+ htmlTableStart
									+ "<TR>"
									+ htmlHeader
									+ goodHeaderSuffix
									+ "</TR>"
									+ goodDataHTML + htmlTableEnd;
						}

						// TODO, possibly fixed but check
						if (badDataHTML.length() > 0) {
							out.println("<b>Below shows records with errors</b>");
							out.println(htmlTableStart);
							out.println("<TR>");
							out.println(htmlHeader);
							out.println("</TR>");
							out.print(badDataHTML);
							out.println(htmlTableEnd);
						}

						out.println(goodDataHTML);

						// TODO, test what happens with sheet containing only
						// headers
						req.getSession().setAttribute(TENANT_UPLD_SESSION_NAME,
								sessionData);
						req.getSession().setAttribute(FILE_SESSION_NAME, item.getName());
					}
				}
			} catch (FileUploadException ex) {
				out.println("<b>File upload failed:</b>" + ex.getMessage());
			} catch (InvalidColumnValueException ex) {
				log.severe(ex.getMessage());
				out.println("<b>Unable to read excel sheet</b><br/> Error was: "
						+ ex.getMessage());
			}
		}
	}

	private String getValue(int row, ColumnAccessor accessor,
			ExcelManager sheetManager, TenantStruct tenantData)
			throws InvalidColumnValueException {
		Object temp;
		String val;
		String headerName = accessor.getHeaderName();
		try {
			temp = sheetManager.getData(row, accessor);

			if (temp == null)
				val = "";
			else
				val = ((String) temp).trim();

			if (headerName.equals(BulkConstants.NAME_HEADER)) {
				tenantData.name = val;
			} else if (headerName.equals(BulkConstants.APT_HEADER)) {
				tenantData.apt = val;
			} else if (headerName.equals(BulkConstants.COMPANY_HEADER)) {
				tenantData.company = val.toUpperCase();
			} else if (headerName.equals(BulkConstants.EMAIL_HEADER)) {
				tenantData.email1 = val.toLowerCase();
			} else if (headerName.equals(BulkConstants.EMAIL2_HEADER)) {
				tenantData.email2 = val.toLowerCase();
			} else if (headerName.equals(BulkConstants.PHONE_HEADER)) {
				tenantData.phone1 = val;
			} else if (headerName.equals(BulkConstants.PHONE2_HEADER)) {
				tenantData.phone2 = val;
			}
		} catch (EmptyColumnValueException ex) {
			if (sheetManager.isBlankRow(row))
				throw ex;
			val = ex.getMessage();
			tenantData.isValid = false;
		} catch (InvalidColumnValueException ex) {
			val = ex.getMessage();
			tenantData.isValid = false;
		}
		return val;
	}

	private void printHtmlList(String[] list, ServletOutputStream out)
			throws IOException {
		out.println("<ul>");
		for (String s : list) {
			out.println("<li>" + s + "</li>");
		}
		out.println("</ul>");
	}

	private void printHtmlList(List<String> list, ServletOutputStream out)
			throws IOException {
		out.println("<ul>");
		for (String s : list) {
			out.println("<li>" + s + "</li>");
		}
		out.println("</ul>");
	}
}

class TenantStruct implements Serializable {
	String apt;
	String name;
	String company;
	String email1;
	String email2;
	String phone1;
	String phone2;
	boolean isValid = true;
}