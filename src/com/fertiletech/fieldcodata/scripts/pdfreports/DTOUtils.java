package com.fertiletech.fieldcodata.scripts.pdfreports;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.fertiletech.fieldcodata.backend.accounting.InetCompanyAccount;
import com.fertiletech.fieldcodata.backend.accounting.InetDeposit;
import com.fertiletech.fieldcodata.backend.accounting.InetResidentAccount;
import com.fertiletech.fieldcodata.backend.accounting.LedgerEntry;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessageHeader;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessageHeader.TableMessageContent;
import com.googlecode.objectify.Key;

public class DTOUtils {
	public static List<TableMessage> getLedgerDTO(List<? extends LedgerEntry> deposits, String headerStr, Map<Key<InetCompanyAccount>, InetCompanyAccount> bankAccts)
	{
		List<TableMessage> result = new ArrayList<TableMessage>(1 + deposits.size());
		//setup header message
		int numOfCols = 7;
		TableMessageHeader header = new TableMessageHeader(numOfCols);
		header.setText(0, "No.", TableMessageContent.NUMBER);
		header.setFormatOption(0,"###");		
		header.setText(1, "Description", TableMessageContent.TEXT);
		header.setText(2, "Type", TableMessageContent.TEXT);			
		header.setText(3, "Amount", TableMessageContent.NUMBER);
		header.setText(4, "Account Bal.", TableMessageContent.NUMBER);		
		header.setText(5, "Value Date", TableMessageContent.DATE);
		header.setText(6, "Entry Date", TableMessageContent.DATE);
		
		header.setMessageId(headerStr);
		result.add(header);

		Collections.sort(deposits, InetResidentAccount.getComparator());
		for(LedgerEntry dep : deposits)
		{
			TableMessage m = new TableMessage(2, 3, 2);
			m.setMessageId(dep.getKey().getString());
			m.setNumber(0, dep.getEntryCount());
			m.setNumber(1, dep.getAmount());
			m.setNumber(2, dep.getAccountBalance());
			
			m.setDate(0, dep.getEffectiveDate());
			m.setDate(1, dep.getEntryDate());	
			String description = "";
			if(dep instanceof InetDeposit)
			{	
				InetDeposit deposit = (InetDeposit) dep;
				InetCompanyAccount ba = bankAccts.get(deposit.getCompanyAccount());
				String helper = "Payment into " + ba.getAccountName() + " acct. with " + ba.getBank();			
				description = deposit.getReferenceId() == null? helper : (helper + ".  [Ref: " + deposit.getReferenceId()+"]"); 
			}
			else
				description = dep.getReferenceId();
			m.setText(0, description);	
			m.setText(1, dep.isCredit()? "Credit" : "Debit");
			result.add(m);
		}
		return result;		
	}

}
