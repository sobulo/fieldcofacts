/**
 * 
 */
package com.fertiletech.fieldcodata.scripts.pdfreports;
import javax.persistence.Id;

import com.google.appengine.api.datastore.Blob;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Unindexed;


/**
 * @author Segun Razaq Sobulo
 *
 */
@Unindexed
@Cached
public class InetImageBlob{
    @Id
    private String key;

    private Blob image;
    
    public InetImageBlob(){}
    
    public InetImageBlob(String key)
    {
    	this.key = key;
    }
    
    public void setImage(byte[] bytes)
    {
    	image = new Blob(bytes);
    }
    
    public byte[] getImage()
    {
    	return image.getBytes();
    }

	public static Key<InetImageBlob> getKey(String key) {
		return new Key<InetImageBlob>(InetImageBlob.class, key);
	}
	
	public Key<InetImageBlob> getKey()
	{
		return getKey(key);
	}

}


