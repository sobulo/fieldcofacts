/**
 * 
 */
package com.fertiletech.fieldcodata.scripts;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fertiletech.fieldcodata.backend.accounting.InetCompanyAccount;
import com.fertiletech.fieldcodata.backend.accounting.InetDAO4Accounts;
import com.fertiletech.fieldcodata.backend.entities.ApplicationParameters;
import com.fertiletech.fieldcodata.backend.entities.Building;
import com.fertiletech.fieldcodata.backend.entities.EntityConstants;
import com.fertiletech.fieldcodata.backend.entities.Facility;
import com.fertiletech.fieldcodata.backend.entities.Tenant;
import com.fertiletech.fieldcodata.backend.entities.Vendor;
import com.fertiletech.fieldcodata.backend.entities.WriteDAO;
import com.fertiletech.fieldcodata.scripts.tasks.TaskQueueHelper;
import com.fertiletech.fieldcodata.ui.server.login.LoginHelper;
import com.fertiletech.fieldcodata.ui.shared.dto.DTOConstants;
import com.fertiletech.fieldcodata.ui.shared.exceptions.DuplicateEntitiesException;
import com.fertiletech.fieldcodata.ui.shared.exceptions.MissingEntitiesException;
import com.google.appengine.api.datastore.QueryResultIterable;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class CommonSupportRequests extends HttpServlet{
    private static final Logger log =
        Logger.getLogger(CommonSupportRequests.class.getName());
    
	static {
		WriteDAO.registerClassesWithObjectify();
	}

    
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
        throws IOException
    {
        resp.setContentType("text/html");
        resp.getWriter().println("<b>Running patch ...</b><br/>");
        String user = LoginHelper.getLoggedInUser(req);
        ArrayList<String> levStudBreaks;
        String type = req.getParameter("op");
        if(type == null)
        {
        	resp.getWriter().println("aborting, didn't find support type");
        	return;
        }
        log.warning("Admin setup script action " + type + " being run by " + user);
        try
        {
        	String[] FTBS = {"mak", "ayo", "chikwendu", "sobulo", "opeyemi" };
            if(type.equals("vendors"))
            {
            	HashMap<String, String> parameters = new HashMap<String, String>();            	
            	List<Vendor> v = ObjectifyService.begin().query(Vendor.class).list();
            	for(Vendor i : v)
            		parameters.put(i.getCategory(), "Category migrated from data pulled off legacy ms-access vendors databased");
            	
            	ApplicationParameters a = WriteDAO.createApplicationParameters(DTOConstants.APP_PARAM_VENDOR_CATEGORY_LIST_KEY, parameters, null);

            	resp.getWriter().println("Created " + a.getKey());
            }
            else if(type.equals("company"))
            {
            	HashMap<String, String> parameters = null;
            	Objectify ofy = ObjectifyService.begin();
            	List<Tenant> t = ofy.query(Tenant.class).list();
            	ApplicationParameters b = ofy.find(ApplicationParameters.getKey(DTOConstants.APP_PARAM_COMPANY_LIST_KEY));
            	boolean update = false;
            	if(b == null || b.getParams() == null)
            		parameters = new HashMap<String, String>();
            	else
            	{
            		update = true;
            		parameters = b.getParams();
            	}
            	for(Tenant i : t)
            	{
            		if(i.getCompanyName()==null || i.getCompanyName().trim().length()==0)
            			continue;
            		if(!parameters.containsKey(i.getCompanyName()))
            			parameters.put(i.getCompanyName(), "Company name migrated off resident data uploaded in pilot-group google drive folder");
            	}
            	if(update)
            		WriteDAO.updateApplicationParameters(user, b.getKey(), parameters);
            	else
            		b = WriteDAO.createApplicationParameters(DTOConstants.APP_PARAM_COMPANY_LIST_KEY, parameters, null);
            	resp.getWriter().println("Created " + b.getKey());            	
            }
            else if(type.equals("vendorcompany"))
            {
            	HashMap<String, String> parameters = null;
            	Objectify ofy = ObjectifyService.begin();
            	List<Vendor> t = ofy.query(Vendor.class).list();
            	ApplicationParameters b = ofy.find(ApplicationParameters.getKey(DTOConstants.APP_PARAM_VENDOR_COMPANY_LIST_KEY));
            	boolean update = false;
            	if(b == null || b.getParams() == null)
            		parameters = new HashMap<String, String>();
            	else
            	{
            		update = true;
            		parameters = b.getParams();
            	}
            	for(Vendor i : t)
            	{
            		if(i.getCompanyName()==null || i.getCompanyName().trim().length()==0)
            			continue;
            		if(!parameters.containsKey(i.getCompanyName()))
            			parameters.put(i.getCompanyName(), "Company name migrated off vendor data uploaded as of 17/7/2014");
            	}
            	if(update)
            		WriteDAO.updateApplicationParameters(user, b.getKey(), parameters);
            	else
            		b = WriteDAO.createApplicationParameters(DTOConstants.APP_PARAM_VENDOR_COMPANY_LIST_KEY, parameters, null);
            	resp.getWriter().println("Created " + b.getKey());            	
            }            
            else if(type.equals("buildings"))
            {
            	resp.getWriter().println("buildings being created ... please wait");

            	String[] buildingNames = {"Rebecca", "Livingold", "Makanjuola", "Cameron", "Lifestyle", "Royal", "Tarino"};
            	String[] displayNames = {"Rebecca Court", "Livingold Terraces", "Makanjuola Court", "Cameron Court", "Lifestyle Terraces", "Royal Terraces", "Tarino Tower"};
            	String[] addresses = {"rebeccacourtng.com", "www.livingoldterraces.com", "www.makanjuolacourt.com", "www.cameroncourt.com", "www.lifestyleterraces.com", "www.royalterraces.com", "www.tarinotower.com"};
            	
            	for(int i = 0; i < buildingNames.length; i++)
            	{
            		Building b = WriteDAO.createBuilding(buildingNames[i], displayNames[i], addresses[i]);
                	resp.getWriter().println("Created building: " + b.getKey());
            	}
            }
            else if(type.equals("facilities"))
            {
            	resp.getWriter().println("facilities being created ... please wait");

            	Objectify ofy = ObjectifyService.begin();
            	List<Key<Building>> buildingKeys = ofy.query(Building.class).listKeys();
            	for(Key<Building> bk : buildingKeys)
            	{
            		for(String name : EntityConstants.FACILITY_TYPES)
            		{
            			Facility f = WriteDAO.createFacility(bk, name);
            			resp.getWriter().println("Created Facility: " + f.getKey());
            		}
            	}
            }
            else if(type.equals("staff"))
            {
            	HashMap<String, String> params = new HashMap<String, String>(EntityConstants.EMPLOYEE_NAMES.length);
            	for(String[] info : EntityConstants.EMPLOYEE_NAMES)
            	{
            		String email = info[2].toLowerCase();
            		String name = info[0] + " " + info[1];
            		params.put(email, name);
            	}
            	ApplicationParameters a = WriteDAO.createApplicationParameters(DTOConstants.APP_PARAM_EMPLOYEE_LIST, params, null);
            	params.clear();
            	params.put("temitope.shoga@fieldcolimited.com", "Granted admin privileges based on SSG role as portal administrators");
            	params.put("oluyemi.adeosun@fieldcolimited.com", "Granted admin privileges based on SSG role as portal administrators");
            	ApplicationParameters b = WriteDAO.createApplicationParameters(DTOConstants.APP_PARAM_ADMINS, params, DTOConstants.APP_PARAM_ADMIN_TYPES);
            	params.clear();
            	params.put("kayode.ogunbunmi@fieldcolimited.com", "Granted admin privileges based on need to manage resident billing/accounts info");
            	params.put("funmilola.oyerogba@fieldcolimited.com", "Granted admin privileges based on need to manage resident billing/accounts info");
            	ApplicationParameters c = WriteDAO.createApplicationParameters(DTOConstants.APP_PARAM_ACCTS, params, DTOConstants.APP_PARAM_ADMIN_TYPES);
            	params.clear();
            	params.put("osita.wakwe@fieldcolimited.com", "Granted admin privileges based on icestone need to manage access to vendor modules");
            	params.put("blessing.ejiogu@fieldcolimited.com", "Granted admin privileges based on icestone need to manage access to vendor modules");
            	ApplicationParameters d = WriteDAO.createApplicationParameters(DTOConstants.APP_PARAM_VENDORS, params, DTOConstants.APP_PARAM_ADMIN_TYPES);            	
            	resp.getWriter().println("Created " + a.getKey() + " and " + b.getKey() + " and " + d.getKey() + " and " + c.getKey());
            }
            else if(type.equals("addftbs"))
            {
            	Objectify ofy = ObjectifyService.begin();
            	ApplicationParameters a = ofy.get(ApplicationParameters.getKey(DTOConstants.APP_PARAM_ADMINS));
            	HashMap<String, String> params = a.getParams();
            	for(String id : FTBS)
            		params.put(id+"@fertiletech.com", "Temporary access granted to FTBS team for support purposes");
            	ofy.put(a);
            }
            else if(type.equals("minusftbs"))
            {
            	Objectify ofy = ObjectifyService.begin();
            	ApplicationParameters a = ofy.get(ApplicationParameters.getKey(DTOConstants.APP_PARAM_ADMINS));
            	HashMap<String, String> params = a.getParams();
            	for(String id : FTBS)
            		params.remove(id+"@fertiletech.com");
            	ofy.put(a);
            	
            }
            else if(type.equals("resacctypes"))
            {
            	ApplicationParameters a = WriteDAO.createApplicationParameters(DTOConstants.APP_PARAM_RES_ACCT_LIST_KEY, EntityConstants.ACCOUNT_MAPPINGS, null);
            	ApplicationParameters b = WriteDAO.createApplicationParameters(DTOConstants.APP_PARAM_RES_BILL_LIST_KEY, EntityConstants.BILL_MAPPINGS, null);

            	resp.getWriter().println("Created: " + a.getKey() + " and " + b.getKey());
            }
            else if(type.equals("resaccts"))
            {
            	Objectify ofy = ObjectifyService.begin();
            	QueryResultIterable<Key<Tenant>> tenantKeys = ofy.query(Tenant.class).fetchKeys();
            	String[] template = new String[EntityConstants.ACCOUNT_MAPPINGS.keySet().size()];
            	int count = 0;
            	for(Key<Tenant> tk : tenantKeys)
            	{
            		count++;
            		TaskQueueHelper.scheduleCreateResidentAccount(tk.getString(), EntityConstants.ACCOUNT_MAPPINGS.keySet().toArray(template), LoginHelper.getLoggedInUser(req));
            	}
            	resp.getWriter().println("scheduled creation of " + EntityConstants.ACCOUNT_MAPPINGS.size() + " accounts for " + count + "tenants");	
            }
            else if(type.equals("gtbacct"))
            {
            	String usr = LoginHelper.getLoggedInUser(req);
            	InetCompanyAccount a = InetDAO4Accounts.createAccount("GTBank PLC", "058152175", "FIELDCO LIMITED", "0009708018", "NGN", usr);
            	resp.getWriter().println("Created: " + a.getKey());
            }
            else if(type.equals("prodcat"))
            {
            	ApplicationParameters a = WriteDAO.createApplicationParameters(DTOConstants.APP_PARAM_PRODUCT_CATEGORY_KEY, EntityConstants.PROD_CAT_MAPPINGS, null);
            	resp.getWriter().println("Created: " + a.getKey());
            	
            }
        }
        catch(DuplicateEntitiesException ex)
        {
        	resp.getWriter().println("<font color = 'red'>Error occured<br/>" + ex.getMessage());
        	log.severe(ex.getMessage());
        	return;
        }        
        catch(MissingEntitiesException ex)
        {
        	resp.getWriter().println("<font color = 'red'>Error occured<br/>" + ex.getMessage());
        	log.severe(ex.getMessage());
        	return;
        }         
        resp.getWriter().println("<b>Patch completed, see log for details ...</b>");
 
    }
    
    
/*    public String createNotices() throws DuplicateEntitiesException
    {
    	StringBuilder result = new StringBuilder("<ul>");
    	for(String id : PanelServiceConstants.ROLE_NOTES)
    		result.append("<li>").append(InetDAO4Creation.createNotice(id, "Notice Board").getKey().getName()).append("</li>");
    	result.append("</ul>");
    	return result.toString();
    }
    
    public String createMessageController(HttpServletRequest req) throws DuplicateEntitiesException
    {
    	String fromAddress = NamespaceFilter.getNamespaceName(req) + InetConstants.APP_EMAIL_SUFFIX;
    	EmailController controller = MessagingDAO.createEmailController(InetConstants.EMAIL_CONTROLLER_ID, fromAddress, 5000, 100, 1000);
    	return "<p>Successfully Created: " + controller.getKey() + " --- " + controller.getFromAddress() + "</p>";
    }
    
    public String createMessagePDFControllers(HttpServletRequest req) throws DuplicateEntitiesException
    {
    	String result = "";
    	String fromRCAddress = "no-reply" + "." + NamespaceFilter.getNamespaceName(req) + InetConstants.APP_EMAIL_SUFFIX;
    	EmailReportCardController controller = MessagingDAO.createEmailReportCardController(InetConstants.EMAIL_RC_CONTROLLER_ID, fromRCAddress, 5000, 100, 1000);
    	result += "<p>Successfully Created: " + controller.getKey() + " --- " + controller.getFromAddress() + "</p>";
    	
    	fromRCAddress = "no-reply" + "." + NamespaceFilter.getNamespaceName(req) + InetConstants.APP_EMAIL_SUFFIX;
    	controller = MessagingDAO.createBillingController(InetConstants.EMAIL_BILL_CONTROLLER_ID, fromRCAddress, 5000, 100, 1000);
    	result += "<p>Successfully Created: " + controller.getKey() + " --- " + controller.getFromAddress() + "</p>";
    	return result;
    }
    
    public String createSMSController(HttpServletRequest req) throws DuplicateEntitiesException
    {
    	String fromAddress = NamespaceFilter.getNamespaceName(req) + " Alert";
    	SMSController controller = MessagingDAO.createSMSController(InetConstants.SMS_CONTROLLER_ID, 
    			fromAddress, 100, 100, 200, "smpp5.routesms.com", 8080, "ferti", "kf3fidlf", "1", "0");
    	return "<p>Successfully Created: " + controller.getKey() + " --- " + controller.getFromAddress() + "</p>";
    }
    */
}
