package com.fertiletech.fieldcodata.ui.shared;

import com.fertiletech.fieldcodata.ui.shared.dto.DTOConstants;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;

public final class UtilHelper {
	
	public final static String ARCHIVE_WARNING = "Warning, you've switched to archive view for a departed resident." +
			" Most likely you're violating a company policy by accessing this panel, obtain management approval " +
			"before proceeding.";
	
	public static String getPrintFriendlyContact(TableMessage m)
	{
		return getPrintFriendlyContact(m, "Review brief summary below before proceeding");	
	}
	
	public static String getPrintFriendlyContact(TableMessage m,String title)
	{
		FormHTMLDisplayBuilder bd = new FormHTMLDisplayBuilder();
		bd.formBegins();
		generatePrintFriendlyContact(bd, m, title);
		bd.formEnds();
		return bd.toString();
	}
	
	public static void generatePrintFriendlyContact(FormHTMLDisplayBuilder bd, TableMessage m, String title)
	{
		bd.headerBegins().appendTextBody("<br/>" + title).headerEnds();
		bd.lineBegins().appendTextBody("First Name",m.getText(DTOConstants.TNT_FNAME_IDX)).
			appendTextBody("Surname", m.getText(DTOConstants.TNT_LNAME_IDX)).lineEnds();
		bd.lineBegins().appendTextBody("Email", m.getText(DTOConstants.TNT_PRIMARY_EMAIL_IDX)).
		appendTextBody("Phone", m.getText(DTOConstants.TNT_PRIMARY_PHONE_IDX)).lineEnds();
	}
	
	public static String getDetailedPrintFriendlyContact(TableMessage m, String title, boolean includeBuilding)
	{
		FormHTMLDisplayBuilder bd = new FormHTMLDisplayBuilder();
		//bd.formBegins();
		if(includeBuilding)
			bd.lineBegins().appendTextBody("Building", m.getText(DTOConstants.TNT_BUILDING_IDX)).appendTextBody("Apartment", m.getText(DTOConstants.TNT_APT_IDX)).lineEnds();		
		generatePrintFriendlyContact(bd, m, title);
		bd.lineBegins().appendTextBody("Title", m.getText(DTOConstants.TNT_TITLE_IDX)).appendTextBody("Gender", m.getText(DTOConstants.TNT_GENDER_IDX)).lineEnds();	
		bd.appendTextBody("Company", m.getText(DTOConstants.TNT_COMPANY_IDX), true);
		bd.appendTextBody("Address", m.getText(DTOConstants.TNT_ADDRESS_IDX), true);		
		int start = (int) Math.round(m.getNumber(DTOConstants.TNT_OTHER_PHONE_START_IDX));
		int end = (int) Math.round(m.getNumber(DTOConstants.TNT_OTHER_PHONE_END_IDX));
		if(start >= 0)
			bd.lineBegins().labelBegins().appendTextBody("Other Numbers").labelEnds().appendTextBody(getMultiVals(start, end, m), ",").lineEnds();
		start = (int) Math.round(m.getNumber(DTOConstants.TNT_OTHER_EMAIL_START_IDX));
		end = (int) Math.round(m.getNumber(DTOConstants.TNT_OTHER_EMAIL_END_IDX));
		if(start >= 0)
			bd.lineBegins().labelBegins().appendTextBody("Other Emails").labelEnds().appendTextBody(getMultiVals(start, end, m), ",").lineEnds();		
		return bd.toString();
	}
	
	public static String[] getMultiVals(int start, int end, TableMessage m)
	{
		if(start < 0 || end < 0)
			return null;		
		String[] multiVals = new String[(end-start)+1];
		int index = 0;
		for(int i=start; i<=end; i++)
			multiVals[index++] = m.getText(i);
		return multiVals;
	}		
}

