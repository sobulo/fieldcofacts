/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fertiletech.fieldcodata.ui.shared.exceptions;


/**
 *
 * @author Segun Razaq Sobulo
 */
public class LoginValidationException extends Exception
{
    public LoginValidationException(){
    }

    public LoginValidationException(String s){
        super(s);
    }
}
