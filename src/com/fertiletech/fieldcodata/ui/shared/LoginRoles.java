/**
 * 
 */
package com.fertiletech.fieldcodata.ui.shared;

import java.io.Serializable;

/**
 * @author Segun Razaq Sobulo
 *
 */
public enum LoginRoles implements Serializable{
	    ROLE_SUPER_ADMIN, ROLE_FIELDCO_STAFF, ROLE_PUBLIC ;	
}
