/**
 * 
 */
package com.fertiletech.fieldcodata.ui.shared;

import java.io.Serializable;

/**
 * @author Segun Razaq Sobulo
 *
 */
public final class UtilConstants {
    public enum CustomMessageTypes implements Serializable{
       GENERIC_MESSAGE;
   }
    
    /*public static String[] BILL_TYPES = {
    									 "Energy (Nepa)", 
    									 "Energy (Diesel)", 
    									 "Adhoc Maintenance", 
    									 "Scheduled Maintenance",
    									 "Other Charges" 
    									};*/
    
    public enum ContactTypes implements Serializable {PROSPECT, TENANT, RELATED_CONTACT, COMPANY_REP};
    public final static int SAVE_CONTACT_KEY_IDX = 0;
    public final static int SAVE_CONTACT_VAL_IDX = 1;
}
