package com.fertiletech.fieldcodata.ui.shared.dto;

import java.util.HashMap;

public class FormConstants {
	
	
	//supplementary fields - client visit
	public final static String PD_REQUEST_DATE = "Est. Move In";	
	public final static String PD_TENANCY_START = "Lease Start";		
	public final static String PD_TENANCY_END = "Lease End"; 		
	public final static String PD_VISIT_DATE = "Visit Date";	
	public final static String PD_BUILDING_FEATURES = "Building Features";
	public final static String PD_PROP_INSPECTED = "Property Inspected"; 
	public final static String PD_YEARS = "Rent Period (years)";
	public final static String PD_BUDGET = "Budget(USD)";
	public final static String PD_PREF_NOTES = "Preference Notes";
	public final static String PD_STAFF_NAME = "Staff Name";

	//supplementary fields - documentation
	public final static String PD_OFFER_SENT = "Offer Sent";
	public final static String PD_OFFER_DATE = "Offer Date";
	public final static String PD_DRAFT_SENT = "Draft Sent";
	public final static String PD_DRAFT_DATE = "Draft Date";
	public final static String PD_DOCS_FOLDER = "Docs Folder";	
	public final static String PD_DOCS_NOTES = "Doc Notes";
	
	//supplementary fields - payment info
	public final static String PD_LEGAL_FEE = "Legal Fee";
	public final static String PD_LEGAL_DATE = "Legal Date";
	public final static String PD_LEGAL_CCY = "Legal Ccy";
	public final static String PD_RENT_FEE = "Rent Fee";
	public final static String PD_RENT_DATE = "Rent Date";
	public final static String PD_RENT_CCY = "Rent Ccy";
	public final static String PD_SERVICE_CHARGE = "Service Charge";
	public final static String PD_SERVICE_CHARGE_DATE = "Service Charge Date";	
	public final static String PD_SERVICE_CCY = "Service Charge Ccy";
	public final static String PD_DIESEL_FEE = "Energy Fee";
	public final static String PD_DIESEL_DATE = "Energy Date";
	public final static String PD_DIESEL_CCY = "Energy Ccy";
	public final static String PD_TRANSACTION_CONCL = "Transaction Concl.";
	public final static String PD_AGENCY_FEE = "Agency Fee";	
	public final static String PD_AGENCY_DATE = "Agency Date";
	public final static String PD_AGENCY_CCY = "Agency Ccy";
	public final static String PD_PAYMENT_NOTES = "Transaction Note";	
			
    //data validation constants
    public final static String REGEX_EMAIL = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$";
    public final static String REGEX_NUMS_ONLY = "^\\d{6,}$";
    public final static String REGEX_PHONE_REPLACE = "-|\\(|\\)";
    
    public final static HashMap<Integer, String> VENDOR_RATINGS = new HashMap<Integer, String>();
    
    static
    {
    	VENDOR_RATINGS.put(1, "Poor");
    	VENDOR_RATINGS.put(2, "Satisfactory");
    	VENDOR_RATINGS.put(3, "Good");
    	VENDOR_RATINGS.put(4, "Excellent");
    }    
}