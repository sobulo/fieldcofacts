/**
 * 
 */
package com.fertiletech.fieldcodata.ui.server.login;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import com.fertiletech.fieldcodata.backend.entities.ApplicationParameters;
import com.fertiletech.fieldcodata.ui.shared.LoginInfo;
import com.fertiletech.fieldcodata.ui.shared.LoginRoles;
import com.fertiletech.fieldcodata.ui.shared.dto.DTOConstants;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserServiceFactory;
import com.google.appengine.api.utils.SystemProperty;
import com.google.apphosting.api.ApiProxy;
import com.googlecode.objectify.ObjectifyService;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class LoginHelper {
	private static final Logger log = Logger.getLogger(LoginHelper.class.getName());
	final static String QUEUE_HEADER = "X-AppEngine-QueueName";
	final static String CRON_HEADER = "X-AppEngine-Cron";
	final static String TASK_USER_PREFIX = "AUTOMATED-";
	
    public static LoginInfo brokeredLogin(String requestUri)
    {
    	LoginInfo loginInfo = new LoginInfo();
        if (obtainedToken()) //logged in?
        {
        	//ok so passed the login check, now let's populate with additional information
        	
        	String domain = UserServiceFactory.getUserService().getCurrentUser().getAuthDomain();
        	//this will of course need to change as more providers enabled
        	LoginProvider provider = null;
        	provider = new GoogleLoginProvider();
        	
        	provider.populateLoginInformation(loginInfo);
        	loginInfo.setLoggedIn(true); //gwt client should check loginInfo.role as well
            loginInfo.setLogoutUrl(provider.getCheckoutUrl(requestUri));
            String msg = "Welcome " + loginInfo.getLoginID() + ", you are now logged into FIELDCO F.A.C.T.S portal";
            loginInfo.setMessage(msg);
        } 
        else 
        {
            loginInfo.setLoggedIn(false);
            final int NUM_OF_PROVIDERS = 1;
            LoginProvider[] providers = new LoginProvider[NUM_OF_PROVIDERS];
            String[] loginUrls = new String[NUM_OF_PROVIDERS];
            providers[LoginInfo.GOOGLE_PROVIDER_IDX] = new GoogleLoginProvider(); //idx value should be 0
            //providers[1] = new YahooLoginProvider();
            StringBuilder msg = new StringBuilder("Login with:<br><ul>");
            for(int i = 0; i <  NUM_OF_PROVIDERS; i++)
            {
            	if(i != LoginInfo.GOOGLE_PROVIDER_IDX) continue;
            	loginUrls[i] = providers[i].getCheckInUrl(requestUri);
            	String[] parts = requestUri.split("#");
            	if(parts.length == 2)
            	{
            		String targetUrl = loginUrls[LoginInfo.GOOGLE_PROVIDER_IDX];
            		if(targetUrl.contains(parts[0]) && !targetUrl.contains(parts[1]))
            		{
            			String hackedUrl = targetUrl.replace(parts[0], parts[0] + "?history=" + parts[1]);
            			loginUrls[LoginInfo.GOOGLE_PROVIDER_IDX] = hackedUrl;
            		}
            	}
            	msg.append("<li><a href='").append(loginUrls[i]).append("'> FIELDCO ")
            		.append(providers[i].getProviderName()).append(" account login</a>")
            		.append("</li>\n");
            }
            msg.append("</ul>");
            
            //to support multiple login types (google, facebook etc), this would need to set multiple urls
            loginInfo.setLoginUrl(loginUrls);
            loginInfo.setMessage(msg.toString());
        }
        return loginInfo;
    }
        
    @SuppressWarnings("serial")
	private static class GoogleLoginProvider implements LoginProvider, Serializable 
    {
    	protected String getOpenIdUrl()
    	{
    		return "https://www.google.com/accounts/o8/id";
    	}
    	
    	@Override
        public String getCheckoutUrl(String url) {
            return UserServiceFactory.getUserService().createLogoutURL(url, getProviderDomain(getProviderName()));
        }

        @Override
        public String getCheckInUrl(String url) {
            return UserServiceFactory.getUserService().createLoginURL(url, getProviderDomain(getProviderName()), 
            		getOpenIdUrl(), getOpenIdOptions(url));
        }

        @Override
        public String getProviderName() {
            return LoginInfo.GOOGLE_PROVIDER_NAME;
        }
        
        @Override
        public void populateLoginInformation(LoginInfo loginInfo)
        {
            User user = UserServiceFactory.getUserService().getCurrentUser();
            String email = user.getEmail().toLowerCase();
            loginInfo.setName(getProviderName());
            loginInfo.setLoginID(email);
            loginInfo.setLoginName(getUserName(email));
            loginInfo.storePrivilleges(getAccessFlags(email));
            loginInfo.setRole(getRole(email));
        }
    }
    
    private static String getProviderDomain(String providerName)
    {
    	return providerName.concat(".com");
    }    
    
    private static boolean obtainedToken() {
        return UserServiceFactory.getUserService().isUserLoggedIn();
    }

	private static Set<String> getOpenIdOptions(String url)
	{
		Set<String> attributesRequest = new HashSet<String>();
		attributesRequest.add("openid.mode=checkid_immediate");
		attributesRequest.add("openid.ns=http://specs.openid.net/auth/2.0");
		attributesRequest.add("openid.return_to=" + url);
		return attributesRequest;
	}
	
	private static boolean[] getAccessFlags(String email)
	{
		ApplicationParameters param = ObjectifyService.begin().get(ApplicationParameters.getKey(DTOConstants.APP_PARAM_ADMINS));
		HashMap<String, String> adminTable = param.getParams();
		boolean[] result = {false};
		if(adminTable.containsKey(email))
			result[0] = true;
		return result;
	}
    
	private static String getUserName(String email)
	{
		ApplicationParameters param = ObjectifyService.begin().get(ApplicationParameters.getKey(DTOConstants.APP_PARAM_EMPLOYEE_LIST));
		HashMap<String, String> staffTable = param.getParams();
		if(staffTable.containsKey(email))
			return staffTable.get(email);
		else
			return email;
	}
    private static LoginRoles getRole(String email)
    {
    	String domain = null;
    	LoginRoles role = null;
        if(SystemProperty.environment.value() == SystemProperty.Environment.Value.Production)
        {        	
        	//recommended way by Google for checking user's organization
	    	domain = (String) ApiProxy.getCurrentEnvironment().getAttributes().get("com.google.appengine.api.users.UserService.user_organization");
	    	if(domain.trim().equalsIgnoreCase(LoginConstants.COMPANY_DOMAIN))
	    		role = LoginRoles.ROLE_FIELDCO_STAFF;
	    	else if(domain.trim().equalsIgnoreCase(LoginConstants.FTBS_DOMAIN))
	    		role = LoginRoles.ROLE_SUPER_ADMIN;
	    	else
	    		role = LoginRoles.ROLE_PUBLIC;
        }
        
        //TODO looks like below is what gets used even for production
        if(domain == null || domain.trim().length() == 0) //except google's way stopped working recently
        {
        	//organization info not available in dev mode, so we use logic below
        	//note that this logic works even in prod but to be on the safe side we use
        	//recommended approach for prod above
            if (email.endsWith(LoginConstants.FTBS_DOMAIN))
            	return LoginRoles.ROLE_SUPER_ADMIN;
            else if(email.endsWith(LoginConstants.COMPANY_DOMAIN))
            	return LoginRoles.ROLE_FIELDCO_STAFF;
            else
            	return	LoginRoles.ROLE_PUBLIC;        	
        }
        else
        {
        	return role;
        }
    }
    
    public static String getLoggedInUser(HttpServletRequest req)
    { 	    	
    	if(!obtainedToken())
    	{
    		//check headers
    		String queueName = req.getHeader(QUEUE_HEADER);
    		if( queueName != null)
    			return TASK_USER_PREFIX + queueName;
    		throw new RuntimeException("Not Logged IN - SECURITY ALERT");
    	}
    	
    	//this only works if we're using openid or google accts. currently as of this writing
    	//we're on openid, in particular yahoo and google accts supported via openid
    	return UserServiceFactory.getUserService().getCurrentUser().getEmail().toLowerCase();
    }
    
    //interface provided in case there is a need to support other forms of login besides
    //google in the future (e.g. open id), for now it's just an overhead since
    //we only support google login
    static interface LoginProvider 
    {
        String getCheckoutUrl(String url);
        String getCheckInUrl(String url);
        String getProviderName();
        void populateLoginInformation(LoginInfo login);
    }
}
