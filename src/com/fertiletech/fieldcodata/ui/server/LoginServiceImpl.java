package com.fertiletech.fieldcodata.ui.server;


import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.fertiletech.fieldcodata.backend.comments.AcitivityComment;
import com.fertiletech.fieldcodata.backend.comments.CommentsDAO;
import com.fertiletech.fieldcodata.backend.comments.RatingComment;
import com.fertiletech.fieldcodata.backend.entities.ApplicationParameters;
import com.fertiletech.fieldcodata.backend.entities.WriteDAO;
import com.fertiletech.fieldcodata.scripts.pdfreports.GenericExcelDownload;
import com.fertiletech.fieldcodata.scripts.pdfreports.InetImageBlob;
import com.fertiletech.fieldcodata.ui.client.LoginService;
import com.fertiletech.fieldcodata.ui.client.gui.ActivityCommentCellList;
import com.fertiletech.fieldcodata.ui.server.login.LoginHelper;
import com.fertiletech.fieldcodata.ui.shared.FormHTMLDisplayBuilder;
import com.fertiletech.fieldcodata.ui.shared.LoginInfo;
import com.fertiletech.fieldcodata.ui.shared.dto.DTOConstants;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessageHeader;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessageHeader.TableMessageContent;
import com.fertiletech.fieldcodata.ui.shared.exceptions.MissingEntitiesException;
import com.google.appengine.api.datastore.Text;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class LoginServiceImpl extends RemoteServiceServlet implements
		LoginService {
	private static final Logger log = Logger.getLogger(LoginServiceImpl.class.getName());
	static
	{
		WriteDAO.registerClassesWithObjectify();
	}

	/* (non-Javadoc)
	 * @see com.fertiletech.addosser.client.GreetingService#login(java.lang.String)
	 */
	@Override
	public LoginInfo login(String requestUrl) {
		LoginInfo info = LoginHelper.brokeredLogin(requestUrl);
		return info;
	}

	/* (non-Javadoc)
	 * @see com.fertiletech.addosser.client.LoginService#loginForApplications(java.lang.String)
	 */
	@Override
	public LoginInfo loginForApplications(String requestUrl) {
		LoginInfo info = login(requestUrl);
		return null;
	}
	

	/* (non-Javadoc)
	 * @see com.fertiletech.addosser.client.LoanMktService#loadSaleLeadComments(java.lang.String)
	 */
	@Override
	public String loadActivityComments(String parentKey) {

		return loadActivityComments(parentKey, false);
	}
	
	private String loadActivityComments(String leadID, boolean respectShowPublic)
	{
		return loadActivityComments(new FormHTMLDisplayBuilder(), leadID, respectShowPublic);
	}
	
	private String loadActivityComments(FormHTMLDisplayBuilder result, String leadID, boolean respectShowPublic)
	{
		Key<? extends Object> key = ObjectifyService.factory().stringToKey(leadID);
		List<AcitivityComment> commentList = CommentsDAO.getActivityComments(ObjectifyService.begin(), key);
		result.formBegins();
		result.headerBegins().appendTextBody("<h3>Prior Comments</h3>").headerEnds();
		boolean commentFound = false;
		
		for(AcitivityComment c : commentList)
		{
			if(respectShowPublic && !c.isShowPublic())
				continue;
			commentFound = true;
			DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
			DateFormat tf = DateFormat.getTimeInstance(DateFormat.MEDIUM);
			result.sectionBegins();
			result.lineBegins().appendTextBody(c.getComment().getValue()).lineEnds();
			result.lineBeginsRight().appendTextBody("User", c.getUser()).appendTextBody("Date", df.format(c.getDateUpdated()) + " " + tf.format(c.getDateUpdated())).lineEndsRight();			
			result.blankLine();
			result.sectionEnds();
		}
		if(!commentFound)
			result.lineBegins().appendTextBody("No comments found").lineEnds();
		result.formEnds();
		return result.toString();
	}

	/* (non-Javadoc)
	 * @see com.fertiletech.addosser.client.LoanMktService#saveLoanComment(java.lang.String, java.lang.String)
	 */
	@Override
	public void saveActivityComment(String loanKeyStr, String text, boolean showPublic) {
		Key<? extends Object> leadKey = ObjectifyService.factory().stringToKey(loanKeyStr);
		CommentsDAO.createComment(new Text(text), showPublic, leadKey, LoginHelper.getLoggedInUser(getThreadLocalRequest()));
	}


	@Override
	public String loadRatings(String pk)
	{
		return loadRatings(pk, false);
	}
	
	public String loadRatings(String parentKey, boolean respectShowPublic) {
		Key<? extends Object> key = ObjectifyService.factory().stringToKey(parentKey);
		List<RatingComment> commentList = CommentsDAO.getRatings(ObjectifyService.begin(), key);
		Map<String, Double> calculatedAverages = RatingComment.getCalculatedAverage(commentList);
		FormHTMLDisplayBuilder result = new FormHTMLDisplayBuilder().formBegins();
		result.headerBegins().appendTextBody("<h3>Prior Ratings</h3>").headerEnds();
		boolean commentFound = false;
		for(RatingComment c : commentList)
		{
			if(respectShowPublic && !c.isShowPublic())
				continue;
			commentFound = true;
			DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
			DateFormat tf = DateFormat.getTimeInstance(DateFormat.MEDIUM);
			result.sectionBegins();
			result.lineBegins().appendTextBody("Rating", String.valueOf(c.getRating())).lineEnds();
			result.lineBegins().appendTextBody(c.getComment().getValue()).lineEnds();
			result.lineBeginsRight().appendTextBody("User", c.getUser()).appendTextBody("Date", df.format(c.getDateUpdated()) + " " + tf.format(c.getDateUpdated())).lineEndsRight();
			result.blankLine();
			result.sectionEnds();
		}
		
		if(calculatedAverages != null)
		{
			result.sectionBegins();
			result.headerBegins().appendTextBody("<h4>Average Rating</h4>").headerEnds();
			for(String k : calculatedAverages.keySet())
				result.lineBegins().appendTextBody(k, String.valueOf(calculatedAverages.get(k))).lineEnds();
			result.sectionEnds();
		}
		
		if(!commentFound)
			result.lineBegins().appendTextBody("No ratings found").lineEnds();
		
		result.formEnds();
		result.blankLine();
		
		return loadActivityComments(result, parentKey, respectShowPublic);
	}

	@Override
	public void saveRatings(String parentKey, int rating, String text,
			boolean showPublic) {
		Key<? extends Object> leadKey = ObjectifyService.factory().stringToKey(parentKey);
		CommentsDAO.createRating(rating, new Text(text), showPublic, leadKey, LoginHelper.getLoggedInUser(getThreadLocalRequest()));		
	}

	@Override
	public List<TableMessage> getApplicationParameter(String ID) {
		Key<ApplicationParameters> pk = ApplicationParameters.getKey(ID);
		ApplicationParameters appObj = ObjectifyService.begin().get(pk);
		HashMap<String, String> params = appObj.getParams();
		ArrayList<TableMessage> result = new ArrayList<TableMessage>();
		TableMessageHeader h = new TableMessageHeader(2);
		h.setText(0, "Unique Identifier", TableMessageContent.TEXT);
		h.setText(1, "Description", TableMessageContent.TEXT);
		h.setMessageId(appObj.getKey().getString());
		result.add(h);
		for(String s : params.keySet())
		{
			TableMessage m = new TableMessage(2, 0, 0);
			m.setText(0, s);
			m.setText(1, params.get(s));
			m.setMessageId(s);
			result.add(m);
		}
		Collections.sort(result);
		return result;
	}

	@Override
	public void saveApplicationParameter(String ID, HashMap<String, String> val) throws MissingEntitiesException {
		Key<ApplicationParameters> pk = ApplicationParameters.getKey(ID);
		String updateUser = LoginHelper.getLoggedInUser(getThreadLocalRequest());
		WriteDAO.updateApplicationParameters(updateUser, pk, val);
	}

	@Override
	public String fetchGenericExcelLink(List<TableMessage> data,
			TableMessageHeader header) {
		//log.severe("RECEIVED EXCEL REQUEST");
		return GenericExcelDownload.getGenericExcelDownloadLink(data, header, getThreadLocalRequest());
	}

	@Override
	public List<TableMessage> getRecentActivityComments() {
		Objectify ofy = ObjectifyService.begin();
		List<AcitivityComment> comments = ofy.query(AcitivityComment.class).order("-dateUpdated").limit(100).list();
		List<TableMessage> result = new ArrayList<TableMessage>();
		for(AcitivityComment c : comments)
		{
			TableMessage m = new TableMessage(2, 0, 1);
			m.setMessageId(c.getKey().getString());
			Text descriptiveComment = c.getComment();
			m.setText(0, c.getUser());
			m.setText(1, descriptiveComment == null?"Oops, no comment found, contact IT":descriptiveComment.getValue());
			//log.warning(descriptiveComment.toString());
			m.setDate(0, c.getDateUpdated());
			result.add(m);
		}
		return result;
	}

	@Override
	public String getImageBlobID(String name) {
		return InetImageBlob.getKey(DTOConstants.PDF_REPORT_LOGO).getString();
	}	
}
