/**
 * 
 */
package com.fertiletech.fieldcodata.ui.server;


import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.fertiletech.fieldcodata.backend.accounting.InetBill;
import com.fertiletech.fieldcodata.backend.accounting.InetCompanyAccount;
import com.fertiletech.fieldcodata.backend.accounting.InetDAO4Accounts;
import com.fertiletech.fieldcodata.backend.accounting.InetDeposit;
import com.fertiletech.fieldcodata.backend.accounting.InetResidentAccount;
import com.fertiletech.fieldcodata.backend.accounting.InetWithdrawal;
import com.fertiletech.fieldcodata.backend.accounting.LedgerEntry;
import com.fertiletech.fieldcodata.backend.entities.Apartment;
import com.fertiletech.fieldcodata.backend.entities.Building;
import com.fertiletech.fieldcodata.backend.entities.Contact;
import com.fertiletech.fieldcodata.backend.entities.ReadDAO;
import com.fertiletech.fieldcodata.backend.entities.Representative;
import com.fertiletech.fieldcodata.backend.entities.Tenant;
import com.fertiletech.fieldcodata.backend.entities.WriteDAO;
import com.fertiletech.fieldcodata.backend.tickets.RequestTicket;
import com.fertiletech.fieldcodata.scripts.pdfreports.BillingInvoiceGenerator;
import com.fertiletech.fieldcodata.scripts.tasks.TaskQueueHelper;
import com.fertiletech.fieldcodata.ui.client.AccountManager;
import com.fertiletech.fieldcodata.ui.server.login.LoginHelper;
import com.fertiletech.fieldcodata.ui.shared.dto.BillDescriptionItem;
import com.fertiletech.fieldcodata.ui.shared.dto.DTOConstants;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessageHeader;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessageHeader.TableMessageContent;
import com.fertiletech.fieldcodata.ui.shared.exceptions.DuplicateEntitiesException;
import com.fertiletech.fieldcodata.ui.shared.exceptions.LoginValidationException;
import com.fertiletech.fieldcodata.ui.shared.exceptions.ManualVerificationException;
import com.fertiletech.fieldcodata.ui.shared.exceptions.MissingEntitiesException;
import com.google.appengine.api.datastore.QueryResultIterable;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class AccountManagerImpl extends RemoteServiceServlet implements AccountManager{

	private static final Logger log = Logger.getLogger(AccountManagerImpl.class.getName());
	static
	{
		WriteDAO.registerClassesWithObjectify();
	}	
	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.AccountManager#createUserBill(java.lang.String, java.lang.String[])
	 */
	@Override
	public String createUserBill(String[] users,
			LinkedHashSet<BillDescriptionItem> bdiList, String type,
			Date start, Date end, Date due, String ticketID, String ccy) throws DuplicateEntitiesException {	
    	//PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
    	String userAuditId = "";//LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());		
		StringBuilder failures = new StringBuilder();
		int count = 0;
		Key<RequestTicket> ticketKey = null;
		if(ticketID != null)
			ticketKey = ObjectifyService.factory().stringToKey(ticketID);
		Key<Tenant> tenantKey = ObjectifyService.factory().stringToKey(users[0]);
		log.warning("creating first bill: " + tenantKey);
		InetBill b = InetDAO4Accounts.createBill(tenantKey, ticketKey, bdiList, type, start, end, due, ccy);
		count++;
		
		//schedule creation of remaining bills from first bill created
		String billTemplate = ObjectifyService.factory().keyToString(b.getKey());
		for(int i = 1; i < users.length; i++)
		{
			String student = users[i];
			log.warning("scheduling bill creation for: " + student);
			try
			{
				TaskQueueHelper.scheduleCreateUserBill(billTemplate, student);
				count++;
			}
			catch(RuntimeException ex)
			{
				log.warning("Error occured: " + ex.getMessage());
				failures.append(student).append(",");
			}
		}
		StringBuilder result = new StringBuilder("Bill creation scheduled succesfully for ").append(count).append(" tenant(s).");
		if(count != users.length)
			result.append(" Please retry for the following users as failures occured: ").append(failures);
		log.warning("BILL CREATION SCHEDULED for: " + users.length + " students --by user " + userAuditId);
		return result.toString();
	}
	
	private List<TableMessage> getBillListSummary(List<InetBill> billList)
	{
		try
		{
			return getBillListSummary(billList, false, true, null);
		}
		catch(MissingEntitiesException ex)
		{
			throw new RuntimeException("Unexpected error: " + ex.getMessage());
		}
	}
	
	public static TableMessageHeader getBillSummaryHeaderForBlotter(boolean includeBillBio)
	{
		int numOfFields = includeBillBio? 11 : 8; 
		TableMessageHeader result = new TableMessageHeader(numOfFields);
		result.setText(3, "Total Due", TableMessageContent.NUMBER);
		result.setText(0, "Apartment ID", TableMessageContent.TEXT);
		result.setText(1, "Last Name", TableMessageContent.TEXT);
		result.setText(2, "First Name", TableMessageContent.TEXT);
		result.setText(4, "Amount Paid", TableMessageContent.NUMBER);
		result.setText(5, "Invoice ID", TableMessageContent.NUMBER);
		result.setFormatOption(5, "###");
		result.setText(6, "Date", TableMessageContent.DATE);
		result.setText(7, "Due Date", TableMessageContent.DATE);
		if(includeBillBio)
		{
			result.setText(8, "Title", TableMessageContent.TEXT);
			result.setText(9, "Category", TableMessageContent.TEXT);
			result.setText(10, "Currency", TableMessageContent.TEXT);		
		}
		
		return result;
	}
	
	private static TableMessage getBillSummaryMessage(InetBill bill, boolean includeBillBio, boolean includeStudentBio, String[] studFields)
	{
		int numOfTextFields = 1; int textFieldCursor = 0;
		if(includeBillBio) numOfTextFields += 3;
		if(includeStudentBio) numOfTextFields += 3;
		TableMessage billInfo = new TableMessage(numOfTextFields, 4, 2);
		billInfo.setMessageId(bill.getKey().getString());
		
		if(includeStudentBio)
		{
			billInfo.setText(textFieldCursor++, studFields[0]);
			billInfo.setText(textFieldCursor++, studFields[1]);
			billInfo.setText(textFieldCursor++, studFields[2]);			
		}
		
		if(includeBillBio)
		{
			billInfo.setText(textFieldCursor++, bill.getFormatedTitle());
			billInfo.setText(textFieldCursor++, bill.getTitle());
			billInfo.setText(textFieldCursor++, bill.getCurrency());
		}
		
		billInfo.setText(textFieldCursor++, bill.isSettled()? "True" : "False");					
		billInfo.setNumber(0, bill.getTotalAmount());
		billInfo.setNumber(1, bill.getSettledAmount());
		billInfo.setNumber(2, bill.getInvoiceID());
		billInfo.setNumber(3, bill.getTicketKey() == null? null : bill.getTicketKey().getId());
		billInfo.setDate(0, bill.getCreateDate());
		billInfo.setDate(1, bill.getDueDate());
		return billInfo;
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.AccountManager#getStudentBills(java.lang.String)
	 */
	@Override
	public List<TableMessage> getStudentBills(String studentID) throws LoginValidationException {
    	//PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
    	//LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());
		Objectify ofy = ObjectifyService.begin();
		Key<Tenant> tenantKey = ofy.getFactory().stringToKey(studentID);
		List<InetBill> billInfoList = InetDAO4Accounts.getStudentBills(tenantKey, ofy);
		return getBillListSummary(billInfoList);
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.AccountManager#savePayment(java.lang.String, double, java.util.Date, java.lang.String, java.lang.String)
	 */
	@Override
	public String savePayment(String billKey, String acctKeyStr, double amount, Date payDate, String comments) 
			throws MissingEntitiesException, ManualVerificationException
	{
    	//PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
    	String userAuditId = "";//LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());
    	Key<InetResidentAccount> depKey = ObjectifyService.factory().stringToKey(acctKeyStr);
    	Key<InetBill> bk = ObjectifyService.factory().stringToKey(billKey);
    	String user = LoginHelper.getLoggedInUser(getThreadLocalRequest());
		InetWithdrawal payment = InetDAO4Accounts.createPayment(bk, depKey, amount, comments, payDate, user);
		log.warning("CREATED: " + payment.getKey() + " payment --by user " + userAuditId);
		String msg = new StringBuilder("Payment ID: ").append(payment.getKey().getId()).append( " Amount: ").
			append(payment.getAmount()).append(" date: ").append(payment.getEffectiveDate()).append( " Reference ID: ").append(payment.getReferenceId()).
			append(" Deducted from Account: ").append(String.valueOf(payment.getKey().getParent().getName())).toString();
		String[] autoComments = {msg, comments};
		TaskQueueHelper.scheduleCreateComment(autoComments, InetResidentAccount.getResident(depKey, false).getString(), user);
		return msg;
	}
	
	public static List<TableMessage> getBillDescriptionInfo(Key<InetBill> bdKey) throws MissingEntitiesException
	{
		Objectify ofy = ObjectifyService.begin();
		InetBill bill = ofy.find(bdKey);
		if(bill == null)
		{
			String msgFmt = "Unable to find bill description: ";
			log.severe(msgFmt + bdKey);
			throw new MissingEntitiesException(msgFmt + bdKey.getName());
		}
		return getBillDescriptionInfo(bill);
	}
	
	public static List<TableMessage> getBillDescriptionInfo(InetBill bill)
	{
		HashSet<BillDescriptionItem> itemizedBill = bill.getItemizedBill();
		List<TableMessage> result = new ArrayList<TableMessage>(2 + itemizedBill.size());
		//setup summary info
		TableMessage summary = new TableMessage(2, 1, 4);
		summary.setText(0, bill.getTitle());
		summary.setText(1, bill.getCurrency());
		summary.setNumber(0, bill.getTotalAmount());
		summary.setDate(0, bill.getDueDate());
		summary.setDate(1, bill.getServicePeriodStartDate());
		summary.setDate(2, bill.getServicePeriodEndDate());
		summary.setDate(3, bill.getCreateDate());
		result.add(summary);
		
		//setup header for itemized bill
		TableMessageHeader itemizedHeader = new TableMessageHeader(3);
		itemizedHeader.setText(0, "Type", TableMessageContent.TEXT);
		itemizedHeader.setText(1, "Description", TableMessageContent.TEXT);
		itemizedHeader.setText(2, "Amount", TableMessageContent.NUMBER);
		result.add(itemizedHeader);
		
		for(BillDescriptionItem item : itemizedBill)
		{
			TableMessage itemRow = new TableMessage(2, 1, 0);
			itemRow.setText(0, item.getName());
			itemRow.setText(1, item.getComments());
			itemRow.setNumber(0, item.getAmount());
			result.add(itemRow);
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.AccountManager#getStudentBillKeys(java.lang.String)
	 */
	@Override
	public HashMap<String, String> getStudentBillKeys(String studentID, boolean isLogin) throws LoginValidationException {
    	//PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN, PanelServiceLoginRoles.ROLE_GUARDIAN};
    	//LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());
    	Key<Tenant> studKey = null;
    	if(isLogin)
    		studKey = new Key<Tenant>(Tenant.class, studentID);
    	else
    		studKey = ObjectifyService.begin().getFactory().stringToKey(studentID);
    	return getStudentBillKeys(studKey);
	}
	
	private static HashMap<String, String> getStudentBillKeys(Key<Tenant> studKey)
	{
		return InetDAO4Accounts.getStudentBillKeys( studKey, ObjectifyService.begin());
	}
	
	private List<TableMessage> getBillPayments(List<InetWithdrawal> payments, String headerStr)
	{
		List<TableMessage> result = new ArrayList<TableMessage>(1 + payments.size());
		//setup header message
		TableMessageHeader header = new TableMessageHeader(5);
		header.setText(0, "Payment ID", TableMessageContent.TEXT);
		header.setText(1, "Payment Amount", TableMessageContent.NUMBER);
		header.setText(2, "Payment Date", TableMessageContent.DATE);
		header.setText(3, "Entry Date", TableMessageContent.DATE);
		header.setText(4, "Comments", TableMessageContent.TEXT);
		header.setMessageId(headerStr);
		result.add(header);
		
		for(InetWithdrawal payInfo : payments)
		{
			TableMessage m = new TableMessage(2, 1, 2);
			m.setText(0, Long.toString(payInfo.getKey().getId()));
			m.setText(1, payInfo.getDescription());
			m.setNumber(0, payInfo.getAmount());
			m.setDate(0, payInfo.getEffectiveDate());
			m.setDate(1, payInfo.getEntryDate());
			result.add(m);
		}
		return result;
	}
	
	private List<TableMessage> getBillPayments(Key<InetBill> billKey, Objectify ofy)
	{
		List<InetWithdrawal> payments = InetDAO4Accounts.getPayments(billKey, ofy);
		return getBillPayments(payments, billKey.getString());
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.AccountManager#getStudentBillAndPayment(java.lang.String)
	 */
	@Override
	public List<TableMessage> getStudentBillAndPayment(String billKeyStr) throws MissingEntitiesException, LoginValidationException
	{
    	//PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN, PanelServiceLoginRoles.ROLE_GUARDIAN, PanelServiceLoginRoles.ROLE_STUDENT};
    	//LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());		
		Objectify ofy = ObjectifyService.begin();
		Key<InetBill> billKey = ofy.getFactory().stringToKey(billKeyStr);
		InetBill studBill = ofy.find(billKey);
		
		if(studBill == null)
		{
			String msgFmt = "Unable to find student bill: ";
			log.severe(msgFmt + billKey);
			throw new MissingEntitiesException(msgFmt + billKey.getName());
		}
		List<TableMessage> result = new ArrayList<TableMessage>();
		
		result.add(getBillSummaryMessage(studBill, false, false, null)); //add student bill
		result.addAll(getBillDescriptionInfo(studBill.getKey())); //add bill description
		result.addAll(getBillPayments(billKey, ofy));
		
		return result;
	}

	
	public static List<TableMessage> getBillListSummary(Collection<InetBill> bills, boolean includeStudentBio, 
			boolean includeBillBio, Objectify ofy) throws MissingEntitiesException
	{
		ArrayList<TableMessage> result = new ArrayList<TableMessage>(bills.size());
		if(bills.size() == 0) return result;
		Map<Key<Tenant>,Tenant> studentMap = null;
		String[] studFields = null;
		
		if(includeStudentBio)
		{
			HashSet<Key<Tenant>> studentKeys = new HashSet<Key<Tenant>>(bills.size());
			for(InetBill b : bills)
				studentKeys.add((Key<Tenant>) b.getUserKey());
			studentMap = ReadDAO.getEntities(studentKeys, ofy, true);
			studFields = new String[3];
		}
		
		for(InetBill b : bills)
		{
			if(includeStudentBio)
			{
				Tenant billedStudent = studentMap.get(b.getUserKey());
				studFields[0] = billedStudent.getApartmentUnit().getParent().getName() + "/" + 
						billedStudent.getApartmentUnit().getName();
				studFields[1] = billedStudent.getLastName();
				studFields[2] = billedStudent.getFirstName();
			}
			TableMessage m = getBillSummaryMessage(b, includeBillBio, includeStudentBio, studFields);
			result.add(m);
		}
		return result;		
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.AccountManager#getBillPayments(java.lang.String)
	 */
	@Override
	public List<TableMessage> getBillPayments(String billKeyStr)
			throws MissingEntitiesException, LoginValidationException 
	{
    	//PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
    	//LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());				
		Key<InetBill> billKey = ObjectifyService.factory().stringToKey(billKeyStr);
		return getBillPayments(billKey, ObjectifyService.begin());
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.AccountManager#getInvoiceDownloadLink(java.lang.String, java.lang.String[])
	 */
	@Override
	public String getInvoiceDownloadLink(String[] billKeyStrs) throws MissingEntitiesException, LoginValidationException {
    	//PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
    	//LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());
		return BillingInvoiceGenerator.getBillingInvoicesLink(billKeyStrs, getThreadLocalRequest());
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.AccountManager#getStudentBillKeys()
	 */
	@Override
	public HashMap<String, String> getStudentBillKeys()
			throws LoginValidationException {
    	//PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_STUDENT};
    	//String userID = LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());
    	//return getStudentBillKeys(new Key<Tenant>(Tenant.class, userID));
		//TODO provide implementation
		return null;
	}

	@Override
	public List<TableMessage> getBills(Date startDate, Date endDate) throws MissingEntitiesException {
		Objectify ofy = ObjectifyService.begin();
		List<InetBill> bills = InetDAO4Accounts.getAllBillsByDate(startDate, endDate, ofy);
		List<TableMessage> result = new ArrayList<TableMessage>(bills.size() + 1);
		result.add(getBillSummaryHeaderForBlotter(true));
		result.addAll(getBillListSummary(bills, true, true, ofy));
		return result;
	}

	@Override
	public List<TableMessage> getDeposits(Date startDate, Date endDate) 
	{
		Objectify ofy = ObjectifyService.begin();
		return getLedgerDTO(InetDAO4Accounts.getDeposits(startDate, endDate, ofy), null, true, InetDAO4Accounts.getCompanyAcctsMap(ofy));
	}
	
	public static List<TableMessage> getLedgerDTO(List<? extends LedgerEntry> deposits, String headerStr, boolean includeAcctBio, Map<Key<InetCompanyAccount>, InetCompanyAccount> bankAccts)
	{
		List<TableMessage> result = new ArrayList<TableMessage>(1 + deposits.size());
		//setup header message
		int numOfCols = includeAcctBio ? 12 : 9;
		TableMessageHeader header = new TableMessageHeader(numOfCols);
		header.setText(0, "No.", TableMessageContent.NUMBER);
		header.setFormatOption(0,"###");		
		header.setText(1, "Description", TableMessageContent.TEXT);
		header.setText(2, "Ref.", TableMessageContent.TEXT);		
		header.setText(3, "Type", TableMessageContent.TEXT);			
		header.setText(4, "Amount", TableMessageContent.NUMBER);
		header.setText(5, "Account Balance", TableMessageContent.NUMBER);		
		header.setText(6, "Value Date", TableMessageContent.DATE);
		header.setText(7, "Entry Date", TableMessageContent.DATE);
		header.setText(8, "Entry User", TableMessageContent.TEXT);		
		if(includeAcctBio)
		{
			header.setText(9, "Bank", TableMessageContent.TEXT);
			header.setText(10, "Building", TableMessageContent.TEXT);
			header.setText(11, "Apt", TableMessageContent.TEXT);
		}
		
		header.setMessageId(headerStr);
		result.add(header);

		Collections.sort(deposits, InetResidentAccount.getComparator());
		for(LedgerEntry dep : deposits)
		{
			TableMessage m = new TableMessage(4 + (includeAcctBio?3:0), 3, 2);
			m.setMessageId(dep.getKey().getString());
			m.setText(0, dep.getDescription());
			m.setText(1, dep.getReferenceId());	
			m.setText(2, dep.isCredit()? "Credit" : "Debit");
			m.setNumber(0, dep.getEntryCount());
			m.setNumber(1, dep.getAmount());
			m.setNumber(2, dep.getAccountBalance());
			
			m.setDate(0, dep.getEffectiveDate());
			m.setDate(1, dep.getEntryDate());	
			m.setText(3, dep.getEntryUser());			
			if(includeAcctBio)
			{
				Key<InetResidentAccount> accountKey = dep.getKey().getParent();
				log.warning("ACCT KEY: " + accountKey);
				String companyAcctName = "";
				if(dep instanceof InetDeposit)
				{	
					InetDeposit deposit= (InetDeposit) dep;
					InetCompanyAccount ba = bankAccts.get(deposit.getCompanyAccount());
					int numLength = ba.getAccountNumber().length();
					companyAcctName = ba.getBank().substring(0, Math.min(ba.getBank().length(), 6)) + ".." + ba.getAccountNumber().substring(numLength-4, numLength);
				}
				Key<Tenant> tenantKey = InetResidentAccount.getResident(accountKey, false);
				Key<Apartment> aptKey = tenantKey.getParent();
				m.setText(4, companyAcctName);
				m.setText(5, aptKey.getParent().getName());
				m.setText(6, aptKey.getName());
			}
			result.add(m);
		}
		return result;		
	}
	

	@Override
	public List<TableMessage> getDeposits(String tenantID) {
		Objectify ofy = ObjectifyService.begin();
		Key<Tenant> tk = ofy.getFactory().stringToKey(tenantID);
		return getLedgerDTO(InetDAO4Accounts.getDeposits(tk, ofy), tenantID, true, InetDAO4Accounts.getCompanyAcctsMap(ofy));	
	}

	@Override
	public String createDeposit(String companyAcctStr, Date depositDate,
			String referenceNumber, String comments, String accountStr, double initialAmount) {
		log.warning("Received key: " + companyAcctStr);
		String userId = LoginHelper.getLoggedInUser(getThreadLocalRequest());
		Key<InetCompanyAccount> acctKey = ObjectifyService.factory().stringToKey(companyAcctStr);
		Key<InetResidentAccount> accountKey = ObjectifyService.factory().stringToKey(accountStr);
		InetDeposit deposit = InetDAO4Accounts.createDeposit(acctKey, depositDate, referenceNumber, 
				comments, accountKey, initialAmount, userId);
		return "Succesfully created deposit with amount of " + NumberFormat.getNumberInstance().format(deposit.getAmount());
	}

	@Override
	public TableMessage createAccount(String bank, String sortCode, String acctName, String acctNumber, String currency) throws DuplicateEntitiesException {
		String user = LoginHelper.getLoggedInUser(getThreadLocalRequest());
		InetCompanyAccount acct = InetDAO4Accounts.createAccount(bank, sortCode, acctName, acctNumber, currency, user);
		return getCompanyAccountDTO(acct);
	}

	@Override
	public List<TableMessage> getAllAccounts() {
		List<InetCompanyAccount> accounts = InetDAO4Accounts.getAllAccounts(ObjectifyService.begin());
		List<TableMessage> result = new ArrayList<TableMessage>(accounts.size());
		
		TableMessageHeader h = new TableMessageHeader(5);
		
		h.setText(DTOConstants.CMP_ACCT_BNK_IDX, "Bank", TableMessageContent.TEXT);
		h.setText(DTOConstants.CMP_ACCT_NAME_IDX, "Account Name", TableMessageContent.TEXT);
		h.setText(DTOConstants.CMP_ACCT_NUM_IDX, "Account Number", TableMessageContent.TEXT);
		h.setText(DTOConstants.CMP_ACCT_SRT_IDX, "Sort Code", TableMessageContent.TEXT);
		h.setText(DTOConstants.CMP_ACCT_CURR_IDX, "Currency", TableMessageContent.TEXT);

		result.add(h);
		for(InetCompanyAccount a : accounts)
		{
			TableMessage m = getCompanyAccountDTO(a);
			result.add(m);
		}
		return result;
		
	}
	
	public static TableMessage getCompanyAccountDTO(InetCompanyAccount a)
	{
		TableMessage m = new TableMessage(5,0,0);
		m.setText(DTOConstants.CMP_ACCT_BNK_IDX, a.getBank());
		m.setText(DTOConstants.CMP_ACCT_NAME_IDX, a.getAccountName());
		m.setText(DTOConstants.CMP_ACCT_NUM_IDX, a.getAccountNumber());
		m.setText(DTOConstants.CMP_ACCT_SRT_IDX, a.getSortCode());
		m.setText(DTOConstants.CMP_ACCT_CURR_IDX, a.getCurrency());
		m.setMessageId(a.getKey().getString());
		return m;		
	}

	@Override
	public HashMap<Long, String> getAllBillIDs() {
		HashMap<Long, String> result = new HashMap<Long, String>();
		QueryResultIterable<InetBill> bills = ObjectifyService.begin().query(InetBill.class).fetch();
		for(InetBill b : bills)
			result.put(b.getInvoiceID(), b.getKey().getString());
		
		return result;
	}

	@Override
	public String getDepositStatementDownloadLink(String depOwner, String[] depositIDs, Date start, Date end) throws MissingEntitiesException {
		return BillingInvoiceGenerator.getDepositStatementLink(depOwner, depositIDs, start, end, getThreadLocalRequest());
	}

	@Override
	public TableMessage createCompanyRep(String firstName,
			String lastName, Date dob, String primaryEmail,
			String primaryNumber, HashSet<String> secondaryNumbers,
			HashSet<String> secondaryEmails, String companyName,
			String address, Boolean isMale, String title)
			throws DuplicateEntitiesException {
		Representative rep = WriteDAO.createRepresentative(firstName, lastName, dob, primaryEmail, primaryNumber, secondaryNumbers, secondaryEmails, companyName, address, isMale, title, false);
		return TenantDAOManagerImpl.getContactDTO(rep, null);
	}

	@Override
	public List<TableMessage> getAllReps() {
		return InetDAO4Accounts.getAllReps();
	}

	@Override
	public List<TableMessage> getCompanyTenants(String repID) {
		Key<Representative> repKey = ObjectifyService.factory().stringToKey(repID);
		return InetDAO4Accounts.getCompanyTenants(repKey, ObjectifyService.begin());
	}

	@Override
	public List<TableMessage> saveCompanyTenants(String[] tenantIDs, String repID) {
		Key<Representative> repKey = ObjectifyService.factory().stringToKey(repID);
		HashSet<Key<Tenant>> tenantKeys = new HashSet<Key<Tenant>>();
		for(String tid : tenantIDs)
		{
			Key<Tenant> tk = ObjectifyService.factory().stringToKey(tid);
			tenantKeys.add(tk);
		}
		List<Tenant> updatedObjects = WriteDAO.updateRepresentative(tenantKeys, repKey);
		StringBuilder sb = new StringBuilder("<b>Company/Billing representative added for the following tenants</b><ul>");
		for(Tenant t : updatedObjects)
		{
			sb.append("<li>").append(t.getFirstName()).append(" ").append(t.getLastName()).append(" @ Apt: ").append(t.getApartmentUnit().getParent().getName()).append("/" ).append(t.getApartmentUnit().getName());
			sb.append(" Company: ").append(t.getCompanyName()).append("</li>");
		}
		sb.append("</ul>");
		List<TableMessage> result = InetDAO4Accounts.getCompanyTenants(repKey, ObjectifyService.begin());
		TableMessageHeader h = (TableMessageHeader) result.get(0);
		h.setCaption(sb.toString());
		return result;
	}

	@Override
	public List<TableMessage> removeCompanyTenants(String[] tenantIDs, String repID) 
		{
		Key<Representative> repKey = ObjectifyService.factory().stringToKey(repID);
		HashSet<Key<Tenant>> tenantKeys = new HashSet<Key<Tenant>>();
		for(String tid : tenantIDs)
		{
			Key<Tenant> tk = ObjectifyService.factory().stringToKey(tid);
			tenantKeys.add(tk);
		}
		List<Tenant> updatedObjects = WriteDAO.removeRepresentative(tenantKeys, repKey);		
		StringBuilder sb = new StringBuilder("<b>Company/Billing removed for the following tenants</b><ul>");
		for(Tenant t : updatedObjects)
		{
			sb.append("<li>").append(t.getFirstName()).append(" ").append(t.getLastName()).append(" @ Apt: ").append(t.getApartmentUnit().getParent().getName()).append("/" ).append(t.getApartmentUnit().getName());
			sb.append(" Company: ").append(t.getCompanyName()).append("</li>");
		}
		sb.append("</ul>");
		List<TableMessage> result = InetDAO4Accounts.getCompanyTenants(repKey, ObjectifyService.begin());
		TableMessageHeader h = (TableMessageHeader) result.get(0);
		h.setCaption(sb.toString());
		return result;
	}

	@Override
	public TableMessage createResidentAccount(String tenantID, String acctType, boolean allowOverDraft, String currency, int depositWarning) throws DuplicateEntitiesException {
		Key<Tenant> tk = ObjectifyService.factory().stringToKey(tenantID);
		String[] acctTypes = {acctType};
		List<InetResidentAccount> ra =  InetDAO4Accounts.createResidentAccount(tk, acctTypes, LoginHelper.getLoggedInUser(getThreadLocalRequest()), allowOverDraft, currency, depositWarning);
		return InetDAO4Accounts.getResidentAcctDTO(ra.get(0));
	}

	@Override
	public TableMessage getResidentAccountInfo(String tenantID, String acctType, String ccy) {
		Key<Tenant> tk = ObjectifyService.factory().stringToKey(tenantID);
		Key<InetResidentAccount> raKey = InetResidentAccount.getKey(tk, acctType, ccy);
		Objectify ofy = ObjectifyService.begin();
		InetResidentAccount ra = ofy.find(raKey);
		if(ra == null) return null;
		return InetDAO4Accounts.getResidentAcctDTO(ra);
	}

	@Override
	public List<TableMessage> getResidentAccountInfo(String tenantID) {
		Objectify ofy = ObjectifyService.begin();
		Key<? extends Contact> tenantKey = ObjectifyService.factory().stringToKey(tenantID);
		HashSet<Key<Tenant>> tkList = new HashSet<Key<Tenant>>();
		String headerId = "";
		if(tenantKey.getParent() == null)
		{
			List<Key<Tenant>> companyTenants = ReadDAO.getCompanyTenants((Key<Representative>) tenantKey, ofy);
			tkList.addAll(companyTenants);
		}
		else 
		{
			headerId = tenantKey.getString();
			tkList.add((Key<Tenant>) tenantKey);
		}
		return getAccountInfoForResident(tkList, ofy, headerId);
	}
	
	public static List<TableMessage> getAccountInfoForResident(HashSet<Key<Tenant>> tkList, Objectify ofy, String headerID)
	{
		List<TableMessage> result = new ArrayList<TableMessage>();
		TableMessageHeader h = InetDAO4Accounts.getResidentAcctDTOHeader();
		h.setMessageId(headerID);
		result.add(h);		
		for(Key<Tenant> tenantKey : tkList)
		{
			Key<InetResidentAccount> raParentKey = InetResidentAccount.getParentAccountKey(tenantKey);
			List<InetResidentAccount> accts = ofy.query(InetResidentAccount.class).ancestor(raParentKey).list();
			for(InetResidentAccount a : accts)
				result.add(InetDAO4Accounts.getResidentAcctDTO(a));
		}
		return result;
	}

	@Override
	public List<TableMessage> getLedgerAccountEntries(String acctID,
			Date startDate, Date endDate) {
		Key<InetResidentAccount> raKey = ObjectifyService.factory().stringToKey(acctID);
		Objectify ofy = ObjectifyService.begin();
		List<LedgerEntry> entries = InetDAO4Accounts.getLedgerEntries(raKey, startDate, endDate, ofy);
		return getLedgerDTO(entries, acctID, false, InetDAO4Accounts.getCompanyAcctsMap(ofy));
	}

	@Override
	public List<TableMessage> getLedgerResidentAcctEntries(String tenantID,
			Date startDate, Date endDate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TableMessage> getResidentAccountsByBuilding(String buildingId) throws MissingEntitiesException {
		Objectify ofy = ObjectifyService.begin();
		Key<Building> bk = ObjectifyService.factory().stringToKey(buildingId);
		//fetch building accounts
		List<InetResidentAccount> residentAccounts = InetDAO4Accounts.getResidentAccountForBuilding(bk, ofy);
		
		//fetch building tenants
		List<Tenant> tenants = ReadDAO.getTenantObjects(ofy, bk, null);
		
		//fetch reps
		HashSet<Key<Representative>> repKeys = new HashSet<Key<Representative>>();
		for(Tenant t : tenants)
			if(t.getCompanyRep() != null)
				repKeys.add(t.getCompanyRep());
		Map<Key<Representative>, Representative> reps = ReadDAO.getEntities(repKeys, ofy, true);
		
		//build account -> rep map
		Map<Key<InetResidentAccount>, Contact> acctRepMap = new HashMap<Key<InetResidentAccount>, Contact>();
		for(Tenant t: tenants)
		{
			Key<InetResidentAccount> raKey = InetResidentAccount.getParentAccountKey(t.getKey());
			Contact rep = t;
			if(t.getCompanyRep() != null && reps.containsKey(t.getCompanyRep()))
				rep = reps.get(t.getCompanyRep());
			acctRepMap.put(raKey, rep);
		}
		
		//build result
		ArrayList<TableMessage> result = new ArrayList<TableMessage>(residentAccounts.size() + 1);
		TableMessageHeader h = new TableMessageHeader(10);
		h.setText(0, "Type", TableMessageContent.TEXT);
		h.setText(1, "Ccy.", TableMessageContent.TEXT);
		h.setText(2, "-ve?", TableMessageContent.TEXT);
		h.setText(3, "Bld.", TableMessageContent.TEXT);
		h.setText(4, "Apt.", TableMessageContent.TEXT);
		h.setText(5, "Company", TableMessageContent.TEXT);
		h.setText(6, "Attn.", TableMessageContent.TEXT);
		h.setText(7, "Bal.", TableMessageContent.NUMBER);
		h.setText(8, "Warn", TableMessageContent.NUMBER);
		h.setText(9, "Req.", TableMessageContent.NUMBER);
		h.setFormatOption(9, "#,##0.00");
		h.setEditable(9, true);
		result.add(h);
		
		HashMap<String, String> buildingNames = ReadDAO.getBuildingNameToDescriptionMap(ofy);
		for(InetResidentAccount ra : residentAccounts)
		{	
			Key<Apartment> ak = ra.getResident().getParent();
			TableMessage m = new TableMessage(10, 3, 2);
			//id
			m.setMessageId(ra.getKey().getString());
			
			//numeric fields
			m.setNumber(DTOConstants.RSD_ACCT_AMT_IDX, ra.getCurrentAmount());
			m.setNumber(DTOConstants.RSD_ACCT_LEV_IDX, ra.getDepositWarningLevel());

			//text fields
			m.setText(DTOConstants.RSD_ACCT_BLD_IDX, ak.getParent().getName());
			m.setText(DTOConstants.RSD_ACCT_APT_IDX, ak.getName());
			m.setText(DTOConstants.RSD_ACCT_TYPE_IDX, ra.getDepositType());			
			m.setText(DTOConstants.RSD_ACCT_CURR_IDX, ra.getCurrency());
			m.setText(DTOConstants.RSD_ACCT_OVER_IDX, String.valueOf(ra.isAllowOverDraft()));
			Key<InetResidentAccount> pk = ra.getKey().getParent();
			Contact c = acctRepMap.get(pk);
			if(c instanceof Representative)
			{
				m.setText(DTOConstants.RSD_ACCT_ATTN_ADDR_IDX, c.getAddress()==null?null:c.getAddress().toString());
			}
			else
			{
				Tenant t = (Tenant) c;
				m.setText(DTOConstants.RSD_ACCT_ATTN_ADDR_IDX, m.getText(DTOConstants.RSD_ACCT_APT_IDX) + ", " + buildingNames.get(m.getText(DTOConstants.RSD_ACCT_BLD_IDX)));
				m.setText(DTOConstants.RSD_ACCT_ARCHIVE_IDX, String.valueOf(t.isArchived()));
			}
			
			m.setText(DTOConstants.RSD_ACCT_COMPANY_IDX, c.getCompanyName());
			m.setText(DTOConstants.RSD_ACCT_ATTN_IDX, c.getFirstName() + " " + c.getLastName());
			m.setText(DTOConstants.RSD_ACCT_ATTN_ID_IDX, c.getKey().getString());
			
			//date fields
			m.setDate(DTOConstants.RSD_ACCT_CRT_DT_IDX, ra.getCreateDate());
			m.setDate(DTOConstants.RSD_ACCT_CRT_DT_IDX, ra.getLastModified());
			
			result.add(m);
		}
		log.warning("Hey, returning: ******" + result.size() + "   accounts:" + residentAccounts.size());
		return result;
	}

	@Override
	public TableMessage updateAccount(String accId, String bank,
			String sortCode, String acctName, String acctNumber, String currency) throws MissingEntitiesException {
		Key<InetCompanyAccount> ak = ObjectifyService.factory().stringToKey(accId);
		String user = LoginHelper.getLoggedInUser(getThreadLocalRequest());
		InetCompanyAccount acct = InetDAO4Accounts.updateAccount(ak, bank, sortCode, acctName, acctNumber, currency, user);
		return getCompanyAccountDTO(acct);
	}

	@Override
	public String getDepositRequestLink(String[] companyAcctId, Date invDate,
			String description, List<TableMessage> requests) 
	{
		
		Key<InetCompanyAccount>[] ck = new Key[companyAcctId.length];
		for(int i = 0; i < companyAcctId.length; i++)
			ck[i] = ObjectifyService.factory().stringToKey(companyAcctId[i]);
		Map<Key<InetCompanyAccount>, InetCompanyAccount> companyAccount = ObjectifyService.begin().get(ck);
		
		return BillingInvoiceGenerator.getDepositRequestLink(companyAccount.values(), invDate, description, requests, getThreadLocalRequest());
	}

	@Override
	public TableMessage updateResidentAccount(String acctID,
			boolean allowOverDraft, int depositLevel)
			throws MissingEntitiesException {
		String user = LoginHelper.getLoggedInUser(getThreadLocalRequest());
		Key<InetResidentAccount> raKey = ObjectifyService.factory().stringToKey(acctID);
		InetResidentAccount ra = InetDAO4Accounts.updateResidentAccount(raKey, user, allowOverDraft, depositLevel);
		return InetDAO4Accounts.getResidentAcctDTO(ra);
	}


}
