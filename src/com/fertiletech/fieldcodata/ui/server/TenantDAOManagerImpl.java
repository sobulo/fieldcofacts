package com.fertiletech.fieldcodata.ui.server;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Logger;

import com.fertiletech.fieldcodata.backend.accounting.InetResidentAccount;
import com.fertiletech.fieldcodata.backend.entities.Apartment;
import com.fertiletech.fieldcodata.backend.entities.Building;
import com.fertiletech.fieldcodata.backend.entities.Contact;
import com.fertiletech.fieldcodata.backend.entities.LeaseNegotiation;
import com.fertiletech.fieldcodata.backend.entities.Location;
import com.fertiletech.fieldcodata.backend.entities.ProspectiveData;
import com.fertiletech.fieldcodata.backend.entities.ProspectiveTenant;
import com.fertiletech.fieldcodata.backend.entities.ReadDAO;
import com.fertiletech.fieldcodata.backend.entities.RelatedContact;
import com.fertiletech.fieldcodata.backend.entities.Tenant;
import com.fertiletech.fieldcodata.backend.entities.WriteDAO;
import com.fertiletech.fieldcodata.scripts.tasks.TaskQueueHelper;
import com.fertiletech.fieldcodata.ui.client.TenantDAOManager;
import com.fertiletech.fieldcodata.ui.server.login.LoginHelper;
import com.fertiletech.fieldcodata.ui.shared.UtilConstants;
import com.fertiletech.fieldcodata.ui.shared.UtilConstants.ContactTypes;
import com.fertiletech.fieldcodata.ui.shared.UtilHelper;
import com.fertiletech.fieldcodata.ui.shared.dto.DTOConstants;
import com.fertiletech.fieldcodata.ui.shared.dto.FormConstants;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessageHeader;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessageHeader.TableMessageContent;
import com.fertiletech.fieldcodata.ui.shared.exceptions.DuplicateEntitiesException;
import com.fertiletech.fieldcodata.ui.shared.exceptions.MissingEntitiesException;
import com.google.appengine.api.datastore.QueryResultIterable;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

public class TenantDAOManagerImpl extends RemoteServiceServlet implements
		TenantDAOManager {

	private static final Logger log = Logger
			.getLogger(TenantDAOManagerImpl.class.getName());
	private final int OTHER_NUMS_FIELD_IDX = 0;
	private final int OTHER_EMAILS_FIELD_IDX = 1;
	
	static
	{
		WriteDAO.registerClassesWithObjectify();
	}
	
	@Override
	public HashMap<String, String> getBuildingNames() {
		List<Key<Building>> buildingKeys = ReadDAO
				.getAllBuildings(ObjectifyService.begin());
		HashMap<String, String> result = new HashMap<String, String>(
				buildingKeys.size());
		for (Key<Building> bk : buildingKeys)
			result.put(bk.getString(), bk.getName());
		return result;
	}
	
	@Override
	public TableMessage createNewTenant(HashMap<String, String> leaseData, TableMessage m)
			throws DuplicateEntitiesException {
		HashSet<String>[] otherFields = getMultiTextFields(m);
		Key<Apartment> aptKey = ObjectifyService.factory().stringToKey(
				m.getText(DTOConstants.TNT_APT_ID_IDX));
		Tenant t = WriteDAO.createTenant(leaseData, m.getText(DTOConstants.TNT_FNAME_IDX),
				m.getText(DTOConstants.TNT_LNAME_IDX),
				m.getDate(DTOConstants.TNT_DATE_OF_BIRTH_IDX),
				m.getText(DTOConstants.TNT_PRIMARY_EMAIL_IDX),
				m.getText(DTOConstants.TNT_PRIMARY_PHONE_IDX),
				otherFields[OTHER_NUMS_FIELD_IDX],
				otherFields[OTHER_EMAILS_FIELD_IDX],
				m.getText(DTOConstants.TNT_COMPANY_IDX),
				m.getText(DTOConstants.TNT_ADDRESS_IDX), Boolean.valueOf(m.getText(DTOConstants.TNT_GENDER_IDX)), 
				m.getText(DTOConstants.TNT_TITLE_IDX), aptKey,
				null, LoginHelper.getLoggedInUser(getThreadLocalRequest()));
		m.setMessageId(t.getKey().getString());
		
		return m;
	}

	@Override
	public TableMessage changeApartmentInfo(String tenantID, String apartmentID, String userComments)
			throws MissingEntitiesException, DuplicateEntitiesException {
		Key<Apartment> aptKey = ObjectifyService.factory().stringToKey(
				apartmentID);
		Key<Tenant> tk = ObjectifyService.factory().stringToKey(tenantID);
		Tenant oldTenantState = ObjectifyService.begin().get(tk); 
		Key<? extends Apartment> oldAptKey = oldTenantState.getApartmentUnit(); 

		Tenant t = WriteDAO.updateApartmentTenant(aptKey, oldTenantState);

		//update comments on old tenant object
		//+ relationships copy
		String aptString = " apartment updated to ";
		String usr = LoginHelper.getLoggedInUser(getThreadLocalRequest());
		if(aptKey.equals(oldAptKey))
			aptString = " restored from archives. apartment is: ";
		String[] comments = {"Resident(" + t.getFirstName() + " " + t.getLastName() +")" + aptString 
				+ aptKey.getName() + "/" + aptKey.getParent().getName(), userComments};
		
		TaskQueueHelper.scheduleCreateComment(comments, tk.getString(), usr);
		if(!aptKey.equals(oldAptKey))
		{
			//update comments on new tenant object
			//+ relationships copy
			List<RelatedContact> relatives = WriteDAO.cloneContacts(t, oldTenantState);
			aptString = " moved from ";
			String[] comments2 = {"Resident contact (" + t.getFirstName() + " " + t.getLastName() +")" + aptString 
					+ oldAptKey.getName() + "/" + oldAptKey.getParent().getName(), "Copied over " + relatives.size() + " contact relationships as well", userComments};
			TaskQueueHelper.scheduleCreateComment(comments2, t.getKey().getString(), usr);
		}
		
		Apartment a = ObjectifyService.begin().get(aptKey);
		return getContactDTO(t, a);
	}
	
	private void deleteTenant(Key<Tenant> tk) throws DuplicateEntitiesException
	{
		List<Key<? extends Object>> associatedTenantObjects = findTenantAssociations(tk);
		String message = "";
		for(Key<? extends Object> objectKey : associatedTenantObjects)
			message += (objectKey.toString() + " <br/>");
		if(message.length() > 0)
			throw new DuplicateEntitiesException("Unable to delete. This tenant has associated objects. Please forward the message to IT<br/>: " + message);
		Objectify ofy = ObjectifyService.beginTransaction();
		try
		{
			ofy.delete(tk);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
	}
	
	public ArrayList<Key<? extends Object>> findTenantAssociations(Key<Tenant> tk)
	{
		ArrayList<Key<? extends Object>> result = new ArrayList<Key<? extends Object>>();
		Objectify ofy = ObjectifyService.begin();
		result.addAll(ofy.query().ancestor(tk).listKeys());
		result.addAll(ofy.query(InetResidentAccount.class).ancestor(InetResidentAccount.getParentAccountKey(tk)).listKeys());
		return result;
	}

	private HashSet<String>[] getMultiTextFields(TableMessage m) {
		int otherNumStartIdx = (int) Math.round(m
				.getNumber(DTOConstants.TNT_OTHER_PHONE_START_IDX));
		int otherNumEndIdx = (int) Math.round(m
				.getNumber(DTOConstants.TNT_OTHER_PHONE_END_IDX));
		int otherEmailStartIdx = (int) Math.round(m
				.getNumber(DTOConstants.TNT_OTHER_EMAIL_START_IDX));
		int otherEmailEndIdx = (int) Math.round(m
				.getNumber(DTOConstants.TNT_OTHER_EMAIL_END_IDX));
		log.warning("NumStart: " + otherNumStartIdx + " NumEnd: " + otherNumEndIdx + " EmailStart: " + otherEmailStartIdx + " EmailEnd: " + otherEmailEndIdx);

		HashSet<String> otherNumbers = new HashSet<String>();
		HashSet<String> otherEmails = new HashSet<String>();
		if(otherNumStartIdx >= 0)
			for (int i = otherNumStartIdx; i <= otherNumEndIdx; i++)
				otherNumbers.add(m.getText(i));
		if(otherEmailStartIdx >= 0)
			for (int i = otherEmailStartIdx; i <= otherEmailEndIdx; i++)
				otherEmails.add(m.getText(i));
		HashSet<String>[] result = new HashSet[2];
		result[OTHER_NUMS_FIELD_IDX] = otherNumbers;
		result[OTHER_EMAILS_FIELD_IDX] = otherEmails;
		return result;
	}

	@Override
	public HashMap<String, String> getBuildingApartments(String buidlingID)
			throws MissingEntitiesException {
		Objectify ofy = ObjectifyService.begin();
		Key<Building> buildingKey = ofy.getFactory().stringToKey(buidlingID);
		List<Key<Apartment>> apartmentKeys = ReadDAO.getApartments(ofy,
				buildingKey);
		Map<Key<Apartment>, Apartment> apartments = ofy.get(apartmentKeys);
		HashMap<String, String> result = new HashMap<String, String>(
				apartments.size());
		for (Key<Apartment> ak : apartmentKeys)
			result.put(ak.getString(), apartments.get(ak).getDisplayName());
		return result;
	}

	@Override
	public List<TableMessage> getBuildingTenants(String buildingID, Boolean archive)
			throws MissingEntitiesException {

		Objectify ofy = ObjectifyService.begin();
		Key<Building> bldKey = null;
		List<Key<Tenant>> buildingTenantKeys = null;
		if(buildingID != null)
		{
			bldKey = ofy.getFactory().stringToKey(buildingID);
			buildingTenantKeys = ReadDAO.getTenants(ofy, bldKey, archive);
		}
		else
		{
			buildingTenantKeys = ReadDAO.addArchiveFlagToTenantQuery(archive, ofy.query(Tenant.class)).listKeys();
		}
		
		List<TableMessage> result = getTenantDTOList(buildingTenantKeys, ofy);
		TableMessageHeader h = (TableMessageHeader) result.get(0);
		h.setMessageId(buildingID);
		h.setCaption((bldKey == null? "All Buildings " : bldKey.getName()) + " (" + buildingTenantKeys.size()
				+ " tenants)");		
		return result;
	}

	public static List<TableMessage> getTenantDTOList(List<Key<Tenant>> selectedTenantKeys, Objectify ofy)
	{
		List<Key<Apartment>> aptKeys = new ArrayList<Key<Apartment>>();
		List<TableMessage> result = new ArrayList<TableMessage>();
		// -1 because aptId is stored in this range but not displayed
		TableMessageHeader h = new TableMessageHeader(8); 
		h.setText(DTOConstants.TNT_APT_IDX, "Apartment",
				TableMessageContent.TEXT);
		h.setText(DTOConstants.TNT_FNAME_IDX, "First Name",
				TableMessageContent.TEXT);
		h.setText(DTOConstants.TNT_LNAME_IDX, "Surname",
				TableMessageContent.TEXT);
		h.setText(DTOConstants.TNT_COMPANY_IDX, "Company",
				TableMessageContent.TEXT);
		h.setText(DTOConstants.TNT_PRIMARY_PHONE_IDX, "Phone",
				TableMessageContent.TEXT);
		h.setText(DTOConstants.TNT_PRIMARY_EMAIL_IDX, "Email",
				TableMessageContent.TEXT);
		//h.setText(DTOConstants.TNT_ADDRESS_IDX, "Permanent Address",
		//		TableMessageContent.TEXT);
		h.setText(DTOConstants.TNT_BUILDING_IDX, "Building",
				TableMessageContent.TEXT);
		h.setText(7, "Birthday", TableMessageContent.DATE);
		result.add(h);
		
		for (Key<Tenant> tk : selectedTenantKeys) {
			Key<Apartment> ak = tk.getParent();
			aptKeys.add(ak);
		}

		Map<Key<Apartment>, Apartment> apartments = ofy.get(aptKeys);
		Map<Key<Tenant>, Tenant> tenants = ofy.get(selectedTenantKeys);

		for (Key<Tenant> tk : selectedTenantKeys) {
			TableMessage m = getContactDTO(tenants.get(tk),
					apartments.get(tk.getParent()));
			result.add(m);
		}
		return result;		
	}
	
	public static TableMessage getTenantDTO(Tenant t, Objectify ofy)
	{
		Apartment a = ofy.find(t.getApartmentUnit());
		TableMessage result = getContactDTO(t, a);
		return result;
	}
	
	private void scheduleTenantBioComment(Contact t, Objectify ofy)
	{
		scheduleTenantBioComment(t, t.getKey(), ofy, "Details");
	}

	private void scheduleTenantBioComment(Contact t, Key<? extends Contact> ck, Objectify ofy, String title)
	{
		String user = LoginHelper.getLoggedInUser(getThreadLocalRequest());
		scheduleTenantBioComment(t, ck, ofy, title, user);
	}
	
	public static void scheduleTenantBioComment(Contact t, Key<? extends Contact> ck, Objectify ofy, String title, String user)
	{
		TableMessage result;
		boolean includeApt;
		if( t instanceof Tenant)
		{
			includeApt = true;
			result = getTenantDTO((Tenant) t, ofy);
		}
		else
		{
			includeApt = false;
			result = getContactDTO(t, null);
		}
		String[] comments = {UtilHelper.getDetailedPrintFriendlyContact(result, title, includeApt)};
		TaskQueueHelper.scheduleCreateComment(comments, ck.getString(), user);		
	}
	
	public static TableMessage getContactDTO(Contact t, Apartment a) {
		HashSet<String> otherNumbers = t.getSecondaryNumbers();
		HashSet<String> otherEmails = t.getSecondaryEmails();
		
		if (otherNumbers == null)
			otherNumbers = new HashSet<String>();
		if (otherEmails == null)
			otherEmails = new HashSet<String>();

		String aptDisplay = "";
		String building = "";
		String aptID = "";
		if(a != null)
		{
			aptDisplay = a.getDisplayName();
			building = a.getKey().getParent().getName();
			aptID = a.getKey().getString();
		}
				
		TableMessage result = new TableMessage(
				DTOConstants.TNT_NUM_OF_PRESPECIFIED_TXT_FIELDS
						+ otherNumbers.size() + otherEmails.size(), 4, 1);
		result.setMessageId(t.getKey().getString());
		result.setText(DTOConstants.TNT_APT_IDX, aptDisplay);
		result.setText(DTOConstants.TNT_BUILDING_IDX, building);
		result.setText(DTOConstants.TNT_APT_ID_IDX, aptID);
		result.setText(DTOConstants.TNT_FNAME_IDX, t.getFirstName());
		result.setText(DTOConstants.TNT_LNAME_IDX, t.getLastName());
		result.setText(DTOConstants.TNT_COMPANY_IDX, t.getCompanyName());
		result.setText(DTOConstants.TNT_PRIMARY_PHONE_IDX, t.getPrimaryNumber());
		result.setText(DTOConstants.TNT_PRIMARY_EMAIL_IDX, t.getPrimaryEmail());
		result.setText(DTOConstants.TNT_TITLE_IDX, t.getTitle());
		result.setDate(DTOConstants.TNT_DATE_OF_BIRTH_IDX, t.getDateOfBirth());
		result.setText(DTOConstants.TNT_GENDER_IDX, t.isMale() == null ? null : String.valueOf(t.isMale()));
		if(t instanceof Tenant)
		{
			result.setText(DTOConstants.TNT_IS_OWNR_IDX, String.valueOf(((Tenant) t).isOwnerOccupier()));
			result.setText(DTOConstants.TNT_IS_ARCH_IDX, String.valueOf(((Tenant) t).isArchived()));
		}
		if (t.getAddress() != null)
			result.setText(DTOConstants.TNT_ADDRESS_IDX, t.getAddress()
					.getValue());
		int startIdx = DTOConstants.TNT_NUM_OF_PRESPECIFIED_TXT_FIELDS;
		if (otherNumbers.size() == 0)
		{
			result.setNumber(DTOConstants.TNT_OTHER_PHONE_START_IDX, -1);
			result.setNumber(DTOConstants.TNT_OTHER_PHONE_END_IDX, - 1);			
		}
		else {
			result.setNumber(DTOConstants.TNT_OTHER_PHONE_START_IDX, startIdx);
			for (String s : otherNumbers)
				result.setText(startIdx++, s);
			result.setNumber(DTOConstants.TNT_OTHER_PHONE_END_IDX, startIdx - 1);
		}

		if (otherEmails.size() == 0)
		{
			result.setNumber(DTOConstants.TNT_OTHER_EMAIL_START_IDX, -1);
			result.setNumber(DTOConstants.TNT_OTHER_EMAIL_END_IDX, - 1);
		}
		else {
			result.setNumber(DTOConstants.TNT_OTHER_EMAIL_START_IDX, startIdx);
			for (String s : otherEmails)
				result.setText(startIdx++, s);
			result.setNumber(DTOConstants.TNT_OTHER_EMAIL_END_IDX, startIdx - 1);
		}
		return result;
	}

	@Override
	public HashMap<String, String> getAllProspects() {
		Objectify ofy = ObjectifyService.begin();
		List<ProspectiveTenant> prospectKeys = ofy.query(
				ProspectiveTenant.class).list();
		return getContactKeyNameMap(prospectKeys);
	}

	@Override
	public HashMap<String, String> getAllTenants() {
		Objectify ofy = ObjectifyService.begin();
		List<Tenant> tenants = ReadDAO.addArchiveFlagToTenantQuery(false, ofy.query(Tenant.class)).list();
		return getContactKeyNameMap(tenants);
	}

	public static HashMap<String, String> getContactKeyNameMap(
			List<? extends Contact> contacts) {
		HashMap<String, String> result = new HashMap<String, String>();
		for (Contact c : contacts)
		{
			String location = c.getCompanyName()==null? "" : (" [" + c.getCompanyName() + "] ");
			if(c instanceof Tenant)
			{
				Tenant t = (Tenant) c;
				location = " [" + t.getKey().getParent().getParent().getName() + "/" + t.getKey().getParent().getName()+ "]";
			}
			result.put(
					ObjectifyService.factory().keyToString(c.getKey()),
					c.getLastName() + " " + c.getFirstName() + location + " {"
							+ c.getKey().getId() + "}");
		}
		return result;
	}

	@Override
	public HashMap<String, String> getProspectiveData(String prospectiveId) {
		Key<ProspectiveTenant> ptKey = ObjectifyService.factory().stringToKey(prospectiveId);
		ProspectiveData dataObject =  ReadDAO.getProspectiveData(ptKey);
		return dataObject == null? null : dataObject.getData();
	}

	@Override
	public String saveProspectiveData(String prospectId,
			HashMap<String, String> supplementary) {
		Key<ProspectiveTenant> ptKey = ObjectifyService.factory().stringToKey(prospectId);
		ProspectiveData data = WriteDAO.saveProspectiveData(ptKey, supplementary);
		HashMap<String, String> dataMap = data.getData();
		StringBuffer bf = new StringBuffer("Saved supplementary information below: <br/><ul>");
		for(String key : dataMap.keySet())
			bf.append("<li>").append(key).append(':').append(dataMap.get(key)).append("</li>");		
		String result = bf.append("</ul>").toString();
		String[] comments = {"<p>supplementary data for client changed</p>" + bf.toString()};
		TaskQueueHelper.scheduleCreateComment(comments, prospectId, LoginHelper.getLoggedInUser(getThreadLocalRequest()));		
		return result;
	}

	@Override
	public String[] createContact(ContactTypes t, String firstName,
			String lastName, Date dob, String primaryEmail, String primaryNumber,
			HashSet<String> secondaryNumbers, HashSet<String> secondaryEmails,
			String companyName, String tenantAddress, Boolean isMale,
			String title, String[] args) {
		Contact c = null;
		switch (t) {
		case PROSPECT:
			c = WriteDAO.createProspectiveTenant(firstName, lastName, dob,
					primaryEmail, primaryNumber, secondaryNumbers,
					secondaryEmails, companyName, tenantAddress, isMale, title);
			scheduleTenantBioComment(c, ObjectifyService.begin());
			break;
		case RELATED_CONTACT:
			Key<Tenant> tk = ObjectifyService.factory().stringToKey(args[0]);
			c = WriteDAO.createRelatedContact(firstName, lastName, dob, primaryEmail, primaryNumber, secondaryNumbers,
					secondaryEmails, companyName, tenantAddress, isMale, title, tk, args[1]);
			scheduleTenantBioComment(c, tk, ObjectifyService.begin(), "Related Contact Modified - " + args[1]);
			break;
		default:
			throw new RuntimeException("Unsupported contact type: " + t);
		}
		String[] result = new String[3];
		result[UtilConstants.SAVE_CONTACT_KEY_IDX] = c.getKey().getString();
		result[UtilConstants.SAVE_CONTACT_VAL_IDX] = "Succesfully created contact info for "
				+ c.getTitle() + " " + c.getFirstName() + " " + c.getLastName();
		return result;
	}

	@Override
	public TableMessage getContact(String contactKeyStr) throws MissingEntitiesException {
		Objectify ofy = ObjectifyService.begin();
		Key<? extends Contact> contactKey = ofy.getFactory().stringToKey(contactKeyStr);
		Contact c = ofy.find(contactKey);
		if(c == null)
			throw new MissingEntitiesException("Unable to find object for contact key " + c);
		return getContactDTO(c, null);
	}

	@Override
	public HashMap<String, String>[] getApartments(String buildingID) {
		Key<Building> bk = ObjectifyService.factory().stringToKey(buildingID);
		return ReadDAO.getApartments(bk);
	}

	@Override
	public TableMessage updateContact(String contactID, String firstName,
			String lastName, Date dob, String primaryEmail, String primaryNumber,
			HashSet<String> secondaryNumbers, HashSet<String> secondaryEmails,
			String companyName, String tenantAddress, Boolean isMale,
			String title, String[] args) throws MissingEntitiesException {
		Key<? extends Contact> ctKey = ObjectifyService.factory().stringToKey(contactID);
		Contact c = WriteDAO.updateContact(ctKey, firstName, lastName, dob, primaryEmail, primaryNumber, secondaryNumbers,
				secondaryEmails, companyName, tenantAddress, isMale, title, args);
		scheduleTenantBioComment(c, ObjectifyService.begin());
		Apartment a = null;
		if(c instanceof Tenant)
		{
			Tenant t = (Tenant) c;
			a = ObjectifyService.begin().get(t.getApartmentUnit());
		}
		return getContactDTO(c, a);
	}

	@Override
	public List<TableMessage> getBuildingApartmentTable(String buildingID) {
		Objectify ofy = ObjectifyService.begin();
		Key<Building> bldKey = ofy.getFactory().stringToKey(buildingID);
		return ReadDAO.getApartmentsTable(bldKey, ofy);
	}

	@Override
	public HashMap<String, String> getAllBuildingAndApartments() {
		Objectify ofy = ObjectifyService.begin();
		QueryResultIterable<Key<Apartment>> aptKeys = ofy.query(Apartment.class).fetchKeys();
		TreeMap<String, String> result = new TreeMap<String, String>();
		for(Key<Apartment> ak : aptKeys)
		{
			result.put(Apartment.getLocationName(ak), ak.getString());
			Key<Building> bk = ak.getParent();
			result.put(Apartment.getLocationName(bk), bk.getString());
		}
		LinkedHashMap<String, String> returnResult = new LinkedHashMap<String, String>();
		for(String key : result.keySet())
			returnResult.put(result.get(key), key);
		return returnResult;
	}
	
	@Override
	public List<TableMessage> getProspectsTable(Date start, Date end)
	{
		Objectify ofy = ObjectifyService.begin();
		List<ProspectiveTenant> prospects = ofy.query(ProspectiveTenant.class).
				filter("lastModified >=", start).filter("lastModified <=", end).list();
		List<ProspectiveData> data = ofy.query(ProspectiveData.class).
				filter("lastModified >=", start).filter("lastModified <=", end).list();
		ArrayList<TableMessage> result = new ArrayList<TableMessage>();
		HashMap<Key<ProspectiveTenant>, TableMessage> resultMap = new HashMap<Key<ProspectiveTenant>, TableMessage>();
		result.add(getProspectsHeader()); 
		for(ProspectiveTenant p : prospects)
		{
			TableMessage m = getContactDTO(p, null);
			m.setText(DTOConstants.TNT_APT_IDX, p.isConvertedLead()==null? "" : String.valueOf(p.isConvertedLead()));
			m.setDate(DTOConstants.TNT_DATE_OF_BIRTH_IDX, p.getLastModified());
			m.setText(DTOConstants.TNT_BUILDING_IDX, "");
			m.setText(DTOConstants.TNT_ADDRESS_IDX, "");
			m.setText(DTOConstants.TNT_TITLE_IDX, "");
			m.setText(DTOConstants.TNT_GENDER_IDX, "");			
			resultMap.put(p.getKey(), m);
			result.add(m);
		}
		for(ProspectiveData d : data)
		{
			Key<ProspectiveData> parentKey = d.getKey().getParent();
			TableMessage m = resultMap.get(parentKey);

			if(m == null) continue;			
			String buildingInspected = d.getData().get(FormConstants.PD_PROP_INSPECTED);
			String offerSent = d.getData().get(FormConstants.PD_OFFER_SENT);
			String draftSent = d.getData().get(FormConstants.PD_DRAFT_SENT);
			String docFolder = d.getData().get(FormConstants.PD_DOCS_FOLDER);				
			
			m.setText(DTOConstants.TNT_BUILDING_IDX, buildingInspected);
			m.setText(DTOConstants.TNT_ADDRESS_IDX, offerSent);
			m.setText(DTOConstants.TNT_TITLE_IDX, draftSent);
			m.setText(DTOConstants.TNT_GENDER_IDX, docFolder);
		}
		return result;
	}
	
	public TableMessageHeader getProspectsHeader()
	{
		TableMessageHeader h = new TableMessageHeader(10); 
		h.setText(DTOConstants.TNT_BUILDING_IDX, "Properties",
				TableMessageContent.TEXT);			
		h.setText(DTOConstants.TNT_APT_IDX, "Concluded Txn",
				TableMessageContent.TEXT);
		h.setText(DTOConstants.TNT_FNAME_IDX, "First Name",
				TableMessageContent.TEXT);
		h.setText(DTOConstants.TNT_LNAME_IDX, "Surname",
				TableMessageContent.TEXT);
		h.setText(DTOConstants.TNT_COMPANY_IDX, "Company",
				TableMessageContent.TEXT);
		h.setText(DTOConstants.TNT_PRIMARY_PHONE_IDX, "Phone",
				TableMessageContent.TEXT);
		h.setText(DTOConstants.TNT_PRIMARY_EMAIL_IDX, "Email",
				TableMessageContent.TEXT);
		//h.setText(DTOConstants.TNT_ADDRESS_IDX, "Permanent Address",
		//		TableMessageContent.TEXT);
		h.setText(DTOConstants.TNT_ADDRESS_IDX, "Letter",
				TableMessageContent.TEXT);
		h.setText(DTOConstants.TNT_TITLE_IDX, "Draft",
				TableMessageContent.TEXT);
		h.setText(9, "Last Modified", TableMessageContent.DATE);
		return h;
	}

	@Override
	public List<TableMessage> getTenantRelations(String tenantID) {
		Key<Tenant> tk = ObjectifyService.factory().stringToKey(tenantID);
		Objectify ofy = ObjectifyService.begin();
		List<RelatedContact> contacts = ofy.query(RelatedContact.class).ancestor(tk).list();
		ArrayList<TableMessage> result = new ArrayList<TableMessage>();
		for(RelatedContact c : contacts)
		{
			TableMessage m = getContactDTO(c, null);
			m.setText(DTOConstants.RLT_TYPE_IDX, c.getRelationship());
			result.add(m);
		}
		return result;
	}

	@Override
	public HashMap<String, List<TableMessage>> getTenantContacts() {
		List<RelatedContact> contacts = ObjectifyService.begin().query(RelatedContact.class).list();
		HashMap<String, List<TableMessage>> result = new HashMap<String, List<TableMessage>>();
		for(RelatedContact c : contacts)
		{
			String key = c.getKey().getParent().getString();
			TableMessage m = getContactDTO(c, null);
			m.setText(DTOConstants.RLT_TYPE_IDX, c.getRelationship());
			if(!result.containsKey(key))
				result.put(key, new ArrayList<TableMessage>());
			result.get(key).add(m);
		}
		return result;
	}

	@Override
	public TableMessage getResident(String locationID) {
		Key<? extends Location> lk = ObjectifyService.factory().stringToKey(locationID);
		Objectify ofy = ObjectifyService.begin();
		Location loc = ofy.get(lk);
		if(loc instanceof Apartment)
		{
			Apartment a = (Apartment) loc;
			Key<Tenant> t = ReadDAO.getCurrentTenant(ofy, a.getKey());
			if(t == null)
			{
				TableMessage m = new TableMessage(1, 0, 0);
				m.setText(0, "Vacant Apartment");
				return m;
			}
			else
				return getContactDTO(ofy.get(t), a);				
		}
		else
		{
			TableMessage m = new TableMessage(1, 0, 0);
			return m;
		}
	}

	@Override
	public List<TableMessage> getLeases(String tenantID)
			throws MissingEntitiesException {
		TableMessageHeader h = new TableMessageHeader(6);
		h.setText(0, "Created", TableMessageContent.DATE);
		h.setText(1, "Starts", TableMessageContent.DATE);
		h.setText(2, "Ends", TableMessageContent.DATE);
		h.setText(3, "Rent", TableMessageContent.NUMBER);
		h.setText(4, "Ccy", TableMessageContent.TEXT);
		h.setText(5, "Owner", TableMessageContent.TEXT);
		Key<Tenant> tk = ObjectifyService.factory().stringToKey(tenantID);
		return ReadDAO.getLeaseDTOs(tk, ObjectifyService.begin(), h);
	}

	@Override
	public List<TableMessage> getLeases(Date startDate, Date endDate) throws MissingEntitiesException {
		TableMessageHeader h = new TableMessageHeader(9);
		h.setText(0, "Created", TableMessageContent.DATE);
		h.setText(1, "Starts", TableMessageContent.DATE);
		h.setText(2, "Ends", TableMessageContent.DATE);
		h.setText(3, "Rent", TableMessageContent.NUMBER);
		h.setText(4, "Ccy", TableMessageContent.TEXT);		
		h.setText(5, "Resident", TableMessageContent.TEXT);
		h.setText(6, "Building", TableMessageContent.TEXT);
		h.setText(7, "Apt.", TableMessageContent.TEXT);
		h.setText(8, "User", TableMessageContent.TEXT);
		return ReadDAO.getLeaseDTOs(startDate, endDate, ObjectifyService.begin(), h);
	}

	@Override
	public TableMessage saveLease(TableMessage m, String userComments) {
		Key<Tenant> tk = ObjectifyService.factory().stringToKey(m.getMessageId());
		m.setText(DTOConstants.LEG_UPDT_USER_USR, LoginHelper.getLoggedInUser(getThreadLocalRequest()));
		LeaseNegotiation l = WriteDAO.createLease(tk, m, userComments);
		return l.getDTO();
	}

	@Override
	public String removeLease(String leaseID, String userComments) throws MissingEntitiesException {
		Key<LeaseNegotiation> lg = ObjectifyService.factory().stringToKey(leaseID);
		Objectify ofy = ObjectifyService.beginTransaction();
		try
		{
			LeaseNegotiation lease = ofy.find(lg);
			if(lease == null)
				throw new MissingEntitiesException("Looks like the lease object might have been deleted already. Refresh your browser to verify this");
		
			String[] comments = {lease.getHTML("<font color='red'><b>DELETED LEASE DETAILS LISTED BELOW</b></font><br/>"), userComments};
			Key<Tenant> tk = lg.getParent();
			ofy.delete(lg);
			List<LeaseNegotiation> leases = ofy.query(LeaseNegotiation.class).ancestor(tk).list();
			Tenant t = WriteDAO.updateLeaseOwnershipFlag(tk, leases, ofy);			
			ofy.put(t);
			ofy.getTxn().commit();
			
			TaskQueueHelper.scheduleCreateComment(comments, lg.getParent().getString(), LoginHelper.getLoggedInUser(getThreadLocalRequest()));
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return leaseID;
	}

	@Override
	public TableMessage archiveTenant(String tenantID, String comments)
			throws DuplicateEntitiesException, MissingEntitiesException {
		Key<Tenant> tk = ObjectifyService.factory().stringToKey(tenantID);
		Tenant t = WriteDAO.updateArchiveFlag(tk, LoginHelper.getLoggedInUser(getThreadLocalRequest()), comments);
		Apartment a = ObjectifyService.begin().get(t.getApartmentUnit());
		return getContactDTO(t, a);
	}
}


