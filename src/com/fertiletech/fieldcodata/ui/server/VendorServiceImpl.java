package com.fertiletech.fieldcodata.ui.server;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import com.fertiletech.fieldcodata.backend.entities.ReadDAO;
import com.fertiletech.fieldcodata.backend.entities.Vendor;
import com.fertiletech.fieldcodata.backend.entities.WriteDAO;
import com.fertiletech.fieldcodata.scripts.tasks.TaskQueueHelper;
import com.fertiletech.fieldcodata.ui.client.VendorService;
import com.fertiletech.fieldcodata.ui.server.login.LoginHelper;
import com.fertiletech.fieldcodata.ui.shared.dto.DTOConstants;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessageHeader;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessageHeader.TableMessageContent;
import com.fertiletech.fieldcodata.ui.shared.exceptions.DuplicateEntitiesException;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class VendorServiceImpl extends RemoteServiceServlet implements VendorService 
{
	static
	{
		WriteDAO.registerClassesWithObjectify();
	}

	@Override
	public List<TableMessage> getAllVendors() {
		List<Vendor> vendors = ReadDAO.getAllVendors(ObjectifyService.begin());
		List<TableMessage> result = new ArrayList<TableMessage>();
		result.add(getHeader());
		for(Vendor v : vendors)
			result.add(getVendorDTO(v));
		
		return result;
	}
	
	private TableMessage getVendorDTO(Vendor v)
	{
		TableMessage m = TenantDAOManagerImpl.getContactDTO(v, null);
		m.setText(DTOConstants.TNT_BUILDING_IDX, String.valueOf(v.getKey().getId()));
		m.setText(DTOConstants.VND_CATEGORY_IDX, v.getCategory());
		return m;
	}
	
	public TableMessageHeader getHeader()
	{
		TableMessageHeader h = new TableMessageHeader(7);
		h.setText(DTOConstants.TNT_BUILDING_IDX, "ID", TableMessageContent.TEXT);
		h.setText(DTOConstants.VND_CATEGORY_IDX, "Category", TableMessageContent.TEXT);
		h.setText(DTOConstants.TNT_FNAME_IDX, "First Name", TableMessageContent.TEXT);
		h.setText(DTOConstants.TNT_LNAME_IDX, "Surname", TableMessageContent.TEXT);
		h.setText(DTOConstants.TNT_COMPANY_IDX, "Company", TableMessageContent.TEXT);
		h.setText(DTOConstants.TNT_PRIMARY_PHONE_IDX, "Phone", TableMessageContent.TEXT);
		h.setText(DTOConstants.TNT_PRIMARY_EMAIL_IDX, "Email", TableMessageContent.TEXT);
		return h;
	}

	@Override
	public TableMessage createVendor(String category, String firstName,
			String lastName, Date dob, String primaryEmail,
			String primaryNumber, HashSet<String> secondaryNumbers,
			HashSet<String> secondaryEmails, String companyName,
			String address, Boolean isMale, String title, boolean throwException) throws DuplicateEntitiesException {
		Vendor v = WriteDAO.createVendor(category, firstName, lastName, dob, primaryEmail, primaryNumber, secondaryNumbers, secondaryEmails, companyName, address, isMale, title, throwException);
		String commentTitle = "Created Vendor Contact for " + companyName;
		TenantDAOManagerImpl.scheduleTenantBioComment(v, v.getKey(), null, commentTitle, LoginHelper.getLoggedInUser(getThreadLocalRequest()));
		return getVendorDTO(v);
	}

	@Override
	public TableMessage changeVendorCategory(String vendorID, String category) {
		Key<Vendor> k = ObjectifyService.factory().stringToKey(vendorID);
		Objectify ofy = ObjectifyService.beginTransaction();
		Vendor v = null;
		try
		{
			v = ofy.get(k);
			String[] comments = {"Vendor category changed from " + v.getCategory() + " to "  + category,
								 "FOR: " + v.getFirstName() + " " + v.getLastName() + " @ " + v.getCompanyName()};
			v.setCategory(category);
			ofy.put(v);
			TaskQueueHelper.scheduleCreateComment(comments, v.getKey().getString(), LoginHelper.getLoggedInUser(getThreadLocalRequest()));
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return getVendorDTO(v);
	}

	@Override
	public HashMap<String, String> getAllVendorsIDs() {
		List<Vendor> vendors = ObjectifyService.begin().query(Vendor.class).list();
		return TenantDAOManagerImpl.getContactKeyNameMap(vendors);
	}
}
