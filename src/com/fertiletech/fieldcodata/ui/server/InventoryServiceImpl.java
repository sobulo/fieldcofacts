package com.fertiletech.fieldcodata.ui.server;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import com.fertiletech.fieldcodata.backend.entities.Location;
import com.fertiletech.fieldcodata.backend.entities.WriteDAO;
import com.fertiletech.fieldcodata.backend.inventory.AllocationDAO;
import com.fertiletech.fieldcodata.backend.inventory.InventoryAllocation;
import com.fertiletech.fieldcodata.backend.inventory.InventoryItem;
import com.fertiletech.fieldcodata.backend.inventory.ProductDetails;
import com.fertiletech.fieldcodata.backend.tickets.RequestTicket;
import com.fertiletech.fieldcodata.ui.client.InventoryService;
import com.fertiletech.fieldcodata.ui.server.login.LoginHelper;
import com.fertiletech.fieldcodata.ui.shared.dto.DTOConstants;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessageHeader;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessageHeader.TableMessageContent;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class InventoryServiceImpl extends RemoteServiceServlet implements InventoryService 
{
	private static final Logger log = Logger
			.getLogger(InventoryServiceImpl.class.getName());
	
	static
	{
		WriteDAO.registerClassesWithObjectify();
	}

	@Override
	public TableMessage getProductInfo(String manufacturer, String modelNumber) {		
		ProductDetails details = AllocationDAO.getProductInfo(manufacturer, modelNumber, ObjectifyService.begin());
		return getProductDetailsAsTableMessage(details);
	}

	@Override
	public String saveProductInfo(TableMessage productInfo) {
		Key<ProductDetails> pk = null;
		if(productInfo.getMessageId().length() > 0)
			pk = ObjectifyService.factory().stringToKey(productInfo.getMessageId());
		ProductDetails p =  AllocationDAO.saveProductInfo(pk, productInfo.getText(DTOConstants.PRODUCT_MANUFACTURER), 
				productInfo.getText(DTOConstants.PRODUCT_MODEL), productInfo.getText(DTOConstants.PRODUCT_CLASSIFICATION),
				productInfo.getText(DTOConstants.PRODUCT_DESCRIPTION));
		return p.getKey().getString();
	}
	
	private TableMessage getProductDetailsAsTableMessage(ProductDetails p)
	{
		if(p == null) return null;
		TableMessage m = new TableMessage(4, 0, 0);
		m.setMessageId(p.getKey().getString());
		m.setText(DTOConstants.PRODUCT_CLASSIFICATION, p.getClassification());
		m.setText(DTOConstants.PRODUCT_DESCRIPTION, p.getDescription() == null? null : p.getDescription().getValue());
		m.setText(DTOConstants.PRODUCT_MANUFACTURER, p.getManufacturer());
		m.setText(DTOConstants.PRODUCT_MODEL, p.getModelNumber());
		return m;
	}

	@Override
	public HashMap<String, String> getProductDefinitiions() {
		HashMap<String, String> result = new HashMap<String, String>();
		for(Key<ProductDetails> pdk : AllocationDAO.getAllProductDefinitions())
			result.put(pdk.getString(), pdk.getName());
		return result;
	}

	@Override
	public TableMessage getInventoryInfo(String buildingID, String productID) {
		Key<Location> buildingKey = ObjectifyService.factory().stringToKey(buildingID);
		Key<ProductDetails> productKey = ObjectifyService.factory().stringToKey(productID);
		Key<InventoryItem> inventoryKey = InventoryItem.getKey(productKey, buildingKey);
		Objectify ofy = ObjectifyService.begin();
		InventoryItem item = ofy.find(inventoryKey);
		if(item == null)
			return null;
		
		Set<String> serialNumbers = AllocationDAO.getInventorySerialNumbers(inventoryKey, true, ofy).keySet();
		TableMessage result = getInventoryDTO(item, serialNumbers);
		return result;
	}

	@Override
	public Set<String> getSerialNumbers(String inventoryID) {
		Key<InventoryItem> invKey = ObjectifyService.factory().stringToKey(inventoryID);
		HashSet<String> result = new HashSet<String>();
		result.addAll(AllocationDAO.getInventorySerialNumbers(invKey, true, ObjectifyService.begin()).keySet());
		return result;
	}

	@Override
	public String[] saveInventoryItems(TableMessage m, String ticketID) {
		String user = LoginHelper.getLoggedInUser(this.getThreadLocalRequest());
		Key<InventoryItem> ik = null; 
		Key<Location> lk = null;
		Key<ProductDetails> pk = null;
		Key<RequestTicket> tk = null;
		if(ticketID != null)
			tk = ObjectifyService.factory().stringToKey(ticketID);
		if(m.getMessageId().length() > 0)
			ik = ObjectifyService.factory().stringToKey(m.getMessageId());
		else
		{
			pk = ObjectifyService.factory().stringToKey(m.getText(DTOConstants.INV_PRODUCT_IDX));
			lk = ObjectifyService.factory().stringToKey(m.getText(DTOConstants.INV_BUILDING_IDX));
		}
		int serialNumStartIdx = (int) Math.round(m
				.getNumber(DTOConstants.INV_SERIAL_START_IDX));
		int serialNumEndIdx = -1;
		if(serialNumStartIdx >= 0)
			serialNumEndIdx = (int) Math.round(m.getNumber(DTOConstants.INV_SERIAL_END_IDX));		
		InventoryItem item = AllocationDAO.saveInventoryItem(ik, lk, pk, 
				m.getNumber(DTOConstants.INV_PRICE_IDX), getMultiTextFields(m, serialNumStartIdx, serialNumEndIdx), user, tk);
		String[] result = {item.getKey().getString(), String.valueOf(item.getQuantity())};
		return result;
	}
	
	private HashSet<String> getMultiTextFields(TableMessage m, int start, int end) {

		if(start < 0) return new HashSet<String>();
		HashSet<String> serialNumbers = new HashSet<String>(
				(end - start) + 1);
		for (int i = start; i <= end; i++)
			serialNumbers.add(m.getText(i));

		return serialNumbers;
	}
	
	public static TableMessage getAllocationDTO(InventoryAllocation alc)
	{
		TableMessage result = new TableMessage(5, 3, 2);
		result.setMessageId(alc.getKey().getString());
		String from = alc.getFromInventory() == null? "N/A - Restock" : alc.getFromInventory().getName();
		String to = alc.getToInventory() == null ? "N/A - Deletion" : alc.getToInventory().getName();
		result.setText(DTOConstants.ALC_FROM_IDX, from);
		result.setText(DTOConstants.ALC_TO_IDX, to);
		result.setText(DTOConstants.ALC_SERIAL_IDX, alc.getSerialNumber());
		result.setText(DTOConstants.ALC_USER_IDX, alc.getUser());
		result.setText(DTOConstants.ALC_PRODUCT_IDX, alc.getInventoryOwner().getParent().getName());
		result.setNumber(DTOConstants.ALC_PRICE_IDX, alc.getPrice());
		result.setNumber(DTOConstants.ALC_TICKET_IDX, alc.getKey().getId());
		result.setNumber(DTOConstants.ALC_ID_IDX, alc.getKey().getId());
		result.setDate(DTOConstants.ALC_DATE_IDX, alc.getAllocationDate());
		result.setDate(DTOConstants.ALC_CRT_DATE_IDX, alc.getCreationDate());
		return result;
	}
	
	public static TableMessage getInventoryDTO(InventoryItem inv, Set<String> serialNumbers) {
		
		if (serialNumbers == null)
			serialNumbers = new HashSet<String>();

				
		TableMessage result = new TableMessage(
				DTOConstants.INV_NUM_OF_PRESPECIFIED_TXT_FIELDS
						+ serialNumbers.size(), 4, 0);
		result.setMessageId(inv.getKey().getString());
		result.setText(DTOConstants.INV_BUILDING_IDX, InventoryItem.getLocationString(inv.getLocation()));
		result.setText(DTOConstants.INV_PRODUCT_IDX, inv.getKey().getParent().getName());
		result.setNumber(DTOConstants.INV_PRICE_IDX, inv.getSuggestedPrice());
		result.setNumber(DTOConstants.INV_QUANTITY_IDX, inv.getQuantity());

		int startIdx = DTOConstants.INV_NUM_OF_PRESPECIFIED_TXT_FIELDS;
		if (serialNumbers.size() == 0)
			result.setNumber(DTOConstants.INV_SERIAL_START_IDX, -1);
		else {
			result.setNumber(DTOConstants.INV_SERIAL_START_IDX, startIdx);
			for (String s : serialNumbers)
				result.setText(startIdx++, s);
			result.setNumber(DTOConstants.INV_SERIAL_END_IDX, startIdx - 1);
		}

		return result;
	}

	@Override
	public String moveInventory(String fromID, String toID, Set<String> serialNumbers, String ticketID, Double price, Date allocationDate) {
		Key<InventoryItem> fromInventory = ObjectifyService.factory().stringToKey(fromID);
		
		Key<RequestTicket> ticketKey = null;
		if(ticketID != null)
			ticketKey = ObjectifyService.factory().stringToKey(ticketID);
		
		Key<Location> destination = null;		
		if(toID != null)
			destination = ObjectifyService.factory().stringToKey(toID);
		
		log.warning("Attempting move from " + fromInventory + " to " + destination);
		
		String user = LoginHelper.getLoggedInUser(getThreadLocalRequest());
		List<InventoryAllocation> allocations = AllocationDAO.moveInventory(destination, fromInventory, serialNumbers, price, allocationDate, user, ticketKey);
		
		StringBuffer result = new StringBuffer("Saved below: <ul>");		
		for(InventoryAllocation a : allocations)
			result.append("<li>").append(a.toString()).append("</li>");
		result.append("</ul>");
		
		return result.toString();
	}

	@Override
	public List<TableMessage> getInventoryByBuilding(String buildingID) {
		log.warning("RECEIVED BUILDING ID: " + buildingID);
		Key<? extends Location> locationKey = ObjectifyService.factory().stringToKey(buildingID);
		List<InventoryItem> items = AllocationDAO.getInventoryByLocation(locationKey);
		List<TableMessage> result = new ArrayList<TableMessage>();
		result.add(getInventoryHeader());
		for(InventoryItem i : items)
			result.add(getInventoryDTO(i, null));
		return result;
	}

	@Override
	public List<TableMessage> getInventoryByProduct(String productID) {
		Key<ProductDetails> productKey = ObjectifyService.factory().stringToKey(productID);
		List<InventoryItem> items = AllocationDAO.getInventoryByProduct(productKey);
		List<TableMessage> result = new ArrayList<TableMessage>();
		result.add(getInventoryHeader());
		for(InventoryItem i : items)
			result.add(getInventoryDTO(i, null));
		return result;
	}

	@Override
	public List<TableMessage> getInventoryAllocations(String inventoryID) {
		ArrayList<TableMessage> result = new ArrayList<TableMessage>();
		result.add(getAllocationsHeader());
		Key<InventoryItem> ik = ObjectifyService.factory().stringToKey(inventoryID);
		List<InventoryAllocation> allocs = AllocationDAO.getAllocations(ik, ObjectifyService.begin());
		for(InventoryAllocation a : allocs)
			result.add(getAllocationDTO(a));
		return result;
	}
	
	public TableMessageHeader getProductsHeader()
	{
		TableMessageHeader h = new TableMessageHeader(3);
		h.setText(0, "Manufacturer", TableMessageContent.TEXT);
		h.setText(1, "Model", TableMessageContent.TEXT);
		h.setText(2, "Classification", TableMessageContent.TEXT);
		return h;
	}
	
	public TableMessageHeader getInventoryHeader()
	{		
		TableMessageHeader h = new TableMessageHeader(4);
		h.setText(0, "Building", TableMessageContent.TEXT);
		h.setText(1, "Product", TableMessageContent.TEXT);
		h.setText(2, "Price", TableMessageContent.NUMBER);
		h.setText(3, "Quantity", TableMessageContent.NUMBER);
		return h;
	}	
	
	public static TableMessageHeader getAllocationsHeader()
	{
	    TableMessageHeader h = new TableMessageHeader(9);
	    h.setText(0, "Serial Number", TableMessageContent.TEXT);
	    h.setText(1, "From", TableMessageContent.TEXT);
	    h.setText(2, "To", TableMessageContent.TEXT);
	    h.setText(3, "Effective Date", TableMessageContent.DATE);
	    h.setText(4, "Transfer Price", TableMessageContent.NUMBER);
	    h.setText(5, "Entry User", TableMessageContent.TEXT);
	    h.setText(6, "Entry Date", TableMessageContent.DATE);
	    h.setText(7, "Product", TableMessageContent.TEXT);
	    h.setText(8, "Transfer ID", TableMessageContent.NUMBER);
	    h.setFormatOption(8, "###");
	    return h;
	}

	@Override
	public List<TableMessage> getProductsTable() {
		List<ProductDetails> products = ObjectifyService.begin().query(ProductDetails.class).list();
		ArrayList<TableMessage> result = new ArrayList<TableMessage>();
		result.add(getProductsHeader());
		for(ProductDetails p : products)
			result.add(getProductDetailsAsTableMessage(p));
		return result;
	}

	@Override
	public List<TableMessage> getInventoryAllocations(String locationID,
			Date startDate, Date endDate) {
		ArrayList<TableMessage> result = new ArrayList<TableMessage>();
		result.add(getAllocationsHeader());
		Key<Location> lk = ObjectifyService.factory().stringToKey(locationID);
		Collection<InventoryAllocation> allocs = AllocationDAO.getAllocations(lk, startDate, endDate, ObjectifyService.begin());
		for(InventoryAllocation a : allocs)
			result.add(getAllocationDTO(a));
		return result;
	}	
}
