package com.fertiletech.fieldcodata.ui.client;


import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("inventory")
public interface InventoryService extends RemoteService {
	TableMessage getProductInfo(String manufacturer, String modelNumber);
	String saveProductInfo(TableMessage productInfo);
	HashMap<String, String> getProductDefinitiions();
	TableMessage getInventoryInfo(String buildingID, String productID);
	Set<String> getSerialNumbers(String inventoryID);
	String[] saveInventoryItems(TableMessage m, String ticketID);
	String moveInventory(String fromID, String toID, Set<String> serialNumbers, String ticketID, Double price, Date allocationDate);
	List<TableMessage> getInventoryByBuilding(String buildingID);
	List<TableMessage> getInventoryByProduct(String productID);
	List<TableMessage> getInventoryAllocations(String inventoryID);
	List<TableMessage> getInventoryAllocations(String locationID, Date startDate, Date endDate);
	List<TableMessage> getProductsTable();
}
