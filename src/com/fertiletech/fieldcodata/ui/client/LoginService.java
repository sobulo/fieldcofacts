package com.fertiletech.fieldcodata.ui.client;


import java.util.HashMap;
import java.util.List;

import com.fertiletech.fieldcodata.ui.shared.LoginInfo;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessageHeader;
import com.fertiletech.fieldcodata.ui.shared.exceptions.MissingEntitiesException;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("login")
public interface LoginService extends RemoteService {
	LoginInfo login(String requestUrl);
	LoginInfo loginForApplications(String requestUrl);
	String loadActivityComments(String parentKey);
	List<TableMessage> getRecentActivityComments();
	String loadRatings(String parentKey);
	void saveRatings(String parentKey, int rating, String text, boolean showPublic);
	void saveActivityComment(String loanKeyStr, String text, boolean showPublic);
	List<TableMessage> getApplicationParameter(String ID);
	void saveApplicationParameter(String id, HashMap<String, String> val) throws MissingEntitiesException;
	String getImageBlobID(String name);
	String fetchGenericExcelLink(List<TableMessage> data,
			TableMessageHeader header);
}
