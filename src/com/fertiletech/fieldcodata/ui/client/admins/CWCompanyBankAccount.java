package com.fertiletech.fieldcodata.ui.client.admins;

import com.fertiletech.fieldcodata.ui.client.ContentWidget;
import com.fertiletech.fieldcodata.ui.client.gui.HelpPageGenerator;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class CWCompanyBankAccount extends ContentWidget{

	public CWCompanyBankAccount() {
		super("Company Bank Accounts", "Warning! Do not use this module without being absolutely sure of what you're doing as this data " +
				"is exposed externally to comapny clients, e.g. when making deposit requests. This module allows you to specify bank accounts that residents pay into.");
	}

	@Override
	public Widget onInitialize() {
		return new EditCompanyBankAccount();
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
		GWT.runAsync(CWCompanyBankAccount.class, new RunAsyncCallback() {

			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}

			@Override
			public void onSuccess() {
				callback.onSuccess(onInitialize());
			}
		});	
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_ACCOUNTS_URL;
	}
	
	@Override
	public boolean hasScrollableContent()
	{
		return false;
	}	

}
