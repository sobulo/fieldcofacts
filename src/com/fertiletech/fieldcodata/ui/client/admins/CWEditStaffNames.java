package com.fertiletech.fieldcodata.ui.client.admins;

import com.fertiletech.fieldcodata.ui.shared.dto.DTOConstants;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class CWEditStaffNames extends CWEditParamsBase{

	public CWEditStaffNames() {
		super("Edit Staff Name", "Use this module to edit staff names. Unique ID should be the email address of staff in question. " +
				"If the person in question hasn't been assigned a company email, use firstname.lastname convention as unique identifer");
	}

	@Override
	protected boolean getShowValues() {
		return true;
	}

	@Override
	protected String getParameterID() {
		return DTOConstants.APP_PARAM_EMPLOYEE_LIST;
	}
	
	@Override
	protected void asyncOnInitialize(AsyncCallback<Widget> callback) {
		GWT.runAsync(CWEditStaffNames.class, super.getAsyncCall(callback));	
	}
	
	@Override
	protected boolean uniqueAsLowerCase() {
		return true;
	}
}
