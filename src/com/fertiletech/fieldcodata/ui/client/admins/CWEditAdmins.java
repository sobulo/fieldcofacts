package com.fertiletech.fieldcodata.ui.client.admins;

import com.fertiletech.fieldcodata.ui.shared.dto.DTOConstants;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class CWEditAdmins extends CWEditParamsBase{

	public CWEditAdmins() {
		super("Edit Admins", "Use this module to edit list of super admins in the database. Unique ID should be the email address of staff in question. " +
				"If the person in question hasn't been assigned a company email, use firstname.lastname convention. For description field, provide an explanation of why" +
				" this person is being made an administrator");
	}

	@Override
	protected boolean getShowValues() {
		return false;
	}

	@Override
	protected String getParameterID() {
		return DTOConstants.APP_PARAM_ADMINS;
	}

	@Override
	protected void asyncOnInitialize(AsyncCallback<Widget> callback) {
		GWT.runAsync(CWEditAdmins.class, super.getAsyncCall(callback));	
	}
	
	@Override
	protected boolean uniqueAsLowerCase() {
		return true;
	}
}
