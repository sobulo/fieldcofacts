package com.fertiletech.fieldcodata.ui.client.admins;

import java.util.List;

import com.fertiletech.fieldcodata.ui.client.gui.LocalDataProvider;
import com.fertiletech.fieldcodata.ui.client.gui.utils.SimpleDialog;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.ListBox;

public class ValuesListBox extends Composite implements HasValue<String>{
	private ListBox values;
	private AsyncCallback<List<TableMessage>> callback = new AsyncCallback<List<TableMessage>>() {

		@Override
		public void onFailure(Throwable caught) {
			values.addItem("ERROR");
			new SimpleDialog("ERROR LOADING VALUES").show(caught.getMessage());
		}

		@Override
		public void onSuccess(List<TableMessage> result) {
			result.remove(0); //remove header
			for(TableMessage m : result)
				values.addItem(m.getText(0));
			if(result.size() > 0)
				values.setEnabled(true);
		}
	};
	
	public ValuesListBox() {
		values = new ListBox();
		initWidget(values);
	}
	
	public ValuesListBox loadBox(String id)
	{
		values.clear();
		values.setEnabled(false);
		LocalDataProvider.COMMENT_SERVICE.getApplicationParameter(id, callback);
		return this;
	}
	
	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<String> handler) {
		return addHandler(handler, ValueChangeEvent.getType());
	}

	@Override
	public String getValue() {
		if(values.getItemCount() == 0 || !values.isEnabled())
			return null;
		return values.getValue(values.getSelectedIndex());
	}

	@Override
	public void setValue(String value) {
		if(values.getItemCount() > 0)
			values.setSelectedIndex(0);
		
		for(int i = 1; i < values.getItemCount(); i++)
			if(values.getValue(i).equals(value))
				values.setSelectedIndex(i);
	}

	@Override
	public void setValue(String value, boolean fireEvents) {
		setValue(value);
		ValueChangeEvent.fire(this, value);
	}
}
