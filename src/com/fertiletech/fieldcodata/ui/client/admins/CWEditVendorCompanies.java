package com.fertiletech.fieldcodata.ui.client.admins;

import com.fertiletech.fieldcodata.ui.shared.dto.DTOConstants;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class CWEditVendorCompanies extends CWEditParamsBase{
	public CWEditVendorCompanies() {
		super("Register Vendor Company", "Use this module to register a vendor company. Unique ID should be the company name and description should" +
				" contain a brief description of the company. You will still need to enter the vendor's contact information under the edit vendor module.");
	}

	@Override
	protected boolean getShowValues() {
		return false;
	}

	@Override
	protected String getParameterID() {
		return DTOConstants.APP_PARAM_VENDOR_COMPANY_LIST_KEY;
	}
	
	@Override
	protected void asyncOnInitialize(AsyncCallback<Widget> callback) {
		GWT.runAsync(CWEditVendorCompanies.class, super.getAsyncCall(callback));	
	}
}
