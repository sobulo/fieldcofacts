package com.fertiletech.fieldcodata.ui.client.admins;

import com.fertiletech.fieldcodata.ui.client.ContentWidget;
import com.fertiletech.fieldcodata.ui.client.gui.HelpPageGenerator;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public abstract class CWEditParamsBase extends ContentWidget{

	public CWEditParamsBase(String name, String description) {
		super(name, description);
	}
	
	protected abstract boolean getShowValues();
	protected abstract String getParameterID();
	
	protected String getDropDownID()
	{
		return null;
	}
	
	protected boolean uniqueAsLowerCase()
	{
		return false;
	}
	
	@Override
	public Widget onInitialize() {
		return new EditAppParameters(getParameterID(), getShowValues(), uniqueAsLowerCase(), getDropDownID());
	}

	protected RunAsyncCallback getAsyncCall(final AsyncCallback<Widget> callback) {
	    return new RunAsyncCallback() {

	        public void onFailure(Throwable caught) {
	          callback.onFailure(caught);
	        }

	        public void onSuccess() {
	          callback.onSuccess(onInitialize());
	        }
	      };
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_ADMIN_URL;
	}
	
	@Override
	public boolean hasScrollableContent()
	{
		return false;
	}

}
