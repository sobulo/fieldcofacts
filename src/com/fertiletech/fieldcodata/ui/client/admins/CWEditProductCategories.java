package com.fertiletech.fieldcodata.ui.client.admins;

import com.fertiletech.fieldcodata.ui.client.gui.HelpPageGenerator;
import com.fertiletech.fieldcodata.ui.shared.dto.DTOConstants;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class CWEditProductCategories extends CWEditParamsBase{

	public CWEditProductCategories() {
		super("Edit Product Categories", "Use this module to setup categories for products in the inventory database");
	}

	@Override
	protected boolean getShowValues() {
		return false;
	}

	@Override
	protected String getParameterID() {
		return DTOConstants.APP_PARAM_PRODUCT_CATEGORY_KEY;
	}

	@Override
	protected void asyncOnInitialize(AsyncCallback<Widget> callback) {
		GWT.runAsync(CWEditProductCategories.class, super.getAsyncCall(callback));
	}
	
	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_VENDORS_URL;
	}	
}