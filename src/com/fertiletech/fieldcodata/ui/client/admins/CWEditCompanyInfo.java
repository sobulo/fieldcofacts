package com.fertiletech.fieldcodata.ui.client.admins;
import com.fertiletech.fieldcodata.ui.shared.dto.DTOConstants;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class CWEditCompanyInfo extends CWEditParamsBase{

	public CWEditCompanyInfo() {
		super("Edit Companies", "Use this module to edit list of companies that residents are employed at. Unique ID should be the company name and description should" +
				" contain additional information such as contact information");
	}

	@Override
	protected boolean getShowValues() {
		return false;
	}

	@Override
	protected String getParameterID() {
		return DTOConstants.APP_PARAM_COMPANY_LIST_KEY;
	}
	
	@Override
	protected void asyncOnInitialize(AsyncCallback<Widget> callback) {
		GWT.runAsync(CWEditCompanyInfo.class, super.getAsyncCall(callback));	
	}	
}
