package com.fertiletech.fieldcodata.ui.client.admins;

import com.fertiletech.fieldcodata.ui.client.gui.HelpPageGenerator;
import com.fertiletech.fieldcodata.ui.shared.dto.DTOConstants;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class CWEditVendorCategories extends CWEditParamsBase{
	public CWEditVendorCategories() {
		super("Edit Vendor Categories", "Use this module to add or remove categories used in the vendor database");
	}

	@Override
	protected boolean getShowValues() {
		return false;
	}

	@Override
	protected String getParameterID() {
		return DTOConstants.APP_PARAM_VENDOR_CATEGORY_LIST_KEY;
	}
	
	@Override
	protected void asyncOnInitialize(AsyncCallback<Widget> callback) {
		GWT.runAsync(CWEditVendorCategories.class, super.getAsyncCall(callback));	
	}
	
	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_VENDORS_URL;
	}	
}
