package com.fertiletech.fieldcodata.ui.client;


import java.util.HashMap;
import java.util.List;

import com.fertiletech.fieldcodata.ui.shared.LoginInfo;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessageHeader;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The async counterpart of <code>GreetingService</code>.
 */
public interface LoginServiceAsync {
	void login(String requestUrl, AsyncCallback<LoginInfo> callback);

	void loginForApplications(String requestUrl,
			AsyncCallback<LoginInfo> callback);

	void loadActivityComments(String parentKey, AsyncCallback<String> callback);

	void saveActivityComment(String loanKeyStr, String text,
			boolean showPublic, AsyncCallback<Void> callback);

	void loadRatings(String parentKey, AsyncCallback<String> callback);

	void saveRatings(String parentKey, int rating, String text,
			boolean showPublic, AsyncCallback<Void> callback);

	void getApplicationParameter(String ID,
			AsyncCallback<List<TableMessage>> callback);

	void saveApplicationParameter(String id, HashMap<String, String> val,
			AsyncCallback<Void> callback);

	void fetchGenericExcelLink(List<TableMessage> data, TableMessageHeader header,
			AsyncCallback<String> callback);

	void getRecentActivityComments(AsyncCallback<List<TableMessage>> callback);

	void getImageBlobID(String name, AsyncCallback<String> callback);
}
