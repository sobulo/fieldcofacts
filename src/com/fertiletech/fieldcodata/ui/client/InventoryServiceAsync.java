package com.fertiletech.fieldcodata.ui.client;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.google.gwt.user.client.rpc.AsyncCallback;


public interface InventoryServiceAsync {

	void getProductInfo(String manufacturer, String modelNumber,
			AsyncCallback<TableMessage> callback);

	void saveProductInfo(TableMessage productInfo,
			AsyncCallback<String> callback);

	void getProductDefinitiions(AsyncCallback<HashMap<String, String>> callback);

	void getInventoryInfo(String buildingID, String productID,
			AsyncCallback<TableMessage> callback);

	void getSerialNumbers(String inventoryID,
			AsyncCallback<Set<String>> callback);

	void saveInventoryItems(TableMessage m, String ticketID, AsyncCallback<String[]> callback);

	void moveInventory(String fromID, String toID, Set<String> serialNumbers,
			String ticketID, Double price, Date allocationDate,
			AsyncCallback<String> callback);

	void getInventoryByBuilding(String buildingID,
			AsyncCallback<List<TableMessage>> callback);

	void getInventoryByProduct(String productID,
			AsyncCallback<List<TableMessage>> callback);

	void getInventoryAllocations(String inventoryID,
			AsyncCallback<List<TableMessage>> callback);

	void getProductsTable(AsyncCallback<List<TableMessage>> callback);

	void getInventoryAllocations(String locationID, Date startDate,
			Date endDate, AsyncCallback<List<TableMessage>> callback);
}
