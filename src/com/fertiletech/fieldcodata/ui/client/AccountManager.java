
package com.fertiletech.fieldcodata.ui.client;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;

import com.fertiletech.fieldcodata.ui.shared.dto.BillDescriptionItem;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.fertiletech.fieldcodata.ui.shared.exceptions.DuplicateEntitiesException;
import com.fertiletech.fieldcodata.ui.shared.exceptions.LoginValidationException;
import com.fertiletech.fieldcodata.ui.shared.exceptions.ManualVerificationException;
import com.fertiletech.fieldcodata.ui.shared.exceptions.MissingEntitiesException;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * @author Segun Razaq Sobulo
 */
@RemoteServiceRelativePath("accounts")
public interface AccountManager extends RemoteService {	
	public HashMap<Long, String> getAllBillIDs();
	public List<TableMessage> getAllAccounts();
	public String createDeposit(String companyAcctStr,
			Date depositDate, String referenceNumber, String comments,
			String acctStr, double initialAmount);
	
	TableMessage createAccount(String bank, String sortCode, String acctName,
			String acctNumber, String currency) throws DuplicateEntitiesException;
	TableMessage updateAccount(String accId, String bank, String sortCode, String acctName,
			String acctNumber, String currency) throws MissingEntitiesException;
	
	public List<TableMessage> getDeposits(Date startDate, Date endDate);
	public List<TableMessage> getDeposits(String tenantID);
	public List<TableMessage> getBills(Date startDate, Date endDate) throws MissingEntitiesException;	
	public String createUserBill(String[] users, LinkedHashSet<BillDescriptionItem> bdiList,
			String type, Date start, Date end, Date due, String ticketID, String ccy) throws DuplicateEntitiesException;
	
	public List<TableMessage> getStudentBills(String studentID) throws LoginValidationException;
	
	public HashMap<String, String> getStudentBillKeys(String studentID, boolean isLogin) throws LoginValidationException;
	
	public HashMap<String, String> getStudentBillKeys() throws LoginValidationException;
		
	String savePayment(String billKey, String depositKey, double amount,
			Date payDate, String comments) throws MissingEntitiesException, ManualVerificationException;
		
	public List<TableMessage> getStudentBillAndPayment(String billKeyStr) throws MissingEntitiesException, LoginValidationException;
	
	public List<TableMessage> getBillPayments(String billKeyStr) throws MissingEntitiesException, LoginValidationException;
	
	String getInvoiceDownloadLink(String[] billIDs) throws MissingEntitiesException, LoginValidationException;
	String getDepositStatementDownloadLink(String contactID, String[] depositIDs, Date start,
			Date end) throws MissingEntitiesException;
	TableMessage createCompanyRep(String firstName, String lastName, Date dob,
			String primaryEmail, String primaryNumber,
			HashSet<String> secondaryNumbers, HashSet<String> secondaryEmails,
			String companyName, String address, Boolean isMale, String title) throws DuplicateEntitiesException;
	List<TableMessage> getAllReps();
	List<TableMessage> getCompanyTenants(String repID);
	List<TableMessage> saveCompanyTenants(String[] tenantIDs, String repID);
	List<TableMessage> removeCompanyTenants(String[] tenantIDs, String repID);
	TableMessage createResidentAccount(String tenantID, String acctType, boolean allowOverDraft, String currency, int depositLevel) throws DuplicateEntitiesException;
	TableMessage getResidentAccountInfo(String tenantID, String acctType,
			String ccy);
	List<TableMessage> getResidentAccountInfo(String tenantID);
	List<TableMessage> getLedgerAccountEntries(String acctID, Date startDate, Date endDate);
	List<TableMessage> getLedgerResidentAcctEntries(String tenantID, Date startDate, Date endDate);
	List<TableMessage> getResidentAccountsByBuilding(String buildingId) throws MissingEntitiesException;
	String getDepositRequestLink(String[] companyAcctId, Date invDate, String description, List<TableMessage> requests);
	TableMessage updateResidentAccount(String acctID, boolean allowOverDraft, int depositLevel) throws MissingEntitiesException;
}
