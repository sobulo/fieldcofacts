package com.fertiletech.fieldcodata.ui.client;


import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.fertiletech.fieldcodata.ui.shared.exceptions.DuplicateEntitiesException;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("vendor")
public interface VendorService extends RemoteService {
	List<TableMessage> getAllVendors();
	TableMessage createVendor(String category, String firstName,
			String lastName, Date dob, String primaryEmail, String primaryNumber,
			HashSet<String> secondaryNumbers, HashSet<String> secondaryEmails,
			String companyName, String address, Boolean isMale, String title, boolean throwDuplicateException) throws DuplicateEntitiesException;
	TableMessage changeVendorCategory(String vendorID, String category);
	HashMap<String, String> getAllVendorsIDs();
}
