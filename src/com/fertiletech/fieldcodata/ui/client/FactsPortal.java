package com.fertiletech.fieldcodata.ui.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import com.google.gwt.user.client.ui.RootPanel;


/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class FactsPortal implements EntryPoint {
	public final String STAFF = "staff";
	public final String RESIDENTS = "tenant";
	public final String DASHBOARD = "dash";
	
	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() 
	{
		RootPanel rootPanel = RootPanel.get(STAFF);
		if( rootPanel != null )
		{
			setupStaff(rootPanel);
			return;
		}
		
		rootPanel = RootPanel.get(RESIDENTS);		
		if(rootPanel != null)
		{
			setupTenant();
			return;
		}
		
		rootPanel = RootPanel.get(DASHBOARD);
		if(rootPanel != null)
		{
			setupDashboard();
			return;
		}			
		Window.Location.replace("http://www.fieldcolimited.com");
	}
	
	
	public void setupStaff(final RootPanel p)
	{
		GWT.runAsync(new RunAsyncCallback() {
			
			@Override
			public void onSuccess() {
				new Showcase(p); //this makes a call to RootLayoutPanel
				
			}
			
			@Override
			public void onFailure(Throwable reason) {
				Window.alert("Code download failed, try refreshing the page." +
						" Please contact info@fertiletech.com if problem persists");				
			}
		});
	}	
	
	public void setupTenant()
	{
		GWT.runAsync(new RunAsyncCallback() {
			
			@Override
			public void onSuccess() {
				RootLayoutPanel.get().add(new HTML("Tenants Database Not Yet Deployed. " +
						"Pending Google Apps License Registration for all Apartments"));
			}
			
			@Override
			public void onFailure(Throwable reason) {
				Window.alert("Code download failed, try refreshing the page." +
						" Please contact info@fertiletech.com if problem persists");
				
			}
		});		
	}
	
	public void setupDashboard()
	{
		GWT.runAsync(new RunAsyncCallback() {
			
			@Override
			public void onSuccess() {
				RootLayoutPanel.get().add(new HTML("Dashboard Not in Scope but Deployable. " +
						"Pending Flat Screen Setup in Board Room or MD's office"));
			}
			
			@Override
			public void onFailure(Throwable reason) {
				Window.alert("Code download failed, try refreshing the page." +
						" Please contact info@fertiletech.com if problem persists");
				
			}
		});		
	}
}
