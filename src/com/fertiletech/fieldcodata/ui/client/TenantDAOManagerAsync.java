/**
 * 
 */
package com.fertiletech.fieldcodata.ui.client;


import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import com.fertiletech.fieldcodata.ui.shared.UtilConstants.ContactTypes;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * @author Segun Razaq Sobulo
 *
 */
public interface TenantDAOManagerAsync {

	void getBuildingNames(AsyncCallback<HashMap<String, String>> callback);
	void createNewTenant(HashMap<String, String> prospectiveData, TableMessage m,
			AsyncCallback<TableMessage> callback);
	void changeApartmentInfo(String tenantID, String apartmentID,
			String comments, AsyncCallback<TableMessage> callback);
	void getBuildingTenants(String buildingID, Boolean archive,
			AsyncCallback<List<TableMessage>> callback);
	void getBuildingApartmentTable(String buildingID,
			AsyncCallback<List<TableMessage>> callback);	
	void getAllProspects(AsyncCallback<HashMap<String, String>> callback);
	void getAllTenants(AsyncCallback<HashMap<String, String>> callback);
	void getProspectiveData(String prospectiveId,
			AsyncCallback<HashMap<String, String>> callback);
	void saveProspectiveData(String prospectId,
			HashMap<String, String> supplementary,
			AsyncCallback<String> callback);
	void createContact(ContactTypes t, String firstName, String lastName,
			Date dob, String primaryEmail, String primaryNumber,
			HashSet<String> secondaryNumbers, HashSet<String> secondaryEmails,
			String companyName, String tenantAddress, Boolean isMale,
			String title, String[] args, AsyncCallback<String[]> callback);
	void getContact(String contactKeyStr, AsyncCallback<TableMessage> callback);
	void getApartments(String buildingID,
			AsyncCallback<HashMap<String, String>[]> callback);
	void updateContact(String contactID, String firstName, String lastName,
			Date dob, String primaryEmail, String primaryNumber,
			HashSet<String> secondaryNumbers, HashSet<String> secondaryEmails,
			String companyName, String tenantAddress, Boolean isMale,
			String title, String[] args, AsyncCallback<TableMessage> callback);
	void getAllBuildingAndApartments(AsyncCallback<HashMap<String, String>> callback);
	void getBuildingApartments(String buildingID,
			AsyncCallback<HashMap<String, String>> callback);
	void getProspectsTable(Date start, Date end, AsyncCallback<List<TableMessage>> callback);
	void getTenantRelations(String tenantID,
			AsyncCallback<List<TableMessage>> callback);
	void getTenantContacts(
			AsyncCallback<HashMap<String, List<TableMessage>>> callback);
	void getResident(String locationID, AsyncCallback<TableMessage> callback);
	void getLeases(String tenantID, AsyncCallback<List<TableMessage>> callback);
	void archiveTenant(String tenantID, String comments,
			AsyncCallback<TableMessage> callback);
	void removeLease(String leaseID, String comments,
			AsyncCallback<String> callback);
	void saveLease(TableMessage m, String comments,
			AsyncCallback<TableMessage> callback);
	void getLeases(Date startDate, Date endDate,
			AsyncCallback<List<TableMessage>> callback);
	
}
