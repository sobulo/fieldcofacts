/*
 * Copyright 2010 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.fertiletech.fieldcodata.ui.client;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.fertiletech.fieldcodata.ui.client.admins.CWCompanyBankAccount;
import com.fertiletech.fieldcodata.ui.client.admins.CWEditAdmins;
import com.fertiletech.fieldcodata.ui.client.admins.CWEditCompanyInfo;
import com.fertiletech.fieldcodata.ui.client.admins.CWEditProductCategories;
import com.fertiletech.fieldcodata.ui.client.admins.CWEditResidentAccountTypes;
import com.fertiletech.fieldcodata.ui.client.admins.CWEditResidentBillTypes;
import com.fertiletech.fieldcodata.ui.client.admins.CWEditStaffNames;
import com.fertiletech.fieldcodata.ui.client.admins.CWEditVendorCategories;
import com.fertiletech.fieldcodata.ui.client.admins.CWEditVendorCompanies;
import com.fertiletech.fieldcodata.ui.client.admins.CWLogoUpload;
import com.fertiletech.fieldcodata.ui.client.gui.WelcomePanel;
import com.fertiletech.fieldcodata.ui.client.gui.accounts.AddBillTemplate;
import com.fertiletech.fieldcodata.ui.client.gui.accounts.AddTenantPayment;
import com.fertiletech.fieldcodata.ui.client.gui.accounts.CWAddTenantDeposit;
import com.fertiletech.fieldcodata.ui.client.gui.accounts.CWEditResidentAccount;
import com.fertiletech.fieldcodata.ui.client.gui.accounts.CWRABlotter;
import com.fertiletech.fieldcodata.ui.client.gui.accounts.CWResidentAccount;
import com.fertiletech.fieldcodata.ui.client.gui.accounts.CWSearchInvoice;
import com.fertiletech.fieldcodata.ui.client.gui.accounts.CWViewDepositBlotter;
import com.fertiletech.fieldcodata.ui.client.gui.accounts.ViewBillBlotter;
import com.fertiletech.fieldcodata.ui.client.gui.accounts.print.CWDepositStatements;
import com.fertiletech.fieldcodata.ui.client.gui.accounts.print.CWPrintDepositRequest;
import com.fertiletech.fieldcodata.ui.client.gui.accounts.print.PrintInvoices;
import com.fertiletech.fieldcodata.ui.client.gui.accounts.reps.CWEditCompanyRep;
import com.fertiletech.fieldcodata.ui.client.gui.accounts.reps.CWEditCompanyResidents;
import com.fertiletech.fieldcodata.ui.client.gui.inventory.CWAllocationPanel;
import com.fertiletech.fieldcodata.ui.client.gui.inventory.CWEditInventoryPanel;
import com.fertiletech.fieldcodata.ui.client.gui.inventory.CWEditProductPanel;
import com.fertiletech.fieldcodata.ui.client.gui.inventory.CWInventoryAllocationHistory;
import com.fertiletech.fieldcodata.ui.client.gui.inventory.CWInventoryView;
import com.fertiletech.fieldcodata.ui.client.gui.inventory.CWProductsView;
import com.fertiletech.fieldcodata.ui.client.gui.leases.CWLeaseAgreement;
import com.fertiletech.fieldcodata.ui.client.gui.leases.CWLeaseBlotter;
import com.fertiletech.fieldcodata.ui.client.gui.mrs.CWUnAttached;
import com.fertiletech.fieldcodata.ui.client.gui.mrs.CWViewTickets;
import com.fertiletech.fieldcodata.ui.client.gui.mrs.EditTicket;
import com.fertiletech.fieldcodata.ui.client.gui.prospects.CWEditProspects;
import com.fertiletech.fieldcodata.ui.client.gui.prospects.CWLeaseConfirmation;
import com.fertiletech.fieldcodata.ui.client.gui.prospects.CWViewProspects;
import com.fertiletech.fieldcodata.ui.client.gui.tenants.BulkTenantAndApartmentUploadPanel;
import com.fertiletech.fieldcodata.ui.client.gui.tenants.CWAddRelationship;
import com.fertiletech.fieldcodata.ui.client.gui.tenants.CWViewTenants;
import com.fertiletech.fieldcodata.ui.client.gui.tenants.CwCellList;
import com.fertiletech.fieldcodata.ui.client.gui.tenants.CwCustomDataGrid;
import com.fertiletech.fieldcodata.ui.client.gui.vendors.BulkVendorUploadPanel;
import com.fertiletech.fieldcodata.ui.client.gui.vendors.CWCategorizeVendors;
import com.fertiletech.fieldcodata.ui.client.gui.vendors.CWEditVendor;
import com.fertiletech.fieldcodata.ui.client.gui.vendors.CWRateVendor;
import com.fertiletech.fieldcodata.ui.client.gui.vendors.CWViewVendor;
import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.core.client.prefetch.RunAsyncCode;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.SelectionModel;
import com.google.gwt.view.client.TreeViewModel;

/**
 * The {@link TreeViewModel} used by the main menu.
 */
public class MainMenuTreeViewModel implements TreeViewModel {

  /**
   * The cell used to render categories.
   */
  private static class CategoryCell extends AbstractCell<Category> {
    @Override
    public void render(Context context, Category value, SafeHtmlBuilder sb) {
      if (value != null) {
        sb.appendEscaped(value.getName());
      }
    }
  }

  /**
   * The cell used to render examples.
   */
  private static class ContentWidgetCell extends AbstractCell<ContentWidget> {
    @Override
    public void render(Context context, ContentWidget value, SafeHtmlBuilder sb) {
      if (value != null) {
        sb.appendEscaped(value.getName());
      }
    }
  }

  /**
   * A top level category in the tree.
   */
  public class Category {

    private final ListDataProvider<ContentWidget> examples =
        new ListDataProvider<ContentWidget>();
    private final String name;
    private NodeInfo<ContentWidget> nodeInfo;
    private final List<RunAsyncCode> splitPoints =
        new ArrayList<RunAsyncCode>();

    public Category(String name) {
      this.name = name;
    }

    public void addExample(ContentWidget example, String style, RunAsyncCode splitPoint) {
    	if(style != null)
    		example.addStyleName(style);
      examples.getList().add(example);
      if (splitPoint != null) {
        splitPoints.add(splitPoint);
      }
      contentCategory.put(example, this);
      contentToken.put(Showcase.getContentWidgetToken(example), example);
    }

    public String getName() {
      return name;
    }

    /**
     * Get the node info for the examples under this category.
     * 
     * @return the node info
     */
    public NodeInfo<ContentWidget> getNodeInfo() {
      if (nodeInfo == null) {
        nodeInfo = new DefaultNodeInfo<ContentWidget>(examples,
            contentWidgetCell, selectionModel, null);
      }
      return nodeInfo;
    }

    /**
     * Get the list of split points to prefetch for this category.
     * 
     * @return the list of classes in this category
     */
    public Iterable<RunAsyncCode> getSplitPoints() {
      return splitPoints;
    }
  }

  /**
   * The top level categories.
   */
  private final ListDataProvider<Category> categories = new ListDataProvider<Category>();

  /**
   * A mapping of {@link ContentWidget}s to their associated categories.
   */
  private final Map<ContentWidget, Category> contentCategory = new HashMap<ContentWidget, Category>();

  /**
   * The cell used to render examples.
   */
  private final ContentWidgetCell contentWidgetCell = new ContentWidgetCell();

  /**
   * A mapping of history tokens to their associated {@link ContentWidget}.
   */
  private final Map<String, ContentWidget> contentToken = new HashMap<String, ContentWidget>();

  /**
   * The selection model used to select examples.
   */
  private final SelectionModel<ContentWidget> selectionModel;

  public MainMenuTreeViewModel(SelectionModel<ContentWidget> selectionModel) {
    this.selectionModel = selectionModel;
    initializeTree();
  }

  /**
   * Get the {@link Category} associated with a widget.
   * 
   * @param widget the {@link ContentWidget}
   * @return the associated {@link Category}
   */
  public Category getCategoryForContentWidget(ContentWidget widget) {
    return contentCategory.get(widget);
  }

  /**
   * Get the content widget associated with the specified history token.
   * 
   * @param token the history token
   * @return the associated {@link ContentWidget}
   */
  public ContentWidget getContentWidgetForToken(String token) {
    return contentToken.get(token);
  }

  public <T> NodeInfo<?> getNodeInfo(T value) {
    if (value == null) {
      // Return the top level categories.
      return new DefaultNodeInfo<Category>(categories, new CategoryCell());
    } else if (value instanceof Category) {
      // Return the examples within the category.
      Category category = (Category) value;
      return category.getNodeInfo();
    }
    return null;
  }

  public boolean isLeaf(Object value) {
    return value != null && !(value instanceof Category);
  }

  /**
   * Get the set of all {@link ContentWidget}s used in the model.
   * 
   * @return the {@link ContentWidget}s
   */
  Set<ContentWidget> getAllContentWidgets() {
    Set<ContentWidget> widgets = new HashSet<ContentWidget>();
    for (Category category : categories.getList()) {
      for (ContentWidget example : category.examples.getList()) {
        widgets.add(example);
      }
    }
    return widgets;
  }

  /**
   * Initialize the top level categories in the tree.
   */
  private void initializeTree() {
    List<Category> catList = categories.getList();
    //Welcome
    {
      Category category = new Category("Welcome");
      catList.add(category);
      // CwCheckBox is the default example, so don't prefetch it.
      category.addExample(new WelcomePanel(),"homeWatermark", null);
    }

    //prospective
    {
    	Category category = new Category("Prospects");
    	catList.add(category);
        category.addExample(new CWEditProspects(), "prospectiveWatermark",
                RunAsyncCode.runAsyncCode(CWEditProspects.class));
        category.addExample(new CWViewProspects(), "prospectiveWatermark",
                RunAsyncCode.runAsyncCode(CWViewProspects.class));      
    }
    
    //leases
    {
    	Category category = new Category("Lease Agreements");
    	catList.add(category);
        category.addExample(new CWLeaseConfirmation(), "prospectiveWatermark",
                RunAsyncCode.runAsyncCode(CWLeaseConfirmation.class));
    	category.addExample(new CWLeaseAgreement(), "prospectiveWatermark",
    			RunAsyncCode.runAsyncCode(CWLeaseAgreement.class));
        category.addExample(new CWLeaseBlotter(), "prospectiveWatermark",
                RunAsyncCode.runAsyncCode(CWLeaseBlotter.class));      	
    }

    //Tenant Contacts
    {
      Category category = new Category("Resident Bio");
      catList.add(category);
      category.addExample(new CwCellList(), "bioWatermark",
              RunAsyncCode.runAsyncCode(CwCellList.class));
      category.addExample(new CWAddRelationship(), "bioWatermark",
              RunAsyncCode.runAsyncCode(CWAddRelationship.class));      
      category.addExample(new CWViewTenants(), "bioWatermark",
              RunAsyncCode.runAsyncCode(CWViewTenants.class));
      category.addExample(new CwCustomDataGrid(), "bioWatermark",
              RunAsyncCode.runAsyncCode(CwCustomDataGrid.class));
      
    }    
    
    //Resident Billing
    {
    	Category category = new Category("Resident Billing");
    	catList.add(category);
        category.addExample(new AddBillTemplate(), "billingWatermark",
                RunAsyncCode.runAsyncCode(AddBillTemplate.class));
        category.addExample(new CWSearchInvoice(), "billingWatermark", RunAsyncCode.runAsyncCode(CWSearchInvoice.class));
        category.addExample(new ViewBillBlotter(), "billingWatermark",
                RunAsyncCode.runAsyncCode(ViewBillBlotter.class));      
        category.addExample(new PrintInvoices(), "billingWatermark",
                RunAsyncCode.runAsyncCode(PrintInvoices.class));
        category.addExample(new CWPrintDepositRequest(), "billingWatermark", RunAsyncCode.runAsyncCode(CWPrintDepositRequest.class));
    }    
    
    //Resident Accounts
    {
    	Category category = new Category("Resident Accounts");
    	catList.add(category);	
        category.addExample(new AddTenantPayment(), "billingWatermark",
                RunAsyncCode.runAsyncCode(AddTenantPayment.class));          	
    	category.addExample(new CWAddTenantDeposit(), "billingWatermark",
    			RunAsyncCode.runAsyncCode(CWAddTenantDeposit.class));    	
        category.addExample(new CWRABlotter(), "billingWatermark",
                RunAsyncCode.runAsyncCode(CWRABlotter.class));          
        category.addExample(new CWDepositStatements(), "billingWatermark",
                RunAsyncCode.runAsyncCode(CWDepositStatements.class));
        category.addExample(new CWViewDepositBlotter(), "billingWatermark",
                RunAsyncCode.runAsyncCode(CWViewDepositBlotter.class));             
    }
    
    //Accounts Config
    {
    	Category category = new Category("Accounts Configuration ");
    	catList.add(category);	
    	category.addExample(new CWEditCompanyRep(), "billingWatermark",
    			RunAsyncCode.runAsyncCode(CWEditCompanyRep.class));
        category.addExample(new CWEditCompanyResidents(), "billingWatermark",
                RunAsyncCode.runAsyncCode(CWEditCompanyResidents.class));      	
    	category.addExample(new CWResidentAccount(), "billingWatermark",
    			RunAsyncCode.runAsyncCode(CWResidentAccount.class));      	
    	category.addExample(new CWEditResidentAccount(), "billingWatermark",
    			RunAsyncCode.runAsyncCode(CWEditResidentAccount.class));      	 	    	
	    category.addExample(new CWEditResidentAccountTypes(), "billingWatermark",
	              RunAsyncCode.runAsyncCode(CWEditResidentAccountTypes.class));
	    category.addExample(new CWEditResidentBillTypes(), "billingWatermark",
	              RunAsyncCode.runAsyncCode(CWEditResidentBillTypes.class));	    
	    category.addExample(new CWCompanyBankAccount(), "billingWatermark",
	              RunAsyncCode.runAsyncCode(CWCompanyBankAccount.class));	    
    }
    

    //Vendors
    {
    	Category category = new Category("ICESTONE");
    	catList.add(category);
    	category.addExample(new CWEditVendorCompanies(), "vendorWatermark",
                RunAsyncCode.runAsyncCode(CWEditVendorCompanies.class));    	
    	category.addExample(new CWEditVendor(), "vendorWatermark",
              RunAsyncCode.runAsyncCode(CWEditVendor.class));      
    	category.addExample(new CWCategorizeVendors(), "vendorWatermark",
                RunAsyncCode.runAsyncCode(CWCategorizeVendors.class));   
    	category.addExample(new CWViewVendor(), "vendorWatermark",
              RunAsyncCode.runAsyncCode(CWViewVendor.class));
    	category.addExample(new CWRateVendor(), "vendorWatermark",
              RunAsyncCode.runAsyncCode(CWRateVendor.class));         
	    category.addExample(new CWEditVendorCategories(), "vendorWatermark",
	              RunAsyncCode.runAsyncCode(CWEditVendorCategories.class));	        	
    	category.addExample(new CWEditProductCategories(), "vendorWatermark",
                RunAsyncCode.runAsyncCode(CWEditProductCategories.class));
    }    
    
    //Inventory
    {
	    Category category = new Category("Building Inventory");
	    catList.add(category);
    	category.addExample(new CWEditProductPanel(), "inventoryWatermark",
    			RunAsyncCode.runAsyncCode(CWEditProductPanel.class));
    	category.addExample(new CWEditInventoryPanel(), "inventoryWatermark",
    			RunAsyncCode.runAsyncCode(CWEditInventoryPanel.class));
    	category.addExample(new CWAllocationPanel(), "inventoryWatermark",
    			RunAsyncCode.runAsyncCode(CWAllocationPanel.class));
    	category.addExample(new CWProductsView(), "inventoryWatermark",
    			RunAsyncCode.runAsyncCode(CWProductsView.class));
    	category.addExample(new CWInventoryView(), "inventoryWatermark",
    			RunAsyncCode.runAsyncCode(CWInventoryView.class));
    	category.addExample(new CWInventoryAllocationHistory(), "inventoryWatermark",
    			RunAsyncCode.runAsyncCode(CWInventoryAllocationHistory.class));    	
    }
    
    //MRS
    {
	    Category category = new Category("Maintenance Requests");
	    catList.add(category);
	    category.addExample(new EditTicket(), "maintenanceWatermark",
	              RunAsyncCode.runAsyncCode(EditTicket.class));	    
	    category.addExample(new CWViewTickets(), "maintenanceWatermark",
	              RunAsyncCode.runAsyncCode(CWViewTickets.class));
	    category.addExample(new CWUnAttached(), "maintenanceWatermark",
	              RunAsyncCode.runAsyncCode(CWUnAttached.class));
    }
    
    //Admins
    {
	    Category category = new Category("General Configuration");
	    catList.add(category);
	    category.addExample(new CWEditCompanyInfo(), "adminWatermark",
	              RunAsyncCode.runAsyncCode(CWEditCompanyInfo.class));
	    category.addExample(new CWEditStaffNames(), "adminWatermark",
	              RunAsyncCode.runAsyncCode(CWEditStaffNames.class));	    	    
	    category.addExample(new CWEditAdmins(), "adminWatermark",
	              RunAsyncCode.runAsyncCode(CWEditAdmins.class));
	    category.addExample(new CWLogoUpload(), "adminWatermark",
	              RunAsyncCode.runAsyncCode(CWLogoUpload.class));	    	    
	    category.addExample(new BulkTenantAndApartmentUploadPanel(), "adminWatermark",
	              RunAsyncCode.runAsyncCode(BulkTenantAndApartmentUploadPanel.class));	    
	    category.addExample(new BulkVendorUploadPanel(), "adminWatermark",
	              RunAsyncCode.runAsyncCode(BulkVendorUploadPanel.class));
    }    
  }
  
  public ContentWidget setBackgroundStyle(ContentWidget w, String styl)
  {
	  w.addStyleName(styl);
	  return w;
  }
}
