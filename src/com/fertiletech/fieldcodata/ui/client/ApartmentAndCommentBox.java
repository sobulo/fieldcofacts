package com.fertiletech.fieldcodata.ui.client;

import com.fertiletech.fieldcodata.ui.client.gui.utils.ApartmentDropDown;
import com.fertiletech.fieldcodata.ui.client.gui.utils.UserCommentPopup;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class ApartmentAndCommentBox extends UserCommentPopup{

	ApartmentDropDown apartmentSelector;
	public ApartmentAndCommentBox(String title) {
		super(title);
	}
	
	@Override
	protected Widget getButtonPanel() {
		apartmentSelector = new ApartmentDropDown();
		VerticalPanel h = new VerticalPanel();
		h.setSpacing(10);
		h.add(apartmentSelector);
		h.add(super.getButtonPanel());
		return h;
	}
	
	public String getSelectedApartment()
	{
		return apartmentSelector.getSelectedApartment();
	}	
}
