/**
 * 
 */
package com.fertiletech.fieldcodata.ui.client.gui;



import com.fertiletech.fieldcodata.ui.client.gui.utils.CommentsPanel;
import com.fertiletech.fieldcodata.ui.shared.dto.DTOConstants;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class ImageUploadPanel extends Composite implements ClickHandler, FormPanel.SubmitCompleteHandler{

	@UiField Button submit;
	@UiField HTML status;
	@UiField FileUpload selectFile;
	@UiField FormPanel imageForm;
	@UiField CommentsPanel comments;
	@UiField CheckBox showComments;
	String commentId;
	
	private static ImageUploadUiBinder uiBinder = GWT
			.create(ImageUploadUiBinder.class);

	interface ImageUploadUiBinder extends UiBinder<Widget, ImageUploadPanel> {
	}

	public ImageUploadPanel() {
		initWidget(uiBinder.createAndBindUi(this));
        submit.addClickHandler(this);
        imageForm.setAction("/upload/images");
        imageForm.addSubmitCompleteHandler(this);
        imageForm.setMethod(FormPanel.METHOD_POST);
        imageForm.setEncoding(FormPanel.ENCODING_MULTIPART);
        selectFile.setName("chooseFile");
        comments.setVisible(false);
        showComments.setEnabled(false);
        showComments.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
			
			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				if(event.getValue())
				{
					showComments.setText("Hide Comments");
					comments.setVisible(true);
				}
				else
				{
					showComments.setText("Show Comments");
					comments.setVisible(false);
				}
			}
		});
        LocalDataProvider.COMMENT_SERVICE.getImageBlobID(DTOConstants.PDF_REPORT_LOGO, new AsyncCallback<String>() {
			
			@Override
			public void onSuccess(String result) {
				comments.setCommentID(result);
				showComments.setEnabled(true);
				commentId = result;
			}
			
			@Override
			public void onFailure(Throwable caught) {
				status.setHTML("Error loading audit trail");
				Window.alert("Error loading comments. " + caught.getMessage());
			}
		});
	}
	
    @Override
    public void onSubmitComplete(FormPanel.SubmitCompleteEvent event)
    {
        String results = event.getResults();
        status.setHTML("<font color='purple'>Logo Saved!!Try generating a pdf report to view, e.g. invoice pdf</font>");
        Window.alert("Saved file succesfully");
        comments.setCommentID(commentId);
    }

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		status.setText("Sending file to server...");
		imageForm.submit();
	}	

}
