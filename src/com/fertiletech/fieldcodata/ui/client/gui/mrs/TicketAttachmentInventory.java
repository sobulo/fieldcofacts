package com.fertiletech.fieldcodata.ui.client.gui.mrs;

import java.util.List;

import com.fertiletech.fieldcodata.ui.client.gui.LocalDataProvider;
import com.fertiletech.fieldcodata.ui.client.gui.utils.ShowcaseTable;
import com.fertiletech.fieldcodata.ui.client.gui.utils.SimpleDialog;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Widget;

public class TicketAttachmentInventory extends Composite{
	@UiField
	AllocationSelector allocBox;
	@UiField
	Button addAlloc;
	@UiField
	Button removeAlloc;

	@UiField(provided = true)
	ShowcaseTable displayAllocation;
	
	private String ticketID;

	private AsyncCallback<List<TableMessage>> alloCallback = new AsyncCallback<List<TableMessage>>() {

		@Override
		public void onFailure(Throwable caught) { 
			Window.alert("Error!" + caught.getMessage());
			displayAllocation.setEmptyWidget(new HTML("<font color='red'><b>Error loading Allocations</d></font><hr/><br/>" + caught.getMessage()));
			enableAlloButtons(true);
		}

		@Override
		public void onSuccess(List<TableMessage> result) {
			displayAllocation.showTable(result);
			enableAlloButtons(true);
		}
	};
	

	
	private static TicketAttachmentInventoryUiBinder uiBinder = GWT
			.create(TicketAttachmentInventoryUiBinder.class);

	interface TicketAttachmentInventoryUiBinder extends
			UiBinder<Widget, TicketAttachmentInventory> {
	}

	public TicketAttachmentInventory() {
		displayAllocation = new ShowcaseTable(null, false, false);
		initWidget(uiBinder.createAndBindUi(this));
		addAlloc.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				GWT.log("Alloc button pressed");
				Long value = allocBox.getID();
				String product = allocBox.getProduct();
				String location = allocBox.getLocation();
				if(value == null)
				{
					new SimpleDialog("INFO").show("<font color='red'><b>Enter an allocation id number</b></font>");
					return;
				}
				GWT.log("Making server side call");
				enableAlloButtons(false);
				LocalDataProvider.TICKET_SERVICEC.addAllocationID(ticketID, value, true, product, location, alloCallback);
			}
		});
		
		removeAlloc.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				Long value = allocBox.getID();
				String product = allocBox.getProduct();
				String location = allocBox.getLocation();
				if(value == null)
				{
					new SimpleDialog("INFO").show("<font color='red'><b>Enter an allocation id number</b></font>");
					return;
				}
				enableAlloButtons(false);
				LocalDataProvider.TICKET_SERVICEC.addAllocationID(ticketID, value, false, product, location, alloCallback);
			}
		});
		
	}
	
	public void enableAlloButtons(boolean enableState)
	{
		addAlloc.setEnabled(enableState);
		removeAlloc.setEnabled(enableState);
	}	
	
	public void loadAttachments(String ticketID)
	{
		loadAttachments(ticketID, alloCallback);
	}
	
	public void loadAttachments(String ticketID, AsyncCallback<List<TableMessage>> callback)
	{
		this.ticketID = ticketID;
		enableAlloButtons(false);
		LocalDataProvider.TICKET_SERVICEC.addAllocationID(ticketID, null, false, null, null, callback);		
	}
	
	public void clearAttachments()
	{
		this.ticketID = null;
		enableAlloButtons(false);
		displayAllocation.clear();
	}
}
