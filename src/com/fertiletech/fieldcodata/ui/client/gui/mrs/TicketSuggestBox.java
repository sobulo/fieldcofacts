package com.fertiletech.fieldcodata.ui.client.gui.mrs;

import com.fertiletech.fieldcodata.ui.client.Showcase;
import com.fertiletech.fieldcodata.ui.client.gui.LocalDataProvider;
import com.fertiletech.fieldcodata.ui.client.gui.utils.UserSuggestBox;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.Hyperlink;

public class TicketSuggestBox extends UserSuggestBox{
	
	public TicketSuggestBox()
	{
		super(new Hyperlink("Ticket", History.getToken()));
		final Hyperlink ticketLabel = (Hyperlink) getDisplayWidget();
		ticketLabel.setTitle("Enter {Ticket ID}. Ensure to start with { and end with }. Link clickable once ticket selected");
		this.addValueChangeHandler(new ValueChangeHandler<String>() {
			
			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				ticketLabel.setTargetHistoryToken(getTokenForSelected());
			}
		});
	}

	@Override
	protected void populateSuggestBox() 
	{
		LocalDataProvider.TICKET_SERVICEC.getAllTickets(getPopulateCallBack());
	}

	@Override
	public String getRole() 
	{
		return "mrs-ticket";
	}
	
	public String getTokenForSelected()
	{
		if(getSelectedUser() == null) return History.getToken();
		return Showcase.getContentWidgetToken(EditTicket.class) + "/" + getSelectedUser();
	}

}
