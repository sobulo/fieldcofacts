package com.fertiletech.fieldcodata.ui.client.gui.mrs;

import com.fertiletech.fieldcodata.ui.client.gui.utils.BuildingSelector;
import com.fertiletech.fieldcodata.ui.client.gui.utils.ProductSelector;
import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.LongBox;

public class AllocationSelector extends Composite{

	ProductSelector productList;
	BuildingSelector buildingList;
	LongBox allocationID;
	
	public AllocationSelector() {
		productList = new ProductSelector(false);
		buildingList = new BuildingSelector(false);
		allocationID = new LongBox();
		HorizontalPanel container = new HorizontalPanel();
		container.add(productList);
		container.add(buildingList);
		container.add(allocationID);
		final String defaultMessage = "Allocation/Transfer ID";
		allocationID.addBlurHandler(new BlurHandler() {
			
			@Override
			public void onBlur(BlurEvent event) {
				if(allocationID.getValue() == null)
					allocationID.setText(defaultMessage);
			}
		});
		allocationID.addFocusHandler(new FocusHandler() {
			
			@Override
			public void onFocus(FocusEvent event) {
				if(allocationID.getValue() == null)
					allocationID.setText("");
			}
		});
		allocationID.setText(defaultMessage);
		productList.setTitle("Select Product");
		buildingList.setTitle("Select inventory location");
		allocationID.setTitle("Enter inventory transfer/allocation id");
		initWidget(container);
	}
	
	String getLocation()
	{
		return buildingList.getSelectedBuilding();
	}
	
	String getProduct()
	{
		return productList.getSelectedValue();
	}
	
	Long getID()
	{
		return allocationID.getValue();
	}
}