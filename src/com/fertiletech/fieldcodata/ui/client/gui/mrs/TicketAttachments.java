package com.fertiletech.fieldcodata.ui.client.gui.mrs;

import java.util.List;

import com.fertiletech.fieldcodata.ui.client.GUIConstants;
import com.fertiletech.fieldcodata.ui.client.Showcase;
import com.fertiletech.fieldcodata.ui.client.gui.LocalDataProvider;
import com.fertiletech.fieldcodata.ui.client.gui.accounts.CWSearchInvoice;
import com.fertiletech.fieldcodata.ui.client.gui.utils.ShowcaseTable;
import com.fertiletech.fieldcodata.ui.client.gui.utils.SimpleDialog;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.LongBox;
import com.google.gwt.user.client.ui.Widget;

public class TicketAttachments extends Composite {
	@UiField
	LongBox invoiceBox;
	@UiField
	Button addInvoice;
	@UiField
	Button removeInvoice;	
	@UiField(provided=true)
	ShowcaseTable displayInvoice;
	
	private String ticketID;
	private AsyncCallback<List<TableMessage>> invCallback = new AsyncCallback<List<TableMessage>>() {

		@Override
		public void onFailure(Throwable caught) {
			displayInvoice.setEmptyWidget(new HTML("<font color='red'><b>Error loading invoices</d></font><hr/><br/>" + caught.getMessage()));
			enableInvButtons(true);
		}

		@Override
		public void onSuccess(List<TableMessage> result) {
			displayInvoice.showTable(result);
			enableInvButtons(true);
		}
	};
	
	private static TicketAttachmentsUiBinder uiBinder = GWT
			.create(TicketAttachmentsUiBinder.class);

	interface TicketAttachmentsUiBinder extends
			UiBinder<Widget, TicketAttachments> {
	}

	public TicketAttachments() {
		
		displayInvoice = new ShowcaseTable(false, false, false);
		initWidget(uiBinder.createAndBindUi(this));
		addInvoice.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				Long value = invoiceBox.getValue();
				if(value == null)
				{
					new SimpleDialog("INFO").show("<font color='red'><b>Enter an invoice id number</b></font>");
					return;
				}
				enableInvButtons(false);
				LocalDataProvider.TICKET_SERVICEC.addInvoiceID(ticketID, value, true, invCallback);
			}
		});
		
		removeInvoice.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				Long value = invoiceBox.getValue();
				if(value == null)
				{
					new SimpleDialog("INFO").show("<font color='red'><b>Enter an invoice id number</b></font>");
					return;
				}
				enableInvButtons(false);
				LocalDataProvider.TICKET_SERVICEC.addInvoiceID(ticketID, value, false, invCallback);
			}
		});
		
		displayInvoice.addValueChangeHandler(new ValueChangeHandler<TableMessage>() {
			
			@Override
			public void onValueChange(ValueChangeEvent<TableMessage> event) {
				TableMessage object = event.getValue();
				if (object == null)
					return;
				String token = Showcase
						.getContentWidgetToken(CWSearchInvoice.class)
						+ "/"
						+ GUIConstants.DEFAULT_INTEGER_FORMAT.format(object
								.getNumber(2));
				GWT.log("about to fire: " + token);
				History.newItem(token);
			}
		});
	}
	
	public void enableInvButtons(boolean enableState)
	{
		addInvoice.setEnabled(enableState);
		removeInvoice.setEnabled(enableState);
	}
	
	public void loadAttachments(String ticketID)
	{
		loadAttachments(ticketID, invCallback);
	}
	
	public void loadAttachments(String ticketID, AsyncCallback<List<TableMessage>> callback)
	{
		this.ticketID = ticketID;
		enableInvButtons(false);
		LocalDataProvider.TICKET_SERVICEC.addInvoiceID(ticketID, null, false, callback);
	}
	
	public void clearAttachments()
	{
		this.ticketID = null;;
		enableInvButtons(false);
		displayInvoice.clear();
	}
}
