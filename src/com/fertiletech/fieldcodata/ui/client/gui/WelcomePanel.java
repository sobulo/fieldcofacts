package com.fertiletech.fieldcodata.ui.client.gui;

import com.fertiletech.fieldcodata.ui.client.ContentWidget;
import com.fertiletech.fieldcodata.ui.client.Showcase;
import com.fertiletech.fieldcodata.ui.client.gui.accounts.ViewBillBlotter;
import com.fertiletech.fieldcodata.ui.client.gui.icons.MyClientBundle;
import com.fertiletech.fieldcodata.ui.client.gui.inventory.CWInventoryView;
import com.fertiletech.fieldcodata.ui.client.gui.mrs.CWViewTickets;
import com.fertiletech.fieldcodata.ui.client.gui.prospects.CWEditProspects;
import com.fertiletech.fieldcodata.ui.client.gui.prospects.CWViewProspects;
import com.fertiletech.fieldcodata.ui.client.gui.tenants.CWViewTenants;
import com.fertiletech.fieldcodata.ui.client.gui.tenants.CwCellList;
import com.fertiletech.fieldcodata.ui.client.gui.vendors.CWEditVendor;
import com.fertiletech.fieldcodata.ui.client.gui.vendors.CWViewVendor;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

public class WelcomePanel extends ContentWidget implements ClickHandler{
	
	@UiField
	Label userField;
	
	@UiField
	HTML loginMessage;	
	
	@UiField FlowPanel iconBoard;
	
	@UiField ActivityCommentCellList activityFeed;
	
	@UiField DockLayoutPanel container;
	
	Widget myDisplay;
	
	final int FEED_WIDTH = 220;
	
	private static MyClientBundle primaryBundle = GWT.create(MyClientBundle.class);
	final Image[] icons = new Image[6];
	

	private static WelcomePanelUiBinder uiBinder = GWT
			.create(WelcomePanelUiBinder.class);

	interface WelcomePanelUiBinder extends UiBinder<Widget, WelcomePanel> {
	}

	public WelcomePanel() {
	    super("Home", "Use the icons below or the left navigation menu to view modules");
		myDisplay = uiBinder.createAndBindUi(this);
		icons[0] = new Image(primaryBundle.prospective());
		icons[1] = new Image(primaryBundle.bio());
		icons[2] = new Image(primaryBundle.accounts());
		icons[3] = new Image(primaryBundle.vendors());
		icons[4] = new Image(primaryBundle.inventory());
		icons[5] = new Image(primaryBundle.MRS());
		
		//setup click handlers
		for(int i = 0; i < icons.length; i++)
		{
			icons[i].addStyleName("iconSpacer");
			icons[i].addClickHandler(this);
			iconBoard.add(icons[i]);
		}
		icons[0].setTitle("Prospective Clients");
		icons[1].setTitle("Resident Bio");
		icons[2].setTitle("Resident Billing");
		icons[3].setTitle("Vendors");
		icons[4].setTitle("Inventory");
		icons[5].setTitle("Maintenance Request System");
	}
	
	@Override
	public boolean hasScrollableContent()
	{
		return false;
	}
	
	@Override
	public Widget onInitialize() {
		return myDisplay;
	}

	@Override
	protected void asyncOnInitialize(AsyncCallback<Widget> callback) {
		callback.onSuccess(onInitialize());
	}
	
	public void setUserName(String user)
	{
		userField.setText(user);
	}

	public void setLoginMessage(String msg)
	{
		loginMessage.setHTML(msg);
	}
	
	public void refreshFeed(boolean show)
	{
		if(show)
		{
			activityFeed.refresh();
			container.setWidgetSize(activityFeed, FEED_WIDTH);
		}
		else
			container.setWidgetSize(activityFeed, 0);
		container.animate(1500);
	}

	@Override
	public void onClick(ClickEvent event) {
		if(event.getSource() == icons[0])
			History.newItem(Showcase.getContentWidgetToken(CWViewProspects.class), true);
		else if(event.getSource() == icons[1])
			History.newItem(Showcase.getContentWidgetToken(CWViewTenants.class), true);
		else if(event.getSource() == icons[2])
			History.newItem(Showcase.getContentWidgetToken(ViewBillBlotter.class), true);
		else if(event.getSource() == icons[3])
			History.newItem(Showcase.getContentWidgetToken(CWViewVendor.class), true);
		else if(event.getSource() == icons[4])
			History.newItem(Showcase.getContentWidgetToken(CWInventoryView.class), true);
		else if(event.getSource() == icons[5])
			History.newItem(Showcase.getContentWidgetToken(CWViewTickets.class), true);
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_WELCOME_URL;
	}	
}
