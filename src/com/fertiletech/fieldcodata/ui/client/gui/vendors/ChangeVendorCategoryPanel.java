package com.fertiletech.fieldcodata.ui.client.gui.vendors;

import java.util.List;

import com.fertiletech.fieldcodata.ui.client.gui.LocalDataProvider;
import com.fertiletech.fieldcodata.ui.client.gui.utils.VendorSuggestBox;
import com.fertiletech.fieldcodata.ui.shared.dto.DTOConstants;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;

public class ChangeVendorCategoryPanel extends Composite{

	@UiField
	VendorSuggestBox vendorPicker;
	
	@UiField
	Button save;
	
	@UiField
	ListBox category;

	private AsyncCallback<List<TableMessage>> categoryCallBack = new AsyncCallback<List<TableMessage>>() {

		@Override
		public void onFailure(Throwable caught) {
			Window.alert("Error fetching categories. Try refreshing your browser. " + caught.getMessage());
		}

		@Override
		public void onSuccess(List<TableMessage> result) 
		{
			if(result.size() > 1)
				category.addItem("Select Category");
			else
				category.addItem("No Categories Found");
			result.remove(0);
			for(TableMessage m : result)
				category.addItem(m.getText(0));
			enableForm(true);
		}
	};

	protected AsyncCallback<TableMessage> saveCallBack = new AsyncCallback<TableMessage>() {

		@Override
		public void onFailure(Throwable caught) {
			Window.alert("Error modifying category for " + vendorPicker.getSelectedUserDisplay() + ". " + caught.getMessage());
			enableForm(true);
		}

		@Override
		public void onSuccess(TableMessage result) {
			Window.alert("Category modified for " + result.getText(DTOConstants.TNT_LNAME_IDX) + " @ " +
					result.getText(DTOConstants.TNT_COMPANY_IDX) + ". You might need a browser refresh to push this change to other F.A.C.T.S modules");
			enableForm(true);
		}
	};
	
	private static ChangeVendorCategoryPanelUiBinder uiBinder = GWT
			.create(ChangeVendorCategoryPanelUiBinder.class);

	interface ChangeVendorCategoryPanelUiBinder extends
			UiBinder<Widget, ChangeVendorCategoryPanel> {
	}

	public ChangeVendorCategoryPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		enableForm(false);
		LocalDataProvider.COMMENT_SERVICE.getApplicationParameter(DTOConstants.APP_PARAM_VENDOR_CATEGORY_LIST_KEY, categoryCallBack );
		save.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				if(!validateFields())
					return;
				enableForm(false);
				LocalDataProvider.VENDOR_SERVICE.changeVendorCategory(vendorPicker.getSelectedUser(), category.getValue(category.getSelectedIndex()), saveCallBack );	
			}
		});
		
	}
	
	private boolean validateFields()
	{
		StringBuilder errors = new StringBuilder();
		if(vendorPicker.getSelectedUser() == null)
			errors.append("Select a vendor contact to change. ");
		if(category.getSelectedIndex() == 0)
			errors.append("Select a valid category");
		if(errors.length() > 0)
			Window.alert(errors.toString());
		return errors.length() == 0;
			
	}
	
	private void enableForm(boolean enabled)
	{
		vendorPicker.setEnabled(enabled);
		category.setEnabled(enabled);
		save.setEnabled(enabled);
	}
}
