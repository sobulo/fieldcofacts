package com.fertiletech.fieldcodata.ui.client.gui.vendors;

import com.fertiletech.fieldcodata.ui.client.gui.utils.RatingsPanel;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SelectionChangeEvent;

public class RateVendorPanel extends Composite {
	
	@UiField
	VendorSelector vendorSelector;
	
	@UiField
	RatingsPanel ratingsPanel;

	private static RateVendorPanelUiBinder uiBinder = GWT
			.create(RateVendorPanelUiBinder.class);

	interface RateVendorPanelUiBinder extends UiBinder<Widget, RateVendorPanel> {
	}

	public RateVendorPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		vendorSelector.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
			
			@Override
			public void onSelectionChange(SelectionChangeEvent event) {
				if(vendorSelector.getSelectedVendor() == null)
					ratingsPanel.clear();
				else
					ratingsPanel.setCommentID(vendorSelector.getSelectedVendor().getMessageId());
			}
		});
	}

}
