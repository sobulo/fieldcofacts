package com.fertiletech.fieldcodata.ui.client.gui.vendors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.fertiletech.fieldcodata.ui.client.VendorService;
import com.fertiletech.fieldcodata.ui.client.VendorServiceAsync;
import com.fertiletech.fieldcodata.ui.client.gui.LocalDataProvider;
import com.fertiletech.fieldcodata.ui.client.gui.tenants.CwCellList.ContactCell;
import com.fertiletech.fieldcodata.ui.client.gui.utils.MultipleMessageDialog;
import com.fertiletech.fieldcodata.ui.client.gui.utils.RangeLabelPager;
import com.fertiletech.fieldcodata.ui.client.gui.utils.ShowMorePagerPanel;
import com.fertiletech.fieldcodata.ui.client.gui.utils.SimpleDialog;
import com.fertiletech.fieldcodata.ui.client.gui.utils.TableMessageKeyProvider;
import com.fertiletech.fieldcodata.ui.shared.dto.DTOConstants;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.cellview.client.HasKeyboardPagingPolicy.KeyboardPagingPolicy;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.SelectionChangeEvent.Handler;
import com.google.gwt.view.client.SelectionChangeEvent.HasSelectionChangedHandlers;
import com.google.gwt.view.client.SingleSelectionModel;

public class VendorSelector extends Composite implements HasSelectionChangedHandlers{
	@UiField
	ShowMorePagerPanel pagerPanel;

	@UiField
	RangeLabelPager rangeLabelPager;

	@UiField
	Label statusMessage;
	
	@UiField
	ListBox categoryNames;
	
	SimpleDialog infoBox;
	MultipleMessageDialog errorBox;
	private CellList<TableMessage> cellList;
	
	private static EditVendorUiBinder uiBinder = GWT
			.create(EditVendorUiBinder.class);

	interface EditVendorUiBinder extends UiBinder<Widget, VendorSelector> {
	}

	static HashMap<String, List<TableMessage>> cachedVendors = null;
	ListDataProvider<TableMessage> dp = new ListDataProvider<TableMessage>();
	public static VendorServiceAsync VENDOR_SERVICE = GWT.create(VendorService.class);

    // Create an asynchronous callback to handle the result.
    private final AsyncCallback<List<TableMessage>> vendorsCallback = new AsyncCallback<List<TableMessage>>() {

        @Override
        public void onSuccess(List<TableMessage> result) {
        	result.remove(0); //remove header
        	categoryNames.setEnabled(true);
        	cachedVendors = buildCache(result);
        	Set<String> vendorCategories = new HashSet<String>();
        	for(int i = 0; i < categoryNames.getItemCount(); i++ )
        	{
        		vendorCategories.add(categoryNames.getValue(i));
        		if(!cachedVendors.containsKey(categoryNames.getValue(i)))
        			cachedVendors.put(categoryNames.getValue(i), new ArrayList<TableMessage>());
        	}
        	
        	ArrayList<String> b = new ArrayList<String>();
        	for(String s : cachedVendors.keySet())
        	{
        		if(!vendorCategories.contains(s))
        			b.add(s + " persited on database but not a registered category");
        	}
        	if(b.size() > 0)
        	{
        		errorBox.show("WARNING: Inconsistencies found with Categories. Contact pilot-group@fieldcolimited.com to add categories below to database (case sensistive)", b);
        	}
        	categoryNames.addChangeHandler(new ChangeHandler() {
				
				@Override
				public void onChange(ChangeEvent event) {
					categoryNameChanged();
				}
			});
        	statusMessage.setText("Loaded " + result.size() + " vendors");
        	categoryNames.setSelectedIndex(0);
        	categoryNameChanged();
        }

        @Override
        public void onFailure(Throwable caught) {
        	errorBox.show("Unable to fetch vendor list. Try refreshing your browser. " +
        			"Contact technology@fertiletech.com if problems persist. <p> Error msg: <b>"+caught.getMessage() + "</b></p>");
        }
    };
    
    public void addVendor(TableMessage m)
    {
    	dp.getList().add(m);
    }
    
    public void refreshVendor(TableMessage m)
    {
    	LocalDataProvider.refreshSingleData(dp.getList(), m);
    }    
    
    public void enable(boolean isEnabled)
    {
    	categoryNames.setEnabled(isEnabled);
    }
    
    public void categoryNameChanged()
    {
    	GWT.log("Made it in here");
    	String selectedCategory = categoryNames.getValue(categoryNames.getSelectedIndex());
    	List<TableMessage> dpList = dp.getList();
    	dpList.clear();
    	dpList.addAll(cachedVendors.get(selectedCategory));
    }
    final SingleSelectionModel<TableMessage> selectionModel;
	public VendorSelector() {
		// Create a CellList.
		ContactCell contactCell = new VendorCell();

		// Set a key provider that provides a unique key for each contact. If
		// key is
		// used to identify contacts when fields (such as the name and address)
		// change.
		cellList = new CellList<TableMessage>(contactCell,
				TableMessageKeyProvider.KEY_PROVIDER);
		cellList.setPageSize(30);
		cellList.setKeyboardPagingPolicy(KeyboardPagingPolicy.INCREASE_RANGE);
		cellList.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.BOUND_TO_SELECTION);

		// Add a selection model so we can select cells.
		selectionModel = new SingleSelectionModel<TableMessage>(
				TableMessageKeyProvider.KEY_PROVIDER);
		cellList.setSelectionModel(selectionModel);
		errorBox = new MultipleMessageDialog("<font color='red'>Error Occured</font>", true);
		infoBox = new SimpleDialog("<font color='green'>INFO</font>", true);
		initWidget(uiBinder.createAndBindUi(this));
		pagerPanel.setDisplay(cellList);
		rangeLabelPager.setDisplay(cellList);
		dp.addDataDisplay(cellList);
		AsyncCallback<List<TableMessage>> categcallback = new AsyncCallback<List<TableMessage>>() {

			@Override
			public void onFailure(Throwable caught) {
				infoBox.show("Failed to load category list. Error was:<br/>" + caught.getMessage());
			}

			@Override
			public void onSuccess(List<TableMessage> result) {
				result.remove(0); //remove the header
				categoryNames.clear();
				for(TableMessage m : result)
					categoryNames.addItem(m.getText(0));
				
				//now go fetch the vendors
				VENDOR_SERVICE.getAllVendors(vendorsCallback);
			}
		};
		LocalDataProvider.COMMENT_SERVICE.getApplicationParameter(DTOConstants.APP_PARAM_VENDOR_CATEGORY_LIST_KEY, categcallback );
	}

	
	public HashMap<String, List<TableMessage>> buildCache(List<TableMessage> vendors)
	{
		HashMap<String, List<TableMessage>> result = new HashMap<String, List<TableMessage>>();
		for(TableMessage v : vendors)
		{
			String category = v.getText(DTOConstants.VND_CATEGORY_IDX);
			if(!result.containsKey(category))
				result.put(category, new ArrayList<TableMessage>());
			List<TableMessage> vendorByCat = result.get(category);
			vendorByCat.add(v);
		}
		return result;
	}
	
	@Override
	public HandlerRegistration addSelectionChangeHandler(Handler handler) {
		return selectionModel.addSelectionChangeHandler(handler);
	}
	
	public TableMessage getSelectedVendor()
	{
		return selectionModel.getSelectedObject();
	}
	
	public String getCategory()
	{
		return categoryNames.getValue(categoryNames.getSelectedIndex());
	}
}

class VendorCell extends ContactCell
{
	@Override
	public void render(Context context, TableMessage value,
			SafeHtmlBuilder sb) {
		// Value can be null, so do a null check..
		if (value == null) {
			return;
		}
		
		String company = value.getText(DTOConstants.TNT_COMPANY_IDX);
		company = (company==null || company.length()==0) ? "ERROR - BLANK COMPANY" : company;
		String fname = value.getText(DTOConstants.TNT_FNAME_IDX) == null?"": value.getText(DTOConstants.TNT_FNAME_IDX);
		String lname = value.getText(DTOConstants.TNT_LNAME_IDX) == null?"": value.getText(DTOConstants.TNT_LNAME_IDX);

		sb.appendHtmlConstant("<table>");

		// Add the contact image.
		sb.appendHtmlConstant("<tr><td rowspan='3'>");
		sb.appendHtmlConstant(getImageHTML());
		sb.appendHtmlConstant("</td>");

		// Add the name and address.
		sb.appendHtmlConstant("<td style='font-size:95%;'>");
		sb.appendEscaped(company);
		sb.appendHtmlConstant("</td></tr><tr><td>");
		sb.appendEscaped(fname + " " + lname);
		sb.appendHtmlConstant("</td></tr></table>");
	}
	
}
