package com.fertiletech.fieldcodata.ui.client.gui.vendors;

import com.fertiletech.fieldcodata.ui.client.ContentWidget;
import com.fertiletech.fieldcodata.ui.client.gui.HelpPageGenerator;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class CWViewVendor extends ContentWidget{
	public CWViewVendor() {
		super("View Vendors", "Use this module to view the list of vendors available in the database");
	}

	@Override
	public Widget onInitialize() {
		return new ViewVendors();
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
	    GWT.runAsync(CWViewVendor.class, new RunAsyncCallback() {

	        public void onFailure(Throwable caught) {
	          callback.onFailure(caught);
	        }

	        public void onSuccess() {
	          callback.onSuccess(onInitialize());
	        }
	      });
	}
	
	@Override
	public boolean hasScrollableContent()
	{
		return false;
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_VENDORS_URL;
	}

}
