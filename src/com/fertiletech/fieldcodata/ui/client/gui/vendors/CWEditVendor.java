package com.fertiletech.fieldcodata.ui.client.gui.vendors;

import com.fertiletech.fieldcodata.ui.client.ContentWidget;
import com.fertiletech.fieldcodata.ui.client.gui.HelpPageGenerator;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class CWEditVendor extends ContentWidget{
	public CWEditVendor()
	{
		super("Edit Vendors", "Use this module to select vendors and edit their contact details");
	}
	
	@Override
	public Widget onInitialize() {
		return new EditVendorPanel();
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
	    GWT.runAsync(CWEditVendor.class, new RunAsyncCallback() {

	        public void onFailure(Throwable caught) {
	          callback.onFailure(caught);
	        }

	        public void onSuccess() {
	          callback.onSuccess(onInitialize());
	        }
	      });
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_VENDORS_URL;
	}
}
