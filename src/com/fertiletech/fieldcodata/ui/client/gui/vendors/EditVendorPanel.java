package com.fertiletech.fieldcodata.ui.client.gui.vendors;

import java.util.ArrayList;

import com.fertiletech.fieldcodata.ui.client.gui.LocalDataProvider;
import com.fertiletech.fieldcodata.ui.client.gui.utils.UserPanel;
import com.fertiletech.fieldcodata.ui.client.gui.utils.YesNoDialog;
import com.fertiletech.fieldcodata.ui.shared.UtilConstants;
import com.fertiletech.fieldcodata.ui.shared.dto.DTOConstants;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.fertiletech.fieldcodata.ui.shared.exceptions.DuplicateEntitiesException;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SelectionChangeEvent;

public class EditVendorPanel extends Composite{

	@UiField
	Button update;
	@UiField
	Button clear;
	@UiField
	VendorSelector vendorSelector;
	@UiField(provided=true)
	UserPanel bioPanel;
	
	final AsyncCallback<TableMessage> saveCallback = new AsyncCallback<TableMessage>() {

		@Override
		public void onSuccess(TableMessage result) {
			bioPanel.setUserID(result.getMessageId());
			vendorSelector.infoBox.show("Updated contact info for "
					+ result.getText(DTOConstants.TNT_LNAME_IDX)
					+ " successfully");
			vendorSelector.refreshVendor(result);
			enableForm(true);
		}

		@Override
		public void onFailure(Throwable caught) {
			vendorSelector.errorBox.show("Unable to save values. Error was: "
					+ caught.getMessage());
			enableForm(true);
		}
	};	

	final AsyncCallback<TableMessage> newCallback = new AsyncCallback<TableMessage>() {

		@Override
		public void onSuccess(TableMessage result) {
			bioPanel.setUserID(result.getMessageId());
			vendorSelector.infoBox.show("Created vendor "
					+ result.getText(DTOConstants.TNT_FNAME_IDX) + " " + result.getText(DTOConstants.TNT_LNAME_IDX) + " for " + result.getText(DTOConstants.TNT_COMPANY_IDX)
					+ " successfully");
			vendorSelector.addVendor(result);
			enableForm(true);
		}

		@Override
		public void onFailure(Throwable caught) {
			if(caught instanceof DuplicateEntitiesException)
			{
				final YesNoDialog confirm = new YesNoDialog("Confirm multiple vendor contacts");
				confirm.setClickHandler(new ClickHandler() {
					
					@Override
					public void onClick(ClickEvent event) {
						createNewVendor(false);
						confirm.hide();
					}
				});
				confirm.show("A vendor contact already exists for " + bioPanel.getCompany() + ". Are you sure you want to create an additional contact for this company?");				
			}
			else
				vendorSelector.errorBox.show("Unable to save values. Error was: "
					+ caught.getMessage());
			enableForm(true);
		}
	};	
	
	private static EditVendorPanelUiBinder uiBinder = GWT
			.create(EditVendorPanelUiBinder.class);

	interface EditVendorPanelUiBinder extends UiBinder<Widget, EditVendorPanel> {
	}

	public EditVendorPanel() {
		bioPanel = new UserPanel(false);
		initWidget(uiBinder.createAndBindUi(this));
		vendorSelector.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
			public void onSelectionChange(SelectionChangeEvent event) {
				if(vendorSelector.getSelectedVendor() == null) 
					bioPanel.clear();
				else
					bioPanel.setContactValues(vendorSelector.getSelectedVendor());
			}
		});
		update.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				ArrayList<String> errors = bioPanel.validateFields();
				if(bioPanel.getCompany().trim().equals(""))
					errors.add("A company name must be selected for the vendor");
				if(errors.size() > 0)
				{
					vendorSelector.errorBox.show("Fix errors below", errors);
					return;
				}
				enableForm(false);
				if(bioPanel.getUserID() != null)
				{
					LocalDataProvider.TENANT_SERVICE.updateContact(
							bioPanel.getUserID(), bioPanel.getFirstName(),
							bioPanel.getLastName(), bioPanel.getDateOfBirth(), bioPanel.getEmail(),
							bioPanel.getPhone(), bioPanel.getOtherNumbers(),
							bioPanel.getOtherMails(), bioPanel.getCompany(),
							bioPanel.getAddress(), bioPanel.isMale(),
							bioPanel.getSalutation(), null, saveCallback);
				}
				else
					createNewVendor(true);
				
			}
		});
		clear.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				bioPanel.clear();
			}
		});
	}
	
	private void createNewVendor(boolean throwException)
	{
		enableForm(false);
		LocalDataProvider.VENDOR_SERVICE.createVendor(vendorSelector.getCategory(), bioPanel.getFirstName(), bioPanel.getLastName(), bioPanel.getDateOfBirth(), bioPanel.getEmail(), bioPanel.getPhone(), bioPanel.getOtherNumbers(), bioPanel.getOtherMails(), bioPanel.getCompany(), 
				bioPanel.getAddress(), bioPanel.isMale(), bioPanel.getSalutation(), throwException, newCallback);
		
	}
	
	private void enableForm(boolean enabled)
	{
		vendorSelector.enable(enabled);
		update.setEnabled(enabled);
		bioPanel.enablePanel(enabled);
	}

}
