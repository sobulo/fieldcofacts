package com.fertiletech.fieldcodata.ui.client.gui.vendors;

import java.util.List;

import com.fertiletech.fieldcodata.ui.client.GUIConstants;
import com.fertiletech.fieldcodata.ui.client.gui.LocalDataProvider;
import com.fertiletech.fieldcodata.ui.client.gui.utils.ShowcaseTable;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

public class ViewVendors extends Composite {

	@UiField
	ShowcaseTable display;
	

	private AsyncCallback<List<TableMessage>> callback = new AsyncCallback<List<TableMessage>>() {

		@Override
		public void onFailure(Throwable caught) {
			Window.alert("Error loading vendors: " + caught.getMessage());
		}

		@Override
		public void onSuccess(List<TableMessage> result) {
			display.showTable(result);
			display.setEmptyWidget(new Label("No results found"));
		}
	};
	
	private static ViewVendorsUiBinder uiBinder = GWT
			.create(ViewVendorsUiBinder.class);

	interface ViewVendorsUiBinder extends UiBinder<Widget, ViewVendors> {
	}

	public ViewVendors() {
		initWidget(uiBinder.createAndBindUi(this));
		LocalDataProvider.VENDOR_SERVICE.getAllVendors(callback);
		GUIConstants.addContactInfoPopup(display, false);
		display.setEmptyWidget(new Label("Loading ..."));
	}

}
