package com.fertiletech.fieldcodata.ui.client.gui.vendors;

import com.fertiletech.fieldcodata.ui.client.ContentWidget;
import com.fertiletech.fieldcodata.ui.client.gui.HelpPageGenerator;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class CWCategorizeVendors extends ContentWidget {
	public CWCategorizeVendors() {
		super("Categorize Vendor", "Use this module to change category classification for existing vendors");
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
	    GWT.runAsync(CWCategorizeVendors.class, new RunAsyncCallback() {

	        public void onFailure(Throwable caught) {
	          callback.onFailure(caught);
	        }

	        public void onSuccess() {
	          callback.onSuccess(onInitialize());
	        }
	      });
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_VENDORS_URL;
	}

	@Override
	public Widget onInitialize() {
		// TODO Auto-generated method stub
		return new ChangeVendorCategoryPanel();
	}
}
