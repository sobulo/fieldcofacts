package com.fertiletech.fieldcodata.ui.client.gui;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import com.fertiletech.fieldcodata.ui.client.AccountManager;
import com.fertiletech.fieldcodata.ui.client.AccountManagerAsync;
import com.fertiletech.fieldcodata.ui.client.GUIConstants;
import com.fertiletech.fieldcodata.ui.client.InventoryService;
import com.fertiletech.fieldcodata.ui.client.InventoryServiceAsync;
import com.fertiletech.fieldcodata.ui.client.LoginService;
import com.fertiletech.fieldcodata.ui.client.LoginServiceAsync;
import com.fertiletech.fieldcodata.ui.client.TenantDAOManager;
import com.fertiletech.fieldcodata.ui.client.TenantDAOManagerAsync;
import com.fertiletech.fieldcodata.ui.client.TicketService;
import com.fertiletech.fieldcodata.ui.client.TicketServiceAsync;
import com.fertiletech.fieldcodata.ui.client.VendorService;
import com.fertiletech.fieldcodata.ui.client.VendorServiceAsync;
import com.fertiletech.fieldcodata.ui.client.gui.utils.SimpleDialog;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessageHeader;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessageHeader.TableMessageContent;
import com.google.gwt.cell.client.DateCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.NumberCell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.AbstractCellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.ListDataProvider;

public class LocalDataProvider {

	private static SimpleDialog errorBox = new SimpleDialog(
			"<font color='red'>Error</font>");
	// Create an asynchronous callback to handle the result.
	private final AsyncCallback<HashMap<String, String>> buildingNamesCallback = new AsyncCallback<HashMap<String, String>>() {

		@Override
		public void onSuccess(HashMap<String, String> result) {
			populateBuildingNamesMap(result);
		}

		@Override
		public void onFailure(Throwable caught) {
			errorBox.show("Unable to retrieve building names. Try refreshing your browser. "
					+ "Contact info@fertiletech.com if problems persist. <p> Error msg: <b>"
					+ caught.getMessage() + "</b></p>");
		}
	};

	public static TenantDAOManagerAsync TENANT_SERVICE = GWT
			.create(TenantDAOManager.class);
	public static AccountManagerAsync ACCOUNT_SERVICE = GWT
			.create(AccountManager.class);
	public static LoginServiceAsync COMMENT_SERVICE = GWT
			.create(LoginService.class);
	public static InventoryServiceAsync INVENTORY_SERVICE = GWT
			.create(InventoryService.class);
	public static TicketServiceAsync TICKET_SERVICEC = GWT
			.create(TicketService.class);
	public static VendorServiceAsync VENDOR_SERVICE = GWT
			.create(VendorService.class);

	
	//these are static though accessed with instance variables. this is to allow switching to non-static versions if
	//static sharing leads to unexpected conflicts in behaviour of gui screens that use the cache
	private static final HashMap<String, ListDataProvider<TableMessage>> buildingTenantsDataProviderMap = new HashMap<String, ListDataProvider<TableMessage>>();
	private static final HashMap<String, ListDataProvider<TableMessage>> buildingApartmentDataProviderMap = new HashMap<String, ListDataProvider<TableMessage>>();
	private static final HashMap<String, String> buildingMap = new HashMap<String, String>();
	private static final ListDataProvider<String> buildingCategories = new ListDataProvider<String>();

	private static void removeDisplayFromMap(HasData<TableMessage> view,
			HashMap<String, ListDataProvider<TableMessage>> map) {
		for (ListDataProvider<TableMessage> dp : map.values()) {
			Set<HasData<TableMessage>> displays = dp.getDataDisplays();
			if (displays.contains(view)) {
				dp.removeDataDisplay(view);
				break;
			}
		}
	}

	public void removeDisplayFromTenantMap(HasData<TableMessage> view) {
		removeDisplayFromMap(view, buildingTenantsDataProviderMap);
		view.setRowCount(0);
	}

	public void removeDisplayFromApartmentMap(HasData<TableMessage> view) {
		removeDisplayFromMap(view, buildingApartmentDataProviderMap);
		view.setRowCount(0);
	}
	
	public static void refreshSingleData(List<TableMessage> providerList, TableMessage m)
	{
		List<TableMessage> list = providerList;
		GWT.log("List has: " + providerList.size());
		for(int i = 0; i < list.size(); i++)
		{
			if(list.get(i).getMessageId().equals(m.getMessageId()))
			{
				list.set(i, m);
				GWT.log("Found resident for refresh");
				return;
			}
		}
		GWT.log("Oops, no resident found");
	}
	

	public int getBuildingNamesSize() {
		return buildingMap.size();
	}

	public void populateListBoxWithBuildingNames(ListBox lb) {
		lb.clear();
		for (String key : buildingMap.keySet())
			lb.addItem(buildingMap.get(key), key);
	}

	private static List<TableMessage> getInfoList(String buildingID,
			HashMap<String, ListDataProvider<TableMessage>> map) {
		ListDataProvider<TableMessage> dp = map.get(buildingID);
		if (dp == null) {
			dp = new ListDataProvider<TableMessage>();
			map.put(buildingID, dp);
		}
		return map.get(buildingID).getList();
	}

	public List<TableMessage> getTenantInfoList(String buildingID) {
		List<TableMessage> m = getInfoList(buildingID, buildingTenantsDataProviderMap);
		GWT.log("Retrieved from building tenant infolistcache " + m.size() + " for: " + buildingID );
		return m;
	}

	public List<TableMessage> getApartmentInfoList(String buildingID) {
		return getInfoList(buildingID, buildingApartmentDataProviderMap);
	}

	private static void addViewToListDataProvider(HasData<TableMessage> view,
			String buildingID,
			HashMap<String, ListDataProvider<TableMessage>> map) {
		map.get(buildingID).addDataDisplay(view);
		view.setRowCount(map.get(buildingID).getList().size());
	}

	public void addViewToTenantListDataProvider(
			HasData<TableMessage> view, String buildingID) {
		addViewToListDataProvider(view, buildingID,
				buildingTenantsDataProviderMap);
	}

	public void addViewToApartmentListDataProvider(
			HasData<TableMessage> view, String buildingID) {
		addViewToListDataProvider(view, buildingID,
				buildingApartmentDataProviderMap);
	}

	public String getBuildingIDFromName(String buildingName) {
		// This breaks code if two buildings can have the same name. We're safe
		// based on the fact we return the keyname of the building and
		// server side checks to ensure no 2 buildings in the database have the
		// same key name. Bottom line don't do something silly like change
		// the code later to return the display name of the building, no checks
		// for duplicates on display name
		// difference between keyname and displayname, one is more descriptive,
		// e.g. Sterling Ct. vs. Sterling Court Luxury Condos
		for (String id : buildingMap.keySet()) {
			if (buildingMap.get(id).equals(buildingName))
				return id;
		}
		return null;
	}

	public void populateBuildingNamesMap(
			HashMap<String, String> buildingNamesMap) {
		buildingMap.clear();
		buildingMap.putAll(buildingNamesMap);
		buildingCategories.getList().clear();
		buildingCategories.getList().addAll(buildingNamesMap.values());
	}

	public void refresh(String buildingID) {
		buildingTenantsDataProviderMap.get(buildingID).refresh();
	}

	public ListDataProvider<String> getBuildingNamesDataProvider() {
		if (getBuildingNamesSize() == 0)
			TENANT_SERVICE.getBuildingNames(buildingNamesCallback);
		return buildingCategories;
	}


	/**
	 * Add the columns to the table. Column addition is done in a generic manner
	 * based on the TableHeader For custom column specification you'll have to
	 * write another init function
	 */
	public static void initTableColumns(TableMessageHeader header,
			final AbstractCellTable<TableMessage> cellTable,
			ListHandler<TableMessage> sortHandler) {
		int dateIdx, textIdx, numIdx;
		dateIdx = textIdx = numIdx = 0;
		for (int i = 0; i < header.getNumberOfHeaders(); i++) {
			TableMessageContent headerType = header.getHeaderType(i);
			GWT.log("Header: " + i + " Type: "
					+ (headerType == null ? null : headerType) + " Name: "
					+ (header.getText(i) == null ? null : header.getText(i)));
			if (headerType.equals(TableMessageContent.TEXT)) {
				final int messageIndex = textIdx++;
				Column<TableMessage, String> textColumn = new Column<TableMessage, String>(
						new TextCell()) {
					@Override
					public String getValue(TableMessage object) {
						return object.getText(messageIndex);
					}
				};
				textColumn.setSortable(true);
				textColumn.setDefaultSortAscending(false);
				sortHandler.setComparator(textColumn, TableMessage
						.getMessageComparator(messageIndex, headerType));
				cellTable.addColumn(textColumn, header.getText(i));
				// cellTable.setColumnWidth(addressColumn, 60, Unit.PCT);
			} else if (headerType.equals(TableMessageContent.NUMBER)) {
				String fmtOption = header.getFormatOption(i);
				if(fmtOption == null)
					fmtOption = "#,##0.00";
				final NumberFormat nf = NumberFormat.getFormat(fmtOption);

				final int messageIndex = numIdx++;
				Column<TableMessage, ?> columnToAdd; 
				if (header.isEditable(i)) {
					final EditTextCell ec = new EditTextCell();
					Column<TableMessage, String> textColumn = new Column<TableMessage, String>(
							ec) {

						@Override
						public String getValue(TableMessage object) {
							Double num = object.getNumber(messageIndex);
							if(num == null) return "edit";
							String numStr = nf.format(num);
							return numStr;
						}
					};
					textColumn
							.setFieldUpdater(new FieldUpdater<TableMessage, String>() {

								@Override
								public void update(int index,
										TableMessage object, String value) {
									try {
										Double newNumber = nf.parse(value);
										object.setNumber(messageIndex, newNumber);
									} catch (NumberFormatException ex) {
										Window.alert("Ignoring entry [" + value +  "]. Invalid number specified");
										object.setNumber(messageIndex, null);
									}	
									ec.clearViewData(object.getMessageId());
									cellTable.redraw();
								}
							});
					columnToAdd = textColumn;
				} else {
					Column<TableMessage, Number> numberColumn = new Column<TableMessage, Number>(
							new NumberCell(nf)) {
						@Override
						public Number getValue(TableMessage object) {
							return object.getNumber(messageIndex);
						}
					};
					columnToAdd = numberColumn;
				}
				columnToAdd.setSortable(true);
				columnToAdd.setDefaultSortAscending(false);
				sortHandler.setComparator(columnToAdd, TableMessage
						.getMessageComparator(messageIndex, headerType));
				cellTable.addColumn(columnToAdd, header.getText(i));
			} else if (headerType.equals(TableMessageContent.DATE)) {
				final int messageIndex = dateIdx++;
				Column<TableMessage, Date> dateColumn = new Column<TableMessage, Date>(
						new DateCell(GUIConstants.DEFAULT_DATE_FORMAT)) {
					@Override
					public Date getValue(TableMessage object) {
						return object.getDate(messageIndex);
					}
				};
				dateColumn.setSortable(true);
				dateColumn.setDefaultSortAscending(false);
				sortHandler.setComparator(dateColumn, TableMessage
						.getMessageComparator(messageIndex, headerType));
				cellTable.addColumn(dateColumn, header.getText(i));
			}
			/*
			 * else if(headerType.equals(TableMessageContent.HTML)) { final int
			 * messageIndex = textIdx++; Column<TableMessage, SafeHtml>
			 * htmlColumn = new Column<TableMessage, SafeHtml>(new
			 * SafeHtmlCell()) {
			 * 
			 * @Override public SafeHtml getValue(TableMessage object) { return
			 * new
			 * SafeHtmlBuilder().appendEscaped(object.getText(messageIndex)).
			 * toSafeHtml(); } }; htmlColumn.setSortable(true);
			 * htmlColumn.setDefaultSortAscending(false);
			 * sortHandler.setComparator(htmlColumn,
			 * TableMessage.getMessageComparator(messageIndex, headerType));
			 * cellTable.addColumn(htmlColumn, header.getText(i)); }
			 */
			else
				continue;
		}
	}
}
