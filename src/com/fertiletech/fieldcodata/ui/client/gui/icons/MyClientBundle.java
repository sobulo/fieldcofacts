package com.fertiletech.fieldcodata.ui.client.gui.icons;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;


public interface MyClientBundle extends ClientBundle {

	//home page logo and slide controls
	ImageResource accounts();
	ImageResource bio();
	ImageResource inventory();
	ImageResource MRS();
	ImageResource prospective();
	ImageResource settings();
	ImageResource vendors();	 
}

