package com.fertiletech.fieldcodata.ui.client.gui.accounts;


import java.util.ArrayList;
import java.util.List;

import com.fertiletech.fieldcodata.ui.client.GUIConstants;
import com.fertiletech.fieldcodata.ui.client.gui.LocalDataProvider;
import com.fertiletech.fieldcodata.ui.client.gui.utils.AccountSelector;
import com.fertiletech.fieldcodata.ui.client.gui.utils.CurrencyBox;
import com.fertiletech.fieldcodata.ui.client.gui.utils.MultipleMessageDialog;
import com.fertiletech.fieldcodata.ui.client.gui.utils.SimpleDialog;
import com.fertiletech.fieldcodata.ui.shared.dto.DTOConstants;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;

public class AddTenantDeposit extends Composite implements ValueChangeHandler<TableMessage>{

	@UiField CurrencyBox amount;
	@UiField TextBox referenceID;
	@UiField TextBox comments;
	@UiField AccountSelector accounts;
	@UiField ListBox type;
	@UiField DateBox depDate;
	@UiField TenantSelectionList tenantSelector;
	@UiField Button saveDeposit;
	
	private void enableFields(boolean enabled)
	{
		amount.setEnabled(enabled);
		referenceID.setEnabled(enabled);
		comments.setEnabled(enabled);
		depDate.setEnabled(enabled);
		accounts.enableBox(enabled);
		type.setEnabled(enabled);
		saveDeposit.setEnabled(enabled);
	}

	public void clear()
	{
		amount.setAmount(null);
		referenceID.setValue(null);
		comments.setValue(null);
		depDate.setValue(null);
	}	
	
	private SimpleDialog infoBox;
	private MultipleMessageDialog errorBox;
	
	private static AddTenantDepositUiBinder uiBinder = GWT
			.create(AddTenantDepositUiBinder.class);

	interface AddTenantDepositUiBinder extends
			UiBinder<Widget, AddTenantDeposit> {
	}
	
	final AsyncCallback<String> saveCallBack = new AsyncCallback<String>() {
		@Override
		public void onFailure(Throwable caught) {
			errorBox.show(caught.getMessage());
			enableFields(true);
		}

		@Override
		public void onSuccess(String result) {
			infoBox.show(result);
			clear();
			enableFields(true);
		}
	};	

	public AddTenantDeposit() {
		initWidget(uiBinder.createAndBindUi(this));
		infoBox = new SimpleDialog("INFO");
		errorBox = new MultipleMessageDialog("ERRORS");
		saveDeposit.setEnabled(false);
		depDate.setFormat(GUIConstants.DEFAULT_DATEBOX_FORMAT);
		enableFields(false);
		saveDeposit.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				if(checkErrors())
					return;
				enableFields(false);
				LocalDataProvider.ACCOUNT_SERVICE.createDeposit(accounts.getValue(accounts.getSelectedIndex()), depDate.getValue(), referenceID.getValue(), 
						comments.getValue(), type.getValue(type.getSelectedIndex()), amount.getAmount(), saveCallBack);
			}
		});
		tenantSelector.addValueChangeHandler(new ValueChangeHandler<TableMessage>() {

			private AsyncCallback<List<TableMessage>> resAcctCallback = new AsyncCallback<List<TableMessage>>() {

				@Override
				public void onFailure(Throwable caught) {
					errorBox.show("Unable to fetch account list<br/>" + caught.getMessage());
				}

				@Override
				public void onSuccess(List<TableMessage> result) {
					populateDepositTypes(result);
					if(type.getItemCount() > 0)
					{
						enableFields(true);
					}
				}
			};

			@Override
			public void onValueChange(ValueChangeEvent<TableMessage> event) {
				if(event.getValue() == null) return;
				type.clear();
				clear();
				enableFields(false);
				TableMessage tenant = event.getValue();
				LocalDataProvider.ACCOUNT_SERVICE.getResidentAccountInfo(tenant.getMessageId(), resAcctCallback );
			}
		});
	}
	
	public boolean checkErrors()
	{
		ArrayList<String> errors = new ArrayList<String>(3);
		if(amount.getAmount() == null)
			errors.add("Amount must be a valid number");
		if(referenceID.getValue().trim().length() == 0)
			errors.add("Reference ID field must be populated");
		if(depDate.getValue() == null)
			errors.add("Specify a valid date for when this amount was deposited at the bank");
		boolean errorsExist = errors.size() > 0;
		if(errorsExist)
			errorBox.show("Please fix issues below", errors);
		return errorsExist;
	}
	
	public void populateDepositTypes(List<TableMessage> acctTypes)
	{
		acctTypes.remove(0); //remove header
		for(TableMessage typeItem : acctTypes)
		{
			String t = typeItem.getText(DTOConstants.RSD_ACCT_TYPE_IDX);
			String c = typeItem.getText(DTOConstants.RSD_ACCT_CURR_IDX);
			String amt = NumberFormat.getDecimalFormat().format(typeItem.getNumber(DTOConstants.RSD_ACCT_AMT_IDX));
			String name = GUIConstants.getAcctDisplayName(t, c);
			type.addItem(name + "/" + amt, typeItem.getMessageId());
		}
	}

	@Override
	public void onValueChange(ValueChangeEvent<TableMessage> event) {
		infoBox.show("Warning: Tenant changed to " + event.getValue().getText(DTOConstants.TNT_LNAME_IDX)+ 
				". Clicking button 'save payment' will reflect a recent deposit by this tenant");
	}
	
	
}
