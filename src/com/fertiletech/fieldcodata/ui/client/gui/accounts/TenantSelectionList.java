package com.fertiletech.fieldcodata.ui.client.gui.accounts;

import java.util.HashMap;
import java.util.List;

import com.fertiletech.fieldcodata.ui.client.gui.LocalDataProvider;
import com.fertiletech.fieldcodata.ui.client.gui.utils.ArchiveButton;
import com.fertiletech.fieldcodata.ui.client.gui.utils.RangeLabelPager;
import com.fertiletech.fieldcodata.ui.client.gui.utils.ShowMorePagerPanel;
import com.fertiletech.fieldcodata.ui.client.gui.utils.SimpleDialog;
import com.fertiletech.fieldcodata.ui.client.gui.utils.TableMessageKeyProvider;
import com.fertiletech.fieldcodata.ui.shared.dto.DTOConstants;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessageHeader;
import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.cellview.client.HasKeyboardPagingPolicy.KeyboardPagingPolicy;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AbstractImagePrototype;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;
import com.sun.corba.se.pept.transport.ContactInfo;

public class TenantSelectionList extends Composite implements ChangeHandler, HasValueChangeHandlers<TableMessage>{

	@UiField
	ArchiveButton archive;
	private static TenantSelectionListUiBinder uiBinder = GWT
			.create(TenantSelectionListUiBinder.class);

	interface TenantSelectionListUiBinder extends
			UiBinder<Widget, TenantSelectionList> {
	}
	
	LocalDataProvider pc = new LocalDataProvider();

	public TenantSelectionList() {
		initWidget(uiBinder.createAndBindUi(this));
		archive.addValueChangeHandler(new ValueChangeHandler<String>() {

			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				switchBuildingDisplay();
			}
		});
		Images images = GWT.create(Images.class);

		// Create a CellList.
		ContactCell contactCell = new ContactCell(images.contact());

		// Set a key provider that provides a unique key for each contact. If
		// key is
		// used to identify contacts when fields (such as the name and address)
		// change.
		cellList = new CellList<TableMessage>(contactCell,
				TableMessageKeyProvider.KEY_PROVIDER);
		//cellList.setPageSize(30);
		cellList.setKeyboardPagingPolicy(KeyboardPagingPolicy.INCREASE_RANGE);
		cellList.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.BOUND_TO_SELECTION);

		// Add a selection model so we can select cells.
		selectionModel = new SingleSelectionModel<TableMessage>(
				TableMessageKeyProvider.KEY_PROVIDER);
		cellList.setSelectionModel(selectionModel);

		errorBox = new SimpleDialog("<font color='red'>Error Occured</font>", true);

		// server side async calls
		buildingNames.addChangeHandler(this);

		// Add the CellList to the data provider in the database.

		// Set the cellList as the display of the pagers. This example has two
		// pagers. pagerPanel is a scrollable pager that extends the range when
		// the
		// user scrolls to the bottom. rangeLabelPager is a pager that displays
		// the
		// current range, but does not have any controls to change the range.
		pagerPanel.setDisplay(cellList);
		//rangeLabelPager.setDisplay(cellList);

		selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {			
			@Override
			public void onSelectionChange(SelectionChangeEvent event) {
				fireTenantChangeEvent();
			}
		});

		
		if(pc.getBuildingNamesSize() == 0)
		{
			buildingHeader.setText("Loading ...");
			LocalDataProvider.TENANT_SERVICE.getBuildingNames(buildingNamesCallback);
		}
		else
		{
			pc.populateListBoxWithBuildingNames(buildingNames);
			switchBuildingDisplay();
		}
		
	}
	
	public void fireTenantChangeEvent()
	{
		ValueChangeEvent.fire(this, selectionModel.getSelectedObject());
	}
	

	SingleSelectionModel<TableMessage> selectionModel;
	/**
	 * The pager used to change the range of data.
	 */

	@UiField
	ShowMorePagerPanel pagerPanel;

	/**
	 * The pager used to display the current range.
	 */

	//@UiField
	//RangeLabelPager rangeLabelPager;

	@UiField
	ListBox buildingNames;
	
	@UiField
	Label buildingHeader;
	
	private SimpleDialog errorBox;	

	/**
	 * The images used for this example.
	 */
	static interface Images extends ClientBundle {
		ImageResource contact();
	}

	/**
	 * The Cell used to render a {@link ContactInfo}.
	 */

	static class ContactCell extends AbstractCell<TableMessage> {

		/**
		 * The html of the image used for contacts.
		 */
		private final String imageHtml;

		public ContactCell(ImageResource image) {
			this.imageHtml = AbstractImagePrototype.create(image).getHTML();
		}

		@Override
		public void render(Context context, TableMessage value,
				SafeHtmlBuilder sb) {
			// Value can be null, so do a null check..
			if (value == null) {
				return;
			}
            String archiveColorStyle = "";
            if(Boolean.valueOf(value.getText(DTOConstants.TNT_IS_ARCH_IDX)))
            	archiveColorStyle = " style='color: red;'";
			sb.appendHtmlConstant("<table" + archiveColorStyle + ">");

			// Add the contact image.
			sb.appendHtmlConstant("<tr><td rowspan='3'>");
			sb.appendHtmlConstant(imageHtml);
			sb.appendHtmlConstant("</td>");

			String firstName = value.getText(DTOConstants.TNT_FNAME_IDX);
			firstName = firstName.length() == 0 ? "" : firstName + " ";
			String fullName = firstName
					+ value.getText(DTOConstants.TNT_LNAME_IDX);
			// Add the name and address.
			sb.appendHtmlConstant("<td style='font-size:95%;'>");
			sb.appendEscaped(fullName);
			sb.appendHtmlConstant("</td></tr><tr><td>");
			sb.appendEscaped(value.getText(DTOConstants.TNT_BUILDING_IDX) + " "
					+ value.getText(DTOConstants.TNT_APT_IDX));
			sb.appendHtmlConstant("</td></tr><tr><td style='font-size:70%;'>");
			sb.appendEscaped("Owner: " + value.getText(DTOConstants.TNT_IS_OWNR_IDX) + " Archived: "
					+ value.getText(DTOConstants.TNT_IS_ARCH_IDX));
			sb.appendHtmlConstant("</td></tr></table>");
		}
	}

	/**
	 * The CellList.
	 */

	private CellList<TableMessage> cellList;
	
    // Create an asynchronous callback to handle the result.
    private final AsyncCallback<HashMap<String, String>> buildingNamesCallback = new AsyncCallback<HashMap<String, String>>() {

        @Override
        public void onSuccess(HashMap<String, String> result) {
        	populateBuildingNames(result);
        	buildingNames.setEnabled(true);
        	pc.populateBuildingNamesMap(result);
        }

        @Override
        public void onFailure(Throwable caught) {
        	errorBox.show("Unable to retrieve building names. Try refreshing your browser. " +
        			"Contact info@fertiletech.com if problems persist. <p> Error msg: <b>"+caught.getMessage() + "</b></p>");
        }
    };
    
    // Create an asynchronous callback to handle the result.
    private final AsyncCallback<List<TableMessage>> tenantListCallback = new AsyncCallback<List<TableMessage>>() {

        @Override
        public void onSuccess(List<TableMessage> result) {
        	TableMessageHeader header = (TableMessageHeader) result.remove(0);
        	String buildingID = header.getMessageId();
        	List<TableMessage> tenantList = pc.getTenantInfoList(buildingID + (ignoreArchiveFlag?"":archive.isArchived()));
        	tenantList.clear();
        	tenantList.addAll(result);
        	buildingHeader.setText(header.getCaption());
    		//add display to the new data source
    		pc.addViewToTenantListDataProvider(cellList, buildingID + (ignoreArchiveFlag?"":archive.isArchived()));
        	buildingNames.setEnabled(true);
        	archive.enableBox(true);
        }

        @Override
        public void onFailure(Throwable caught) {
        	errorBox.show("Unable to retrieve tenant names. Try refreshing your browser. " +
        			"Contact info@fertiletech.com if problems persist. <p> Error msg: <b>"+caught.getMessage() + "</b></p>");
        }
    };      

    public TableMessage getSelectedTenant()
    {
    	return selectionModel.getSelectedObject();
    }
	

  private void populateBuildingNames(HashMap<String, String> nameMap)
  {
	  for(String id : nameMap.keySet())
		  buildingNames.addItem(nameMap.get(id), id);
	  buildingNames.setSelectedIndex(0);
	  switchBuildingDisplay();
  }
	


	private void switchBuildingDisplay() {
		buildingNames.setEnabled(false);
		archive.enableBox(false);
		//remove our display from its current data source
		pc.removeDisplayFromTenantMap(cellList);
		
		//fetch list for newly created building
		String buildingID = buildingNames.getValue(buildingNames.getSelectedIndex());
		String buildingName = buildingNames.getItemText(buildingNames.getSelectedIndex());
		
		List<TableMessage> tenantInfoList = pc.getTenantInfoList(buildingID + (ignoreArchiveFlag?"":archive.isArchived()));
		if(tenantInfoList.size() == 0) //nothing found locally, check server to see if data is available
		{
			buildingHeader.setText("Retrieving tenant list, please wait ...");
			LocalDataProvider.TENANT_SERVICE.getBuildingTenants(buildingID, ignoreArchiveFlag?null:archive.isArchived(), tenantListCallback);
		}
		else
		{
			buildingHeader.setText(buildingName + "(" + tenantInfoList.size() + " tenants)");
			buildingNames.setEnabled(true);
			archive.enableBox(true);
			//add display to the new data source
			pc.addViewToTenantListDataProvider(cellList, buildingID + (ignoreArchiveFlag?"":archive.isArchived()));
		}
	}
	
	public void refresh()
	{
		//fetch list for newly created building
		String buildingID = buildingNames.getValue(buildingNames.getSelectedIndex());		
		pc.refresh(buildingID + (ignoreArchiveFlag?"":archive.isArchived()));
	}
	
	boolean ignoreArchiveFlag = false;
	public void ignoreArchiveSettings()
	{
		if(archive == null) return;
		archive.setVisible(false);
		ignoreArchiveFlag = true;
		if(buildingNames.getItemCount() > 0)//minimize race condition
			switchBuildingDisplay(); 
	}

	@Override
	public void onChange(ChangeEvent event) {
		switchBuildingDisplay();
	}

	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<TableMessage> handler) {
		return addHandler(handler, ValueChangeEvent.getType());
	}
}
