/*
 * Copyright 2010 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.fertiletech.fieldcodata.ui.client.gui.accounts.print;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

import com.fertiletech.fieldcodata.ui.client.ContentWidget;
import com.fertiletech.fieldcodata.ui.client.gui.HelpPageGenerator;
import com.fertiletech.fieldcodata.ui.client.gui.LocalDataProvider;
import com.fertiletech.fieldcodata.ui.client.gui.utils.SimpleDialog;
import com.fertiletech.fieldcodata.ui.client.gui.utils.TableMessageKeyProvider;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessageHeader;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.cellview.client.SimplePager.TextLocation;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.view.client.DefaultSelectionEventManager;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.MultiSelectionModel;

//import com.fertiletech.fieldcodata.ui.client.gui.LocalDataProvider;

/**
 * Example file.
 */

public class PrintInvoices extends ContentWidget {

	/*@UiField
	Label typeDisplay;
	@UiField
	CheckBox searchByCreationDate;*/
	@UiField
	DateBox startDate;
	@UiField
	DateBox endDate;

	@UiField
	Button search;
	@UiField
	Button print;
	@UiField
	HTML displayLinkSlot;

	private SimpleDialog errorBox;
	private SimpleDialog infoBox;

	boolean setupHeaderOnFirstCallBack = true;

	ListDataProvider<TableMessage> dataProvider;

	private final static HashMap<String, List<TableMessage>> cachedPayments = new HashMap<String, List<TableMessage>>();

	// Create an asynchronous callback to handle the result.
	private final AsyncCallback<List<TableMessage>> tenantSearchCallback = new AsyncCallback<List<TableMessage>>() {

		@Override
		public void onSuccess(List<TableMessage> result) {
			GWT.log("print searcher returned with: " + result.size());
			TableMessageHeader header = (TableMessageHeader) result.remove(0);
			List<TableMessage> billList = dataProvider.getList();
			billList.clear();
			billList.addAll(result);
			if (setupHeaderOnFirstCallBack) {
				// Attach a column sort handler to the ListDataProvider to sort
				// the list.
				ListHandler<TableMessage> sortHandler = new ListHandler<TableMessage>(
						billList);
				cellTable.addColumnSortHandler(sortHandler);
				initTable(header, sortHandler);
				setupHeaderOnFirstCallBack = false;
			}
			print.setEnabled(true);
			search.setEnabled(true);
		}

		@Override
		public void onFailure(Throwable caught) {
			errorBox.show("Unable to retrieve invoices. Try refreshing your browser. "
					+ "Contact info@fertiletech.com if problems persist. <p> Error msg: <b>"
					+ caught.getMessage() + "</b></p>");
			search.setEnabled(true);
		}
	};

	final AsyncCallback<String> invoiceCallBack = new AsyncCallback<String>() {
		@Override
		public void onFailure(Throwable caught) {
			search.setEnabled(true);
			print.setEnabled(false);
		}

		@Override
		public void onSuccess(String result) {
			infoBox.show("Setup for invoice/bill generation completed. "
					+ "Please click on link in status box to start download");
			displayLinkSlot.setHTML(result);
			search.setEnabled(true);
			print.setEnabled(false);
		}
	};

	/**
	 * The UiBinder interface used by this example.
	 */

	interface Binder extends UiBinder<Widget, PrintInvoices> {
	}

	/**
	 * The main CellTable.
	 */
	@UiField(provided = true)
	CellTable<TableMessage> cellTable;

	/**
	 * The pager used to change the range of data.
	 */
	@UiField(provided = true)
	SimplePager pager;

	/**
	 * An instance of the constants.
	 */
	// MultiSelectionModel<TableMessage> selectionModel;
	/**
	 * Constructor.
	 * 
	 * @param constants
	 *            the constants
	 */
	public PrintInvoices() {
		super("Print Invoices", "Use this module to print out an invoice. Useful in instances where you want to issue a resident an invoice/receipt for a recently billed service");
	}

	@Override
	public boolean hasMargins() {
		return false;
	}

	/**
	 * Initialize this example.
	 */
	@Override
	public Widget onInitialize() {
		// Create a CellTable.

		// Set a key provider that provides a unique key for each contact. If
		// key is
		// used to identify contacts when fields (such as the name and address)
		// change.
		cellTable = new CellTable<TableMessage>(
				TableMessageKeyProvider.KEY_PROVIDER);
		cellTable.setWidth("100%", true);

		// Do not refresh the headers and footers every time the data is
		// updated.
		cellTable.setAutoHeaderRefreshDisabled(true);
		cellTable.setAutoFooterRefreshDisabled(true);

		// Create a Pager to control the table.
		SimplePager.Resources pagerResources = GWT
				.create(SimplePager.Resources.class);
		pager = new SimplePager(TextLocation.CENTER, pagerResources, false, 0,
				true);
		pager.setDisplay(cellTable);

		// Add a selection model so we can select cells.
		final MultiSelectionModel<TableMessage> selectionModel = new MultiSelectionModel<TableMessage>(
				TableMessageKeyProvider.KEY_PROVIDER);
		cellTable.setSelectionModel(selectionModel,
				DefaultSelectionEventManager
						.<TableMessage> createCheckboxManager());

		// Initialize the columns.
		// Checkbox column. This table will uses a checkbox column for
		// selection.
		// Alternatively, you can call cellTable.setSelectionEnabled(true) to
		// enable
		// mouse selection.
		Column<TableMessage, Boolean> checkColumn = new Column<TableMessage, Boolean>(
				new CheckboxCell(true, false)) {
			@Override
			public Boolean getValue(TableMessage object) {
				// Get the value from the selection model.
				return selectionModel.isSelected(object);
			}
		};

		cellTable.addColumn(checkColumn,
				SafeHtmlUtils.fromSafeConstant("<br/>"));
		cellTable.setColumnWidth(checkColumn, 40, Unit.PX);
		dataProvider = new ListDataProvider<TableMessage>();
		dataProvider.addDataDisplay(cellTable);

		// Create the UiBinder.
		Binder uiBinder = GWT.create(Binder.class);
		Widget widget = uiBinder.createAndBindUi(this);

		// add click handlers
		search.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				print.setEnabled(false);
				search.setEnabled(false);
				displayLinkSlot.setHTML("");
				LocalDataProvider.ACCOUNT_SERVICE.getBills(startDate.getValue(), endDate.getValue(), tenantSearchCallback);
			}
		});

		print.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				Set<TableMessage> chosen = selectionModel.getSelectedSet();
				
				if(chosen.size() == 0)
				{
					errorBox.show("You must first select an invoice. Try searching first, then select the invoices you'd like to print");
					return;
				}
				
				String[] tenantKeys = new String[chosen.size()];
				int i = 0;
				for (TableMessage m : chosen)
					tenantKeys[i++] = m.getMessageId();

				search.setEnabled(false);
				print.setEnabled(false);
				LocalDataProvider.ACCOUNT_SERVICE.getInvoiceDownloadLink(tenantKeys,invoiceCallBack);
			}
		});
		print.setEnabled(false);
		search.setEnabled(true);
		infoBox = new SimpleDialog("INFO");
		errorBox = new SimpleDialog("Error");
		return widget;
	}

	private void initTable(TableMessageHeader h,
			ListHandler<TableMessage> sortHandler) {
		LocalDataProvider.initTableColumns(h, cellTable, sortHandler);
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
		GWT.runAsync(PrintInvoices.class, new RunAsyncCallback() {

			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}

			@Override
			public void onSuccess() {
				callback.onSuccess(onInitialize());
			}
		});
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_ACCOUNTS_URL;
	}
}
