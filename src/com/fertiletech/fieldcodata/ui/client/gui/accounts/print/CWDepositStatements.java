package com.fertiletech.fieldcodata.ui.client.gui.accounts.print;

import com.fertiletech.fieldcodata.ui.client.ContentWidget;
import com.fertiletech.fieldcodata.ui.client.gui.HelpPageGenerator;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class CWDepositStatements extends ContentWidget{

	public CWDepositStatements() {
		super("Account Statements", "Use this module to generate a printable statement of accounts" +
				" showing debits/credits on selected resident accounts.");
	}

	@Override
	public Widget onInitialize() {
		return new PrintDepositStatement();
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
		GWT.runAsync(CWDepositStatements.class, new RunAsyncCallback() {

			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}

			@Override
			public void onSuccess() {
				callback.onSuccess(onInitialize());
			}
		});
	}

	@Override
	public boolean hasScrollableContent()
	{
		return false;
	}
	
	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_ACCOUNTS_URL;
	}
}
