package com.fertiletech.fieldcodata.ui.client.gui.accounts.reps;

import com.fertiletech.fieldcodata.ui.client.ContentWidget;
import com.fertiletech.fieldcodata.ui.client.gui.HelpPageGenerator;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class CWEditCompanyRep extends ContentWidget{

	public CWEditCompanyRep() {
		super("Billing Rep", "Use this module to setup contact information of billing recipient. Useful in instances where the resident's company " +
				"(i.e. not the resident) is responsible for funding deposit requests");
	}

	@Override
	public Widget onInitialize() {
		return new EditRepsPanel();
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
		GWT.runAsync(CWEditCompanyRep.class, new RunAsyncCallback() {

			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}

			@Override
			public void onSuccess() {
				callback.onSuccess(onInitialize());
			}
		});
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_ACCOUNTS_URL;
	}
}