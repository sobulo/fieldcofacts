package com.fertiletech.fieldcodata.ui.client.gui.accounts;

import com.fertiletech.fieldcodata.ui.client.ContentWidget;
import com.fertiletech.fieldcodata.ui.client.gui.HelpPageGenerator;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class CWResidentAccount extends ContentWidget{

	public CWResidentAccount() {
		super("Create Resident Account", "Use this module to create resident accounts. " +
				"Use the deposit module to enter an opening balance once the account has been created.");
	}

	@Override
	public Widget onInitialize() {
		return new ResidentAccount(false);
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
	    GWT.runAsync(CWResidentAccount.class, new RunAsyncCallback() {

	        public void onFailure(Throwable caught) {
	          callback.onFailure(caught);
	        }

	        public void onSuccess() {
	          callback.onSuccess(onInitialize());
	        }
	      });		
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_ACCOUNTS_URL;
	}

}
