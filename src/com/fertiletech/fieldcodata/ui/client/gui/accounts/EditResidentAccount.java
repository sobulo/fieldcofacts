package com.fertiletech.fieldcodata.ui.client.gui.accounts;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class EditResidentAccount extends Composite {

	private static EditResidentAccountUiBinder uiBinder = GWT
			.create(EditResidentAccountUiBinder.class);

	interface EditResidentAccountUiBinder extends
			UiBinder<Widget, EditResidentAccount> {
	}

	public EditResidentAccount() {
		initWidget(uiBinder.createAndBindUi(this));
	}

}
