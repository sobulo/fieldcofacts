package com.fertiletech.fieldcodata.ui.client.gui.accounts;

import java.util.List;

import com.fertiletech.fieldcodata.ui.client.gui.LocalDataProvider;
import com.fertiletech.fieldcodata.ui.client.gui.utils.DepositSearchPanel;
import com.fertiletech.fieldcodata.ui.client.gui.utils.ShowcaseTable;
import com.fertiletech.fieldcodata.ui.client.gui.utils.SimpleDialog;
import com.fertiletech.fieldcodata.ui.client.gui.utils.TableMessageKeyProvider;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessageHeader;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.cellview.client.SimplePager.TextLocation;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.DefaultSelectionEventManager;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.SingleSelectionModel;

public class ViewDepositBlotter extends Composite{
	@UiField
	DepositSearchPanel depositSearcher;
	
	@UiField(provided=true)
	ShowcaseTable display;


	private static ViewDepositBlotterUiBinder uiBinder = GWT
			.create(ViewDepositBlotterUiBinder.class);

	interface ViewDepositBlotterUiBinder extends
			UiBinder<Widget, ViewDepositBlotter> {
	}

	public ViewDepositBlotter() {
		display = new ShowcaseTable(null);
		// create widget from binder
		initWidget(uiBinder.createAndBindUi(this));

		depositSearcher
				.addValueChangeHandler(new ValueChangeHandler<List<TableMessage>>() {

					@Override
					public void onValueChange(
							ValueChangeEvent<List<TableMessage>> event) {
						if(event.getValue() == null)
							display.clear();
						else
							display.showTable(event.getValue());
					}
				});
	}

}
