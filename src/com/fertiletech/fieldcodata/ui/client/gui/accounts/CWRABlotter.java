package com.fertiletech.fieldcodata.ui.client.gui.accounts;

import com.fertiletech.fieldcodata.ui.client.ContentWidget;
import com.fertiletech.fieldcodata.ui.client.gui.HelpPageGenerator;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class CWRABlotter extends ContentWidget{

	public CWRABlotter() {
		super("Accounts Blotter","Use this panel to view all debits and credits on a resident account");
	}

	@Override
	public Widget onInitialize() {
		return new ResidentAccountBlotter();
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
	    GWT.runAsync(CWRABlotter.class, new RunAsyncCallback() {

	        public void onFailure(Throwable caught) {
	          callback.onFailure(caught);
	        }

	        public void onSuccess() {
	          callback.onSuccess(onInitialize());
	        }
	      });
	}

	@Override
	public boolean hasScrollableContent()
	{
		return false;
	}
	
	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_ACCOUNTS_URL;
	}

}
