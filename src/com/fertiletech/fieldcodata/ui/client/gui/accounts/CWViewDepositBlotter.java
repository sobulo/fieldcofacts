package com.fertiletech.fieldcodata.ui.client.gui.accounts;

import com.fertiletech.fieldcodata.ui.client.ContentWidget;
import com.fertiletech.fieldcodata.ui.client.gui.HelpPageGenerator;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class CWViewDepositBlotter extends ContentWidget
{
	public CWViewDepositBlotter() 
	{
		super("Deposit Blotter", "Search for information on money paid in by tenants");
	}

	@Override
	public Widget onInitialize() {
		return new ViewDepositBlotter();
	}

	@Override
	public boolean hasScrollableContent()
	{
		return false;
	}
	
	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
		GWT.runAsync(CWViewDepositBlotter.class, new RunAsyncCallback() {

	        public void onFailure(Throwable caught) {
	          callback.onFailure(caught);
	        }

	        public void onSuccess() {
	          callback.onSuccess(onInitialize());
	        }
	      });
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_ACCOUNTS_URL;
	}

}
