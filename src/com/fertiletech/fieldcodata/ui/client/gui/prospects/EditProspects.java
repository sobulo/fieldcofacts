package com.fertiletech.fieldcodata.ui.client.gui.prospects;

import com.fertiletech.fieldcodata.ui.client.gui.utils.ProspectiveSuggestBox;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class EditProspects extends Composite{

	@UiField ProspectiveTenantPanel prospectPanel;
	@UiField ProspectiveSuggestBox searchBox;
	
	private static EditProspectsUiBinder uiBinder = GWT
			.create(EditProspectsUiBinder.class);

	interface EditProspectsUiBinder extends UiBinder<Widget, EditProspects> {
	}

	public EditProspects() {
		initWidget(uiBinder.createAndBindUi(this));
		prospectPanel.addValueChangeHandler(new ValueChangeHandler<Boolean>() {

			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				searchBox.clear();
			}
		});

		searchBox.addValueChangeHandler(new ValueChangeHandler<String>() {
			
			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				GWT.log("Received suggestion:" + event.getValue());
				GWT.log("search box selected user: " + searchBox.getSelectedUser());				
				GWT.log("search box selected display: " + searchBox.getSelectedUserDisplay());
				prospectPanel.loadProspect(event.getValue(), false);
			}
		});
	}
}
