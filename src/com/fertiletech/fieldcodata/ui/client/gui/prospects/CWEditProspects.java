package com.fertiletech.fieldcodata.ui.client.gui.prospects;

import com.fertiletech.fieldcodata.ui.client.CWArguments;
import com.fertiletech.fieldcodata.ui.client.ContentWidget;
import com.fertiletech.fieldcodata.ui.client.gui.HelpPageGenerator;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class CWEditProspects extends ContentWidget implements CWArguments{
	EditProspects prospectContainer = null;
	String savedId = null;

	public CWEditProspects() {
		super("Edit Prospect", "Search for prospective resident information that's been saved. " +
				"For people confirmed as residents already, use the edit resident module");
	}

	@Override
	public Widget onInitialize() {
		prospectContainer = new EditProspects();
		if(savedId != null)
			setParameterValues(savedId);
		return prospectContainer;
	}
	
	
	public boolean hasScrollableContent()
	{
		return false;
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
	    GWT.runAsync(CWEditProspects.class, new RunAsyncCallback() {

	        public void onFailure(Throwable caught) {
	          callback.onFailure(caught);
	        }

	        public void onSuccess() {
	          callback.onSuccess(onInitialize());
	        }
	      });
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_PROSPECT_URL;
	}

	@Override
	public void setParameterValues(String args) {
		if(prospectContainer != null)
			prospectContainer.prospectPanel.loadProspect(args, true);
		else
			savedId = args;
	}

}
