package com.fertiletech.fieldcodata.ui.client.gui.prospects;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import com.fertiletech.fieldcodata.ui.client.GUIConstants;
import com.fertiletech.fieldcodata.ui.client.gui.LocalDataProvider;
import com.fertiletech.fieldcodata.ui.client.gui.utils.CommentsPanel;
import com.fertiletech.fieldcodata.ui.client.gui.utils.CurrencyBox;
import com.fertiletech.fieldcodata.ui.client.gui.utils.MoneySignBox;
import com.fertiletech.fieldcodata.ui.client.gui.utils.MultipleMessageDialog;
import com.fertiletech.fieldcodata.ui.client.gui.utils.SimpleDialog;
import com.fertiletech.fieldcodata.ui.client.gui.utils.UserPanel;
import com.fertiletech.fieldcodata.ui.shared.UtilConstants;
import com.fertiletech.fieldcodata.ui.shared.UtilConstants.ContactTypes;
import com.fertiletech.fieldcodata.ui.shared.dto.DTOConstants;
import com.fertiletech.fieldcodata.ui.shared.dto.FormConstants;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.logical.shared.BeforeSelectionEvent;
import com.google.gwt.event.logical.shared.BeforeSelectionHandler;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.StackLayoutPanel;
import com.google.gwt.user.client.ui.TabLayoutPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;

public class ProspectiveTenantPanel extends Composite implements HasValueChangeHandlers<Boolean>{

	@UiField
	UserPanel bioPanel;
	@UiField
	Button save;

	// supplementary fields - client visit
	@UiField
	StackLayoutPanel supplementContainer;
	@UiField
	DateBox startOfTenancyPeriod;
	@UiField
	DateBox endOfTenancyPeriod;
	@UiField
	DateBox requestDate;
	@UiField
	DateBox visitDate;
	@UiField
	ListBox buildingFeatures;
	@UiField
	TextArea propertiesInspected;
	@UiField
	ListBox years;
	@UiField
	CurrencyBox budget;
	@UiField
	TextArea prefComments;
	@UiField
	TextBox staffNameTextBox;

	// supplementary fields - documentation
	@UiField
	RadioButton offerLetterSentYes;
	@UiField
	RadioButton offerLetterSentNo;
	@UiField
	DateBox letterSentDate;
	@UiField
	RadioButton draftSentYes;
	@UiField
	RadioButton draftSentNo;
	@UiField
	DateBox draftSentDate;
	@UiField
	TextBox docsFolder;
	@UiField
	Anchor docsFolderLink;
	@UiField
	TextArea docComments;

	// supplementary fields - payment info
	@UiField
	CurrencyBox legalFeeTextBox;
	@UiField
	MoneySignBox legalCcyBox;
	@UiField
	DateBox legalFeeDateBox;
	@UiField
	CurrencyBox rentTextBox;
	@UiField
	MoneySignBox rentCcyBox;	
	@UiField
	DateBox rentDateBox;
	@UiField
	CheckBox transactionConcluded;
	@UiField
	CurrencyBox serviceChargeTextBox;
	@UiField
	MoneySignBox serviceCcyBox;	
	@UiField
	DateBox serviceChargeDateBox;
	@UiField
	CurrencyBox phcnDieselChargesTextBox;
	@UiField
	MoneySignBox phcnCcyBox;	
	@UiField
	DateBox phcnDieselChargesDateBox;
	@UiField
	CurrencyBox agencyFeeTextBox;
	@UiField
	MoneySignBox agencyCcyBox;	
	@UiField
	DateBox agencyDateBox;
	@UiField
	TextArea paymentComments;
	
	@UiField 
	CommentsPanel generalComments;

	@UiField
	TabLayoutPanel propectiveClientDetailsTabPanel;

	@UiField
	Button supplementSave;
	@UiField
	Button clear;


	private SimpleDialog infoBox;
	private MultipleMessageDialog errorBox;
	private final static int SUPPLEMENT_TAB_IDX = 1;
	private final static int COMMENT_TAB_IDX = 2;
	boolean supplementLoaded, commentsLoaded;
	
	private static ProspectiveTenantUiBinder uiBinder = GWT
			.create(ProspectiveTenantUiBinder.class);

	interface ProspectiveTenantUiBinder extends
			UiBinder<Widget, ProspectiveTenantPanel> {
	}

	final AsyncCallback<String[]> newCallback = new AsyncCallback<String[]>() {

		@Override
		public void onSuccess(String[] result) {
			bioPanel.setUserID(result[UtilConstants.SAVE_CONTACT_KEY_IDX]);
			infoBox.show("Updated contact info for "
					+ result[UtilConstants.SAVE_CONTACT_VAL_IDX]
					+ " successfully");
			save.setEnabled(true);
		}

		@Override
		public void onFailure(Throwable caught) {
			errorBox.show("Unable to save values. Error was: "
					+ caught.getMessage());
			save.setEnabled(true);
		}
	};
	
	final AsyncCallback<TableMessage> saveCallback = new AsyncCallback<TableMessage>() {

		@Override
		public void onSuccess(TableMessage result) {
			bioPanel.setUserID(result.getMessageId());
			infoBox.show("Updated contact info for "
					+ result.getText(DTOConstants.TNT_LNAME_IDX)
					+ " successfully");
			save.setEnabled(true);
		}

		@Override
		public void onFailure(Throwable caught) {
			errorBox.show("Unable to save values. Error was: "
					+ caught.getMessage());
			save.setEnabled(true);
		}
	};	

	final AsyncCallback<String> saveProspectiveCallback = new AsyncCallback<String>() {

		@Override
		public void onSuccess(String result) {
			GWT.log("Saved prospective info");
			infoBox.show("Saved client/prospective resident information succesfully: <br/>");
			supplementSave.setEnabled(true);
		}

		@Override
		public void onFailure(Throwable caught) {
			GWT.log("error occured saving prospective info");
			errorBox.show("Unable to save values. Error was: "
					+ caught.getMessage());
			supplementSave.setEnabled(true);
		}
	};

	public ProspectiveTenantPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		//datebox formats
		startOfTenancyPeriod.setFormat(GUIConstants.DEFAULT_DATEBOX_FORMAT);
		endOfTenancyPeriod.setFormat(GUIConstants.DEFAULT_DATEBOX_FORMAT);
		requestDate.setFormat(GUIConstants.DEFAULT_DATEBOX_FORMAT);
		visitDate.setFormat(GUIConstants.DEFAULT_DATEBOX_FORMAT);
		letterSentDate.setFormat(GUIConstants.DEFAULT_DATEBOX_FORMAT);
		draftSentDate.setFormat(GUIConstants.DEFAULT_DATEBOX_FORMAT);
		agencyDateBox.setFormat(GUIConstants.DEFAULT_DATEBOX_FORMAT);
		rentDateBox.setFormat(GUIConstants.DEFAULT_DATEBOX_FORMAT);
		serviceChargeDateBox.setFormat(GUIConstants.DEFAULT_DATEBOX_FORMAT);
		legalFeeDateBox.setFormat(GUIConstants.DEFAULT_DATEBOX_FORMAT);
		phcnDieselChargesDateBox.setFormat(GUIConstants.DEFAULT_DATEBOX_FORMAT);
		infoBox = new SimpleDialog("INFO");
		errorBox = new MultipleMessageDialog("Error");
		setupYears();

		docsFolder.addMouseOutHandler(new MouseOutHandler() {

			@Override
			public void onMouseOut(MouseOutEvent event) {
				docsFolderLink.setHref(docsFolder.getText());
			}
		});
		
		docsFolder.addKeyUpHandler(new KeyUpHandler() {
			
			@Override
			public void onKeyUp(KeyUpEvent event) {
				docsFolderLink.setHref(docsFolder.getText());
			}
		});
		
		supplementContainer.addBeforeSelectionHandler(new BeforeSelectionHandler<Integer>() {

			@Override
			public void onBeforeSelection(BeforeSelectionEvent<Integer> event) {
				if(event.getItem() == supplementContainer.getWidgetCount() - 1)
					event.cancel();
			}
		});
		
		save.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				ArrayList<String> errors = bioPanel.validateFields();
				if (errors.size() > 0) {
					errorBox.show("Fix the errors shown below to save data",
							errors);
					return;
				}
				save.setEnabled(false);
				if (bioPanel.getUserID() == null)
					LocalDataProvider.TENANT_SERVICE.createContact(
							ContactTypes.PROSPECT, bioPanel.getFirstName(),
							bioPanel.getLastName(), bioPanel.getDateOfBirth(), bioPanel.getEmail(),
							bioPanel.getPhone(), bioPanel.getOtherNumbers(),
							bioPanel.getOtherMails(), bioPanel.getCompany(),
							bioPanel.getAddress(), bioPanel.isMale(),
							bioPanel.getSalutation(), null, newCallback);
				else
					LocalDataProvider.TENANT_SERVICE.updateContact(
							bioPanel.getUserID(), bioPanel.getFirstName(),
							bioPanel.getLastName(), bioPanel.getDateOfBirth(), bioPanel.getEmail(),
							bioPanel.getPhone(), bioPanel.getOtherNumbers(),
							bioPanel.getOtherMails(), bioPanel.getCompany(),
							bioPanel.getAddress(), bioPanel.isMale(),
							bioPanel.getSalutation(), null, saveCallback);

			}
		});

		supplementSave.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				GWT.log("checking for errors before saving data");
				ArrayList<String> errors = checkForInvalidProspectiveDataValues(getSupplementaryValues());
				// beforeselection event handling should prevent this from being
				// null, this is just a sanity check
				if (bioPanel.getUserID() == null)
					errors.add("You must first save client bio before you can save supplementary info");				
				if (errors.size() > 0) {
					errorBox.show("Fix issues below", errors);
					return;
				}
				supplementSave.setEnabled(false);
				GWT.log("making call to server to persist suppkementary info");				
				LocalDataProvider.TENANT_SERVICE.saveProspectiveData(
						bioPanel.getUserID(), getSupplementaryValues(),
						saveProspectiveCallback);
			}
		});
		
		clear.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				bioPanel.clear();
				ValueChangeEvent.fire(ProspectiveTenantPanel.this, true);
			}
		});
		
		transactionConcluded.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if(transactionConcluded.getValue())
				{
					ArrayList<String> errors = checkMandatoryProspectiveDataIsGiven(getSupplementaryValues());
					if (errors.size() > 0) {
						errorBox.show("You must fix issues below before you can mark transaction as concluded", errors);
						transactionConcluded.setValue(false);
						return;
					}
					else errors = checkForInvalidProspectiveDataValues(getSupplementaryValues());
					if(errors.size() > 0)
						errorBox.show("Fix issues below", checkForInvalidProspectiveDataValues(getSupplementaryValues()));
				}
			}
		});

		propectiveClientDetailsTabPanel
				.addBeforeSelectionHandler(new BeforeSelectionHandler<Integer>() {
					@Override
					public void onBeforeSelection(
							BeforeSelectionEvent<Integer> event) 
					{
						if (event.getItem() > 0 && bioPanel.getUserID() == null) {
							errorBox.show("You must save bio details of the client before you can access this tab");
							event.cancel();
							return;
						}
						
						if(event.getItem() == SUPPLEMENT_TAB_IDX && !supplementLoaded)
						{
							loadSupplementaryInfo(bioPanel.getUserID());
							supplementLoaded = true;
						}
						else if(event.getItem() == COMMENT_TAB_IDX && !commentsLoaded)
						{
							generalComments.setCommentID(bioPanel.getUserID());
							commentsLoaded = true;
						}
					}
				});

		propectiveClientDetailsTabPanel.setAnimationDuration(1000);
		propectiveClientDetailsTabPanel.setAnimationVertical(true);

	}

	private void setupYears() {
		for (int i = 1; i <= 10; i++)
			years.addItem(String.valueOf(i));
	}

	public HashMap<String, String> getSupplementaryValues() {
		HashMap<String, String> result = new HashMap<String, String>();

		// payment info
		result.put(FormConstants.PD_LEGAL_DATE, formatDate(legalFeeDateBox));
		result.put(FormConstants.PD_RENT_DATE, formatDate(rentDateBox));
		result.put(FormConstants.PD_SERVICE_CHARGE_DATE,
				formatDate(serviceChargeDateBox));
		result.put(FormConstants.PD_DIESEL_DATE,
				formatDate(phcnDieselChargesDateBox));
		result.put(FormConstants.PD_AGENCY_DATE, formatDate(agencyDateBox));
		result.put(FormConstants.PD_LEGAL_FEE, legalFeeTextBox.getAmountString());
		result.put(FormConstants.PD_RENT_FEE, rentTextBox.getAmountString());
		result.put(FormConstants.PD_SERVICE_CHARGE,
				serviceChargeTextBox.getAmountString());
		result.put(FormConstants.PD_DIESEL_FEE,
				phcnDieselChargesTextBox.getAmountString());
		result.put(FormConstants.PD_AGENCY_FEE, agencyFeeTextBox.getAmountString());
		
		result.put(FormConstants.PD_LEGAL_CCY, legalCcyBox.getSign());
		result.put(FormConstants.PD_RENT_CCY, rentCcyBox.getSign());
		result.put(FormConstants.PD_SERVICE_CCY, serviceCcyBox.getSign());
		result.put(FormConstants.PD_DIESEL_CCY, phcnCcyBox.getSign());
		result.put(FormConstants.PD_AGENCY_CCY, agencyCcyBox.getSign());

		
		result.put(FormConstants.PD_PAYMENT_NOTES, paymentComments.getValue());
		result.put(FormConstants.PD_TRANSACTION_CONCL,
				String.valueOf(transactionConcluded.getValue()));

		// client documentation
		result.put(FormConstants.PD_OFFER_SENT,
				offerLetterSentYes.getValue() ? "yes" : "no");
		result.put(FormConstants.PD_OFFER_DATE, formatDate(letterSentDate));
		result.put(FormConstants.PD_DRAFT_DATE, formatDate(draftSentDate));
		result.put(FormConstants.PD_DRAFT_SENT, draftSentYes.getValue() ? "yes"
				: "no");
		result.put(FormConstants.PD_DOCS_FOLDER, docsFolder.getValue());
		result.put(FormConstants.PD_DOCS_NOTES, docComments.getValue());

		GWT.log("Sending doc folder over: " + docsFolder.getValue());
		// client visit
		result.put(FormConstants.PD_TENANCY_START,
				formatDate(startOfTenancyPeriod));
		result.put(FormConstants.PD_REQUEST_DATE,
				formatDate(requestDate));		
		result.put(FormConstants.PD_TENANCY_END, formatDate(endOfTenancyPeriod));
		result.put(FormConstants.PD_VISIT_DATE, formatDate(visitDate));
		result.put(FormConstants.PD_BUILDING_FEATURES,
				getSelectedItems(buildingFeatures));
		result.put(FormConstants.PD_PROP_INSPECTED,
				propertiesInspected.getValue());
		result.put(FormConstants.PD_YEARS,
				years.getValue(years.getSelectedIndex()));
		result.put(FormConstants.PD_BUDGET, budget.getAmountString());
		result.put(FormConstants.PD_PREF_NOTES, prefComments.getValue());
		result.put(FormConstants.PD_STAFF_NAME, staffNameTextBox.getValue());

		return result;
	}

	public static ArrayList<String> checkForInvalidProspectiveDataValues(
			HashMap<String, String> result) {
		ArrayList<String> errors = new ArrayList<String>();
		if (!isEmptyField(result, FormConstants.PD_RENT_FEE) && 
				validateDouble(result.get(FormConstants.PD_RENT_FEE)) == null)
			errors.add("Invalid amount specified for rent");
		if (!isEmptyField(result, FormConstants.PD_RENT_DATE) && 
				validateDate(result.get(FormConstants.PD_RENT_DATE)) == null)
			errors.add("Invalid date specified for rent");
		if (!isEmptyField(result, FormConstants.PD_LEGAL_FEE) && 
				validateDouble(result.get(FormConstants.PD_LEGAL_FEE)) == null)
			errors.add("Invalid amount specified for legal fee");
		if (!isEmptyField(result, FormConstants.PD_LEGAL_DATE) 
				&& validateDate(result.get(FormConstants.PD_LEGAL_DATE)) == null)
			errors.add("Invalid date specified for legal fee");
		if (!isEmptyField(result, FormConstants.PD_AGENCY_FEE) && 
				validateDouble(result.get(FormConstants.PD_AGENCY_FEE)) == null)
			errors.add("Invalid amount specified for agency fee amount");
		if (!isEmptyField(result, FormConstants.PD_AGENCY_DATE)
				&& validateDate(result.get(FormConstants.PD_AGENCY_DATE)) == null)
			errors.add("Invalid date specified for agency fee");
		if(!isEmptyField(result, FormConstants.PD_DOCS_FOLDER) && !result.get(FormConstants.PD_DOCS_FOLDER).toLowerCase().startsWith("https://drive.google.com"))
			errors.add("Client/prospects folder should be a google drive url (web address) that starts with: https://drive.google.com");

		//additional checks
		if (!isEmptyField(result, FormConstants.PD_TENANCY_START) 
				&& validateDate(result.get(FormConstants.PD_TENANCY_START)) == null)
			errors.add("Please specify a valid lease start date");
		if (!isEmptyField(result, FormConstants.PD_TENANCY_END) 
				&& validateDate(result.get(FormConstants.PD_TENANCY_END)) == null)
			errors.add("Please specify a valid lease end date");
		
		return errors;
	}
	
	private static boolean isEmptyField(HashMap<String, String> formData, String key)
	{
		if(formData.get(key) == null || formData.get(key).trim().length() == 0)
			return true;
		else
			return false;
	}
	
	public static ArrayList<String> checkMandatoryProspectiveDataIsGiven(HashMap<String, String> result)
	{
		GWT.log("Docs folder: [" + result.get(FormConstants.PD_DOCS_FOLDER) + "]");
		ArrayList<String> errors = new ArrayList<String>();		
		if (isEmptyField(result, FormConstants.PD_RENT_FEE))
			errors.add("No amount specified for rent");
		if (isEmptyField(result, FormConstants.PD_RENT_DATE))
			errors.add("No date specified for rent");
		if (isEmptyField(result, FormConstants.PD_LEGAL_FEE))
			errors.add("No amount specified for legal fee");
		if (isEmptyField(result, FormConstants.PD_LEGAL_DATE))
			errors.add("No date specified for legal fee");
		if (isEmptyField(result, FormConstants.PD_AGENCY_FEE))
			errors.add("No amount specified for agency fee amount");
		if (isEmptyField(result, FormConstants.PD_AGENCY_DATE))
			errors.add("No date specified for agency fee");
		if (isEmptyField(result, FormConstants.PD_DOCS_FOLDER))
			errors.add("No docs folder specified. Ensure you've uploaded relevant documents to a google drive" +
					" folder created for this client/prospect. At the bare minimum it should contain scanned (or soft) copies of final cover letter, draft lease and lease agreements");
		if (isEmptyField(result, FormConstants.PD_TENANCY_START))
			errors.add("No date specified for lease start");
		if (!isEmptyField(result, FormConstants.PD_TENANCY_END) 
				&& validateDate(result.get(FormConstants.PD_TENANCY_END)) == null)
			errors.add("No date specified for lease end");
		if (!Boolean.valueOf(result.get(FormConstants.PD_TRANSACTION_CONCL)))
			errors.add("Prospective tenant information hasn't been checked as transaction concluded");
		return errors;

	}

	private static Double validateDouble(String doubleStr) {
		try {
			if(doubleStr == null) return null;
			return Double.parseDouble(doubleStr);
		} catch (NumberFormatException ex) {
			return null;
		}
	}

	private static DateBox errorValidation = new DateBox();

	private static Date validateDate(String dateStr) {
		if (dateStr == null) return null;
		return GUIConstants.DEFAULT_DATEBOX_FORMAT.parse(errorValidation,
				dateStr, false);
	}

	private String formatDate(DateBox db) {
		if (db.getValue() == null)
			return null;
		else
			return GUIConstants.DEFAULT_DATEBOX_FORMAT
					.format(db, db.getValue());
	}

	public String getSelectedItems(ListBox lb) {
		String result = "";

		for (int i = 0; i < lb.getItemCount(); i++)
			if (lb.isItemSelected(i))
				result += (lb.getItemText(i) + "|");

		if (result.equals(""))
			return null;

		return result;
	}

	public void setSupplementaryValues(HashMap<String, String> result) {
		// payment info
		legalFeeDateBox.getTextBox().setValue(
				result.get(FormConstants.PD_LEGAL_DATE));
		rentDateBox.getTextBox().setValue(
				result.get(FormConstants.PD_RENT_DATE));
		serviceChargeDateBox.getTextBox().setValue(
				result.get(FormConstants.PD_SERVICE_CHARGE_DATE));
		phcnDieselChargesDateBox.getTextBox().setValue(
				result.get(FormConstants.PD_DIESEL_DATE));
		agencyDateBox.getTextBox().setValue(
				result.get(FormConstants.PD_AGENCY_DATE));
		legalFeeTextBox.setAmountString(result.get(FormConstants.PD_LEGAL_FEE));
		rentTextBox.setAmountString(result.get(FormConstants.PD_RENT_FEE));
		serviceChargeTextBox.setAmountString(result
				.get(FormConstants.PD_SERVICE_CHARGE));
		phcnDieselChargesTextBox.setAmountString(result
				.get(FormConstants.PD_DIESEL_FEE));
		agencyFeeTextBox.setAmountString(result.get(FormConstants.PD_AGENCY_FEE));
		
		legalCcyBox.setVal(result.get(FormConstants.PD_LEGAL_CCY));
		rentCcyBox.setVal(result.get(FormConstants.PD_RENT_CCY));
		serviceCcyBox.setVal(result
				.get(FormConstants.PD_SERVICE_CCY));
		phcnCcyBox.setVal(result
				.get(FormConstants.PD_DIESEL_CCY));
		agencyCcyBox.setVal(result.get(FormConstants.PD_AGENCY_CCY));
		
		
		paymentComments.setValue(result.get(FormConstants.PD_PAYMENT_NOTES));
		setCheckBox(transactionConcluded, FormConstants.PD_TRANSACTION_CONCL,
				result);

		// client documentation
		boolean flip = !setCheckBox(offerLetterSentYes,
				FormConstants.PD_OFFER_SENT, result);
		offerLetterSentNo.setValue(flip);
		flip = !setCheckBox(draftSentYes, FormConstants.PD_DRAFT_SENT, result);
		draftSentNo.setValue(flip);
		docsFolder.setValue(result.get(FormConstants.PD_DOCS_FOLDER));
		docsFolderLink.setHref(result.get(FormConstants.PD_DOCS_FOLDER));
		letterSentDate.getTextBox().setValue(
				result.get(FormConstants.PD_OFFER_DATE));
		draftSentDate.getTextBox().setValue(
				result.get(FormConstants.PD_DRAFT_DATE));
		docComments.setValue(result.get(FormConstants.PD_DOCS_NOTES));

		// client visit
		startOfTenancyPeriod.getTextBox().setValue(
				result.get(FormConstants.PD_TENANCY_START));
		requestDate.getTextBox().setValue(
				result.get(FormConstants.PD_REQUEST_DATE));		
		endOfTenancyPeriod.getTextBox().setValue(
				result.get(FormConstants.PD_TENANCY_END));
		visitDate.getTextBox()
				.setValue(result.get(FormConstants.PD_VISIT_DATE));
		selectMultipleListBox(buildingFeatures,
				result.get(FormConstants.PD_BUILDING_FEATURES));
		propertiesInspected.setValue(result
				.get(FormConstants.PD_PROP_INSPECTED));
		selectAListBoxItem(years, result.get(FormConstants.PD_YEARS));
		budget.setAmountString(result.get(FormConstants.PD_BUDGET));
		prefComments.setValue(result.get(FormConstants.PD_PREF_NOTES));
		staffNameTextBox.setValue(result.get(FormConstants.PD_STAFF_NAME));
	}

	public void clearSupplementaryValues() {
		// payment info
		legalFeeDateBox.getTextBox().setValue(null);
		rentDateBox.getTextBox().setValue(null);
		serviceChargeDateBox.getTextBox().setValue(null);
		phcnDieselChargesDateBox.getTextBox().setValue(null);
		agencyDateBox.getTextBox().setValue(null);
		legalFeeTextBox.setAmountString(null);
		rentTextBox.setAmountString(null);
		serviceChargeTextBox.setAmountString(null);
		phcnDieselChargesTextBox.setAmountString(null);
		agencyFeeTextBox.setAmountString(null);
		paymentComments.setValue(null);
		setCheckBox(transactionConcluded, null, null);
		legalCcyBox.reset();
		rentCcyBox.reset();
		serviceCcyBox.reset();
		phcnCcyBox.reset();
		agencyCcyBox.reset();

		// client documentation
		boolean flip = !setCheckBox(offerLetterSentYes, null, null);
		offerLetterSentNo.setValue(flip);
		flip = !setCheckBox(draftSentYes, null, null);
		draftSentNo.setValue(flip);
		docsFolder.setValue(null);
		docsFolderLink.setHref("");
		letterSentDate.getTextBox().setValue(null);
		draftSentDate.getTextBox().setValue(null);
		docComments.setValue(null);

		// client visit
		startOfTenancyPeriod.getTextBox().setValue(null);
		requestDate.getTextBox().setValue(null);
		endOfTenancyPeriod.getTextBox().setValue(null);
		visitDate.getTextBox().setValue(null);
		for (int i = 0; i < buildingFeatures.getItemCount(); i++)
			buildingFeatures.setItemSelected(i, false);
		propertiesInspected.setValue(null);
		for (int i = 0; i < years.getItemCount(); i++)
			years.setItemSelected(i, false);
		budget.setAmountString(null);
		prefComments.setValue(null);
		staffNameTextBox.setValue(null);
	}

	private void selectMultipleListBox(ListBox lb, String val) {
		if(val == null) return;
		String[] valAsArray = val.trim().split("|");
		for (String s : valAsArray)
			selectAListBoxItem(lb, s);
	}

	private boolean setCheckBox(CheckBox cb, String key,
			HashMap<String, String> result) {
		if (result == null) {
			cb.setValue(false);
			return false;
		} else if (result.get(key).equals("yes")) {
			cb.setValue(true);
			return true;
		} else if (result.get(key).equals("no")) {
			cb.setValue(false);
			return false;
		} else {
			cb.setValue(Boolean.valueOf(result.get(key)));
			return cb.getValue();
		}
	}

	public void loadProspect(String prospectID, boolean fireEvent) {
		supplementLoaded = false; commentsLoaded = false;
		clearSupplementaryValues(); // clear supplement panel
		generalComments.clear(); //clear comments panel
		bioPanel.loadUserInfo(prospectID); //load another client bio
		propectiveClientDetailsTabPanel.selectTab(0, false);
		if(fireEvent)
			ValueChangeEvent.fire(this, true);
	}

	public String getUserKey() {
		return bioPanel.getUserID();
	}

	private void loadSupplementaryInfo(String prospectID){
		AsyncCallback<HashMap<String, String>> callback = new AsyncCallback<HashMap<String, String>>() {

			@Override
			public void onFailure(Throwable caught) {
				errorBox.show("Unable to load supplementary information for prospective tenant");
			}

			@Override
			public void onSuccess(HashMap<String, String> result) {
				propectiveClientDetailsTabPanel.selectTab(1);
				clearSupplementaryValues();
				if(result == null) return;
				GWT.log("received supplementary data with: " + result.size());
				setSupplementaryValues(result);
			}
		};
		GWT.log("making a call to fetch supplementary data: "
				+ prospectID);
		LocalDataProvider.TENANT_SERVICE.getProspectiveData(prospectID,
				callback);
	}
	
	private int selectAListBoxItem(ListBox l, String val)
	{
		for( int i = 0; i < l.getItemCount(); i++)
			if(l.getValue(i).equals(val))
			{
				l.setSelectedIndex(i);
				return i;
			}
		return -1;
	}

	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<Boolean> handler) {
		return addHandler(handler, ValueChangeEvent.getType());
	}
	
}
