package com.fertiletech.fieldcodata.ui.client.gui.prospects;

import com.fertiletech.fieldcodata.ui.client.ContentWidget;
import com.fertiletech.fieldcodata.ui.client.gui.HelpPageGenerator;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class CWLeaseConfirmation extends ContentWidget{

	public CWLeaseConfirmation() {
		super("New Resident", "Use this module once a prospective client has " +
				"signed all lease agreements to indicate he/she is now a resident of a property under management");
	}

	@Override
	public Widget onInitialize() {
		return new LeaseConfirmation();
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {	
	    GWT.runAsync(CWLeaseConfirmation.class, new RunAsyncCallback() {

	        public void onFailure(Throwable caught) {
	          callback.onFailure(caught);
	        }

	        public void onSuccess() {
	          callback.onSuccess(onInitialize());
	        }
	      });		
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_PROSPECT_URL;
	}
}
