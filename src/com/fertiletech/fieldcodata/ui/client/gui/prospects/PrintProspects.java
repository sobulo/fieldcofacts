package com.fertiletech.fieldcodata.ui.client.gui.prospects;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class PrintProspects extends Composite {

	private static ViewProspectsUiBinder uiBinder = GWT
			.create(ViewProspectsUiBinder.class);

	interface ViewProspectsUiBinder extends UiBinder<Widget, PrintProspects> {
	}

	public PrintProspects() {
		initWidget(uiBinder.createAndBindUi(this));
	}

}
