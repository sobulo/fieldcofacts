package com.fertiletech.fieldcodata.ui.client.gui.prospects;

import java.util.Date;
import java.util.List;

import com.fertiletech.fieldcodata.ui.client.gui.HelpPageGenerator;
import com.fertiletech.fieldcodata.ui.client.gui.LocalDataProvider;
import com.fertiletech.fieldcodata.ui.client.gui.utils.ShowcaseTable;
import com.fertiletech.fieldcodata.ui.shared.dto.DTOConstants;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.google.gwt.cell.client.SafeHtmlCell;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.datepicker.client.DateBox;

public class ViewProspects extends Composite{
	
	ShowcaseTable display;
	DateBox startDate;
	DateBox endDate;
	private AsyncCallback<List<TableMessage>> callback = new AsyncCallback<List<TableMessage>>() {

		@Override
		public void onFailure(Throwable caught) {
			Window.alert("Error message: " + caught.getMessage());
			search.setEnabled(true);
			
		}

		@Override
		public void onSuccess(List<TableMessage> result) {
			boolean alreadyInitialized = display.isInitialized();
			display.showTable(result);
			if(!alreadyInitialized)
			{
				SafeHtmlCell cell = new SafeHtmlCell();
				Column<TableMessage, SafeHtml> folderColumn = new Column<TableMessage, SafeHtml>(cell) {

					@Override
					public SafeHtml getValue(TableMessage object) {
						String url = object.getText(DTOConstants.TNT_GENDER_IDX);
						SafeHtmlBuilder sb = new SafeHtmlBuilder();
						if(url == null || url.trim().length()==0) 
							return sb.appendHtmlConstant("<div style='color: red'>none</div>").toSafeHtml();
						sb.appendHtmlConstant("<a target='_blank' href='" + url + "'>Folder</a>");
						return sb.toSafeHtml();
					}
				};
				display.addColumn(folderColumn, "Client Folder");
			}
			search.setEnabled(true);
		}
	};
	
	Button search = new Button("Search");
	public ViewProspects() {
		Grid searchPanel = new Grid(1, 5);
		searchPanel.setCellSpacing(10);
		startDate = new DateBox();
		endDate = new DateBox();
		searchPanel.setWidget(0, 0,new Label("Start: "));
		searchPanel.setWidget(0, 1,(startDate));
		searchPanel.setWidget(0, 2, new Label(" End: "));
		searchPanel.setWidget(0, 3, endDate);
		searchPanel.setWidget(0, 4,search);
		display = new ShowcaseTable();
		display.addValueChangeHandler(new ValueChangeHandler<TableMessage>() {
			
			@Override
			public void onValueChange(ValueChangeEvent<TableMessage> event) {
				if(event.getValue() == null) return;
				else History.newItem(HelpPageGenerator.getHistoryTokenWithArgs(CWEditProspects.class,
						event.getValue().getMessageId()));
				
			}
		});
		
		final DockLayoutPanel h = new DockLayoutPanel(Unit.PX);
		h.addNorth(searchPanel, 50);
		h.add(display);
		search.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				Date start = startDate.getValue();
				Date end = endDate.getValue();
				if(start == null || end == null)
				{
					Window.alert("Please specify values for both start and end dates");
					return;
				}
				search.setEnabled(false);
				LocalDataProvider.TENANT_SERVICE.getProspectsTable(start, end, callback );
			}
		});

		initWidget(h);
	}
}

