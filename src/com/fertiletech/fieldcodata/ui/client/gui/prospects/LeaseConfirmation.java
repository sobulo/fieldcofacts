package com.fertiletech.fieldcodata.ui.client.gui.prospects;

import java.util.ArrayList;
import java.util.HashMap;

import com.fertiletech.fieldcodata.ui.client.gui.LocalDataProvider;
import com.fertiletech.fieldcodata.ui.client.gui.utils.MultipleMessageDialog;
import com.fertiletech.fieldcodata.ui.client.gui.utils.ProspectiveSuggestBox;
import com.fertiletech.fieldcodata.ui.shared.UtilHelper;
import com.fertiletech.fieldcodata.ui.shared.dto.DTOConstants;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;

public class LeaseConfirmation extends Composite implements ChangeHandler, ValueChangeHandler<String>{

	@UiField ListBox buildingNames;
	@UiField Label buildingStatus;
	@UiField ListBox apartmentNames;
	@UiField Button save;
	@UiField ProspectiveSuggestBox searchBox;
	@UiField HTML newTenantSummary;
	MultipleMessageDialog errorBox;
	HashMap<String, HashMap<String, String>[]> cachedApartments = new HashMap<String, HashMap<String, String>[]>();
	TableMessage lastSelectedProspected = null;
	LocalDataProvider pc = new LocalDataProvider();
	private static LeaseConfirmationUiBinder uiBinder = GWT
			.create(LeaseConfirmationUiBinder.class);

	interface LeaseConfirmationUiBinder extends
			UiBinder<Widget, LeaseConfirmation> {
	}

    // Create an asynchronous callback to handle the result.
    private final AsyncCallback<HashMap<String, String>[]> apartmentNamesCallback = new AsyncCallback<HashMap<String, String>[]>() {

        @Override
        public void onSuccess(HashMap<String, String>[] result) {
        	populateApartmentNames(result[DTOConstants.APT_VACANT_IDX]);
           	cachedApartments.put(buildingNames.getValue(buildingNames.getSelectedIndex()), result);
           	buildingStatus.setText("Occuppied: " + result[DTOConstants.APT_OCCUPIED_IDX].size() + 
           			" Vacant: " + result[DTOConstants.APT_VACANT_IDX].size());
           	enableFields(true);
        }

        @Override
        public void onFailure(Throwable caught) {
        	errorBox.show("Unable to retrieve apartment names. Try refreshing your browser. " +
        			"Contact info@fertiletech.com if problems persist. <p> Error msg: <b>"+caught.getMessage() + "</b></p>");
        }
    };

    private final AsyncCallback<TableMessage> prospectNameCallback = new AsyncCallback<TableMessage>() {

        @Override
        public void onSuccess(TableMessage result) {
        	lastSelectedProspected = result;
        	newTenantSummary.setHTML(UtilHelper.getPrintFriendlyContact(result));
        	enableFields(true);
        }

        @Override
        public void onFailure(Throwable caught) {
        	errorBox.show("Unable to retrieve details for selected contact. Try refreshing your browser. " +
        			"Contact info@fertiletech.com if problems persist. <p> Error msg: <b>"+caught.getMessage() + "</b></p>");
        }
    };
    
    private final AsyncCallback<TableMessage> newTenantCallback = new AsyncCallback<TableMessage>() {

        @Override
        public void onSuccess(TableMessage result) {
        	String title = "Database succesfully updated to reflect selected apartment"  + 
        			" is now occupied by resident below";
        	newTenantSummary.setHTML(UtilHelper.getPrintFriendlyContact(result, title));
        	errorBox.show(title);
        	enableFields(true);
        }

        @Override
        public void onFailure(Throwable caught) {
        	errorBox.show("Unable to retrieve details for selected contact. Try refreshing your browser. " +
        			"Contact info@fertiletech.com if problems persist. <p> Error msg: <b>"+caught.getMessage() + "</b></p>");
        }
    };     
    
    // Create an asynchronous callback to handle the result.
    private final AsyncCallback<HashMap<String, String>> buildingNamesCallback = new AsyncCallback<HashMap<String, String>>() {

        @Override
        public void onSuccess(HashMap<String, String> result) {
        	populateBuildingNames(result);
        	enableFields(true);
        	pc.populateBuildingNamesMap(result);
        	loadBuildingVacancies();
        }

        @Override
        public void onFailure(Throwable caught) {
        	errorBox.show("Unable to retrieve building names. Try refreshing your browser. " +
        			"Contact info@fertiletech.com if problems persist. <p> Error msg: <b>"+caught.getMessage() + "</b></p>");
        }
    };
	
	public LeaseConfirmation() {
		initWidget(uiBinder.createAndBindUi(this));
		errorBox = new MultipleMessageDialog("INFO");
		if(pc.getBuildingNamesSize() == 0)
		{
			buildingStatus.setText("Loading building names");
			LocalDataProvider.TENANT_SERVICE.getBuildingNames(buildingNamesCallback);
			enableFields(false);
		}
		else
			pc.populateListBoxWithBuildingNames(buildingNames);
		buildingNames.addChangeHandler(this);
		searchBox.addValueChangeHandler(this);
		save.addClickHandler(new ClickHandler() {
			AsyncCallback<HashMap<String, String>> callback = new AsyncCallback<HashMap<String,String>>() {

				@Override
				public void onFailure(Throwable caught) {
					String msg = "Unable to check paperwork/transaction status, try refreshing your browser";
					errorBox.show(msg);
					buildingStatus.setText(msg);
				}

				@Override
				public void onSuccess(HashMap<String, String> result) {
					enableFields(true);
					ArrayList<String> errors = ProspectiveTenantPanel.checkMandatoryProspectiveDataIsGiven(result);
					if(errors.size() > 0)
					{
						errorBox.show("Please fix errors below", errors);
						buildingStatus.setText("Paperwork/transaction confirmation check failed: Found " + errors.size() + " errors");
						return;
					}
					else errors = ProspectiveTenantPanel.checkForInvalidProspectiveDataValues(result);
					if(errors.size() > 0)
					{
						errorBox.show("Please fix errors below", errors);
						buildingStatus.setText("Paperwork/transaction confirmation check failed: Found " + errors.size() + " errors");
						return;
					}
					lastSelectedProspected.setText(DTOConstants.TNT_APT_ID_IDX, apartmentNames.getValue(apartmentNames.getSelectedIndex()));
					LocalDataProvider.TENANT_SERVICE.createNewTenant(result, lastSelectedProspected, newTenantCallback);
				}
			};
			
			@Override
			public void onClick(ClickEvent event) {
				enableFields(false);
				if(lastSelectedProspected == null)
				{
					errorBox.show("Please select a contact to save as a tenant");
					return;
				}
				buildingStatus.setText("Checking database to confirm paperwork/transaction has been marked concluded");
				LocalDataProvider.TENANT_SERVICE.getProspectiveData(lastSelectedProspected.getMessageId(), callback);
			}
		});	
	}
	
	private void enableFields(boolean enabled)
	{
		searchBox.setEnabled(enabled);
		save.setEnabled(enabled);
		buildingNames.setEnabled(enabled);
		apartmentNames.setEnabled(enabled);
	}
	
	  private void populateBuildingNames(HashMap<String, String> nameMap)
	  {
		  for(String id : nameMap.keySet())
			  buildingNames.addItem(nameMap.get(id), id);
		  buildingNames.setSelectedIndex(0);
	  }
	  private void populateApartmentNames(HashMap<String, String> nameMap)
	  {
		  apartmentNames.clear();
		  for(String id : nameMap.keySet())
			  apartmentNames.addItem(nameMap.get(id), id);
		  
		  if(nameMap.size() == 0)
			  apartmentNames.addItem("No Vacancies");
		  
		  apartmentNames.setSelectedIndex(0);
	  }	  

	@Override
	public void onChange(ChangeEvent event) {
		loadBuildingVacancies();
	}

	private void loadBuildingVacancies()
	{
		String buildingID = buildingNames.getValue(buildingNames.getSelectedIndex());
		String buildingName = buildingNames.getItemText(buildingNames.getSelectedIndex());
		HashMap<String, String>[] apartments = cachedApartments.get(buildingID);
		if(apartments == null)
		{
			enableFields(false);
			buildingStatus.setText("Loading vacant apartments for " + buildingName);
			LocalDataProvider.TENANT_SERVICE.getApartments(buildingID, apartmentNamesCallback);
			return;
		}
		populateApartmentNames(apartments[DTOConstants.APT_VACANT_IDX]);
	}

	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		enableFields(false);
		LocalDataProvider.TENANT_SERVICE.getContact(event.getValue(), prospectNameCallback);
	}
}
