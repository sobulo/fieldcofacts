package com.fertiletech.fieldcodata.ui.client.gui.prospects;

import com.fertiletech.fieldcodata.ui.client.ContentWidget;
import com.fertiletech.fieldcodata.ui.client.gui.HelpPageGenerator;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class CWViewProspects extends ContentWidget{
	
	public CWViewProspects() {
		super("View Prospects", "Use this panel to view a list of prospective clients. " +
				"Note that the edit prospects module provides a more detailed view of prospective clients");
	}

	@Override
	public Widget onInitialize() {
		return new ViewProspects();
	}
	
	@Override
	public boolean hasScrollableContent()
	{
		return false;
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
	    GWT.runAsync(CWViewProspects.class, new RunAsyncCallback() {

	        public void onFailure(Throwable caught) {
	          callback.onFailure(caught);
	        }

	        public void onSuccess() {
	          callback.onSuccess(onInitialize());
	        }
	      });		
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_PROSPECT_URL;
	}
}
