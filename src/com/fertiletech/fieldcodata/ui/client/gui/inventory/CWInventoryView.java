package com.fertiletech.fieldcodata.ui.client.gui.inventory;

import com.fertiletech.fieldcodata.ui.client.ContentWidget;
import com.fertiletech.fieldcodata.ui.client.gui.HelpPageGenerator;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class CWInventoryView extends ContentWidget{
	
	public CWInventoryView() {
		super("View Inventory", "Use this module to view inventory stock");
	}

	@Override
	public boolean hasScrollableContent()
	{
		return false;
	}
	
	@Override
	public Widget onInitialize() {
		return new ViewInventoryPanel();
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
	    GWT.runAsync(CWInventoryView.class, new RunAsyncCallback() {

	        public void onFailure(Throwable caught) {
	          callback.onFailure(caught);
	        }

	        public void onSuccess() {
	          callback.onSuccess(onInitialize());
	        }
	      });		
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_INVENTORY_URL;
	}
}
