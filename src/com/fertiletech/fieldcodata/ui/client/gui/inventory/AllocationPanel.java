package com.fertiletech.fieldcodata.ui.client.gui.inventory;

import java.util.HashSet;
import java.util.Set;

import com.fertiletech.fieldcodata.ui.client.gui.LocalDataProvider;
import com.fertiletech.fieldcodata.ui.client.gui.mrs.TicketSuggestBox;
import com.fertiletech.fieldcodata.ui.client.gui.utils.ApartmentSelector;
import com.fertiletech.fieldcodata.ui.client.gui.utils.SimpleDialog;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;

public class AllocationPanel extends Composite {

	@UiField
	InventorySelector inventorySelector;
	
	@UiField
	InventorySelector displayInventory;
	
	@UiField
	DoubleBox price;
	
	@UiField
	SimplePanel serialSlot;
	
	@UiField
	DateBox date;
	
	@UiField
	Button save;
	
	@UiField
	TicketSuggestBox ticketBox;
	
	@UiField
	ApartmentSelector apartmentSelector;
	ListBox serialNumbers;
	SimpleDialog infoBox;
	
	private static AllocationPanelUiBinder uiBinder = GWT
			.create(AllocationPanelUiBinder.class);

	interface AllocationPanelUiBinder extends UiBinder<Widget, AllocationPanel> {
	}

	public AllocationPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		serialNumbers = new ListBox(true);
		serialNumbers.setVisibleItemCount(5);
		serialNumbers.setWidth("200px");
		infoBox = new SimpleDialog("INFO");
		serialSlot.add(serialNumbers);
		apartmentSelector.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
			
			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				GWT.log("RECEIVED APT CHANGE NOTIFICATION with value: " + event.getValue());
				if(event.getValue() == true) //apartment change
				{
					GWT.log("OLD APT ID: " + apartmentSelector.getSelectedApartment() + " NEW APT NAME: " + apartmentSelector.getSelectedApartmentName());
					displayInventory.loadInventory(apartmentSelector.getSelectedApartment(), true);
				}
				else
				{
					inventorySelector.loadInventory(apartmentSelector.getSelectedBuilding(), true);
				}
			}
		});
				
		inventorySelector.addValueChangeHandler(new ValueChangeHandler<String>() {

			private AsyncCallback<Set<String>> callback = new AsyncCallback<Set<String>>() {
				
				@Override
				public void onSuccess(Set<String> result) {
					serialNumbers.clear();
					for(String serial : result)
						serialNumbers.addItem(serial);
					enableFields(true);
				}
				
				@Override
				public void onFailure(Throwable caught) {
					infoBox.show("Error loading serial numbers. Try refereshing your browser\n\n" + caught.getMessage());
				}
			};

			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				enableFields(false);
				LocalDataProvider.INVENTORY_SERVICE.getSerialNumbers(event.getValue(), callback);
			}
		});
		
		save.addClickHandler(new ClickHandler() {
			
			private AsyncCallback<String> callback = new AsyncCallback<String>() {
				
				@Override
				public void onSuccess(String result) {
					infoBox.show("Save completed<br/>" + result);
					enableFields(true);
				}
				
				@Override
				public void onFailure(Throwable caught) {
					infoBox.show("<b>Error, inventory move failed</b><br/>" + caught.getMessage());
					enableFields(true);
				}
			};

			@Override
			public void onClick(ClickEvent event) {
				final String ULSTART = "Fix issues listed below: <ul>";
				HashSet<String> selectedSerials = new HashSet<String>();
				StringBuffer error = new StringBuffer(ULSTART);
				for(int i = 0; i < serialNumbers.getItemCount(); i++)
				{
					if(serialNumbers.isItemSelected(i))
						selectedSerials.add(serialNumbers.getValue(i));
				}
				if(selectedSerials.size() == 0)
					error.append("<li>Select at least one product serial number to transfer</li>");
				if(price.getValue() == null)
					error.append("<li>Enter a valid price estimate for the product being transfered</li>");
				if(date.getValue() == null)
					error.append("<li>Enter a valid effective date for this product transfer</li>");
				String selectedApartment = apartmentSelector.getSelectedApartment();
				if(selectedApartment == null)
					error.append("<li>Select an apartment</li>");
				if(error.length() > ULSTART.length())
				{
					infoBox.show(error.append("</ul>").toString());
					return;
				}
				
				enableFields(false);
				LocalDataProvider.INVENTORY_SERVICE.moveInventory(inventorySelector.getSelectedValue(), 
						selectedApartment, selectedSerials, ticketBox.getSelectedUser(), price.getValue(), date.getValue(), callback);
			}
		});
	}
	
	public void enableFields(boolean enabled)
	{
		date.setEnabled(enabled);
		save.setEnabled(enabled);
		serialNumbers.setEnabled(enabled);
		price.setEnabled(enabled);
		inventorySelector.enableBox(enabled);
		apartmentSelector.enableSelector(enabled);
	}
}
