package com.fertiletech.fieldcodata.ui.client.gui.inventory;

import java.util.ArrayList;
import java.util.List;

import com.fertiletech.fieldcodata.ui.client.gui.LocalDataProvider;
import com.fertiletech.fieldcodata.ui.client.gui.utils.CustomRichTextArea;
import com.fertiletech.fieldcodata.ui.client.gui.utils.MultipleMessageDialog;
import com.fertiletech.fieldcodata.ui.client.gui.utils.SimpleDialog;
import com.fertiletech.fieldcodata.ui.shared.dto.DTOConstants;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

public class EditProductPanel extends Composite implements ClickHandler{
	@UiField TextBox manufacturer;
	@UiField TextBox modelNumber;
	@UiField ListBox classification;
	@UiField CustomRichTextArea description;
	@UiField Button lookup;
	@UiField Button cancel;
	@UiField Button save;
	String productID;
	SimpleDialog infoBox;
	MultipleMessageDialog errorBox;
	
	AsyncCallback<List<TableMessage>> categorycallback = new AsyncCallback<List<TableMessage>>() {
		
		@Override
		public void onFailure(Throwable caught) {
			infoBox.show("Unable to retrieve product categories: " + caught.getMessage());
		}

		@Override
		public void onSuccess(List<TableMessage> result) {
			result.remove(0); //remove header
			classification.clear();
			classification.addItem("               ", "");
			for(TableMessage m : result)
				classification.addItem(m.getText(0));
			enableKeyFields(true);
			reset();
		}
	};  	
	
	final AsyncCallback<String> saveCallback = new AsyncCallback<String>() {
		@Override
		public void onSuccess(String result) {
			productID = result;
			enableAdditionalFields(true);
			infoBox.show("Product information has been updated");
		}

		@Override
		public void onFailure(Throwable caught) {
			errorBox.show("Unable to save values. Error was: "
					+ caught.getMessage());
			enableAdditionalFields(true);
		}
	};	
	
	final AsyncCallback<TableMessage> lookupCallback = new AsyncCallback<TableMessage>() {

		@Override
		public void onSuccess(TableMessage result) {
			if(result == null)
			{
				infoBox.show("No existing product found that matches the specified Manufacturer and Model.\n" +
						" Hitting save will create a new product in the database.");
			}
			else
			{
				infoBox.show("Found a product that matches the specified Manufacturer and Model.\n" +
						" Hitting save will update the existing product definition.");				
				productID = result.getMessageId();
				for(int i = 0; i < classification.getItemCount();i++)
				{
					String cls = result.getText(DTOConstants.PRODUCT_CLASSIFICATION);
					if(classification.getValue(i).equalsIgnoreCase(cls))
					{
						classification.setSelectedIndex(i);
						break;
					}
				}
				description.setValue(result.getText(DTOConstants.PRODUCT_DESCRIPTION));
			}
			enableAdditionalFields(true);
		}

		@Override
		public void onFailure(Throwable caught) {
			errorBox.show("Unable to lookup values. Error was: "
					+ caught.getMessage());
		}
	};	
	
	private static EditProductPanelUiBinder uiBinder = GWT
			.create(EditProductPanelUiBinder.class);

	interface EditProductPanelUiBinder extends
			UiBinder<Widget, EditProductPanel> {
	}	
	
	public EditProductPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		infoBox = new SimpleDialog("Info");
		errorBox = new MultipleMessageDialog("<b><font color='red'>ERROR</font>", true);
		save.addClickHandler(this);
		lookup.addClickHandler(this);
		cancel.addClickHandler(this);
		enableKeyFields(false);
		LocalDataProvider.COMMENT_SERVICE.getApplicationParameter(DTOConstants.APP_PARAM_PRODUCT_CATEGORY_KEY, categorycallback);
	}
	
	private void enableAdditionalFields(boolean enabled)
	{
		classification.setEnabled(enabled);
		description.enable(enabled);
		save.setEnabled(enabled);		
		cancel.setEnabled(enabled);		
	}
	
	private void enableKeyFields(boolean enabled)
	{
		manufacturer.setEnabled(enabled);
		modelNumber.setEnabled(enabled);
		lookup.setEnabled(enabled);
	}
	
	private void reset()
	{
		manufacturer.setValue(null);
		modelNumber.setValue(null);
		description.setValue(null);
		lookup.setEnabled(true);
		classification.setSelectedIndex(0);
		enableAdditionalFields(false);
		enableKeyFields(true);
		productID = null;
	}

	@Override
	public void onClick(ClickEvent event) {
		if(event.getSource() == cancel)
		{
			reset();
		}
		else if(event.getSource() == lookup) //async call to do a lookup
		{
			productID = null;
			enableKeyFields(false);
			LocalDataProvider.INVENTORY_SERVICE.getProductInfo(manufacturer.getValue(),
					modelNumber.getValue(), lookupCallback);
			
		}
		else //async call to save product info
		{
			String manu = manufacturer.getValue();
			String mod = modelNumber.getValue();
			String descr = description.getValue();
			String cls = null;
			if(classification.getItemCount() > 0)
				cls = classification.getValue(classification.getSelectedIndex());
			TableMessage productInfo = new TableMessage(4, 0, 0);
			ArrayList<String> errors = new ArrayList<String>();
			if(manu.trim().length() == 0)
				errors.add("Manufacturer Field must not be blank");
			if(mod.trim().length() == 0)
				errors.add("Model Field must not be blank");
			if(descr.trim().length() < 20)
				errors.add("Product description field should be more detailed");
			if(cls == null || cls.trim().length() == 0)
				errors.add("Enter a classification, e.g. refridgerator, electronics, lighting, plumbing etc");
			if(errors.size() > 0)
			{
				errorBox.show("Fix the issues listed below", errors);
				return;
			}
			enableAdditionalFields(false);
			if(productID == null)//new product
			{
				productInfo.setText(DTOConstants.PRODUCT_MANUFACTURER, manu);
				productInfo.setText(DTOConstants.PRODUCT_MODEL, mod);
			}
			else
				productInfo.setMessageId(productID);
	
			productInfo.setText(DTOConstants.PRODUCT_CLASSIFICATION, cls);
			productInfo.setText(DTOConstants.PRODUCT_DESCRIPTION, descr);			
			LocalDataProvider.INVENTORY_SERVICE.saveProductInfo(productInfo, saveCallback);

		}
	}
}
