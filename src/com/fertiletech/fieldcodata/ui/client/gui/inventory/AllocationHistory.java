package com.fertiletech.fieldcodata.ui.client.gui.inventory;

import java.util.Date;
import java.util.List;

import com.fertiletech.fieldcodata.ui.client.gui.LocalDataProvider;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.user.datepicker.client.DateBox;

public class AllocationHistory extends NewInventorySelector implements ClickHandler
{	
	DateBox start;
	DateBox end;
	Button search;
	private AsyncCallback<List<TableMessage>> allocationCallback = new AsyncCallback<List<TableMessage>>() {

		@Override
		public void onFailure(Throwable caught) {
			Window.alert("Error fetching allocations: " + caught.getMessage());
			enableSelector(true);
		}

		@Override
		public void onSuccess(List<TableMessage> result) {
			display.showTable(result);
			enableSelector(true);			
		}
	};
	
	public AllocationHistory(DateBox start, DateBox end, Button search) {
		super(null, true);
		this.start = start;
		this.end = end;
		this.search = search;
		init(end, 0);
		init(start, -7);
		search.addClickHandler(this);
	}
	
	@Override
	protected void loadInventoryCall(String location) {
		if(start.getValue() == null || end.getValue() == null)
		{
			Window.alert("Both start and end date must contain a valid value");
			return;
		}
		if(location == null)
		{
			Window.alert("Select a valid location");
			return;
		}
		enableSelector(false);
		LocalDataProvider.INVENTORY_SERVICE.getInventoryAllocations(getInventoryLocation(), start.getValue(), end.getValue(), allocationCallback);
	}
	
	@Override
	public void enableSelector(boolean enabled)
	{
		super.enableSelector(enabled);
		start.setEnabled(enabled);
		end.setEnabled(enabled);
		search.setEnabled(enabled);
	}
	
	public void init(DateBox d, int diff)
	{
		Date current = new Date();
		CalendarUtil.addDaysToDate(current, diff);
		d.setValue(current);
	}

	@Override
	public void onClick(ClickEvent event) {
		loadInventoryCall();
	}
}
