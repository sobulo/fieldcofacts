package com.fertiletech.fieldcodata.ui.client.gui.inventory;

import java.util.List;

import com.fertiletech.fieldcodata.ui.client.gui.LocalDataProvider;
import com.fertiletech.fieldcodata.ui.client.gui.utils.SimpleDialog;
import com.fertiletech.fieldcodata.ui.shared.dto.DTOConstants;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.ListBox;

public class InventorySelector extends Composite implements HasValueChangeHandlers<String>{
	ListBox inventoryList;
	final static String LOAD_MESSAGE = "Loading inventory, please wait";
	SimpleDialog errorBox = new SimpleDialog("Error Loading Inventory - Try Refresh?");
	boolean ignorePrice;
	boolean enabledState;
	
	AsyncCallback<List<TableMessage>> callback = new AsyncCallback<List<TableMessage>>() {

		@Override
		public void onFailure(Throwable caught) {
			errorBox.show(caught.getMessage());
		}

		@Override
		public void onSuccess(List<TableMessage> result) {
			if(result.size() == 0)
				errorBox.show("No inventory found matching search criteria");
			else
			{
				inventoryList.clear();
				result.remove(0); //ignore header
				GWT.log("Inventory list returned is of size: " + result.size());
				for(TableMessage inv : result)
				{
					Double price = inv.getNumber(DTOConstants.INV_PRICE_IDX);
					String priceStr = price == null ? "" : NumberFormat.getCurrencyFormat().format(inv.getNumber(DTOConstants.INV_PRICE_IDX));
					String quantity = String.valueOf(Math.round(inv.getNumber(DTOConstants.INV_QUANTITY_IDX))) + " ";
					String amount = ignorePrice || price == null ? "" : " @ " + priceStr;
					String display = quantity + " " + inv.getText(DTOConstants.INV_PRODUCT_IDX) + amount;
					inventoryList.addItem(display, inv.getMessageId());
				}
				fire();
				inventoryList.setEnabled(enabledState);
			}
		}
	};
	
	public InventorySelector()
	{
		inventoryList = new ListBox();
		inventoryList.setWidth("200px");
		inventoryList.addItem("Waiting for load request ...");
		inventoryList.addChangeHandler(new ChangeHandler() {
			
			@Override
			public void onChange(ChangeEvent event) {
				fire();
			}
		});
		initWidget(inventoryList);
	}
	
	public void setVisibleArea(int visibleItems)
	{
		inventoryList.setVisibleItemCount(visibleItems);
	}
	
	public void setIgnorePrice(boolean ignore)
	{
		ignorePrice = ignore;
	}
	
	public void enableBox(boolean enabled)
	{
		enabledState = enabled;
		inventoryList.setEnabled(enabled);
	}
	
	public void loadInventory(String id, boolean searchByBuilding)
	{
		inventoryList.clear();
		inventoryList.addItem(LOAD_MESSAGE);
		inventoryList.setEnabled(false);
		if(id==null)
		{
			errorBox.show("Attempt to send invalid ID to server, aborting. Try a browser refresh to fix");
			return;
		}
		if(searchByBuilding)
		{
			GWT.log("OLD: " + id + " Guessing");
			LocalDataProvider.INVENTORY_SERVICE.getInventoryByBuilding(id, callback);
		}
		else
			LocalDataProvider.INVENTORY_SERVICE.getInventoryByProduct(id, callback);
	}
	
	private void fire()
	{
		String selected = getSelectedValue();
		if(selected == null) return;
		ValueChangeEvent.fire(this, selected);
	}
	
	public String getSelectedValue()
	{
		if(inventoryList.getItemCount() == 0 || inventoryList.getValue(0).equals(LOAD_MESSAGE))
			return null;
		return inventoryList.getValue(inventoryList.getSelectedIndex());
	}
	
	public String getSelectedText()
	{
		if(inventoryList.getItemCount() < 0 || inventoryList.getValue(0).equals(LOAD_MESSAGE))
			return null;
		return inventoryList.getItemText(inventoryList.getSelectedIndex());
		
	}
	
	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<String> handler) {
		return addHandler(handler, ValueChangeEvent.getType());
	}
}
