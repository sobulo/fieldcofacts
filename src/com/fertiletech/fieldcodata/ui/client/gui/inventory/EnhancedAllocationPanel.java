package com.fertiletech.fieldcodata.ui.client.gui.inventory;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.fertiletech.fieldcodata.ui.client.GUIConstants;
import com.fertiletech.fieldcodata.ui.client.gui.LocalDataProvider;
import com.fertiletech.fieldcodata.ui.client.gui.mrs.TicketSuggestBox;
import com.fertiletech.fieldcodata.ui.client.gui.utils.CurrencyBox;
import com.fertiletech.fieldcodata.ui.client.gui.utils.SerialNumberPopup;
import com.fertiletech.fieldcodata.ui.client.gui.utils.SimpleDialog;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.user.datepicker.client.DateBox;

public class EnhancedAllocationPanel extends Composite {
	@UiField
	CurrencyBox price;
		
	@UiField
	DateBox date;
	
	@UiField
	Button save;
	
	@UiField
	TicketSuggestBox ticketBox;
	
	@UiField(provided = true)
	NewInventorySelector toInventory;
	
	@UiField(provided = true)
	NewInventorySelector fromInventory;
	
	SimpleDialog infoBox;
	SerialNumberPopup serialPopup;
	
	private static EnhancedAllocationPanelUiBinder uiBinder = GWT
			.create(EnhancedAllocationPanelUiBinder.class);

	interface EnhancedAllocationPanelUiBinder extends
			UiBinder<Widget, EnhancedAllocationPanel> {
	}
	
	private AsyncCallback<String> saveCallback = new AsyncCallback<String>() {
		
		@Override
		public void onSuccess(String result) {
			serialPopup.setSerialNumbers(new HashSet<String>()); //clear the popup
			serialPopup.hide();
			infoBox.show("<font color='green'><b>Save completed</b></font><br/>" + result);
			toInventory.loadInventoryCall();
			fromInventory.loadInventoryCall();
			enableFields(true);
		}
		
		@Override
		public void onFailure(Throwable caught) {
			serialPopup.hide();
			infoBox.show("<b>Error, inventory move failed</b><br/>" + caught.getMessage());
			enableFields(true);
		}
	};
	

	public EnhancedAllocationPanel() {
		fromInventory = new NewInventorySelector(false, false);
		toInventory = new NewInventorySelector(null, false);
		infoBox = new SimpleDialog("INFO");
		initWidget(uiBinder.createAndBindUi(this));
		date.setValue(new Date());
		date.addValueChangeHandler(new ValueChangeHandler<Date>() {
			
			@Override
			public void onValueChange(ValueChangeEvent<Date> event) {
				if(!CalendarUtil.isSameDate(event.getValue(), new Date()))
					infoBox.show("<font color='red'>Warning! Potential policy violation. Consider changing allocation date back to today.<br/>" +
							" Proceed if you're sure you want an inventory transfer date different from today's date</font>");
			}
		});
		date.setFormat(GUIConstants.DEFAULT_DATEBOX_FORMAT);
		serialPopup = new SerialNumberPopup("Serial Numbers Selection");
		serialPopup.setClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				serialPopup.show("Moving inventory, please wait...");
				GWT.log("Making call to move inventory");
				serialPopup.enableFields(false);
				LocalDataProvider.INVENTORY_SERVICE.moveInventory(fromInventory.getSelectedInventory().getMessageId(), 
						toInventory.getInventoryLocation(), serialPopup.getSerialNumbers(), ticketBox.getSelectedUser(), 
						price.getAmount(), date.getValue(), saveCallback);
			}
		});
		save.addClickHandler(new ClickHandler() {
			
			private AsyncCallback<Set<String>> serialCallback = new AsyncCallback<Set<String>>() {

				@Override
				public void onFailure(Throwable caught) {
					serialPopup.hide();
					infoBox.show("Loading serial numbers failed: <br/>" + caught.getMessage());
					enableFields(true);
				}

				@Override
				public void onSuccess(Set<String> result) {
					serialPopup.enableFields(true);
					serialPopup.show("Please select serial numbers to move below. You may select multiple numbers by using shift or ctrl keys");
					serialPopup.setSerialNumbers(result);
				}
			};

			@Override
			public void onClick(ClickEvent event) {
				final String ULSTART = "Fix issues listed below: <ul>";
				StringBuilder error = new StringBuilder(ULSTART);
				if(price.getAmount() == null)
					error.append("<li>Enter a valid price estimate for the product being transfered</li>");
				if(date.getValue() == null)
					error.append("<li>Enter a valid effective date for product transfer</li>");
				if(fromInventory.getSelectedInventory() == null)
					error.append("<li>You must select an inventory item to transfer</li>");
			
				if(error.length() > ULSTART.length())
				{
					infoBox.show(error.append("</ul>").toString());
					return;
				}		
				enableFields(false);
				serialPopup.setSerialNumbers(new HashSet<String>());
				serialPopup.show("Loading serial numbers, please wait ...");
				LocalDataProvider.INVENTORY_SERVICE.getSerialNumbers(fromInventory.getSelectedInventory().getMessageId(), serialCallback );

			}
		});		
	}
	
	private void enableFields(boolean enabled)
	{
		date.setEnabled(enabled);
		save.setEnabled(enabled);
		price.setEnabled(enabled);
		toInventory.enableSelector(enabled);
		fromInventory.enableSelector(enabled);	
	}	

}
