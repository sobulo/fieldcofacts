package com.fertiletech.fieldcodata.ui.client.gui.inventory;

import java.util.HashSet;

import com.fertiletech.fieldcodata.ui.client.gui.LocalDataProvider;
import com.fertiletech.fieldcodata.ui.client.gui.mrs.TicketSuggestBox;
import com.fertiletech.fieldcodata.ui.client.gui.utils.BuildingSelector;
import com.fertiletech.fieldcodata.ui.client.gui.utils.CurrencyBox;
import com.fertiletech.fieldcodata.ui.client.gui.utils.ProductSelector;
import com.fertiletech.fieldcodata.ui.client.gui.utils.SimpleDialog;
import com.fertiletech.fieldcodata.ui.client.gui.utils.Text2ListBox;
import com.fertiletech.fieldcodata.ui.client.gui.utils.TextBoxWrapper;
import com.fertiletech.fieldcodata.ui.client.gui.utils.YesNoDialog;
import com.fertiletech.fieldcodata.ui.shared.dto.DTOConstants;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

public class EditInventoryPanel extends Composite{
	
	@UiField
	BuildingSelector buildingList;
	
	@UiField
	ProductSelector productList;
	
	@UiField
	Label quantity;
	
	@UiField
	CurrencyBox price;
	
	@UiField
	Button save;
	
	@UiField
	Text2ListBox serialNumbers;
	
	@UiField
	Label type;
	
	@UiField 
	TicketSuggestBox ticketBox;
	
	final String DEFAULT_LB_WIDTH = "200px";
	final int VISIBLE_SERIAL_NUMBERS = 10;
	String inventoryID;
	SimpleDialog infoBox = new SimpleDialog("INFO");
	
	int lastSavedQuantity = 0;
	final AsyncCallback<String[]> saveCallback = new AsyncCallback<String[]>() {
		@Override
		public void onSuccess(String[] result) {
			inventoryID = result[0];
			lastSavedQuantity = Integer.valueOf(result[1]);
			enableFields(true);
			infoBox.show("Save Succeeded");
			type.setText("Last save was succesfull");
		}

		@Override
		public void onFailure(Throwable caught) {
			enableFields(true);
			infoBox.show("Save Failed!!\n\n" + caught.getMessage());
			type.setText("Last save failed. Retry");			
		}
	};	
	
	final AsyncCallback<TableMessage> lookupCallback = new AsyncCallback<TableMessage>() {

		@Override
		public void onSuccess(TableMessage result) {
			GWT.log("Enabling Fields");
			enableFields(true);
			if(result == null)
			{
				lastSavedQuantity = 0;
				inventoryID = null;
				type.setText("Add product to building inventory");
				return;
			}
			type.setText("Update the serial numbers");
			inventoryID = result.getMessageId();
			int start = (int) Math.round(result.getNumber(DTOConstants.INV_SERIAL_START_IDX));
			int end = -1;
				if(start >-0)
					end = (int) Math.round(result.getNumber(DTOConstants.INV_SERIAL_END_IDX));
			serialNumbers.initializeList(getMultiVals(start, end, result));
			price.setAmount(result.getNumber(DTOConstants.INV_PRICE_IDX));
			lastSavedQuantity = serialNumbers.getList().size();
		}

		@Override
		public void onFailure(Throwable caught) {
			type.setText("Lookup failed. Refresh bowser?" + caught.getMessage());
		}
	};		
	
	private static EditInventoryPanelUiBinder uiBinder = GWT
			.create(EditInventoryPanelUiBinder.class);

	interface EditInventoryPanelUiBinder extends
			UiBinder<Widget, EditInventoryPanel> {
	}

	public EditInventoryPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		setupTextListBox(serialNumbers);
		serialNumbers.addValueChangeHandler(new ValueChangeHandler<Integer>() {
			
			@Override
			public void onValueChange(ValueChangeEvent<Integer> event) {
				quantity.setText(String.valueOf(event.getValue()));
			}
		});
		
		productList.addValueChangeHandler(new ValueChangeHandler<String>() {
			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				String bID = buildingList.getSelectedBuilding();
				if(bID == null) return;
				requestLookup(bID, event.getValue());
			}
		});
		buildingList.addValueChangeHandler(new ValueChangeHandler<String>() {
			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				String pID = productList.getSelectedValue();
				if(pID == null) return;
				requestLookup(event.getValue(), pID);
			}
		});		
		save.addClickHandler(new ClickHandler() {
			YesNoDialog confirmSave = new YesNoDialog("POLICY VIOLATION?");
			@Override
			public void onClick(ClickEvent event) {
				final HashSet<String> serials = serialNumbers.getList();
				final TableMessage m = new TableMessage(DTOConstants.INV_NUM_OF_PRESPECIFIED_TXT_FIELDS + serials.size(),  4, 0);
				if(inventoryID == null)
				{
					String bID = buildingList.getSelectedBuilding();
					String pID = productList.getSelectedValue();
					if(pID == null || bID == null)
					{
						infoBox.show("Product and/or Building list still being setup. Try again shortly");
						return;
					}
					m.setText(DTOConstants.INV_BUILDING_IDX, bID);
					m.setText(DTOConstants.INV_PRODUCT_IDX, pID);
				}
				else 
					m.setMessageId(inventoryID);
				m.setNumber(DTOConstants.INV_PRICE_IDX, price.getAmount());						
				
				if(lastSavedQuantity > serials.size())
				{
					confirmSave.setClickHandler(new ClickHandler() {
						
						@Override
						public void onClick(ClickEvent event) {
							doSave(m, serials);
							confirmSave.hide();
						}
					});
					confirmSave.show("<p>Looks like you're trying to reduce building inventory. Old: " + lastSavedQuantity + " New: " + serials.size() + 
							" For reasons pertaining to company policy, we recommend using transfer inventory instead.</p><p>" +
							" If your objective is not achievable with the inventory transfer module, click yes to proceed</p>");
			
				}
				else if(serials.size() > 0)
				{
					doSave(m, serials);
				}
				else
				{
					infoBox.show("Hmm, looks like you're trying to create an empty inventory item. " +
							"You need at least one serial number to create an inventory item");
				}							
			}
		});
	}
	
	private void doSave(TableMessage m, HashSet<String> serials)
	{
		if(serials.size() == 0)
			m.setNumber(DTOConstants.INV_SERIAL_START_IDX, -1);	
		else
		{
			int startIdx = DTOConstants.INV_NUM_OF_PRESPECIFIED_TXT_FIELDS;
			m.setNumber(DTOConstants.INV_SERIAL_START_IDX, startIdx);
			for(String s : serials)
				m.setText(startIdx++, s);
			m.setNumber(DTOConstants.INV_SERIAL_END_IDX, startIdx - 1);					
		}
		enableFields(false);
		LocalDataProvider.INVENTORY_SERVICE.saveInventoryItems(m, ticketBox.getSelectedUser(), saveCallback);
	}
	
	
	private void requestLookup(String buildingID, String productID)
	{
		clearFields();
		enableFields(false);
		LocalDataProvider.INVENTORY_SERVICE.getInventoryInfo(buildingID, 
				productID, lookupCallback);		
	}

	private void enableFields(boolean enabled)
	{
		save.setEnabled(enabled);
		if(enabled)
			serialNumbers.enableBox();
		else
			serialNumbers.disableBox();
		price.setEnabled(enabled);
		productList.enableSelector(enabled);
		buildingList.enableSelector(enabled);
		
	}

	private void clearFields()
	{
		serialNumbers.clear();
		price.setAmount(null);
		quantity.setText("");
	}	
	
	public HashSet<String> getMultiVals(int start, int end, TableMessage m)
	{
		GWT.log("start: " + start + " end: " + end);
		if(start < 0 || end < 0)
			return null;		
		HashSet<String> multiVals = new HashSet<String>();
		for(int i=start; i<=end; i++)
			multiVals.add(m.getText(i));
		return multiVals;
	}	
	
    private void setupTextListBox(Text2ListBox t2l)
    {
        t2l.initInput(new TextBoxWrapper());
        t2l.setVisbileArea(VISIBLE_SERIAL_NUMBERS, DEFAULT_LB_WIDTH);
        t2l.enableBox();
        t2l.removeAdditionalFeatures();
    }
}
