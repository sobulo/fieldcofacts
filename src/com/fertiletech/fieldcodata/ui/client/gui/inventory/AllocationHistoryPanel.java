package com.fertiletech.fieldcodata.ui.client.gui.inventory;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.SimpleLayoutPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;

public class AllocationHistoryPanel extends Composite {

	@UiField
	DateBox startDate;
	@UiField
	DateBox endDate;
	@UiField
	SimpleLayoutPanel allocationPanel;
	@UiField
	Button trigger;
	
	private static AllocationHistoryPanelUiBinder uiBinder = GWT
			.create(AllocationHistoryPanelUiBinder.class);

	interface AllocationHistoryPanelUiBinder extends
			UiBinder<Widget, AllocationHistoryPanel> {
	}

	public AllocationHistoryPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		allocationPanel.add(new AllocationHistory(startDate, endDate, trigger));
	}

}
