package com.fertiletech.fieldcodata.ui.client.gui.inventory;

import java.util.HashMap;
import java.util.List;

import com.fertiletech.fieldcodata.ui.client.gui.LocalDataProvider;
import com.fertiletech.fieldcodata.ui.client.gui.utils.BuildingSelector;
import com.fertiletech.fieldcodata.ui.client.gui.utils.MessageListToGrid;
import com.fertiletech.fieldcodata.ui.client.gui.utils.ProductSelector;
import com.fertiletech.fieldcodata.ui.client.gui.utils.ShowcaseTable;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.SplitLayoutPanel;
import com.google.gwt.user.client.ui.Widget;

public class ViewInventoryPanel extends Composite {

	@UiField(provided = true)
	FlowPanel searchType;

	@UiField
	SimplePanel selectorSlot;

	@UiField
	ShowcaseTable invTable;	

	@UiField
	ScrollPanel auditSlot;
	
	@UiField
	SplitLayoutPanel container;

	private RadioButton useProductCheck;
	private RadioButton useBuildingCheck;
	private ProductSelector productSelector;
	private BuildingSelector buildingSelector;

	private final static HashMap<String, List<TableMessage>> cachedAllocations = new HashMap<String, List<TableMessage>>();

	final AsyncCallback<List<TableMessage>> viewAllocationsCallBack = new AsyncCallback<List<TableMessage>>() {
		@Override
		public void onFailure(Throwable caught) {
			auditSlot.clear();
			HTML popupStatus = new HTML("<font color='red'>"
					+ caught.getMessage() + "</font>");
			auditSlot.add(popupStatus);
		}

		@Override
		public void onSuccess(List<TableMessage> result) {
			String inventoryID = result.get(0).getMessageId();
			cachedAllocations.put(inventoryID, result);
			setupAllocationTable(result);
		}
	};

	private static ViewInventoryPanelUiBinder uiBinder = GWT
			.create(ViewInventoryPanelUiBinder.class);

	interface ViewInventoryPanelUiBinder extends
			UiBinder<Widget, ViewInventoryPanel> {
	}

	public ViewInventoryPanel() {
		// Create a CellTable.
		searchType = getSearchTypePanel();
		initWidget(uiBinder.createAndBindUi(this));
		invTable.addValueChangeHandler(new ValueChangeHandler<TableMessage>() {

			@Override
			public void onValueChange(ValueChangeEvent<TableMessage> event) {
				if(event.getValue() == null)
				{
					container.setWidgetSize(auditSlot, 0);
					auditSlot.clear();
					auditSlot.add(new HTML("Select an inventory item to <br/>view allocation/transfer history"));
				}
				else
				{
					onRowSelected(event.getValue());
					Double currentSize = container.getWidgetSize(auditSlot);
					double width = 650;
					if(currentSize > 100)
						width = currentSize;
					container.setWidgetSize(auditSlot, width);
				}
				container.animate(900);
			}
		});
	}

	public FlowPanel getSearchTypePanel() {
		FlowPanel panel = new FlowPanel();
		useBuildingCheck = new RadioButton("selectorType", "By Building");
		useProductCheck = new RadioButton("selectorType", "By Product");
		ClickHandler radioHandler = new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				selectorSlot.clear();
				if (event.getSource() == useBuildingCheck) {
					if (buildingSelector == null)
					{
						buildingSelector = new BuildingSelector();
						buildingSelector.addValueChangeHandler(new ValueChangeHandler<String>() {
							
							private AsyncCallback<List<TableMessage>> callback = new AsyncCallback<List<TableMessage>>() {

								@Override
								public void onFailure(Throwable caught) {
									auditSlot.clear();
									auditSlot.add(new HTML("Error Occured, unable to load inventory by building<br/>" + caught.getMessage()));
								}

								@Override
								public void onSuccess(List<TableMessage> result) {
									auditSlot.clear();
									auditSlot.add(new HTML("Displaying search results from building query<br/>"));
									invTable.showTable(result);
								}
							};

							@Override
							public void onValueChange(ValueChangeEvent<String> event) {
								LocalDataProvider.INVENTORY_SERVICE.getInventoryByBuilding(event.getValue(), callback);
							}
						});
					}
					selectorSlot.add(buildingSelector);
				} else {
					if (productSelector == null)
					{
						productSelector = new ProductSelector();
						productSelector.addValueChangeHandler(new ValueChangeHandler<String>() {

							private AsyncCallback<List<TableMessage>> callback = new AsyncCallback<List<TableMessage>>() {

								@Override
								public void onFailure(Throwable caught) {
									auditSlot.clear();
									auditSlot.add(new HTML("Error occured, unable to load inventory by product<br/" + 
									caught.getMessage()));
								}

								@Override
								public void onSuccess(List<TableMessage> result) {
									auditSlot.clear();
									auditSlot.add(new HTML("Displaying search results from product query<br/>"));
									invTable.showTable(result);
								}
							};

							@Override
							public void onValueChange(
									ValueChangeEvent<String> event) {
								LocalDataProvider.INVENTORY_SERVICE.getInventoryByProduct(event.getValue(), callback);
							}
						});
					}
					selectorSlot.add(productSelector);
				}
			}
		};
		useBuildingCheck.addClickHandler(radioHandler);
		useProductCheck.addClickHandler(radioHandler);
		HorizontalPanel radioGroup = new HorizontalPanel();
		radioGroup.add(new Label("Search Type"));
		radioGroup.add(useBuildingCheck);
		radioGroup.add(useProductCheck);
		panel.add(radioGroup);
		return panel;
	}

	private void onRowSelected(TableMessage m) {
		// Do we need something similar to paySummary in the viewblotters?
		// TODO: From below looks like above comment/task is done, pls confirm
		auditSlot.clear();
		auditSlot
				.add(new Label("Fetching allocation history, please wait ..."));
		String selectedInventoryItem = m.getMessageId();
		List<TableMessage> cp = cachedAllocations.get(selectedInventoryItem);
		if (cp != null)
			setupAllocationTable(cp);
		else
			LocalDataProvider.INVENTORY_SERVICE.getInventoryAllocations(
					selectedInventoryItem, viewAllocationsCallBack);

	}

	private void setupAllocationTable(List<TableMessage> data) {
		auditSlot.clear();
		if (data.size() > 1)
			auditSlot.add(new MessageListToGrid(data));
		else
			auditSlot.add(new Label( "Strange? No allocations found. You might be looking at stale data, "
							+ "try refreshing browser"));
	}
}
