package com.fertiletech.fieldcodata.ui.client.gui.inventory;

import java.util.List;

import com.fertiletech.fieldcodata.ui.client.gui.LocalDataProvider;
import com.fertiletech.fieldcodata.ui.client.gui.utils.MessageListToGrid;
import com.fertiletech.fieldcodata.ui.client.gui.utils.ShowcaseTable;
import com.fertiletech.fieldcodata.ui.client.gui.utils.SimpleDialog;
import com.fertiletech.fieldcodata.ui.shared.dto.DTOConstants;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.SplitLayoutPanel;
import com.google.gwt.user.client.ui.Widget;

public class VeiwProductsPanel extends Composite {

	@UiField
	ShowcaseTable display;
	
	@UiField
	Button refresh;
	
	@UiField
	SimplePanel auditSlot;
	
	@UiField
	SplitLayoutPanel container;
	
	SimpleDialog infoBox;
	
	TableMessage lastSelectedObject = null;

	private AsyncCallback<List<TableMessage>> callback = new AsyncCallback<List<TableMessage>>() {

		@Override
		public void onFailure(Throwable caught) {
			String error = "Unable to refresh. Error reported was<br/>" + caught.getLocalizedMessage();
			auditSlot.clear();
			auditSlot.add(new HTML(error));
			refresh.setEnabled(true);
		}

		@Override
		public void onSuccess(List<TableMessage> result) {
			infoBox.show("Products data has been refreshed.<br/>Found " + (result.size()-1) + " entries in the database");
			display.showTable(result);
			auditSlot.clear();
			refresh.setEnabled(true);
		}
	};
	
	private static VeiwProductsPanelUiBinder uiBinder = GWT
			.create(VeiwProductsPanelUiBinder.class);

	interface VeiwProductsPanelUiBinder extends
			UiBinder<Widget, VeiwProductsPanel> {
	}

	public VeiwProductsPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		infoBox = new SimpleDialog("INFO");
		display.addValueChangeHandler(new ValueChangeHandler<TableMessage>() {
			
			private AsyncCallback<List<TableMessage>> productsCallback = new AsyncCallback<List<TableMessage>>() {

				@Override
				public void onFailure(Throwable caught) {
					infoBox.show("Error loading product details");
					auditSlot.clear();
					auditSlot.add(new HTML("Error reported:<br/>" + caught.getMessage()));
				}

				@Override
				public void onSuccess(List<TableMessage> result) {
					auditSlot.clear();
					FlowPanel p = new FlowPanel();
					p.add(new HTML("<div style='background-color:black;color:white;margin-bottom:10px'>" +
							lastSelectedObject.getText(DTOConstants.PRODUCT_DESCRIPTION) + "</div>"));
					p.add(new MessageListToGrid(result));
					auditSlot.add(p);
				}
			};

			@Override
			public void onValueChange(ValueChangeEvent<TableMessage> event) {
				if(event.getValue() ==  null)
				{
					auditSlot.clear();
					auditSlot.add(new HTML("<font color='red'>Select a product to view additional details</font>"));
					container.setWidgetSize(auditSlot, 0);
				}
				else
				{
					lastSelectedObject = event.getValue();
					auditSlot.clear();
					auditSlot.add(new HTML("Loading ..."));
					LocalDataProvider.INVENTORY_SERVICE.getInventoryByProduct(lastSelectedObject.getMessageId(), productsCallback );
					Double currentSize = container.getWidgetSize(auditSlot);
					double width = 500;
					if(currentSize > 100)
						width = currentSize;
					container.setWidgetSize(auditSlot, width);
				}
				container.animate(550);
			}
		});
		refresh.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				refreshTable();
			}
		});
		refreshTable();
	}
	
	public void refreshTable()
	{
		refresh.setEnabled(false);
		display.clear();
		auditSlot.clear();
		auditSlot.add(new HTML("Fetching data, please wait ..."));
		LocalDataProvider.INVENTORY_SERVICE.getProductsTable(callback);
	}

}
