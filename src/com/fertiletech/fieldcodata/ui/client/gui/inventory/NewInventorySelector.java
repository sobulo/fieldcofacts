package com.fertiletech.fieldcodata.ui.client.gui.inventory;

import java.util.Date;
import java.util.List;

import com.fertiletech.fieldcodata.ui.client.gui.LocalDataProvider;
import com.fertiletech.fieldcodata.ui.client.gui.utils.ApartmentDropDown;
import com.fertiletech.fieldcodata.ui.client.gui.utils.BuildingSelector;
import com.fertiletech.fieldcodata.ui.client.gui.utils.ShowcaseTable;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

public class NewInventorySelector extends Composite implements ValueChangeHandler<Boolean>
{
	@UiField(provided = true)
	ShowcaseTable display;
	@UiField
	FlowPanel headerPanel;

	RadioButton byBuilding;
	RadioButton byApartment;
	RadioButton byFacility;
	SimplePanel selectorSlot;

		
	//selectors
	BuildingSelector buildingSelector;
	ApartmentDropDown apartmentSelector;
	
	private static NewInventorySelectorUiBinder uiBinder = GWT
			.create(NewInventorySelectorUiBinder.class);

	interface NewInventorySelectorUiBinder extends
			UiBinder<Widget, NewInventorySelector> {
	}
	
	public interface InventoryStyle extends CssResource {
	    String floatLeft();
	  }
	
	@UiField InventoryStyle style;
	
	private AsyncCallback<List<TableMessage>> loadCallback = new AsyncCallback<List<TableMessage>>() {

		@Override
		public void onFailure(Throwable caught) {
			Window.alert("Unable to load inventory. Try refreshing your browser");
			enableSelector(true);
		}

		@Override
		public void onSuccess(List<TableMessage> result) {
			display.showTable(result);
			enableSelector(true);
		}
	};	

	public NewInventorySelector(Boolean isMultipleSelection, boolean includeFooter) {
		display = new ShowcaseTable(isMultipleSelection, includeFooter, includeFooter);
		initWidget(uiBinder.createAndBindUi(this));
		String groupName = "newInvSelector" + (new Date().getTime());
		
		byBuilding = new RadioButton(groupName, "Building");
		byApartment = new RadioButton(groupName, "Apartment");
		byFacility = new RadioButton(groupName, "Facility");
		selectorSlot = new SimplePanel();
		
		//chrome having issues detecting clicks
		final RadioButton[] buttons = {byBuilding, byApartment, byFacility};
		for(int i = 0; i < buttons.length; i++)
			setupRadioFocus(buttons[i]);
		
		byBuilding.addStyleName(style.floatLeft());
		byApartment.addStyleName(style.floatLeft());
		byFacility.addStyleName(style.floatLeft());
		
		headerPanel.add(byBuilding);
		headerPanel.add(byApartment);
		headerPanel.add(byFacility);
		headerPanel.add(selectorSlot);
		
		byBuilding.addValueChangeHandler(this);
		byApartment.addValueChangeHandler(this);
		byFacility.addValueChangeHandler(this);		
	}
	
	void setupRadioFocus(final RadioButton b)
	{
		FocusPanel f = new FocusPanel(b);
		f.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				b.setValue(true, true);
			}
		});		
	}

	@Override
	public void onValueChange(ValueChangeEvent<Boolean> event) {
		display.clear();
		selectorSlot.clear();
		if(event.getValue())
		{
			if(event.getSource() == byBuilding)
			{
				if(buildingSelector == null)
				{
					buildingSelector = new BuildingSelector();
					buildingSelector.addValueChangeHandler(getSelectorHandler());
				}
				else
				{
					GWT.log("Selector: " + buildingSelector);
					GWT.log("Value: " + buildingSelector.getSelectedBuilding());
					GWT.log("Name: " + buildingSelector.getSelectedBuildingName());
					GWT.log(buildingSelector.getSelectedBuildingName() + " Building selector switch. Going to load inventory with: " + buildingSelector.getSelectedBuilding());
					loadInventoryCall(buildingSelector.getSelectedBuilding());
				}
				selectorSlot.add(buildingSelector);
			}
			else if(event.getSource() == byApartment)
			{
				if(apartmentSelector == null)
				{
					apartmentSelector = new ApartmentDropDown(false);
					apartmentSelector.addValueChangeHandler(getSelectorHandler());
				}
				else
				{
					GWT.log(apartmentSelector.getSelectedApartmentName() + " Apartment selector switch. Going to load inventory with: " + buildingSelector.getSelectedBuilding());				
					loadInventoryCall(apartmentSelector.getSelectedApartment());					
				}
				selectorSlot.add(apartmentSelector);
			}
		}
	}
	
	ValueChangeHandler<String> selectorHandler;
	private ValueChangeHandler<String> getSelectorHandler()
	{
		if(selectorHandler == null)
		{
			selectorHandler = new ValueChangeHandler<String>() {

				@Override
				public void onValueChange(ValueChangeEvent<String> event) {
					if(event.getSource()== buildingSelector)
					{
						GWT.log("Building fired with: " + event.getValue());
					}
					else if(event.getSource() == apartmentSelector)
					{
						GWT.log("Apartment fired with: " + event.getValue());
						GWT.log("APT Name: " + apartmentSelector.getSelectedApartment() + " " + apartmentSelector.getSelectedApartmentName());
					}
					loadInventoryCall(event.getValue());
				}
			};
		}
		return selectorHandler;
	}
	
	protected void loadInventoryCall(String location)
	{
		GWT.log("Making a call to loadInventory with: " + location);
		enableSelector(false);
		LocalDataProvider.INVENTORY_SERVICE.getInventoryByBuilding(location, loadCallback);
	}
	
	void enableSelector(boolean enable)
	{
		byBuilding.setEnabled(enable);
		byApartment.setEnabled(enable);
		byFacility.setEnabled(enable);
		if(buildingSelector != null)
			buildingSelector.enableSelector(enable);
		if(apartmentSelector != null)
			apartmentSelector.enableFields(enable);
	}
	
	void loadInventoryCall()
	{
		String location = getInventoryLocation();
		if(location == null) return;
		loadInventoryCall(location);
	}
	
	public TableMessage getSelectedInventory()
	{
		return display.getSelectedRow();
	}
	
	public String getInventoryLocation()
	{
		Widget w = selectorSlot.getWidget();
		if(w != null)
		{		
			if(w == buildingSelector)
				return buildingSelector.getSelectedBuilding();
			else if(w == apartmentSelector)
				return apartmentSelector.getSelectedApartment();
		}
		Window.alert("No location selected. Choose one of building, apartment or facility");
		return null;		
	}
}
