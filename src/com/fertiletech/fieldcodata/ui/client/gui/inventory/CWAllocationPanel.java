package com.fertiletech.fieldcodata.ui.client.gui.inventory;

import com.fertiletech.fieldcodata.ui.client.ContentWidget;
import com.fertiletech.fieldcodata.ui.client.gui.HelpPageGenerator;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class CWAllocationPanel extends ContentWidget{

	@Override
	public boolean hasScrollableContent()
	{
		return false;
	}
	
	public CWAllocationPanel() {
		super("Inventory Transfer", "Use this module to move inventory from one location to another");
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
	    GWT.runAsync(CWAllocationPanel.class, new RunAsyncCallback() {

	        public void onFailure(Throwable caught) {
	          callback.onFailure(caught);
	        }

	        public void onSuccess() {
	          callback.onSuccess(onInitialize());
	        }
	      });
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_INVENTORY_URL;
	}

	@Override
	public Widget onInitialize() {
		return new EnhancedAllocationPanel();
	}

}
