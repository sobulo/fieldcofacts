package com.fertiletech.fieldcodata.ui.client.gui.dashboard;

  import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.SimpleLayoutPanel;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.gwt.charts.client.ChartLoader;
import com.googlecode.gwt.charts.client.ChartPackage;
import com.googlecode.gwt.charts.client.ColumnType;
import com.googlecode.gwt.charts.client.DataTable;
import com.googlecode.gwt.charts.client.corechart.PieChart;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class SimplePieChart extends Composite{
        private SimpleLayoutPanel layoutPanel;
        private PieChart pieChart;
        
        SimplePieChart() {
        	    // Create the API Loader
        	initWidget(getSimpleLayoutPanel());
        	GWT.log("Calling chart loader");
                ChartLoader chartLoader = new ChartLoader(ChartPackage.CORECHART);
                chartLoader.loadApi(new Runnable() {

                        @Override
                        public void run() {
                        	GWT.log("Initializing Pie chart");
                                getSimpleLayoutPanel().setWidget(getPieChart());
                                drawPieChart();
                                GWT.log("Pie chart drawn");
                        }
                });
        }

        private SimpleLayoutPanel getSimpleLayoutPanel() {
                if (layoutPanel == null) {
                        layoutPanel = new SimpleLayoutPanel();
                        layoutPanel.setSize("500px", "500px");
                }
                return layoutPanel;
        }

        private Widget getPieChart() {
                if (pieChart == null) {
                        pieChart = new PieChart();
                }
                return pieChart;
        }

        private void drawPieChart() {
                // Prepare the data
                DataTable dataTable = DataTable.create();
                dataTable.addColumn(ColumnType.STRING, "Name");
                dataTable.addColumn(ColumnType.NUMBER, "Donuts eaten");
                dataTable.addRows(4);
                dataTable.setValue(0, 0, "Michael");
                dataTable.setValue(1, 0, "Elisa");
                dataTable.setValue(2, 0, "Robert");
                dataTable.setValue(3, 0, "John");
                dataTable.setValue(0, 1, 5);
                dataTable.setValue(1, 1, 7);
                dataTable.setValue(2, 1, 3);
                dataTable.setValue(3, 1, 2);

                // Draw the chart
                pieChart.draw(dataTable);
        }
}