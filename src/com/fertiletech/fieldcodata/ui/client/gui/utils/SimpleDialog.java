/**
 * 
 */
package com.fertiletech.fieldcodata.ui.client.gui.utils;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class SimpleDialog {
	protected PopupPanel alertBox;
	protected HTML displayMessage;
	
	public SimpleDialog(String title)
	{
		this(title, false);
	}
	
	public SimpleDialog(String title, boolean asHtml)
	{
		this(title, asHtml, "275px");
	}
	
	public SimpleDialog(String title, boolean asHtml, String width)
	{
		alertBox = new PopupPanel();
		alertBox.setWidth(width);
		displayMessage = new HTML();
		displayMessage.setWordWrap(true);
		ScrollPanel scroller = new ScrollPanel();
		FlowPanel contentPanel = new FlowPanel();
		String titleStr = "<div style='margin-bottom:5px;background-color:#c6cbca;border-bottom: 1px solid grey;text-align:center'>"+
				(asHtml? title : ("<b>" + title + "</b>")) + "</div>";
		HTML headerPanel = new HTML(titleStr);
		contentPanel.add(headerPanel);
		contentPanel.add(displayMessage);
		contentPanel.add(getButtonPanel());		
		scroller.add(contentPanel);
		alertBox.setWidget(scroller);

        alertBox.setAnimationEnabled(true);
        alertBox.setGlassEnabled(true);
        alertBox.hide();		
	}
	
	protected Widget getButtonPanel()
	{
		Button ok = new Button("OK");
		ok.addClickHandler( new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				alertBox.hide();
			}
		});
		return ok;
	}
	
	public void show(String message)
	{
		displayMessage.setHTML(message + "<br/><hr>");
		alertBox.center();
	}
}
