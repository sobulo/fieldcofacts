/**
 * 
 */
package com.fertiletech.fieldcodata.ui.client.gui.utils;

import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public interface DropDownSelector {
	
	public String getSelectedDropDownValue();
	public String getSelectedDropDownDisplayName();
	public Widget getWidgetDisplay();
	public void initData();

}
