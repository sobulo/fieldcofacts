/**
 * 
 */
package com.fertiletech.fieldcodata.ui.client.gui.utils;

/**
 * @author Segun Razaq Sobulo
 * 
 */
public class TenantSuggestBox extends UserSuggestBox {
	
	public TenantSuggestBox()
	{
		super();
		setDisplayLabel("Select Resident");
	}

	@Override
	public void populateSuggestBox()
	{
		getUserService().getAllTenants(getPopulateCallBack());
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.UserSuggestBox#getRole()
	 */
	@Override
	public String getRole() 
	{
		return "tenants";
	}

}
