/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fertiletech.fieldcodata.ui.client.gui.utils;

import java.util.HashSet;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author Administrator
 */
public class Text2ListBox extends Composite implements ClickHandler, HasValueChangeHandlers<Integer>{

    private static Text2ListBoxUiBinder uiBinder = GWT.create(Text2ListBoxUiBinder.class);

    interface Text2ListBoxUiBinder extends UiBinder<Widget, Text2ListBox> {
    }

    @UiField FlowPanel inputSlot;
    @UiField Label statusLabel;
    @UiField Button saveList, addInput, removeInput;
    @UiField ListBox targetList;
    @UiField HTMLPanel additionalFeatures;
    Text2ListBoxInput input;

    protected HashSet<String> targetSet;
    private final static int DEFAULT_SIZE = 4;

    public static int getDefaultSize()
    {
        return DEFAULT_SIZE;
    }

    public Text2ListBox() {
        initWidget(uiBinder.createAndBindUi(this));
        targetSet = new HashSet<String>(DEFAULT_SIZE);
        addInput.addClickHandler(this);
        removeInput.addClickHandler(this);
        disableBox();
    }

    //hack, ideally should have uitemplate specify a constructor
    //for now user is required to call init func below asap
    public void initInput(Text2ListBoxInput i)
    {
        input = i;
        inputSlot.add(i.getInputWidget());
        input.disableInput();
    }

    public void setStatus(String status)
    {
        statusLabel.setText(status);
    }

    public HandlerRegistration registerSaveButtonHandler(ClickHandler h)
    {
        return saveList.addClickHandler(h);
    }

    public void clear()
    {
        if(input != null)
            input.reset();
        if(statusLabel != null)
            statusLabel.setText("");
        targetList.clear();
        targetSet.clear();
        fireEvent();
    }

    public void setVisbileArea(int displaySize, String width)
    {
        targetList.setVisibleItemCount(displaySize);
        targetList.setWidth(width);
    }
    
    private void addItemToList(String value, boolean fire)
    {
    	String val = value.trim();
         if( val.length()!=0 && !targetSet.contains(val) && targetSet.add(val))
         {
            targetList.addItem(val);
            if(fire)
            	fireEvent();
         }
    }

    public void initializeList(String[] items)
    {
        clear();
        for(String s : items)
            addItemToList(s.trim(), false);
        enableBox();
        fireEvent();
    }

    public void initializeList(HashSet<String> items)
    {
        clear();
        if(items != null)
        {
	        for(String s: items)
	            addItemToList(s.trim(), false);
        }
        enableBox();
        fireEvent();
    }

    public void onClick(ClickEvent event) {
        if(event.getSource().equals(addInput))
        {
        	if(input.getErrorMessage().length() > 0)
        	{
        		statusLabel.setText("ERROR, unable to add because: " + input.getErrorMessage());
        		return;
        	}
        	
            String val = input.getCustomText();
            addItemToList(val, true);
            input.reset();
        }
        else if(event.getSource().equals(removeInput))
        {
            String val = targetList.getValue(targetList.getSelectedIndex());
            if(targetSet.contains(val) && targetSet.remove(val))
            {
                targetList.removeItem(targetList.getSelectedIndex());
                fireEvent();
            }
        }
    }

    public HashSet<String> getList()
    {
        return targetSet;
    }

    public void disableBox()
    {
        addInput.setEnabled(false);
        removeInput.setEnabled(false);
        if(saveList != null)
            saveList.setEnabled(false);
        if(input != null)
            input.disableInput();
    }

    public void enableBox()
    {
        addInput.setEnabled(true);
        removeInput.setEnabled(true);
        if(saveList != null)
            saveList.setEnabled(true);
        if(input != null)
            input.enableInput();
    }

    public boolean isSaveButton(Object b)
    {
        return (saveList.equals(b));
    }

    public void removeAdditionalFeatures()
    {
        additionalFeatures.remove(saveList);
        additionalFeatures.remove(statusLabel);
        saveList = null;
        statusLabel = null;
        additionalFeatures.removeFromParent();
    }
    
    private void fireEvent()
    {
    	ValueChangeEvent.fire(this, targetSet.size());
    }

	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<Integer> handler) {
		return addHandler(handler, ValueChangeEvent.getType()); 
	}
}