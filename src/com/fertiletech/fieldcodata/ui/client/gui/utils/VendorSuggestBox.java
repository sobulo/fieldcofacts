package com.fertiletech.fieldcodata.ui.client.gui.utils;

import com.fertiletech.fieldcodata.ui.client.gui.LocalDataProvider;

public class VendorSuggestBox extends UserSuggestBox{

	public VendorSuggestBox()
	{
		super();
		setDisplayLabel("Select Vendor");
	}
	@Override
	protected void populateSuggestBox() {
		LocalDataProvider.VENDOR_SERVICE.getAllVendorsIDs(getPopulateCallBack());
	}

	@Override
	public String getRole() {
		return "vendors";
	}

}
