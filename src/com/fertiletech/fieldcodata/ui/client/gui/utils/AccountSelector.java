package com.fertiletech.fieldcodata.ui.client.gui.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.fertiletech.fieldcodata.ui.client.gui.LocalDataProvider;
import com.fertiletech.fieldcodata.ui.shared.dto.DTOConstants;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.ListBox;

public class AccountSelector extends Composite implements HasValueChangeHandlers<Boolean>{
	private final static HashMap<String, TableMessage> cachedAccounts = new HashMap<String, TableMessage>();
	ListBox accounts;
	final AsyncCallback<List<TableMessage>> accountListCallBack = new AsyncCallback<List<TableMessage>>() {
		@Override
		public void onFailure(Throwable caught) {
			Window.alert("Unable to retrieve list of company accounts");
		}

		@Override
		public void onSuccess(List<TableMessage> result) {
			GWT.log("Came back with this many accounts: " + result.size());
			if(result.size() > 1)
			{
				result.remove(0); //get rid of the header
				for(TableMessage m : result)
					cachedAccounts.put(m.getMessageId(), m);
				populateAccountListFromCache();
			}
			else
			{
				Window.alert("No company accounts found in the database!!Please contact pilot-group@fieldcolimited for assistance.");
			}
		}
	};
	
	private void populateAccountListFromCache()
	{
		for(TableMessage m : cachedAccounts.values())
			accounts.addItem(getAccountDisplayName(m), m.getMessageId());
		fireChange();
	}
	
	public AccountSelector()
	{
		this(false);
	}
	
	public AccountSelector(boolean isMultiple)
	{
		accounts = new ListBox(isMultiple);
		if(isMultiple)
		{
			accounts.setTitle("Use CTRL key to select multiple accounts. Useful when grouping resident accounts with different currencies");
			accounts.setVisibleItemCount(DTOConstants.CURRENCY_MAP_NAME.size());
		}
		else
		{
			accounts.addChangeHandler(new ChangeHandler() {
				
				@Override
				public void onChange(ChangeEvent event) {
					fireChange();
				}
			});
		}
		
		accounts.setWidth("200px");
		initWidget(accounts);
		if(cachedAccounts.size() == 0)
			LocalDataProvider.ACCOUNT_SERVICE.getAllAccounts(accountListCallBack);
		else
			populateAccountListFromCache();
	}
	
	public void enableBox(boolean enable)
	{
		accounts.setEnabled(enable);
	}
	
	private void fireChange()
	{
		ValueChangeEvent.fire(this, true);
	}
	
	public String getAccountDisplayName(TableMessage m)
	{
		StringBuilder display = new StringBuilder();
		int maxLength = 6;
		String bank = m.getText(DTOConstants.CMP_ACCT_BNK_IDX);
		String acct = m.getText(DTOConstants.CMP_ACCT_NUM_IDX);
		String curr = m.getText(DTOConstants.CMP_ACCT_CURR_IDX);
		display.append(bank.substring(0, Math.min(maxLength, bank.length()))).append("../");
		display.append(acct.substring(0, Math.min(maxLength, acct.length()))).append("../[");
		display.append(curr).append("]");
		return display.toString();
	}

	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<Boolean> handler) {
		return addHandler(handler, ValueChangeEvent.getType());
	}
	
	public int getSelectedIndex()
	{
		return accounts.getSelectedIndex();
	}
	
	public List<TableMessage> getSelectedValues()
	{
		ArrayList<TableMessage> result = new ArrayList<TableMessage>(); 
		for(int i = 0; i < accounts.getItemCount(); i++)
		{
			if(accounts.isItemSelected(i))
				result.add(getDTO(i));
		}
		return result;
	}
	
	public String getValue(int idx)
	{
		return accounts.getValue(idx);
	}
	
	public TableMessage getDTO(int idx)
	{
		return cachedAccounts.get(accounts.getValue(idx));
	}
	
	public void setMessage(TableMessage m)
	{
		if(cachedAccounts.containsKey(m.getMessageId()))
		{
			for(int i = 0; i < getItemCount(); i++)
			{
				if(accounts.getValue(i).equals(m.getMessageId()))
				{
					accounts.removeItem(i);
					break;
				}
			}
		}
		accounts.addItem(getAccountDisplayName(m), m.getMessageId());
		accounts.setSelectedIndex(accounts.getItemCount() - 1);
		cachedAccounts.put(m.getMessageId(), m);
	}
	
	public int getItemCount()
	{
		return accounts.getItemCount();
	}
}