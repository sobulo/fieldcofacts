package com.fertiletech.fieldcodata.ui.client.gui.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class SerialNumberPopup extends YesNoDialog {

	ListBox serialNumbers;
	Label counts;
	public SerialNumberPopup(String title) {
		super(title, "Continue", "Cancel", "      ");
	}

	public void enableFields(boolean enabled)
	{
		serialNumbers.setEnabled(enabled);
		yes.setEnabled(enabled);
		no.setEnabled(enabled);
	}
	
	protected Widget getButtonPanel() {
		HorizontalPanel panel = new HorizontalPanel();
		VerticalPanel container = new VerticalPanel();
		container.setSpacing(5);
		panel.setSpacing(10);
		yes = new Button("Continue");
		no = new Button("Cancel");
		serialNumbers = new ListBox(true);
		serialNumbers.setVisibleItemCount(10);
		counts = new Label();
		no.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				alertBox.hide();
			}
		});
		serialNumbers.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				counts.setText("Selected Items: " + getSerialNumbers().size());
			}
		});

		panel.add(yes);
		panel.add(no);
		container.add(serialNumbers);
		container.add(counts);
		container.add(panel);
		return container;
	}
	
	public HashSet<String> getSerialNumbers()
	{
		HashSet<String> selectedSerials = new HashSet<String>();
		for(int i = 0; i < serialNumbers.getItemCount(); i++)
		{
			if(serialNumbers.isItemSelected(i))
				selectedSerials.add(serialNumbers.getValue(i));
		}
		return selectedSerials;
	}
	
	public void setSerialNumbers(Set<String> numbers)
	{
		counts.setText("");
		serialNumbers.clear();
		if(numbers.size() == 0) return;
		ArrayList<String> sortedList = new ArrayList<String>();
		sortedList.addAll(numbers);
		Collections.sort(sortedList);
		for(String serial : numbers)
			serialNumbers.addItem(serial);
	}
	
	public void enableModal(boolean enable)
	{
		alertBox.setModal(enable);
	}
}
