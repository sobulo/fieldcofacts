package com.fertiletech.fieldcodata.ui.client.gui.utils;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class UserCommentPopup extends YesNoDialog{
	TextArea comments;
	Label counts;

	public UserCommentPopup(String title) {
		super(title, "Continue", "Cancel", "      ");
	}
	
	protected Widget getButtonPanel()
	{
		HorizontalPanel panel = new HorizontalPanel();
		VerticalPanel container = new VerticalPanel();
		container.setSpacing(5);
		panel.setSpacing(10);
		yes = new Button("Continue");
		no = new Button("Cancel");
		comments = new TextArea();
		comments.setSize("90%", "100px");
		counts = new Label();
		no.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				alertBox.hide();
			}
		});
		
		panel.add(yes);
		panel.add(no);
		container.add(comments);
		container.add(counts);
		container.add(panel);
		return container;
	}
	
	public String getComments()
	{
		return comments.getValue();
	}
	
	public void setMinimumCommentLength(final int minLength)
	{
		comments.addKeyUpHandler(new KeyUpHandler() {
			
			@Override
			public void onKeyUp(KeyUpEvent event) {
				int charCount = comments.getValue().trim().length();
				if( charCount < minLength)
				{
					if(yes.isEnabled())
						yes.setEnabled(false);
				}
				else
				{
					if(!yes.isEnabled())
						yes.setEnabled(true);
				}
				counts.setText("Char count: " + charCount + " Min: " + minLength);
			}
		});		
	}
	
	public void clear()
	{
		comments.setValue(null);
	}
}
