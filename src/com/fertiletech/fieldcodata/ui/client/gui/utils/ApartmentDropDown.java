package com.fertiletech.fieldcodata.ui.client.gui.utils;

import java.util.HashMap;

import com.fertiletech.fieldcodata.ui.client.gui.LocalDataProvider;
import com.fertiletech.fieldcodata.ui.shared.dto.DTOConstants;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;

public class ApartmentDropDown extends Composite implements ChangeHandler, HasValueChangeHandlers<String>{
	ListBox buildingNames;
	ListBox apartmentNames;
	Label buildingStatus;
	Boolean originalVacantMode;
	
	HashMap<String, HashMap<String, String>[]> cachedApartments = new HashMap<String, HashMap<String, String>[]>();

	LocalDataProvider pc = new LocalDataProvider();
	
    // Create an asynchronous callback to handle the result.
    private final AsyncCallback<HashMap<String, String>[]> apartmentNamesCallback = new AsyncCallback<HashMap<String, String>[]>() {

        @Override
        public void onSuccess(HashMap<String, String>[] result) {
        	//hope commenting this out doesn't break anything
        	//populateApartmentNames(result[DTOConstants.APT_VACANT_IDX]);
           	cachedApartments.put(buildingNames.getValue(buildingNames.getSelectedIndex()), result);
           	buildingStatus.setText("Occuppied: " + result[DTOConstants.APT_OCCUPIED_IDX].size() + 
           			" Vacant: " + result[DTOConstants.APT_VACANT_IDX].size());
           	
           	//this replaces commented code above, not removing cachedApartments line above will turn this call into
           	//an infinite loop of server calls as load vacancies refers to this callback
           	loadBuildingVacancies();
           	enableFields(true);
        }

        @Override
        public void onFailure(Throwable caught) {
        	Window.alert("Unable to retrieve apartment names. Try refreshing your browser. " +
        			"Contact info@fertiletech.com if problems persist. <p> Error msg: <b>"+caught.getMessage() + "</b></p>");
        }
    };	
	
    // Create an asynchronous callback to handle the result.
    private final AsyncCallback<HashMap<String, String>> buildingNamesCallback = new AsyncCallback<HashMap<String, String>>() {

        @Override
        public void onSuccess(HashMap<String, String> result) {
        	populateBuildingNames(result);
        	enableFields(true);
        	pc.populateBuildingNamesMap(result);
        	buildingStatus.setText("Retrieved: " + result.size() + " building names");
        	loadBuildingVacancies();
        }

        @Override
        public void onFailure(Throwable caught) {
        	Window.alert("Unable to retrieve building names. Try refreshing your browser. " +
        			"Contact info@fertiletech.com if problems persist. <p> Error msg: <b>"+caught.getMessage() + "</b></p>");
        }
    };
	
    public ApartmentDropDown()
    {
    	this(true);
    }
	
	public ApartmentDropDown(Boolean vacant)
	{
		this.originalVacantMode = vacant;
		buildingNames = new ListBox();
		apartmentNames = new ListBox();
		buildingStatus = new Label("Loading building names ...");
		FlexTable b = new FlexTable();
		if(vacant == null || !vacant)
		{
			//hack, this was originally built for just vacant before being extended
			//status hidden in other modes for layout purposes
			buildingStatus.setVisible(false);
			b.setText(0, 0, "Building: ");
			b.setWidget(0, 1, buildingNames);
			b.setText(0, 2, "Apartment: ");
			b.setWidget(0, 3, apartmentNames);
		}
		else
		{
			b.setText(0, 0, "Building: ");
			b.setText(1, 0, "Apartment: ");
			b.setWidget(0, 1, buildingNames);
			b.setWidget(0, 2, buildingStatus);
			b.setWidget(1, 1, apartmentNames);
			b.getFlexCellFormatter().setColSpan(1, 1, 2);
		}
		
		initWidget(b);
		buildingNames.addChangeHandler(this);
		apartmentNames.addChangeHandler(this);
		if(pc.getBuildingNamesSize() == 0)
		{
			buildingStatus.setText("Loading building names");
			LocalDataProvider.TENANT_SERVICE.getBuildingNames(buildingNamesCallback);
			enableFields(false);
		}
		else
			pc.populateListBoxWithBuildingNames(buildingNames);
	}
	
	public void enableFields(boolean enabled)
	{
		buildingNames.setEnabled(enabled);
		apartmentNames.setEnabled(enabled);
	}	
	
	  private void populateBuildingNames(HashMap<String, String> nameMap)
	  {
		  for(String id : nameMap.keySet())
			  buildingNames.addItem(nameMap.get(id), id);
		  buildingNames.setSelectedIndex(0);
	  }
	  private void populateApartmentNames(HashMap<String, String> nameMap)
	  {
		  apartmentNames.clear();
		  for(String id : nameMap.keySet())
			  apartmentNames.addItem(nameMap.get(id), id);
		  
		  if(nameMap.size() == 0 && originalVacantMode!= null && originalVacantMode)
			  apartmentNames.addItem(NO_VACANCIES);
		  
		  apartmentNames.setSelectedIndex(0);
		  buildingStatus.setText("No. of vacant apartments: " + nameMap.size());
		  fireEvent();
	  }	   

	@Override
	public void onChange(ChangeEvent event) {
		if(event.getSource() == buildingNames)
			loadBuildingVacancies();
		else
			fireEvent();
	}

	final static String NO_VACANCIES = "No Vacancies";
	private void loadBuildingVacancies()
	{
		String buildingID = buildingNames.getValue(buildingNames.getSelectedIndex());
		String buildingName = buildingNames.getItemText(buildingNames.getSelectedIndex());
		HashMap<String, String>[] apartments = cachedApartments.get(buildingID);
		if(apartments == null)
		{
			enableFields(false);
			buildingStatus.setText("Loading vacant apartments for " + buildingName);
			LocalDataProvider.TENANT_SERVICE.getApartments(buildingID, apartmentNamesCallback);
			return;
		}
		HashMap<String, String> apartmentsToUse = null;
		
		if(originalVacantMode == null)
		{
			apartmentsToUse = new HashMap<String, String>();
			apartmentsToUse.putAll(apartments[DTOConstants.APT_VACANT_IDX]);
			apartmentsToUse.putAll(apartments[DTOConstants.APT_OCCUPIED_IDX]);
		}
		else if(originalVacantMode)
			apartmentsToUse = apartments[DTOConstants.APT_VACANT_IDX];
		else
			apartmentsToUse = apartments[DTOConstants.APT_OCCUPIED_IDX];
		
		populateApartmentNames(apartmentsToUse);
	}
	
	public String getSelectedApartment()
	{
		if(apartmentNames.getItemCount() == 0) return null; 
		
		String returnVal = apartmentNames.getValue(apartmentNames.getSelectedIndex());
		if(returnVal.equals(NO_VACANCIES))
			returnVal = "";
		return returnVal;
	}

	public String getSelectedApartmentName()
	{
		if(apartmentNames.getItemCount() == 0) return null; 
		
		String returnVal = apartmentNames.getItemText(apartmentNames.getSelectedIndex());
		if(returnVal.equals(NO_VACANCIES))
			returnVal = "";
		return returnVal;
	}	
	
	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<String> handler) {
		return addHandler(handler, ValueChangeEvent.getType());
	}
	
	void fireEvent()
	{
		GWT.log("Firing Apartment Change for " + getSelectedApartmentName());
		ValueChangeEvent.fire(this, getSelectedApartment());
	}
	
}
