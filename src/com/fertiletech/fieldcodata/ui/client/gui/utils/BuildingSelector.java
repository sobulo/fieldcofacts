package com.fertiletech.fieldcodata.ui.client.gui.utils;

import java.util.HashMap;

import com.fertiletech.fieldcodata.ui.client.gui.LocalDataProvider;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.SimplePanel;

public class BuildingSelector extends Composite implements HasValueChangeHandlers<String>{
	ListBox buildings;
	SimplePanel slot;
    private final AsyncCallback<HashMap<String, String>> buildingNamesCallback = new AsyncCallback<HashMap<String, String>>() {

        @Override
        public void onSuccess(HashMap<String, String> result) {
        	for(String key : result.keySet())
        		buildings.addItem(result.get(key), key);
        	slot.clear();
        	if(result.size() == 0)
        	{
        		buildings = null;
        		slot.clear();
        		slot.add(new HTML("No buildings registered in the database"));
        	}
        	else
        	{
        		buildings.addChangeHandler(new ChangeHandler() {
					
					@Override
					public void onChange(ChangeEvent event) {
						fireChange();
					}
				});
        		slot.add(buildings);
        		GWT.log("Selected returns: " + getSelectedBuilding());
        		fireChange();
        	}
        }

        @Override
        public void onFailure(Throwable caught) {
        	slot.clear();
        	slot.add(new HTML("Unable to retrieve building names. Try refreshing your browser. " +
        			"Contact info@fertiletech.com if problems persist. <p> Error msg: <b>" + caught.getMessage() + "</b></p>"));
        }
    };
    
    public BuildingSelector(boolean buildingsOnly) {
    	slot = new SimplePanel();
    	buildings = new ListBox();
    	initWidget(slot);
    	if(buildingsOnly)
    	{
    		slot.add(new HTML("Loading buildings ..."));
    		LocalDataProvider.TENANT_SERVICE.getBuildingNames(buildingNamesCallback);
    	}
    	else
    	{
    		slot.add(new HTML("Loading ..."));
    		LocalDataProvider.TENANT_SERVICE.getAllBuildingAndApartments(buildingNamesCallback);
    	}
    		
	}
    
    public BuildingSelector()
    {
    	this(true);
    }
    
    public String getSelectedBuilding()
    {
    	if(buildings == null || buildings.getItemCount() == 0) return null; //timing issue, selector could exist and this can be called prior to listbox initialization
    	return buildings.getValue(buildings.getSelectedIndex());
    }

    public String getSelectedBuildingName()
    {
    	if(buildings == null) return null;
    	return buildings.getItemText(buildings.getSelectedIndex());
    }
    
    
    public void selectBuilding(String value)
    {
    	for(int i = 0; i < buildings.getItemCount(); i++)
    		if(buildings.getValue(i).equals(value))
    		{
    			buildings.setSelectedIndex(i);
    			fireChange();
    			break;
    		}
    }
    
    public void selectBuildingName(String itemName)
    {
    	for(int i = 0; i < buildings.getItemCount(); i++)
    		if(buildings.getItemText(i).equals(itemName))
    		{
    			buildings.setSelectedIndex(i);
    			break;
    		}
    }
    
    public void enableSelector(boolean enabled)
    {
    	buildings.setEnabled(enabled);
    }
    
    private void fireChange()
    {
    	String val = getSelectedBuilding();
    	GWT.log("Firing with value: " + val + " name: " + getSelectedBuildingName());
    	ValueChangeEvent.fire(this, val);
    }    
    
	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<String> handler) {
		return addHandler(handler, ValueChangeEvent.getType());
	}
	    
}
