package com.fertiletech.fieldcodata.ui.client.gui.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import com.fertiletech.fieldcodata.ui.client.gui.LocalDataProvider;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;

public class ProductSelector extends Composite implements HasValueChangeHandlers<String>{
	ListBox productList;
	Label label;
	HorizontalPanel slot;
	
    // Create an asynchronous callback to handle the result.
    private final AsyncCallback<HashMap<String, String>> productsCallback = new AsyncCallback<HashMap<String, String>>() {

        @Override
        public void onSuccess(HashMap<String, String> result) {
        	productList = new ListBox();
        	TreeMap<String, String> orderedMap = new TreeMap<String, String>();
        	for(String key : result.keySet())
        		orderedMap.put(result.get(key), key);
        	Map<String, String> map = orderedMap;
        	if(orderedMap.size() != result.size()) //somehow 2 products have same name
        		map = result; //revert to unsorted map
        	for(String key : map.keySet())
        		productList.addItem(key, map.get(key));
        	
        	if(productList.getItemCount() == 0)
        	{
        		Window.alert("No Products available in Inventory Database");
        		slot.add(new HTML("<font color='red'>0 product definitions in database</font>"));
        		return;
        	};
        	
        	productList.addChangeHandler(new ChangeHandler() {				
				@Override
				public void onChange(ChangeEvent event) {
					fireChange();
				}
			});
        	
        	slot.add(productList);
        	fireChange();
        }

        @Override
        public void onFailure(Throwable caught) {
        	Window.alert("Error - Try Refereshing<br/><marquee>" + caught.getMessage());
    		slot.add(new HTML("<font color='red'>Error fetching product definitions. Browser refresh?</font>"));
        }
    };	
    
    private void fireChange()
    {
    	String selectedValue = getSelectedValue();
    	if(selectedValue == null) return;
    	ValueChangeEvent.fire(this, selectedValue);
    }
    
    public ProductSelector()
    {
    	this(true);
    }
    
	public ProductSelector(boolean showLabel) 
	{
		slot = new HorizontalPanel();
		if(showLabel)
		{
			label = new HTML();
			slot.setSpacing(5);
			label.setText("Select Product");
			slot.add(label);
		}
		LocalDataProvider.INVENTORY_SERVICE.getProductDefinitiions(productsCallback);
		initWidget(slot);	
	}
			
	public String getSelectedValue()
	{
		if(productList == null) return null;
		return productList.getValue(productList.getSelectedIndex());
	}
	
	public String getSelectedName()
	{
		return productList.getItemText(productList.getSelectedIndex());
	}	

	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<String> handler) {
		return addHandler(handler, ValueChangeEvent.getType());
	}
	
	public void enableSelector(boolean enabled)
	{
		productList.setEnabled(enabled);
	}
}
