package com.fertiletech.fieldcodata.ui.client.gui.utils;

import com.fertiletech.fieldcodata.ui.client.gui.LocalDataProvider;
import com.fertiletech.fieldcodata.ui.shared.dto.FormConstants;
import com.google.gwt.user.client.ui.VerticalPanel;

public class RatingsPanel extends CommentsPanel{
	MultiChoicePanelGenerator choice;	

	@Override
	protected void loadComments() {
		LocalDataProvider.COMMENT_SERVICE.loadRatings(commentID, loadCallback);
	}

	@Override
	protected void saveComments() {
		Integer rating = getRating();
		if(rating == null)
		{
			infoBox.show("You must select a rating");
			return;
		}
			LocalDataProvider.COMMENT_SERVICE.saveRatings(commentID, rating, 
				super.richText.getHTML(), false, super.saveCallback);
	}

	@Override
	protected void clear(boolean clearPrior) {
		super.clear(clearPrior);
		choice.setAnswer(null);
	}

	@Override
	protected VerticalPanel getRichTextContainer() {
		VerticalPanel p = super.getRichTextContainer();
		choice = new MultiChoicePanelGenerator();
		p.add(choice.getMultiChoicePanel("Specify a new rating", FormConstants.VENDOR_RATINGS));
		return p;
	}
	
	private Integer getRating()
	{
		String rating = choice.getAnswer();
		if(rating == null)
			return null;
		return Integer.valueOf(rating);
	}

}
