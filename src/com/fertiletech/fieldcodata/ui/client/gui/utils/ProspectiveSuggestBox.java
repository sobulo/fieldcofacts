package com.fertiletech.fieldcodata.ui.client.gui.utils;

public class ProspectiveSuggestBox extends UserSuggestBox {
	
	public ProspectiveSuggestBox()
	{
		super();
		setDisplayLabel("Select Client");
	}

	@Override
	protected void populateSuggestBox() {
		getUserService().getAllProspects(getPopulateCallBack());
	}

	@Override
	public String getRole() {
		return "prospects";
	}

}
