/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fertiletech.fieldcodata.ui.client.gui.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import com.fertiletech.fieldcodata.ui.client.GUIConstants;
import com.fertiletech.fieldcodata.ui.client.gui.LocalDataProvider;
import com.fertiletech.fieldcodata.ui.shared.dto.DTOConstants;
import com.fertiletech.fieldcodata.ui.shared.dto.FormConstants;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;

/**
 *
 * @author Administrator
 */
public class UserPanel extends Composite implements HasValueChangeHandlers<String>
{
    @UiField TextBox firstName;
    @UiField TextBox lastName;
    @UiField TextBox email;
    @UiField TextBox phone;
    @UiField Text2ListBox otherNumbers;
    @UiField Text2ListBox otherMail;
    @UiField TextArea address;
    @UiField TextBox salutation;
    @UiField FlowPanel sexPanel;
    RadioButton male;
    RadioButton female;
    @UiField Label companyLabel;
    @UiField ListBox companyBox;
    @UiField ListBox birthYear;
    @UiField DateBox dateOfBirth;
    SimpleDialog infoBox;
    String userID;
    String tempUserID;
    String companyParamId;
    
	AsyncCallback<List<TableMessage>> companycallback = new AsyncCallback<List<TableMessage>>() {
		
		@Override
		public void onFailure(Throwable caught) {
			infoBox.show("Unable to retrieve company list");
		}

		@Override
		public void onSuccess(List<TableMessage> result) {
			result.remove(0); //remove header
			companyBox.clear();
			companyBox.addItem("                  ", "");
			for(TableMessage m : result)
				companyBox.addItem(m.getText(0).toUpperCase());
			if(tempUserID != null)
			{
				LocalDataProvider.TENANT_SERVICE.getContact(tempUserID, callback);
				tempUserID = null;
			}
		}
	};    

    private static UserPanelUiBinder uiBinder = GWT.create(UserPanelUiBinder.class);
    private static final RegExp EMAIL_MATCHER = RegExp.compile(FormConstants.REGEX_EMAIL, "i");
    private static final RegExp PHONE_NUM_MATCHER = RegExp.compile(FormConstants.REGEX_NUMS_ONLY);
    int DEFAULT_PHONE_PER_USER = 5;
    String DEFAULT_LB_WIDTH = "200px";
    
	AsyncCallback<TableMessage> callback = new AsyncCallback<TableMessage>() {

		@Override
		public void onFailure(Throwable caught) {
			infoBox.show("Error loading contact information");
		}

		@Override
		public void onSuccess(TableMessage result) {
			GWT.log("received a table message with: " + result.getNumberOfTextFields());
			userID = result.getMessageId();
			setContactValues(result);
			infoBox.show("Succesfully loaded information for " + firstName.getValue());
			fireUserIDEvent();
		}
	};
    
    @SuppressWarnings("deprecation")
	public UserPanel(boolean companyForResident)
    {
        initWidget(uiBinder.createAndBindUi(this));
        infoBox = new SimpleDialog("BIO MESSAGE");
        setupTextListBox(otherMail);
        setupTextListBox(otherNumbers);
        male=new RadioButton("sex", "Male");
        female=new RadioButton("sex", "Female");
        sexPanel.add(male);
        sexPanel.add(female);
        Date currentDate = new Date();
        int start = currentDate.getYear();
        for(int i = start; i > start - 100; i--)
        	birthYear.addItem(String.valueOf(i + 1900), String.valueOf(i));
        birthYear.addChangeHandler(new ChangeHandler() {
			
			@Override
			public void onChange(ChangeEvent event) {
				int year = Integer.valueOf(birthYear.getValue(birthYear.getSelectedIndex()));
				Date d = dateOfBirth.getValue();
				d = d==null?new Date() : d;
				d.setYear(year);
				dateOfBirth.setValue(d);
			}
		});
        dateOfBirth.addValueChangeHandler(new ValueChangeHandler<Date>() {

			@Override
			public void onValueChange(ValueChangeEvent<Date> event) {
				int year = event.getValue().getYear();
				for(int i = 0; i < birthYear.getItemCount(); i++)
					if(Integer.valueOf(birthYear.getValue(i)) == year)
						birthYear.setSelectedIndex(i);
			}
		});
        dateOfBirth.setFormat(GUIConstants.DEFAULT_DATEBOX_FORMAT);
        companyParamId = DTOConstants.APP_PARAM_COMPANY_LIST_KEY;
        if(!companyForResident)
        	companyParamId = DTOConstants.APP_PARAM_VENDOR_COMPANY_LIST_KEY;
    	LocalDataProvider.COMMENT_SERVICE.getApplicationParameter(companyParamId, companycallback);
    }
    
    public UserPanel()
    {
    	this(true);
    }
    
    private void fireUserIDEvent()
    {
    	ValueChangeEvent.fire(this, userID);
    }
    
	public void setContactValues(TableMessage m)
	{
		if(m == null) clear();
		this.setFirstName(m.getText(DTOConstants.TNT_FNAME_IDX));
		this.setLastName(m.getText(DTOConstants.TNT_LNAME_IDX));
		this.setEmail(m.getText(DTOConstants.TNT_PRIMARY_EMAIL_IDX));
		this.setPhone(m.getText(DTOConstants.TNT_PRIMARY_PHONE_IDX));
		dateOfBirth.setValue(m.getDate(DTOConstants.TNT_DATE_OF_BIRTH_IDX));

		int startIdx = (int) m.getNumber(DTOConstants.TNT_OTHER_PHONE_START_IDX).doubleValue();
		if(startIdx < 0)
			this.setOtherNumbers(null);
		else
			this.setOtherNumbers(getMultiVals(startIdx, 
				(int) m.getNumber(DTOConstants.TNT_OTHER_PHONE_END_IDX).doubleValue(), m));
		

		startIdx = (int) m.getNumber(DTOConstants.TNT_OTHER_EMAIL_START_IDX).doubleValue();
		if(startIdx < 0)
			this.setOtherMails(null);
		else
			this.setOtherMails(getMultiVals(startIdx, 
					(int) m.getNumber(DTOConstants.TNT_OTHER_EMAIL_END_IDX).doubleValue(), m));

		this.setAddress(m.getText(DTOConstants.TNT_ADDRESS_IDX));
		this.setSalutation(m.getText(DTOConstants.TNT_TITLE_IDX));
		String boolString = m.getText(DTOConstants.TNT_GENDER_IDX);
		this.setMale(boolString==null?null:Boolean.valueOf(boolString));
		this.setCompany(m.getText(DTOConstants.TNT_COMPANY_IDX));
		this.userID = m.getMessageId();
	}
	
	public String getUserID()
	{
		return userID;
	}
	
	public void setUserID(String id)
	{
		userID = id;
	}
	
	public void loadUserInfo(String userKey)
	{
		clear();
		GWT.log("make call to load prospective info");
		infoBox.show("Loading contact information...");
		tempUserID = userKey;
		if(companyBox.getItemCount() == 0)
			LocalDataProvider.COMMENT_SERVICE.getApplicationParameter(companyParamId, companycallback);
		else
			LocalDataProvider.TENANT_SERVICE.getContact(userKey, callback);
	}
	
	
	public HashSet<String> getMultiVals(int start, int end, TableMessage m)
	{
		GWT.log("start: " + start + " end: " + end);
		if(start < 0 || end < 0)
			return null;		
		HashSet<String> multiVals = new HashSet<String>();
		for(int i=start; i<=end; i++)
			multiVals.add(m.getText(i));
		return multiVals;
	}	
    
    private void setupTextListBox(Text2ListBox t2l)
    {
        t2l.initInput(new TextBoxWrapper());
        t2l.setVisbileArea(DEFAULT_PHONE_PER_USER, DEFAULT_LB_WIDTH);
        t2l.enableBox();
        t2l.removeAdditionalFeatures();    	
    }
    
    
    public void disableNameEdits()
    {
    	firstName.setEnabled(false);
    	lastName.setEnabled(false);
    }

    public String getEmail() {
        return email.getValue().trim().toLowerCase();
    }
    
    public boolean isMale()
    {
    	if(male.getValue())
    		return true;
    	else
    		return false;
    }
    
    public String getPhone() {
        return phone.getValue().trim().toLowerCase();
    }

    public String getFirstName() {
        return firstName.getValue().trim();
    }

    public String getLastName() {
        return lastName.getValue().trim();
    }

    public Date getDateOfBirth()
    {
    	return dateOfBirth.getValue();
    }
    
    public HashSet<String> getOtherNumbers() {
        return otherNumbers.getList();
    }

    public HashSet<String> getOtherMails() {
        return otherMail.getList();
    }
    
    public String getAddress()
    {
    	return address.getValue().trim();
    }
    
    public void setEmail(String eml) {
        email.setValue(eml);
    }
    
    public void setPhone(String eml) {
        phone.setValue(eml);
    }
    
    public void setFirstName(String fnm) {
        firstName.setValue(fnm);
    }

    public void setLastName(String lnm) {
        lastName.setValue(lnm);
    }

    public void setOtherNumbers(HashSet<String> phnms) {
        otherNumbers.initializeList(phnms);
    }
    
    public void setAddress(String addy)
    {
    	address.setValue(addy);
    }
    
    public void setMale(Boolean isMale)
    {
    	if(isMale == null)
    	{
    		male.setValue(null);
    		female.setValue(null);
    	}
    	else if(isMale)
    		male.setValue(true);
    	else
    		female.setValue(true);
    }
    
    public void setOtherMails(HashSet<String> emails) {
        otherMail.initializeList(emails);
    }
    
    public boolean isValidEmail(String email)
    {
    	return EMAIL_MATCHER.exec(email) != null;
    }
    
    public boolean isValidNum(String num)
    {
    	return PHONE_NUM_MATCHER.exec(num.replaceAll(FormConstants.REGEX_PHONE_REPLACE, "")) != null;
    }
    
    public void showCompany(boolean visible)
    {
    	companyBox.setVisible(visible);
    	companyLabel.setVisible(visible);
    }
    
    public String getCompany()
    {
    	return companyBox.getValue(companyBox.getSelectedIndex());
    }
    
    public void setCompany(String company)
    {
    	company = company == null? "" : company;
    	int index = selectAListBoxItem(companyBox, company);
    	if(index < 0 && company != null && company.length() > 0)
    		infoBox.show("<b>WARNING</b>: company " + company + " not on drop down list.<br/> Add the company under admin modules or" +
    				" select a name from the drop down list that represents the company you're trying to select");
    }
    
	private int selectAListBoxItem(ListBox l, String val)
	{
		for( int i = 0; i < l.getItemCount(); i++)
			if(l.getValue(i).equals(val))
			{
				l.setSelectedIndex(i);
				return i;
			}
		return -1;
	}

    public void clear()
    {
    	birthYear.setSelectedIndex(0);
    	dateOfBirth.setValue(null);
        otherNumbers.clear();
        otherMail.clear();
        firstName.setValue(null);
        lastName.setValue(null);
        email.setValue(null);
        phone.setValue(null);
        salutation.setValue(null);
        male.setValue(null);
        female.setValue(null);
        address.setValue(null);        
        companyBox.setSelectedIndex(0);
        userID = null;
    }
    
    public void enablePanel(boolean enabled)
    {
    	birthYear.setEnabled(enabled);
    	dateOfBirth.setEnabled(enabled);
    	if(enabled)
    	{
    		otherNumbers.enableBox();
    		otherMail.enableBox();    		
    	}
    	else
    	{
    		otherNumbers.disableBox();
    		otherMail.disableBox();
    	}
    	firstName.setEnabled(enabled);
        lastName.setEnabled(enabled);
        email.setEnabled(enabled);
        phone.setEnabled(enabled);
        salutation.setEnabled(enabled);
        male.setEnabled(enabled);
        female.setEnabled(enabled);
        address.setEnabled(enabled);
        companyBox.setEnabled(enabled);
    }    
    
    public String getSalutation() {
		return salutation.getValue();
	}

	public void setSalutation(String string) {
		salutation.setValue(string);
	}

	public ArrayList<String> validateFields()
    {
        ArrayList<String> result = new ArrayList<String>();

        //validate fields
        if(getFirstName().length() == 0)
        	result.add("First name field is blank");
        
        if(getLastName().length() == 0)
        	result.add("Last name field is blank");
                
        if(getEmail().length() != 0 && !isValidEmail(getEmail()))
        	result.add("Email address is not valid");

        if(getPhone().length() != 0 && !isValidNum(getPhone()))
        	result.add("Email address is not valid");
        
        if(male.getValue() == false && female.getValue() == false)
        	result.add("No gender selected (male or female)");

        for(String num : getOtherNumbers())
        	if(!isValidNum(num))
        		result.add("Phone number: " + num + " is not valid");
        
        for(String eml : getOtherMails())
        	if(!isValidEmail(eml))
        		result.add("Email: " + eml + " is not valid");        
        
        return result;
    }

    interface UserPanelUiBinder extends UiBinder<Widget, UserPanel> {
    }

	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<String> handler) {
		return addHandler(handler, ValueChangeEvent.getType());
	}
}