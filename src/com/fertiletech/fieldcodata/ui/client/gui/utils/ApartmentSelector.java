package com.fertiletech.fieldcodata.ui.client.gui.utils;

import java.util.HashMap;
import java.util.List;

import com.fertiletech.fieldcodata.ui.client.gui.LocalDataProvider;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessageHeader;
import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.cellview.client.HasKeyboardPagingPolicy.KeyboardPagingPolicy;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AbstractImagePrototype;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.DefaultSelectionEventManager;
import com.google.gwt.view.client.NoSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;
import com.sun.corba.se.pept.transport.ContactInfo;

public class ApartmentSelector extends Composite implements ChangeHandler, HasValueChangeHandlers<Boolean>{
	/**
	 * The pager used to change the range of data.
	 */

	@UiField
	ShowMorePagerPanel pagerPanel;

	/**
	 * The pager used to display the current range.
	 */

	@UiField
	RangeLabelPager rangeLabelPager;

	@UiField
	ListBox buildingNames;

	@UiField
	Label buildingHeader;

	/**
	 * The CellList.
	 */

	private CellList<TableMessage> cellList;
	
	private SingleSelectionModel<TableMessage> selectionModel;
	private NoSelectionModel<TableMessage> noSelectionModel;
	private DefaultSelectionEventManager<TableMessage> whiteList;
	
	private static ApartmentSelectorUiBinder uiBinder = GWT
			.create(ApartmentSelectorUiBinder.class);

	interface ApartmentSelectorUiBinder extends
			UiBinder<Widget, ApartmentSelector> {
	}

	LocalDataProvider providerCache = new LocalDataProvider();
	// Create an asynchronous callback to handle the result.
	private final AsyncCallback<HashMap<String, String>> buildingNamesCallback = new AsyncCallback<HashMap<String, String>>() {

		@Override
		public void onSuccess(HashMap<String, String> result) {
			populateBuildingNames(result);
			buildingNames.setEnabled(true);
			providerCache.populateBuildingNamesMap(result);
			fireChangeNotification(false);
			buildingHeader.setText("Select Building");
		}

		@Override
		public void onFailure(Throwable caught) {
			buildingHeader.setText("Unable to retrieve building names. Try refreshing your browser. "
					+ "Contact info@fertiletech.com if problems persist.\nError msg:\n"
					+ caught.getMessage());
		}
	};
	
    // Create an asynchronous callback to handle the result.
    private final AsyncCallback<List<TableMessage>> apartmentListCallback = new AsyncCallback<List<TableMessage>>() {

        @Override
        public void onSuccess(List<TableMessage> result) {
        	GWT.log("Apartment List had size: " + result.size());
        	TableMessageHeader header = (TableMessageHeader) result.remove(0);
        	String buildingID = header.getMessageId();
        	List<TableMessage> apartmentList = providerCache.getApartmentInfoList(buildingID);
        	apartmentList.addAll(result);
        	GWT.log("Apartment view list had size: " + apartmentList.size());
        	buildingHeader.setText(header.getCaption());
        	buildingNames.setEnabled(true);
        	fireChangeNotification(true);
        }

        @Override
        public void onFailure(Throwable caught) {
        	buildingHeader.setText("Unable to retrieve apartment names. Try refreshing your browser. " +
        			"Contact info@fertiletech.com if problems persist. Error msg: " + caught.getMessage());
        }
    };	

	public ApartmentSelector() {
		initWidget(uiBinder.createAndBindUi(this));
		Images image = GWT.create(Images.class);
		ApartmentCell apartmentCell = new ApartmentCell();
		// Set a key provider that provides a unique key for each contact. If
		// key is
		// used to identify contacts when fields (such as the name and address)
		// change.
		cellList = new CellList<TableMessage>(apartmentCell,
				TableMessageKeyProvider.KEY_PROVIDER);
		cellList.setPageSize(30);
		cellList.setKeyboardPagingPolicy(KeyboardPagingPolicy.INCREASE_RANGE);
		cellList.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.BOUND_TO_SELECTION);

		// Add a selection model so we can select cells.
		selectionModel = new SingleSelectionModel<TableMessage>(
				TableMessageKeyProvider.KEY_PROVIDER);
		selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
			
			@Override
			public void onSelectionChange(SelectionChangeEvent event) {
				fireChangeNotification(true);
			}
		});
		cellList.setSelectionModel(selectionModel);
		
		//instantiate no selection model and selection manager (both needed for disabling selection)
		noSelectionModel = new NoSelectionModel<TableMessage>();
		whiteList = DefaultSelectionEventManager.<TableMessage>createWhitelistManager();
		
		
		// server side async calls
		buildingNames.addChangeHandler(this);

		// Add the CellList to the data provider in the database.

		// Set the cellList as the display of the pagers. This example has two
		// pagers. pagerPanel is a scrollable pager that extends the range when
		// the
		// user scrolls to the bottom. rangeLabelPager is a pager that displays
		// the
		// current range, but does not have any controls to change the range.
		pagerPanel.setDisplay(cellList);
		rangeLabelPager.setDisplay(cellList);
		
		if(providerCache.getBuildingNamesSize() == 0)
		{
			buildingHeader.setText("Retrieving building names, please wait ...");
			LocalDataProvider.TENANT_SERVICE.getBuildingNames(buildingNamesCallback);
		}
		else
		{
			buildingHeader.setText("Select Building");
			providerCache.populateListBoxWithBuildingNames(buildingNames);
			fireChangeNotification(false);
		}
	}

	private void populateBuildingNames(HashMap<String, String> nameMap) {
		for (String id : nameMap.keySet())
			buildingNames.addItem(nameMap.get(id), id);
		buildingNames.setSelectedIndex(0);
		switchBuildingDisplay();
	}
	
	@Override
	public void onChange(ChangeEvent event) {
		switchBuildingDisplay();
		fireChangeNotification(false);
	}

	private void switchBuildingDisplay() {
		buildingNames.setEnabled(false);
		//remove our display from its current data source, this removes it from old building id so it can later be added in this
		//function to the currently selected building id
		GWT.log("removing view from cell list");
		providerCache.removeDisplayFromApartmentMap(cellList);
		
		//fetch list for newly created building
		String buildingID = buildingNames.getValue(buildingNames.getSelectedIndex());
		String buildingName = buildingNames.getItemText(buildingNames.getSelectedIndex());
		GWT.log("Building Name: " + buildingName);
		GWT.log("Building ID: " + buildingID);
		
		List<TableMessage> apartmentInfoList = providerCache.getApartmentInfoList(buildingID);
		GWT.log("Apartment size when switching displays: " + apartmentInfoList.size());
		if(apartmentInfoList.size() == 0) //nothing found locally, check server to see if data is available
		{
			buildingHeader.setText("Retrieving apartment list, please wait ...");
			LocalDataProvider.TENANT_SERVICE.getBuildingApartmentTable(buildingID, apartmentListCallback);
		}
		else
		{
			buildingHeader.setText(buildingName + "(" + apartmentInfoList.size() + " tenants)");
			buildingNames.setEnabled(true);
			fireChangeNotification(true);
		}
		//add display to the new data source
		providerCache.addViewToApartmentListDataProvider(cellList, buildingID);
		GWT.log("Adding view to cell list");
	}	

	/**
	 * The images used for this example.
	 */
	static interface Images extends ClientBundle {
		ImageResource apartment();
	}

	/**
	 * The Cell used to render a {@link ContactInfo}.
	 */

	public static class ApartmentCell extends AbstractCell<TableMessage> {

		/**
		 * The html of the image used for contacts.
		 */
		private final String imageHtml;

		static Images images = GWT.create(Images.class);

		public ApartmentCell(ImageResource image) {
			this.imageHtml = AbstractImagePrototype.create(image).getHTML();
		}

		public String getImageHTML() {
			return imageHtml;
		}

		public ApartmentCell() {
			this(images.apartment());
		}

		@Override
		public void render(Context context, TableMessage value,
				SafeHtmlBuilder sb) {
			GWT.log("Made it inside render");
			// Value can be null, so do a null check..
			if (value == null) {
				return;
			}

			sb.appendHtmlConstant("<table>");

			// Add the contact image.
			sb.appendHtmlConstant("<tr><td rowspan='2'>");
			sb.appendHtmlConstant(imageHtml);
			sb.appendHtmlConstant("</td>");

			// Add the name and address.
			sb.appendHtmlConstant("<td style='font-size:95%;'>");
			sb.appendEscaped(value.getText(0));
			sb.appendHtmlConstant("</td></tr><tr><td>");
			sb.appendEscaped(value.getText(1));
			sb.appendHtmlConstant("</td></tr></table>");
			GWT.log("Done rendering: "  + value.getText(1));
		}
	}
	
	public String getSelectedBuilding()
	{
		if(buildingNames.getItemCount() == 0)
			return null;
		return buildingNames.getValue(buildingNames.getSelectedIndex());
	}
	
	public String getSelectedApartment()
	{
		TableMessage m = selectionModel.getSelectedObject();
		if(m == null) return null;
		return m.getMessageId();
	}
	
	public String getSelectedApartmentName()
	{
		TableMessage m = selectionModel.getSelectedObject();
		if(m == null) return null;
		return m.getText(0) + " " + m.getText(1);
	}	
	
	public void fireChangeNotification(Boolean apartmentChanged)
	{
		GWT.log("Building NOT: " + getSelectedBuilding());
		GWT.log("Apartment NOT: " + getSelectedApartment());
		if(apartmentChanged == null) return;
		if(apartmentChanged && getSelectedApartment() == null) return;
		if(getSelectedBuilding() == null) return;
		ValueChangeEvent.fire(this, apartmentChanged);
	}
	
	public void enableSelector(boolean enabled)
	{
		buildingNames.setEnabled(true);
		if(enabled)
			cellList.setSelectionModel(selectionModel);
		else
			cellList.setSelectionModel(noSelectionModel, whiteList);
	}

	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<Boolean> handler) {
		return addHandler(handler, ValueChangeEvent.getType());
	}

}
