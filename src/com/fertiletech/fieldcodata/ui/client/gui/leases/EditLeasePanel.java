package com.fertiletech.fieldcodata.ui.client.gui.leases;

import java.util.ArrayList;

import com.fertiletech.fieldcodata.ui.client.GUIConstants;
import com.fertiletech.fieldcodata.ui.client.gui.utils.CurrencyBox;
import com.fertiletech.fieldcodata.ui.client.gui.utils.MoneySignBox;
import com.fertiletech.fieldcodata.ui.shared.dto.DTOConstants;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessageHeader.TableMessageContent;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;

public class EditLeasePanel extends Composite {
	@UiField
	DateBox startOfTenancyPeriod;
	@UiField
	DateBox endOfTenancyPeriod;
	@UiField
	TextBox staffNameTextBox;

	@UiField
	TextBox docsFolder;
	@UiField
	Anchor docsFolderLink;
	@UiField
	CurrencyBox legalFeeTextBox;
	@UiField
	MoneySignBox legalCcyBox;
	@UiField
	DateBox legalFeeDateBox;
	@UiField
	CurrencyBox rentTextBox;
	@UiField
	MoneySignBox rentCcyBox;	
	@UiField
	DateBox rentDateBox;
	@UiField
	CurrencyBox agencyFeeTextBox;
	@UiField
	MoneySignBox agencyCcyBox;	
	@UiField
	DateBox agencyDateBox;
	@UiField
	RadioButton ownerOccupier;
	@UiField
	RadioButton other;

	private static EditLeasePanelUiBinder uiBinder = GWT
			.create(EditLeasePanelUiBinder.class);

	interface EditLeasePanelUiBinder extends UiBinder<Widget, EditLeasePanel> {
	}

	public EditLeasePanel() {
		initWidget(uiBinder.createAndBindUi(this));
		//date formatters
		startOfTenancyPeriod.setFormat(GUIConstants.DEFAULT_DATEBOX_FORMAT);
		endOfTenancyPeriod.setFormat(GUIConstants.DEFAULT_DATEBOX_FORMAT);
		agencyDateBox.setFormat(GUIConstants.DEFAULT_DATEBOX_FORMAT);
		rentDateBox.setFormat(GUIConstants.DEFAULT_DATEBOX_FORMAT);
		legalFeeDateBox.setFormat(GUIConstants.DEFAULT_DATEBOX_FORMAT);
		
		docsFolder.addMouseOutHandler(new MouseOutHandler() {

			@Override
			public void onMouseOut(MouseOutEvent event) {
				docsFolderLink.setHref(docsFolder.getText());
			}
		});
		
		docsFolder.addKeyUpHandler(new KeyUpHandler() {
			
			@Override
			public void onKeyUp(KeyUpEvent event) {
				docsFolderLink.setHref(docsFolder.getText());
			}
		});
		ownerOccupier.setValue(false);
		ownerOccupier.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
			
			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				Window.alert("You've marked this lease as being for an occupier. Uncheck this option if this was not intentional");
			}
		});
	}
	
	private boolean ownerFieldTypeBoolean = true;
	public void setOwnerFieldType(boolean isBoolean)
	{
		ownerFieldTypeBoolean = isBoolean;
	}
	
	public void setValues(TableMessage m)
	{
		legalFeeTextBox.setAmount(m.getNumber(DTOConstants.LEG_FEE_LG_IDX));
		rentTextBox.setAmount(m.getNumber(DTOConstants.LEG_FEE_RT_IDX));
		agencyFeeTextBox.setAmount(m.getNumber(DTOConstants.LEG_FEE_AG_IDX));
		legalFeeDateBox.setValue(m.getDate(DTOConstants.LEG_DT_LG_IDX)); 
		rentDateBox.setValue(m.getDate(DTOConstants.LEG_DT_RT_IDX));		
		agencyDateBox.setValue(m.getDate(DTOConstants.LEG_DT_AG_IDX));
		legalCcyBox.setVal(m.getText(DTOConstants.LEG_CCY_LG_IDX));
		agencyCcyBox.setVal(m.getText(DTOConstants.LEG_CCY_AG_IDX));
		rentCcyBox.setVal(m.getText(DTOConstants.LEG_CCY_RT_IDX));
		startOfTenancyPeriod.setValue(m.getDate(DTOConstants.LEG_DT_STR_IDX)); 
		endOfTenancyPeriod.setValue(m.getDate(DTOConstants.LEG_DT_END_IDX)); 
		docsFolder.setValue(m.getText(DTOConstants.LEG_CLNT_FLDR_IDX));
		staffNameTextBox.setValue(m.getText(DTOConstants.LEG_STAFF_IDX));
		docsFolderLink.setHref(m.getText(DTOConstants.LEG_CLNT_FLDR_IDX));
		String ownr = m.getText(DTOConstants.LEG_OWNR_IDX);
		if(ownerFieldTypeBoolean)
		{
			if(Boolean.valueOf(ownr))
				ownerOccupier.setValue(true);
			else
				other.setValue(true);
		}
		else
		{
			if(ownr.endsWith("[O]*") || ownr.endsWith("[O]"))
				ownerOccupier.setValue(true);
			else
				other.setValue(true);
		}
	}
	
	public void enablePanel(boolean enabled)
	{
		legalFeeTextBox.setEnabled(enabled);
		rentTextBox.setEnabled(enabled);
		agencyFeeTextBox.setEnabled(enabled);
		legalFeeDateBox.setEnabled(enabled); 
		rentDateBox.setEnabled(enabled);
		agencyCcyBox.enableBox(enabled);
		rentCcyBox.enableBox(enabled);
		legalCcyBox.enableBox(enabled);
		agencyDateBox.setEnabled(enabled); 		
		startOfTenancyPeriod.setEnabled(enabled); 
		endOfTenancyPeriod.setEnabled(enabled); 
		docsFolder.setEnabled(enabled);
		staffNameTextBox.setEnabled(enabled);		
		ownerOccupier.setEnabled(enabled);
		other.setEnabled(enabled);
	}
			
	public TableMessage getValues()
	{
		TableMessage m = new TableMessage(9,3,6);
		m.setNumber(DTOConstants.LEG_FEE_LG_IDX, legalFeeTextBox.getAmount());
		m.setNumber(DTOConstants.LEG_FEE_RT_IDX, rentTextBox.getAmount());
		m.setNumber(DTOConstants.LEG_FEE_AG_IDX, agencyFeeTextBox.getAmount());
		m.setDate(DTOConstants.LEG_DT_LG_IDX, legalFeeDateBox.getValue());
		m.setDate(DTOConstants.LEG_DT_RT_IDX, rentDateBox.getValue());
		m.setDate(DTOConstants.LEG_DT_AG_IDX, agencyDateBox.getValue());		
		m.setDate(DTOConstants.LEG_DT_STR_IDX, startOfTenancyPeriod.getValue());
		m.setDate(DTOConstants.LEG_DT_END_IDX, endOfTenancyPeriod.getValue());
		m.setText(DTOConstants.LEG_CLNT_FLDR_IDX, docsFolder.getValue());
		m.setText(DTOConstants.LEG_STAFF_IDX, staffNameTextBox.getValue());
		m.setText(DTOConstants.LEG_OWNR_IDX, String.valueOf(ownerOccupier.getValue()));
		m.setText(DTOConstants.LEG_CCY_AG_IDX, agencyCcyBox.getSign());
		m.setText(DTOConstants.LEG_CCY_LG_IDX, legalCcyBox.getSign());
		m.setText(DTOConstants.LEG_CCY_RT_IDX, rentCcyBox.getSign());
		return m;
	}
	
	public void clear()
	{
		legalFeeTextBox.setAmount(null);
		rentTextBox.setAmount(null);
		agencyFeeTextBox.setAmount(null);
		legalFeeDateBox.setValue(null); 
		rentDateBox.setValue(null);
		agencyDateBox.setValue(null); 		
		startOfTenancyPeriod.setValue(null); 
		endOfTenancyPeriod.setValue(null); 
		docsFolder.setValue(null);
		staffNameTextBox.setValue(null);		
		docsFolderLink.setHref("");
		ownerOccupier.setValue(false);
		legalCcyBox.reset();
		agencyCcyBox.reset();
		rentCcyBox.reset();
	}
	
	public static ArrayList<String> checkMandatoryDataIsGiven(TableMessage m)
	{
		ArrayList<String> errors = new ArrayList<String>();		
		if (isEmptyField(m, DTOConstants.LEG_FEE_RT_IDX, TableMessageContent.NUMBER))
			errors.add("No amount specified for rent");
		if (isEmptyField(m, DTOConstants.LEG_DT_RT_IDX, TableMessageContent.DATE))
			errors.add("No date specified for rent");
		if (isEmptyField(m, DTOConstants.LEG_FEE_LG_IDX, TableMessageContent.NUMBER))
			errors.add("No amount specified for legal fee");
		if (isEmptyField(m, DTOConstants.LEG_DT_LG_IDX, TableMessageContent.DATE))
			errors.add("No date specified for legal fee");
		if (isEmptyField(m, DTOConstants.LEG_FEE_AG_IDX, TableMessageContent.NUMBER))
			errors.add("No amount specified for agency fee amount");
		if (isEmptyField(m, DTOConstants.LEG_DT_AG_IDX, TableMessageContent.DATE))
			errors.add("No date specified for agency fee");
		if (isEmptyField(m, DTOConstants.LEG_CLNT_FLDR_IDX, TableMessageContent.TEXT) || !m.getText(DTOConstants.LEG_CLNT_FLDR_IDX).toLowerCase().startsWith("https://drive.google.com"))
			errors.add("No docs folder specified. Ensure you've uploaded relevant documents to a google drive" +
					" folder created for this client/prospect. At the bare minimum it should contain scanned (or soft) copies of final offer letter, draft lease and signed lease agreements");
		if (isEmptyField(m, DTOConstants.LEG_DT_END_IDX, TableMessageContent.DATE))
			errors.add("No date specified for start of lease");
		if (isEmptyField(m, DTOConstants.LEG_DT_STR_IDX, TableMessageContent.DATE))
			errors.add("No date specified for end of lease");
			return errors;
	}
	
	private static boolean isEmptyField(TableMessage m, int idx, TableMessageContent c)
	{
		switch(c)
		{
		case DATE:
			return m.getDate(idx) == null;
		case NUMBER:
			return m.getNumber(idx) == null;
		case TEXT:
			return m.getText(idx) == null || m.getText(idx).trim().length()==0;
		default:
			return true;
		}
	}	
}
