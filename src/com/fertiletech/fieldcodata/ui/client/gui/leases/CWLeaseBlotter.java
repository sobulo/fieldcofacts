package com.fertiletech.fieldcodata.ui.client.gui.leases;

import com.fertiletech.fieldcodata.ui.client.ContentWidget;
import com.fertiletech.fieldcodata.ui.client.gui.HelpPageGenerator;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class CWLeaseBlotter extends ContentWidget{

	public CWLeaseBlotter() {
		super("View Leases", "Use this panel to search through lease objects in the database. " +
				"Note that only lease information for buildings under management is captured here. " +
				"Use view prospects for all other residents");
	}

	@Override
	public Widget onInitialize() {
		return new LeaseBlotter();
	}
	
	@Override
	public boolean hasScrollableContent()
	{
		return false;
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
	    GWT.runAsync(CWLeaseBlotter.class, new RunAsyncCallback() {

	        public void onFailure(Throwable caught) {
	          callback.onFailure(caught);
	        }

	        public void onSuccess() {
	          callback.onSuccess(onInitialize());
	        }
	      });
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_TENANTS_URL;
	}

}
