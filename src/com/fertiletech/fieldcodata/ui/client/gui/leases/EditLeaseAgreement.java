package com.fertiletech.fieldcodata.ui.client.gui.leases;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.fertiletech.fieldcodata.ui.client.ApartmentAndCommentBox;
import com.fertiletech.fieldcodata.ui.client.gui.LocalDataProvider;
import com.fertiletech.fieldcodata.ui.client.gui.accounts.TenantSelectionList;
import com.fertiletech.fieldcodata.ui.client.gui.utils.MultipleMessageDialog;
import com.fertiletech.fieldcodata.ui.client.gui.utils.ShowcaseTable;
import com.fertiletech.fieldcodata.ui.client.gui.utils.UserCommentPopup;
import com.fertiletech.fieldcodata.ui.shared.dto.DTOConstants;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.ResizeLayoutPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.SplitLayoutPanel;
import com.google.gwt.user.client.ui.Widget;

public class EditLeaseAgreement extends Composite {

	@UiField
	SplitLayoutPanel contentPanel;
	@UiField
	ScrollPanel editContainer;
	
	@UiField
	Button newLease;
	@UiField
	Button archiveTenant;
	@UiField
	Button removeLease;
	
	@UiField
	FlowPanel buttonsContainer;
	
	@UiField
	TenantSelectionList tenantSelector;
	@UiField
	EditLeasePanel leaseInfo;
	@UiField(provided=true)
	ShowcaseTable display;
	
	@UiField
	Button cancel;
	@UiField
	Button save;
	
	public final static int NEW_POS = 400;
	public final static int MIN_COMMENT_LENGTH = 20;
	static MultipleMessageDialog errorBox = new MultipleMessageDialog("Error");
	PopupPanel pop = new PopupPanel();
	HTML popMessage = new HTML();
	UserCommentPopup saveLeasePopup, removeLeasePopup, archiveTenantPopup;
	ApartmentAndCommentBox changeApartmentPopup;
	boolean forceRefresh=false;
	
 	protected AsyncCallback<List<TableMessage>> tenantLeasesDisplayCallback = new AsyncCallback<List<TableMessage>>() {

		@Override
		public void onFailure(Throwable caught) {
			Window.alert("Request for resident leases failed " + caught.getMessage());
		}

		@Override
		public void onSuccess(List<TableMessage> result) {
			cachedTenantLeases.put(result.get(0).getMessageId(), result);
			showTable(result);
		}
	};
	
	protected AsyncCallback<TableMessage> saveLeaseCallback = new AsyncCallback<TableMessage>() {

		@Override
		public void onFailure(Throwable caught) {
			pop.hide();
			Window.alert("Error saving: \n" + caught.getMessage());
			save.setEnabled(true);
			cancel.setEnabled(true);
			enableTopButtons(true);
		}

		@Override
		public void onSuccess(TableMessage result) {
			updateOwnershipFlag(result);
			saveLeasePopup.clear();
			display.getDataList().add(result);
			display.refreshAll();
			reduceEditPanel();
			Window.alert("Saved succesfully");
		}
	};
	
	protected AsyncCallback<String> removeLeaseCallback = new AsyncCallback<String>() {

		@Override
		public void onFailure(Throwable caught) {
			pop.hide();
			Window.alert("Unable to delete lease. Error is: " + caught.getMessage());
			enableTopButtons(true);
		}

		@Override
		public void onSuccess(String result) {
			TableMessage lease = display.getSelectedRow();
			updateOwnershipFlag(lease);
			removeLeasePopup.clear();
			List<TableMessage> rows = display.getDataList();
			GWT.log("Row count=" + rows.size());
			boolean found = false;
			for(int i = 0; i < rows.size(); i++)
			{
				if(rows.get(i).getMessageId().equals(result))
				{
					rows.remove(i);
					found = true;
					break;
				}
			}
			pop.hide();
			GWT.log("Row count2=" + rows.size());
			display.refreshAll();
			GWT.log("Row count3=" + rows.size());;
			GWT.log("Row count4=" + display.getDataList().size());
			Window.alert("Removed lease!");
			//sanity check
			if(!found)
				Window.alert("However looks like your browser needs a refresh as you switched tenants" +
						" during removal. Please notify IT as you shouldn't be able to switch tenants while removing a lease");
			enableTopButtons(true);
		}
	};
	protected AsyncCallback<TableMessage> archiveTenantCallback = new AsyncCallback<TableMessage>() {

		@Override
		public void onFailure(Throwable caught) {
			pop.hide();
			Window.alert("Archiving failed: " + caught.getMessage());
			enableTopButtons(true);
		}

		@Override
		public void onSuccess(TableMessage result) {

			archiveTenantPopup.clear();
			pop.hide();
			enableTopButtons(true);
			Window.alert("Archiving succeeded. A browser refresh will now be triggered. Advice other users to refresh their browsers as well " +
					"in order to reflect new arhcive state of " + result.getText(DTOConstants.TNT_FNAME_IDX) + 
					" " + result.getText(DTOConstants.TNT_LNAME_IDX) + " in their resident selectors");
			Window.Location.reload();
		}
	};
	protected AsyncCallback<TableMessage> changeApartmentCallback = new AsyncCallback<TableMessage>() {

		@Override
		public void onFailure(Throwable caught) {
			pop.hide();
			Window.alert("Restoration failed: " + caught.getMessage());
			enableTopButtons(true);
		}

		@Override
		public void onSuccess(TableMessage result) {
			TableMessage tenant = tenantSelector.getSelectedTenant();
			Boolean status = Boolean.valueOf(tenant.getText(DTOConstants.TNT_IS_ARCH_IDX));
			if(!status)
				Window.Location.reload(); //sanity check, shouldn't get here
			tenant.setText(DTOConstants.TNT_IS_ARCH_IDX, String.valueOf(false));
			changeApartmentPopup.clear();
			pop.hide();
			enableTopButtons(true);
		}		
	};
	
	final static HashMap<String, List<TableMessage>> cachedTenantLeases = new HashMap<String, List<TableMessage>>();

	private static EditLeaseAgreementUiBinder uiBinder = GWT
			.create(EditLeaseAgreementUiBinder.class);

	interface EditLeaseAgreementUiBinder extends
			UiBinder<Widget, EditLeaseAgreement> {
	}

	public EditLeaseAgreement() {
		display = new ShowcaseTable(false, false, false);
		display.addValueChangeHandler(new ValueChangeHandler<TableMessage>() {

			@Override
			public void onValueChange(ValueChangeEvent<TableMessage> event) {
				if(event.getValue() == null)
				{
					reduceEditPanel();
				}
				else
				{
					showEditPanel(false);
					cancel.setEnabled(true);
					leaseInfo.setValues(event.getValue());
				}
			}
		});
		pop.setSize("150px", "100px");
		popMessage.setSize("145px", "90px");
		pop.add(popMessage);
		pop.setAnimationEnabled(true);
		pop.setGlassEnabled(true);
		pop.setModal(true);
		initWidget(uiBinder.createAndBindUi(this));
		//date formats
		tenantSelector.ignoreArchiveSettings();
		//animation effects for splitters
		contentPanel.setWidgetMinSize(editContainer, 70);
		contentPanel.setWidgetMinSize(tenantSelector, 70);
		contentPanel.setWidgetMinSize(buttonsContainer, 25);
		contentPanel.setWidgetSnapClosedSize(editContainer, 70);
		contentPanel.setWidgetSnapClosedSize(tenantSelector, 70);
		contentPanel.setWidgetSnapClosedSize(buttonsContainer, 25);
		
		display.setEmptyWidget(new HTML("<font color='red'>No lease information exists on file for this resident/apartment combination</font>"));
		tenantSelector.addValueChangeHandler(new ValueChangeHandler<TableMessage>() {
			
			@Override
			public void onValueChange(ValueChangeEvent<TableMessage> event) {
				TableMessage tenant = event.getValue();
				List<TableMessage> leases = cachedTenantLeases.get(tenant.getMessageId());
				if(leases == null)
				{
					enableTopButtons(false);
					LocalDataProvider.TENANT_SERVICE.getLeases(tenant.getMessageId(), tenantLeasesDisplayCallback );
				}
				else
				{
					reduceEditPanel();
					showTable(leases);
				}
			}
		});
		cancel.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				reduceEditPanel();
			}
		});
		newLease.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				showEditPanel(true);
			}
		});
		saveLeasePopup = new UserCommentPopup("Confirm Save Lease");
		saveLeasePopup.setMinimumCommentLength(MIN_COMMENT_LENGTH);
		saveLeasePopup.setClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				saveLeasePopup.hide();
				enableTopButtons(false);
				save.setEnabled(false);
				cancel.setEnabled(false);
				popMessage.setHTML("Saving, please wait...");
				pop.center();
				TableMessage m = leaseInfo.getValues();
				m.setMessageId(tenantSelector.getSelectedTenant().getMessageId());
				LocalDataProvider.TENANT_SERVICE.saveLease(m, saveLeasePopup.getComments(), saveLeaseCallback );			
			}
		});		
		save.addClickHandler(new ClickHandler() {						
			@Override
			public void onClick(ClickEvent event) {
				ArrayList<String> errors = EditLeasePanel.checkMandatoryDataIsGiven(leaseInfo.getValues()); 	
				TableMessage tenant = tenantSelector.getSelectedTenant();
				if(tenant == null)
					errors.add("Select a resident");			
				if(errors.size() > 0)
				{
					errorBox.show("Please fix issues below", errors);
					return;
				}
				saveLeasePopup.show("Please add comments explaining why lease being added, e.g. lease renewal");
			}
		});

		removeLeasePopup = new UserCommentPopup("Confirm Lease Removal");
		removeLeasePopup.setMinimumCommentLength(MIN_COMMENT_LENGTH);
		removeLeasePopup.setClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				removeLeasePopup.hide();
				enableTopButtons(false);
				popMessage.setHTML("Removing lease, please wait ...");
				pop.center();
				LocalDataProvider.TENANT_SERVICE.removeLease(display.getSelectedRow().getMessageId(), 
						removeLeasePopup.getComments(), removeLeaseCallback );

			}
		});
		removeLease.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				TableMessage selected = display.getSelectedRow();
				if(selected == null)
				{
					Window.alert("Please select a lease to remove");
					return;
				}
				removeLeasePopup.show("Please add comments explaining why this lease is being removed, e.g. to fix a mistake");
			}
		});
		
		changeApartmentPopup = new ApartmentAndCommentBox("Confirm restoration of Resident");
		changeApartmentPopup.setMinimumCommentLength(MIN_COMMENT_LENGTH);
		changeApartmentPopup.setClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				changeApartmentPopup.hide();
				popMessage.setHTML("Restoring resident, please wait ...");
				pop.center();
				enableTopButtons(false);
				LocalDataProvider.TENANT_SERVICE.changeApartmentInfo(tenantSelector.getSelectedTenant().getMessageId(),
						changeApartmentPopup.getSelectedApartment(), changeApartmentPopup.getComments(), changeApartmentCallback );
				
			}
		});
		archiveTenantPopup = new UserCommentPopup("Confirm Resident Archiving");
		archiveTenantPopup.setMinimumCommentLength(MIN_COMMENT_LENGTH);
		archiveTenantPopup.setClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				archiveTenantPopup.hide();	
				popMessage.setHTML("Archiving resident contact, please wait ...");
				pop.center();
				enableTopButtons(false);
				LocalDataProvider.TENANT_SERVICE.archiveTenant(tenantSelector.getSelectedTenant().getMessageId(), archiveTenantPopup.getComments(),
						archiveTenantCallback );
			}
		});
		archiveTenant.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				TableMessage m = tenantSelector.getSelectedTenant();
				if(m == null)
				{
					Window.alert("Please select a resident first");
					return;
				}
				
				if(isFlagSet(m, DTOConstants.TNT_IS_ARCH_IDX))
				{
					changeApartmentPopup.show("Select a vacant apartment to restore resident into then add comments for audit trail");
				}
				else
				{
					archiveTenantPopup.show("Please add comments explaining why this resident is being archived," +
							" e.g. lease expired and resident has moved out");
				}
			}
		});
		reduceEditPanel();
	}
	
	private boolean isFlagSet(TableMessage m, int idx)
	{
		if(m == null) return false;
		return Boolean.valueOf(m.getText(idx));
	}
	
	public void updateOwnershipFlag(TableMessage lease)
	{
		TableMessage tenant = tenantSelector.getSelectedTenant();			
		tenant.setText(DTOConstants.TNT_IS_OWNR_IDX, lease.getText(DTOConstants.LEG_OWNR_IDX));
		tenantSelector.refresh();
	}
	
	public void updateArchiveFlag(TableMessage result)
	{
		TableMessage tenant = tenantSelector.getSelectedTenant();			
		if(result.getMessageId().equals(result.getMessageId()))
		{
			tenant.setText(DTOConstants.TNT_IS_ARCH_IDX, result.getText(DTOConstants.TNT_IS_ARCH_IDX));
			tenantSelector.refresh();
		}
		else	
			Window.Location.reload(); //sanity check, shouldn't get here					
	}
	
	public void reduceEditPanel()
	{
		save.setEnabled(false);
		cancel.setEnabled(false);
		leaseInfo.clear();
		leaseInfo.enablePanel(false);
		contentPanel.setWidgetSize(editContainer, 0);
		contentPanel.animate(800);
		pop.hide();
		enableTopButtons(true);
	}
	
	public void showTable(List<TableMessage> newTableContent)
	{
		display.showTable(newTableContent);
		enableTopButtons(true);
	}
	
	public void showEditPanel(boolean enable)
	{
		leaseInfo.clear();
		leaseInfo.enablePanel(enable);
		contentPanel.setWidgetSize(editContainer, NEW_POS);
		contentPanel.animate(600);
		save.setEnabled(enable);
		cancel.setEnabled(enable);		
	}
	
	public void enableTopButtons(boolean enabled)
	{
		boolean enableLeaseRemoval = true;
		boolean enableLeaseAdd = true;
		String archiveText = "Archive Resident";
		TableMessage tenant = tenantSelector.getSelectedTenant();
		if(tenant == null) return;
		if(isFlagSet(tenant, DTOConstants.TNT_IS_ARCH_IDX))
		{
			archiveText = "Restore Resident";
			enableLeaseRemoval = false;
			enableLeaseAdd = false;
		}
		if(isFlagSet(tenant, DTOConstants.TNT_IS_OWNR_IDX))
		{
			enableLeaseAdd = false;
		}
		archiveTenant.setText(archiveText);
		newLease.setEnabled(enabled && enableLeaseAdd);
		removeLease.setEnabled(enabled && enableLeaseRemoval && display.getDataList().size() > 0);
		archiveTenant.setEnabled(enabled);
	}
}


