package com.fertiletech.fieldcodata.ui.client.gui.leases;

import java.util.Date;
import java.util.List;

import com.fertiletech.fieldcodata.ui.client.gui.LocalDataProvider;
import com.fertiletech.fieldcodata.ui.client.gui.utils.ShowcaseTable;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;

public class LeaseBlotter extends Composite {
	
	@UiField
	DateBox start;
	
	@UiField
	DateBox end;
	
	@UiField
	Button search;
	
	@UiField
	ShowcaseTable display;
	
	protected AsyncCallback<List<TableMessage>> searchCallback = new AsyncCallback<List<TableMessage>>() {

		@Override
		public void onFailure(Throwable caught) {
			Window.alert("Error fetching info");
		}

		@Override
		public void onSuccess(List<TableMessage> result) {
			search.setEnabled(true);
			display.showTable(result);
		}
	};

	private static LeaseBlotterUiBinder uiBinder = GWT
			.create(LeaseBlotterUiBinder.class);

	interface LeaseBlotterUiBinder extends UiBinder<Widget, LeaseBlotter> {
	}

	public LeaseBlotter() {
		initWidget(uiBinder.createAndBindUi(this));
		search.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				Date s = start.getValue();
				Date e = end.getValue();
				search.setEnabled(false);
				LocalDataProvider.TENANT_SERVICE.getLeases(s, e, searchCallback );
			}
		});
		display.addValueChangeHandler(new ValueChangeHandler<TableMessage>() {
			
			PopupPanel p = new PopupPanel(true);
			EditLeasePanel editor = new EditLeasePanel();

			{
				editor.setOwnerFieldType(false);
				p.setGlassEnabled(true);
				p.add(editor);
			}
			
			@Override
			public void onValueChange(ValueChangeEvent<TableMessage> event) {
				GWT.log("User selection detected");
				if(event.getValue() == null)
				{
					p.hide();
					return;
				}
				GWT.log("Setting up popup display");
				TableMessage selectedLease = event.getValue();
				editor.setValues(selectedLease);
				editor.enablePanel(false);
				p.show();
				GWT.log("Popup has been displayed");
			}
		});
	}

}
