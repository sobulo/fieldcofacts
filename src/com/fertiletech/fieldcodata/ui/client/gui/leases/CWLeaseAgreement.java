package com.fertiletech.fieldcodata.ui.client.gui.leases;

import com.fertiletech.fieldcodata.ui.client.ContentWidget;
import com.fertiletech.fieldcodata.ui.client.gui.HelpPageGenerator;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class CWLeaseAgreement extends ContentWidget{

	public CWLeaseAgreement() {
		super("Modify Lease", "Use this module to modify lease information. In particular you may add or remove a lease, " +
				"archive residents that have expired leases as well as restoring such resident information" +
				". For residents switching apartments when their lease expires, you must first archive the resident then restore the individual into a different apartment");
	}

	@Override
	public Widget onInitialize() {
		return new EditLeaseAgreement();
	}
	
	@Override
	public boolean hasScrollableContent()
	{
		return false;
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
	    GWT.runAsync(CWLeaseAgreement.class, new RunAsyncCallback() {

	        public void onFailure(Throwable caught) {
	          callback.onFailure(caught);
	        }

	        public void onSuccess() {
	          callback.onSuccess(onInitialize());
	        }
	      });		
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_TENANTS_URL;
	}

}
