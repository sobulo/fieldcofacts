/*
 * Copyright 2010 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.fertiletech.fieldcodata.ui.client.gui.tenants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.fertiletech.fieldcodata.ui.client.ContentWidget;
import com.fertiletech.fieldcodata.ui.client.gui.HelpPageGenerator;
import com.fertiletech.fieldcodata.ui.client.gui.LocalDataProvider;
import com.fertiletech.fieldcodata.ui.client.gui.utils.CommentsPanel;
import com.fertiletech.fieldcodata.ui.client.gui.utils.MultipleMessageDialog;
import com.fertiletech.fieldcodata.ui.client.gui.utils.RangeLabelPager;
import com.fertiletech.fieldcodata.ui.client.gui.utils.ShowMorePagerPanel;
import com.fertiletech.fieldcodata.ui.client.gui.utils.SimpleDialog;
import com.fertiletech.fieldcodata.ui.client.gui.utils.TableMessageKeyProvider;
import com.fertiletech.fieldcodata.ui.client.gui.utils.UserPanel;
import com.fertiletech.fieldcodata.ui.shared.UtilConstants;
import com.fertiletech.fieldcodata.ui.shared.dto.DTOConstants;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessageHeader;
import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.BeforeSelectionEvent;
import com.google.gwt.event.logical.shared.BeforeSelectionHandler;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.cellview.client.HasKeyboardPagingPolicy.KeyboardPagingPolicy;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AbstractImagePrototype;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TabLayoutPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;
import com.sun.corba.se.pept.transport.ContactInfo;

public class CwCellList extends ContentWidget implements ChangeHandler {

	@UiField
	TabLayoutPanel container;
	
	@UiField
	CommentsPanel generalComments;
	
	private boolean commentsLoaded;
	final static int COMMENT_TAB_IDX = 1;
	/**
	 * The UiBinder interface used by this example.
	 */
	interface Binder extends UiBinder<Widget, CwCellList> {
	}


	/**
	 * The images used for this example.
	 */
	static interface Images extends ClientBundle {
		ImageResource contact();
	}

	LocalDataProvider pc = new LocalDataProvider();
	/**
	 * The Cell used to render a {@link ContactInfo}.
	 */

	public static class ContactCell extends AbstractCell<TableMessage> {

		/**
		 * The html of the image used for contacts.
		 */
		private final String imageHtml;

		static Images images = GWT.create(Images.class);
		
		public ContactCell(ImageResource image) {
			this.imageHtml = AbstractImagePrototype.create(image).getHTML();
		}
		
		public String getImageHTML(){ return imageHtml; }
		
		public ContactCell()
		{
			this(images.contact());
		}

		@Override
		public void render(Context context, TableMessage value,
				SafeHtmlBuilder sb) {
			// Value can be null, so do a null check..
			if (value == null) {
				return;
			}

			sb.appendHtmlConstant("<table>");

			// Add the contact image.
			sb.appendHtmlConstant("<tr><td rowspan='3'>");
			sb.appendHtmlConstant(imageHtml);
			sb.appendHtmlConstant("</td>");

			String firstName = value.getText(DTOConstants.TNT_FNAME_IDX);
			firstName = firstName.length() == 0 ? "" : firstName + " ";
			String fullName = firstName
					+ value.getText(DTOConstants.TNT_LNAME_IDX);
			// Add the name and address.
			sb.appendHtmlConstant("<td style='font-size:95%;'>");
			sb.appendEscaped(fullName);
			sb.appendHtmlConstant("</td></tr><tr><td>");
			sb.appendEscaped(value.getText(DTOConstants.TNT_BUILDING_IDX) + " "
					+ value.getText(DTOConstants.TNT_APT_IDX));
			sb.appendHtmlConstant("</td></tr></table>");
		}
	}

	/**
	 * The contact form used to update contacts.
	 */

	@UiField
	UserPanel bioForm;

	/**
	 * The button used to generate more contacts.
	 */

	@UiField
	Button updateButton;

	/**
	 * The pager used to change the range of data.
	 */

	@UiField
	ShowMorePagerPanel pagerPanel;

	/**
	 * The pager used to display the current range.
	 */

	//@UiField
	//RangeLabelPager rangeLabelPager;

	@UiField
	ListBox buildingNames;
	
	@UiField
	Label buildingHeader;
	
	private SimpleDialog infoBox;
	private MultipleMessageDialog errorBox;

	/**
	 * The CellList.
	 */

	private CellList<TableMessage> cellList;
	
    // Create an asynchronous callback to handle the result.
    private final AsyncCallback<HashMap<String, String>> buildingNamesCallback = new AsyncCallback<HashMap<String, String>>() {

        @Override
        public void onSuccess(HashMap<String, String> result) {
        	populateBuildingNames(result);
        	buildingNames.setEnabled(true);
        	pc.populateBuildingNamesMap(result);
        }

        @Override
        public void onFailure(Throwable caught) {
        	errorBox.show("Unable to retrieve building names. Try refreshing your browser. " +
        			"Contact info@fertiletech.com if problems persist. <p> Error msg: <b>"+caught.getMessage() + "</b></p>");
        }
    };
    
    // Create an asynchronous callback to handle the result.
    private final AsyncCallback<List<TableMessage>> tenantListCallback = new AsyncCallback<List<TableMessage>>() {

        @Override
        public void onSuccess(List<TableMessage> result) {
        	GWT.log("Received result of size: " + result.size());
        	TableMessageHeader header = (TableMessageHeader) result.remove(0);
        	String buildingID = header.getMessageId() + false;
        	List<TableMessage> tenantList = pc.getTenantInfoList(buildingID);
        	tenantList.addAll(result);
        	buildingHeader.setText(header.getCaption());
    		//add display to the new data source
    		pc.addViewToTenantListDataProvider(cellList, buildingID);
    		buildingNames.setEnabled(true);
        }

        @Override
        public void onFailure(Throwable caught) {
        	errorBox.show("Unable to retrieve tenant names. Try refreshing your browser. " +
        			"Contact info@fertiletech.com if problems persist. <p> Error msg: <b>"+caught.getMessage() + "</b></p>");
        }
    };      

	final AsyncCallback<TableMessage> saveCallback = new AsyncCallback<TableMessage>() {

		@Override
		public void onSuccess(TableMessage result) {
			commentsLoaded = false;
			bioForm.setUserID(result.getMessageId());
			infoBox.show("Updated tenant info for "
					+ result.getText(DTOConstants.TNT_LNAME_IDX)
					+ " successfully");
			String id = buildingNames.getValue(buildingNames.getSelectedIndex()) + false;
			LocalDataProvider.refreshSingleData(pc.getTenantInfoList(id), result);
			updateButton.setEnabled(true);
			buildingNames.setEnabled(true);
			
		}

		@Override
		public void onFailure(Throwable caught) {
			errorBox.show("Unable to save values. Error was: "
					+ caught.getMessage());
			updateButton.setEnabled(true);
			buildingNames.setEnabled(true);
		}
	};    
    
	@Override
	public boolean hasScrollableContent()
	{
		return false;
	}
	
	/**
	 * Constructor.
	 * 
	 * @param constants
	 *            the constants
	 */
	public CwCellList() {
		super("Edit Resident", "Edit bio information for residents. Comments tab also captures audit trail of activities on resident data");
	}

	/**
	 * Initialize this example.
	 */

	@Override
	public Widget onInitialize() {

		// Create a CellList.
		ContactCell contactCell = new ContactCell();

		// Set a key provider that provides a unique key for each contact. If
		// key is
		// used to identify contacts when fields (such as the name and address)
		// change.
		cellList = new CellList<TableMessage>(contactCell,
				TableMessageKeyProvider.KEY_PROVIDER);

		cellList.setKeyboardPagingPolicy(KeyboardPagingPolicy.INCREASE_RANGE);
		cellList.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.BOUND_TO_SELECTION);

		// Add a selection model so we can select cells.
		final SingleSelectionModel<TableMessage> selectionModel = new SingleSelectionModel<TableMessage>(
				TableMessageKeyProvider.KEY_PROVIDER);
		cellList.setSelectionModel(selectionModel);
		selectionModel
				.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
					public void onSelectionChange(SelectionChangeEvent event) {
						commentsLoaded = false;
						bioForm.setContactValues(selectionModel
								.getSelectedObject());
					}
				});

		errorBox = new MultipleMessageDialog("<font color='red'>Error Occured</font>", true);
		infoBox = new SimpleDialog("<font color='green'>INFO</font>", true);
		// Create the UiBinder.
		Binder uiBinder = GWT.create(Binder.class);
		Widget widget = uiBinder.createAndBindUi(this);

		container.addBeforeSelectionHandler(new BeforeSelectionHandler<Integer>() {
			@Override
			public void onBeforeSelection(
					BeforeSelectionEvent<Integer> event) 
			{
				if (event.getItem() > 0 && bioForm.getUserID() == null) {
					infoBox.show("You must first select a resident before you can view comments");
					event.cancel();
					return;
				}				
				else if(event.getItem() == COMMENT_TAB_IDX && !commentsLoaded)
				{
					generalComments.setCommentID(bioForm.getUserID());
					commentsLoaded = true;
				}
			}
		});		
		// server side async calls
		buildingNames.addChangeHandler(this);

		// Add the CellList to the data provider in the database.

		// Set the cellList as the display of the pagers. This example has two
		// pagers. pagerPanel is a scrollable pager that extends the range when
		// the
		// user scrolls to the bottom. rangeLabelPager is a pager that displays
		// the
		// current range, but does not have any controls to change the range.
		pagerPanel.setDisplay(cellList);
		//rangeLabelPager.setDisplay(cellList);

		// Handle events from the generate button.
		updateButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				ArrayList<String> errors = bioForm.validateFields();
				if (bioForm.getUserID() == null)
					errors.add("You must select a tenant before you can save");
				if (errors.size() > 0) {
					errorBox.show("Fix the errors shown below to save data",
							errors);
					return;
				}
				buildingNames.setEnabled(false);
				updateButton.setEnabled(false);
				LocalDataProvider.TENANT_SERVICE.updateContact(
						bioForm.getUserID(), bioForm.getFirstName(),
						bioForm.getLastName(), bioForm.getDateOfBirth(), bioForm.getEmail(),
						bioForm.getPhone(), bioForm.getOtherNumbers(),
						bioForm.getOtherMails(), bioForm.getCompany(),
						bioForm.getAddress(), bioForm.isMale(),
						bioForm.getSalutation(), null, saveCallback);


			}
		});

		
		if(pc.getBuildingNamesSize() == 0)
		{
			buildingHeader.setText("Loading ...");
			LocalDataProvider.TENANT_SERVICE.getBuildingNames(buildingNamesCallback);
		}
		else
		{
			pc.populateListBoxWithBuildingNames(buildingNames);
			switchBuildingDisplay();
		}
		return widget;
	}

  private void populateBuildingNames(HashMap<String, String> nameMap)
  {
	  for(String id : nameMap.keySet())
		  buildingNames.addItem(nameMap.get(id), id);
	  buildingNames.setSelectedIndex(0);
	  switchBuildingDisplay();
  }


	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
		GWT.runAsync(CwCellList.class, new RunAsyncCallback() {

			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}

			public void onSuccess() {
				callback.onSuccess(onInitialize());
			}
		});
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_TENANTS_URL;
	}

	@Override
	public void onChange(ChangeEvent event) {
		switchBuildingDisplay();
	}

	private void switchBuildingDisplay() {
		buildingNames.setEnabled(false);
		//remove our display from its current data source
		pc.removeDisplayFromTenantMap(cellList);
		
		//fetch list for newly created building
		GWT.log("Building Index" + buildingNames.getSelectedIndex() + " count: " + buildingNames.getItemCount());
		String buildingID = buildingNames.getValue(buildingNames.getSelectedIndex());
		String buildingName = buildingNames.getItemText(buildingNames.getSelectedIndex());
		
		List<TableMessage> tenantInfoList = pc.getTenantInfoList(buildingID + false);
		if(tenantInfoList.size() == 0) //nothing found locally, check server to see if data is available
		{
			GWT.log("Going to server to fetch tenant list for: " + buildingName);
			buildingHeader.setText("Loading ...");
			LocalDataProvider.TENANT_SERVICE.getBuildingTenants(buildingID, false, tenantListCallback);
		}
		else
		{
			buildingHeader.setText(buildingName + "(" + tenantInfoList.size() + " tenants)");
			buildingNames.setEnabled(true);
			//add display to the new data source
			pc.addViewToTenantListDataProvider(cellList, buildingID + false);
		}

	}
	
}
