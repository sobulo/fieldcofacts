package com.fertiletech.fieldcodata.ui.client.gui.tenants;

import java.util.List;

import com.fertiletech.fieldcodata.ui.client.GUIConstants;
import com.fertiletech.fieldcodata.ui.client.gui.LocalDataProvider;
import com.fertiletech.fieldcodata.ui.client.gui.utils.ArchiveButton;
import com.fertiletech.fieldcodata.ui.client.gui.utils.BuildingSelector;
import com.fertiletech.fieldcodata.ui.client.gui.utils.ShowcaseTable;
import com.fertiletech.fieldcodata.ui.client.gui.utils.SimpleDialog;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

public class ViewTenants extends Composite implements ValueChangeHandler<String>{

	@UiField
	BuildingSelector buildingSelector;
	
	@UiField
	ShowcaseTable display;
	
	@UiField
	ArchiveButton archive;
	
	private SimpleDialog infoBox;
	
	private static ViewTenantsUiBinder uiBinder = GWT
			.create(ViewTenantsUiBinder.class);

	interface ViewTenantsUiBinder extends UiBinder<Widget, ViewTenants> {
	}

	public ViewTenants() {
		initWidget(uiBinder.createAndBindUi(this));
		GUIConstants.addContactInfoPopup(display);
		infoBox = new SimpleDialog("INFO");
		buildingSelector.addValueChangeHandler(this);
		archive.addValueChangeHandler(this);	
	}
	
	private AsyncCallback<List<TableMessage>> callback = new AsyncCallback<List<TableMessage>>() {

		@Override
		public void onFailure(Throwable caught) {
			infoBox.show("Unable to display tenant list<br/>" + caught.getMessage());
			buildingSelector.enableSelector(true);
			archive.enableBox(true);
		}

		@Override
		public void onSuccess(List<TableMessage> result) {
			display.showTable(result);
			buildingSelector.enableSelector(true);
			archive.enableBox(true);
		}
	};

	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		buildingSelector.enableSelector(false);
		archive.enableBox(false);
		LocalDataProvider.TENANT_SERVICE.getBuildingTenants(buildingSelector.getSelectedBuilding(), archive.isArchived(), callback);
	}	
}
