/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fertiletech.fieldcodata.ui.client.gui.tenants;



import java.util.HashMap;

import com.fertiletech.fieldcodata.ui.client.ContentWidget;
import com.fertiletech.fieldcodata.ui.client.gui.HelpPageGenerator;
import com.fertiletech.fieldcodata.ui.client.gui.LocalDataProvider;
import com.fertiletech.fieldcodata.ui.client.gui.utils.SimpleDialog;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteEvent;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Hidden;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
/**
 *
 * @author Administrator
 */
public class BulkTenantAndApartmentUploadPanel extends ContentWidget implements 
        ClickHandler, FormPanel.SubmitCompleteHandler
{
    
    private final static String LOAD_PARAM_NAME = "loadData";
    private final static String GOOD_DATA_PREFIX = "<i></i>";
    public final static String INIT_DISPLAY = "Please choose a spreadsheet " +
            "and hit submit to upload, ensure spreadsheet has the following " +
            "columns only<b>Name, Email 1, Phone 1, Email 2, Phone 2, Company, Apartment</b>";
    
    
    @UiField FileUpload selectFile;
    @UiField Button submitFile;
    @UiField FormPanel tenantUploadForm;
    @UiField HTML formResponse;
    @UiField SimplePanel topLevelPanel;
    @UiField ListBox buildingList;

    FormPanel confirmationForm;
    Button confirmData;
    Button resetData;
    //HTML confirmDisplay;
    private SimpleDialog errorBox;
    LocalDataProvider pc = new LocalDataProvider();
    private static BulkTenantAndApartmentUploadPanelUiBinder uiBinder = GWT.create(BulkTenantAndApartmentUploadPanelUiBinder.class);

    interface BulkTenantAndApartmentUploadPanelUiBinder extends UiBinder<Widget, BulkTenantAndApartmentUploadPanel> {
    }
    
    // Create an asynchronous callback to handle the result.
    final AsyncCallback<HashMap<String, String>> buildingNamesCallback = new AsyncCallback<HashMap<String, String>>() {

        @Override
        public void onSuccess(HashMap<String, String> result) {
        	submitFile.setEnabled(true);
        	pc.populateBuildingNamesMap(result);
        	pc.populateListBoxWithBuildingNames(buildingList);
        }

        @Override
        public void onFailure(Throwable caught) {
        	errorBox.show("Unable to retrieve building names. Try refreshing your browser. " +
        			"Contact info@fertiletech.com if problems persist. <p> Error msg: <b>"+caught.getMessage() + "</b></p>");
        	//save.setEnabled(false);
        }
    };     

    public BulkTenantAndApartmentUploadPanel()
    {
    	super("Bulk upload residents", "Use this module to upload a spreadsheet containing a list of tenants. It will create both the resident and the apartment unit. " +
    			"For that reason do not attempt to use this to migrate a resident into an apartment already in the database. It will yield unexpected results.");
    }

    public void resetPanel()
    {
        tenantUploadForm.reset();
        confirmationForm.reset();
        formResponse.setHTML(INIT_DISPLAY);
        topLevelPanel.remove(topLevelPanel.getWidget());
        topLevelPanel.add(tenantUploadForm);
    }
    
    public void onClick(ClickEvent event)
    {
        if(event.getSource().equals(submitFile))
        {
        	if(!selectFile.getFilename().endsWith("xls"))
        	{
        		errorBox.show("Please select an excel 97-2003 worksheet (.xls extension)");
        		return;
        	}
            formResponse.setHTML("<b>Sending file to server ...</b>");
            tenantUploadForm.submit();
        }
        else if(event.getSource().equals(confirmData))
        {
            //confirmDisplay.setHTML("<b>Saving confirmed data ...</b>");
        	formResponse.setHTML("<b>Saving confirmed data ...</b>");
            confirmationForm.submit();
        }
        else if(event.getSource().equals(resetData))
        {
            resetPanel();
        }
    }

    public void onSubmitComplete(SubmitCompleteEvent event)
    {
        String results = event.getResults();
        //confirmDisplay.setHTML(event.getResults());
        formResponse.setHTML(event.getResults());
        if(results.startsWith(GOOD_DATA_PREFIX)) //hacky way 2 determine state
            confirmData.setVisible(true);
        else
            confirmData.setVisible(false);
        if(topLevelPanel.getWidget().equals(tenantUploadForm))
        {
            topLevelPanel.remove(tenantUploadForm);
            topLevelPanel.add(confirmationForm);
        }
    }

	@Override
	public Widget onInitialize() {
		// TODO Auto-generated method stub
		Widget w = uiBinder.createAndBindUi(this);
		final String ACTION_URL = "/upload/tenants";
        selectFile.setName("chooseFile");
        buildingList.setName("buildingkey");
        submitFile.addClickHandler(this);
        tenantUploadForm.setAction(ACTION_URL);
        tenantUploadForm.addSubmitCompleteHandler(this);
        tenantUploadForm.setMethod("post");
        tenantUploadForm.setEncoding(FormPanel.ENCODING_MULTIPART);

        confirmationForm = new FormPanel();
        FlowPanel formComponents = new FlowPanel();
        confirmationForm.setAction(ACTION_URL);
        confirmationForm.setMethod("post");
        confirmationForm.addSubmitCompleteHandler(this);
        confirmData = new Button("Save Loaded Data");
        resetData = new Button("Load new File");
        confirmData.addClickHandler(this);
        resetData.addClickHandler(this);
        //confirmDisplay = new HTML();
        formComponents.add(new Hidden(LOAD_PARAM_NAME));
        //formComponents.add(confirmDisplay);
        formComponents.add(confirmData);
        formComponents.add(resetData);
        confirmationForm.add(formComponents);
        
        errorBox = new SimpleDialog("An error occured");
        submitFile.setEnabled(false);
        if(pc.getBuildingNamesSize() > 0)
        	pc.populateListBoxWithBuildingNames(buildingList);
        else
        	LocalDataProvider.TENANT_SERVICE.getBuildingNames(buildingNamesCallback);
        return w;
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
	    GWT.runAsync(BulkTenantAndApartmentUploadPanel.class, new RunAsyncCallback() {

	        public void onFailure(Throwable caught) {
	          callback.onFailure(caught);
	        }

	        public void onSuccess() {
	          callback.onSuccess(onInitialize());
	        }
	      });
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_ADMIN_URL;
	}
}