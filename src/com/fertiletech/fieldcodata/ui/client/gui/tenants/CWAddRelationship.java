package com.fertiletech.fieldcodata.ui.client.gui.tenants;

import com.fertiletech.fieldcodata.ui.client.ContentWidget;
import com.fertiletech.fieldcodata.ui.client.gui.HelpPageGenerator;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class CWAddRelationship extends ContentWidget{

	public CWAddRelationship() {
		super("Add Relationships", "Use this panel to add people related/living with residents (e.g. nephew, daughter, son, wife etc)");
	}
	
	@Override
	public boolean hasScrollableContent()
	{
		return false;
	}
	
	@Override
	public Widget onInitialize() {
		return new AddTenantRelationship();
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
	    GWT.runAsync(CWAddRelationship.class, new RunAsyncCallback() {

	        public void onFailure(Throwable caught) {
	          callback.onFailure(caught);
	        }

	        public void onSuccess() {
	          callback.onSuccess(onInitialize());
	        }
	      });		
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_TENANTS_URL;
	}
}
