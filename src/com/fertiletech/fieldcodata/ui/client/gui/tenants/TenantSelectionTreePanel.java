/*
 * Copyright 2010 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.fertiletech.fieldcodata.ui.client.gui.tenants;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import com.fertiletech.fieldcodata.ui.client.gui.utils.TableMessageKeyProvider;
import com.fertiletech.fieldcodata.ui.shared.dto.DTOConstants;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellTree;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.MultiSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;

/**
 * Example file.
 */
public class TenantSelectionTreePanel extends Composite implements HasValueChangeHandlers<Integer>{
		
	interface Binder extends UiBinder<Widget, TenantSelectionTreePanel> {
	}

	/**
	 * The CellTree.
	 */
	@UiField(provided = true)
	CellTree cellTree;

	/**
	 * The label that shows selected names.
	 */
	@UiField
	Label selectedLabel;

	@UiField
	ListBox selectionList;
	
	@UiField
	SimplePanel viewPortPanel;
	

	/**
	 * Constructor.
	 * 
	 * @param constants
	 *            the constants
	 */
	public TenantSelectionTreePanel() {
		selectionModel = new MultiSelectionModel<TableMessage>(
				TableMessageKeyProvider.KEY_PROVIDER);
		initWidget(onInitialize());
		selectionList.setVisibleItemCount(20);
		selectionList.setEnabled(false);
		selectionModel
		.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
			public void onSelectionChange(SelectionChangeEvent event) {
				List<TableMessage> selected = new ArrayList<TableMessage>(
						selectionModel.getSelectedSet());
				
				Collections.sort(selected, new Comparator<TableMessage>() {

					@Override
					public int compare(TableMessage o1, TableMessage o2) {
						return o1.getText(DTOConstants.TNT_LNAME_IDX).compareToIgnoreCase(o2.getText(DTOConstants.TNT_LNAME_IDX));
					}
				});
				
				selectionList.clear();
				for (TableMessage value : selected) {
					selectionList.addItem(value.getText(DTOConstants.TNT_LNAME_IDX));
				}
				String selectCount = selected.size() + " tenant(s)";
				selectedLabel.setText(selectCount);
				fireSelectedCountChanged();
			}
		});				
	}

	/**
	 * Initialize this example.
	 */

	final MultiSelectionModel<TableMessage> selectionModel;
	public Widget onInitialize() {
		// Create the UiBinder.		
		CellTree.Resources res = GWT.create(CellTree.BasicResources.class);
		cellTree = new CellTree(new ContactTreeViewModel(selectionModel), null,
				res);
		cellTree.setAnimationEnabled(true);
		Binder uiBinder = GWT.create(Binder.class);
		Widget widget = uiBinder.createAndBindUi(this);
		return widget;
	}
	
	public void fireSelectedCountChanged()
	{
		String[] selectedTenants = getSelectedTenants();
		ValueChangeEvent.fire(this, selectedTenants == null?0:selectedTenants.length);
	}
	
	public String[] getSelectedTenants()
	{
		Set<TableMessage> selectedTenants = selectionModel.getSelectedSet();
		if(selectedTenants.size() == 0)
			return null;
		String[] tenantIDs = new String[selectedTenants.size()];
		int i = 0;
		for(TableMessage m : selectedTenants)
		{
			GWT.log("Message ID is: " + m.getMessageId());
			tenantIDs[i++] = m.getMessageId();
		}
		return tenantIDs;
	}
	
	public void selectNone()
	{
		Set<TableMessage> selectedTenants = selectionModel.getSelectedSet();
		for(TableMessage m : selectedTenants)
			selectionModel.setSelected(m, false);
	}

	public Set<TableMessage> getSelectedTenantObjects()
	{
		return selectionModel.getSelectedSet();
	}
	
	public void setViewPortContent(Widget w)
	{
		viewPortPanel.add(w);
	}
	
	public void hideAdditionalWidgets()
	{
		selectedLabel.setVisible(false);
		selectionList.setVisible(false);
		viewPortPanel.setVisible(false);
	}

	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<Integer> handler) {
		return addHandler(handler, ValueChangeEvent.getType());
	}
}
