package com.fertiletech.fieldcodata.ui.client.gui.tenants;

import java.util.HashMap;
import java.util.List;

import com.fertiletech.fieldcodata.ui.client.gui.LocalDataProvider;
import com.fertiletech.fieldcodata.ui.client.gui.utils.SimpleDialog;
import com.fertiletech.fieldcodata.ui.client.gui.utils.TenantSuggestBox;
import com.fertiletech.fieldcodata.ui.client.gui.utils.UserPanel;
import com.fertiletech.fieldcodata.ui.shared.UtilConstants;
import com.fertiletech.fieldcodata.ui.shared.UtilConstants.ContactTypes;
import com.fertiletech.fieldcodata.ui.shared.dto.DTOConstants;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

public class AddTenantRelationship extends Composite {

	@UiField
	TenantSuggestBox tenantBox;
	@UiField
	ListBox existingRelationships;
	@UiField
	TextBox relationship;
	@UiField
	UserPanel bio;
	@UiField
	Button reset;
	@UiField
	Button save;
	
	private static AddTenantRelationshipUiBinder uiBinder = GWT
			.create(AddTenantRelationshipUiBinder.class);

	interface AddTenantRelationshipUiBinder extends
			UiBinder<Widget, AddTenantRelationship> {
	}
	
	String selectedTenant = null;
	SimpleDialog infoBox = new SimpleDialog("INFO");
	HashMap<String, List<TableMessage>> tenantRelationships = new HashMap<String, List<TableMessage>>();
	int idxHack = 0;
	private AsyncCallback<List<TableMessage>> existingCallback = new AsyncCallback<List<TableMessage>>() {

		@Override
		public void onFailure(Throwable caught) {
			existingRelationships.clear();
			existingRelationships.addItem("ERROR LOADING", "");
			save.setEnabled(true);
			tenantBox.setEnabled(true);
		}

		@Override
		public void onSuccess(List<TableMessage> result) {
			populateRelationships(result);
			GWT.log("key: " + selectedTenant + " val:" + result);
			tenantRelationships.put(selectedTenant, result);
			save.setEnabled(true);
			tenantBox.setEnabled(true);
			selectRelationship();
		}
	};	
	
	public AddTenantRelationship() {
		initWidget(uiBinder.createAndBindUi(this));
		save.addClickHandler(new ClickHandler() {
			
			private AsyncCallback<String[]> callback = new AsyncCallback<String[]>() {

				@Override
				public void onFailure(Throwable caught) {
					save.setEnabled(true);
					infoBox.show("Error saving: " + caught.getMessage());
				}

				@Override
				public void onSuccess(String[] result) {
					bio.setUserID(result[UtilConstants.SAVE_CONTACT_KEY_IDX]);
					infoBox.show("Updated contact info for "
							+ result[UtilConstants.SAVE_CONTACT_VAL_IDX]
							+ " successfully");
					//refresh the drop down list
					LocalDataProvider.TENANT_SERVICE.getTenantRelations(selectedTenant, existingCallback);
					
				}
			};
			
			private AsyncCallback<TableMessage> saveCallback = new AsyncCallback<TableMessage>() {

				@Override
				public void onFailure(Throwable caught) {
					save.setEnabled(true);
					infoBox.show("Error saving: " + caught.getMessage());
				}

				@Override
				public void onSuccess(TableMessage result) {
					bio.setUserID(result.getMessageId());
					infoBox.show("Updated contact info for "
							+ result.getText(DTOConstants.TNT_LNAME_IDX)
							+ " successfully");
					//refresh the drop down list
					LocalDataProvider.TENANT_SERVICE.getTenantRelations(selectedTenant, existingCallback);
				}
			};			

			@Override
			public void onClick(ClickEvent event) {
				String[] args = {selectedTenant, relationship.getValue()};
				if(selectedTenant == null)
				{
					infoBox.show("You must first select a resident before you can add/edit relationships");
					return;
				}
				if(relationship.getValue().trim().length() == 0)
				{
					infoBox.show("Please enter a value for relationship");
					return;
				}
				if(bio.getFirstName().length() == 0 || bio.getLastName().length() == 0)
				{
					infoBox.show("Please enter values for both first name and last name");
					return;
				}
				if(bio.getUserID() == null || bio.getUserID().length() == 0)
					LocalDataProvider.TENANT_SERVICE.createContact(ContactTypes.RELATED_CONTACT, bio.getFirstName(), bio.getLastName(), 
							bio.getDateOfBirth(), bio.getEmail(), bio.getPhone(), bio.getOtherNumbers(), 
							bio.getOtherMails(), bio.getCompany(), bio.getAddress(),bio.isMale(), bio.getTitle(), args, callback );
				else
					LocalDataProvider.TENANT_SERVICE.updateContact(bio.getUserID(), bio.getFirstName(), bio.getLastName(), 
							bio.getDateOfBirth(), bio.getEmail(), bio.getPhone(), bio.getOtherNumbers(), bio.getOtherMails(), 
							bio.getCompany(), bio.getAddress(), bio.isMale(), bio.getTitle(), args, saveCallback);
				
			}
		});
		reset.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				bio.clear();
				tenantBox.clear();
				existingRelationships.clear();
				relationship.setValue(null);
				save.setEnabled(true);
				existingRelationships.setEnabled(true);
				selectedTenant = null;
			}
		});
		existingRelationships.addChangeHandler(new ChangeHandler() {
			
			@Override
			public void onChange(ChangeEvent event) {
				String selectedRelationship = existingRelationships.getValue(existingRelationships.getSelectedIndex());
				if(selectedRelationship.length() == 0)
				{
					bio.clear();
					relationship.setValue(null); 
					return;
				}
				List<TableMessage> cachedRelations = tenantRelationships.get(selectedTenant);
				GWT.log("Cached: " + cachedRelations + " size " + tenantRelationships.size() + " selected:" + selectedRelationship);
				TableMessage selectedRelationMessage = cachedRelations.get(existingRelationships.getSelectedIndex() - 1);
				bio.setContactValues(selectedRelationMessage);
				String rlt = selectedRelationMessage.getText(DTOConstants.RLT_TYPE_IDX);
				relationship.setValue(rlt);
				selectedRelationship = selectedRelationMessage.getMessageId();
			}
		});
		existingRelationships.setWidth("150px");
		tenantBox.addValueChangeHandler(new ValueChangeHandler<String>() {

			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				selectedTenant = event.getValue();
				GWT.log("Set selected tenant to: " + selectedTenant);
				List<TableMessage> cachedRelations = tenantRelationships.get(selectedTenant);
				if(cachedRelations == null)
				{
					existingRelationships.clear();
					existingRelationships.addItem("LOADING...", "");
					save.setEnabled(false);
					tenantBox.setEnabled(false);
					LocalDataProvider.TENANT_SERVICE.getTenantRelations(selectedTenant, existingCallback);
				}
				else
					populateRelationships(cachedRelations);
			}
		});
	}
	
	private void populateRelationships(List<TableMessage> result)
	{
		existingRelationships.clear();
		existingRelationships.addItem("NEW RELATION", "");
		for(TableMessage m : result)
			existingRelationships.addItem(m.getText(DTOConstants.RLT_TYPE_IDX) +
					": " + m.getText(DTOConstants.TNT_FNAME_IDX), m.getMessageId());
	}
	
	public void selectRelationship()
	{
		for(int i = 0; i < existingRelationships.getItemCount(); i++)
			if(existingRelationships.getValue(i).equals(bio.getUserID()))
				existingRelationships.setSelectedIndex(i);
	}
}
