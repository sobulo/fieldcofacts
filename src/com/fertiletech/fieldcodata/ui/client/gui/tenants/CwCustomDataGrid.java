/*
 * Copyright 2011 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.fertiletech.fieldcodata.ui.client.gui.tenants;

import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.fertiletech.fieldcodata.ui.client.ContentWidget;
import com.fertiletech.fieldcodata.ui.client.GUIConstants;
import com.fertiletech.fieldcodata.ui.client.gui.HelpPageGenerator;
import com.fertiletech.fieldcodata.ui.client.gui.LocalDataProvider;
import com.fertiletech.fieldcodata.ui.client.gui.utils.ArchiveButton;
import com.fertiletech.fieldcodata.ui.client.gui.utils.SimpleDialog;
import com.fertiletech.fieldcodata.ui.client.gui.utils.TableMessageKeyProvider;
import com.fertiletech.fieldcodata.ui.shared.dto.DTOConstants;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.cell.client.ClickableTextCell;
import com.google.gwt.cell.client.DateCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.dom.builder.shared.DivBuilder;
import com.google.gwt.dom.builder.shared.TableCellBuilder;
import com.google.gwt.dom.builder.shared.TableRowBuilder;
import com.google.gwt.dom.client.Style.Cursor;
import com.google.gwt.dom.client.Style.OutlineStyle;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.text.shared.AbstractSafeHtmlRenderer;
import com.google.gwt.text.shared.SafeHtmlRenderer;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.AbstractCellTable.Style;
import com.google.gwt.user.cellview.client.AbstractCellTableBuilder;
import com.google.gwt.user.cellview.client.AbstractHeaderOrFooterBuilder;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.ColumnSortList;
import com.google.gwt.user.cellview.client.ColumnSortList.ColumnSortInfo;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.cellview.client.Header;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.cellview.client.SimplePager.TextLocation;
import com.google.gwt.user.cellview.client.TextHeader;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.SelectionModel;


public class CwCustomDataGrid extends ContentWidget {

	@UiField(provided = true)
	ArchiveButton archive;
	ListDataProvider<TableMessage> dataProvider = null;
	SimpleDialog infoBox = null;
	HashMap<String, List<TableMessage>> cachedRelations = null;
	List<TableMessage> existingTenantList = null;
	List<TableMessage> archiveTenantList = null;
	
	protected AsyncCallback<HashMap<String, List<TableMessage>>> contactsCallBack = new AsyncCallback<HashMap<String,List<TableMessage>>>() {

		@Override
		public void onFailure(Throwable caught) {
			infoBox.show("retrieved " + existingTenantList.size() + " residents but failed to retrieve their relationship info <br/>" + caught.getMessage());
		}

		@Override
		public void onSuccess(HashMap<String, List<TableMessage>> result) {
			cachedRelations = result;
			dataProvider.getList().addAll(existingTenantList);
			infoBox.show("Residents (" + existingTenantList.size() + "). Relationships(" + result.size() + ")");
			buildTable();
		}
	};
    // Create an asynchronous callback to handle the result.
    private final AsyncCallback<List<TableMessage>> tenantListCallback = new AsyncCallback<List<TableMessage>>() {

        @Override
        public void onSuccess(List<TableMessage> result) {
        	result.remove(0);
        	existingTenantList = result;
        	LocalDataProvider.TENANT_SERVICE.getTenantContacts(contactsCallBack );
        }

        @Override
        public void onFailure(Throwable caught) {
        	infoBox.show("Unable to retrieve tenant names. Try refreshing your browser. " +
        			"Contact info@fertiletech.com if problems persist. <p> Error msg: <b>"+caught.getMessage() + "</b></p>");
        }
    };
    
    // Create an asynchronous callback to handle the result.
    private final AsyncCallback<List<TableMessage>> archivedTenantListCallback = new AsyncCallback<List<TableMessage>>() {

        @Override
        public void onSuccess(List<TableMessage> result) {
        	result.remove(0);
        	archiveTenantList = result;
        	archive.enableBox(true);
        	archive.addValueChangeHandler(new ValueChangeHandler<String>() {
				
				@Override
				public void onValueChange(ValueChangeEvent<String> event) {
					GWT.log("Event called with: " + event.getValue());
					dataProvider.getList().clear();
					if(archive.isArchived())
					{
						dataProvider.getList().addAll(archiveTenantList);
						GWT.log("Archive: " + archiveTenantList.size());
					}
					else
					{
						dataProvider.getList().addAll(existingTenantList);
						GWT.log("Existing: " + existingTenantList.size());						
					}
					dataGrid.redraw();
				}
			});
        }

        @Override
        public void onFailure(Throwable caught) {
        	infoBox.show("Unable to retrieve tenant names. Try refreshing your browser. " +
        			"Contact info@fertiletech.com if problems persist. <p> Error msg: <b>"+caught.getMessage() + "</b></p>");
        }
    };    

  interface Binder extends UiBinder<Widget, CwCustomDataGrid> {
  }


  interface Resources extends ClientBundle {

    /**
     * Get the styles used but this example.
     */
    @Source("CwCustomDataGrid.css")
    Styles styles();
  }


  interface Styles extends CssResource {
    /**
     * Indents cells in child rows.
     */
    String childCell();

    /**
     * Applies to group headers.
     */
    String groupHeaderCell();
  }

  /**
   * Renders custom table headers. The top header row includes the groups "Name"
   * and "Information", each of which spans multiple columns. The second row of
   * the headers includes the contacts' first and last names grouped under the
   * "Name" category. The second row also includes the age, category, and
   * address of the contacts grouped under the "Information" category.
   */
  private class CustomHeaderBuilder extends AbstractHeaderOrFooterBuilder<TableMessage> {

    private Header<String> firstNameHeader = new TextHeader("First Name");
    private Header<String> lastNameHeader = new TextHeader("LastName");
    private Header<String> dobHeader = new TextHeader("Birthday");
    private Header<String> emailHeader = new TextHeader("Email");
    private Header<String> phoneHeader = new TextHeader("Phone Number");
    private Header<String> addressHeader = new TextHeader("Address");
    private Header<String> buildingHeader = new TextHeader("Building");

    public CustomHeaderBuilder() {
      super(dataGrid, false);
      setSortIconStartOfLine(false);
    }

    @Override
    protected boolean buildHeaderOrFooterImpl() {
      Style style = dataGrid.getResources().style();
      String groupHeaderCell = resources.styles().groupHeaderCell();

      // Add a 2x2 header above the checkbox and show friends columns.
      TableRowBuilder tr = startRow();
      tr.startTH().colSpan(1).rowSpan(2)
          .className(style.header() + " " + style.firstColumnHeader());
      tr.endTH();

      /*
       * Name group header. Associated with the last name column, so clicking on
       * the group header sorts by last name.
       */
      TableCellBuilder th = tr.startTH().colSpan(3).className(groupHeaderCell);
      enableColumnHandlers(th, lastNameColumn);
      th.style().trustedProperty("border-right", "10px solid white").cursor(Cursor.POINTER)
          .endStyle();
      th.text("Bio").endTH();

      // Information group header.
      th = tr.startTH().colSpan(4).className(groupHeaderCell);
      th.text("Information").endTH();

      // Get information about the sorted column.
      ColumnSortList sortList = dataGrid.getColumnSortList();
      ColumnSortInfo sortedInfo = (sortList.size() == 0) ? null : sortList.get(0);
      Column<?, ?> sortedColumn = (sortedInfo == null) ? null : sortedInfo.getColumn();
      boolean isSortAscending = (sortedInfo == null) ? false : sortedInfo.isAscending();

      // Add column headers.
      tr = startRow();
      buildHeader(tr, firstNameHeader, firstNameColumn, sortedColumn, isSortAscending, false, false);
      buildHeader(tr, lastNameHeader, lastNameColumn, sortedColumn, isSortAscending, false, false);
      buildHeader(tr, dobHeader, dobColumn, sortedColumn, isSortAscending, false, false);
      buildHeader(tr, emailHeader, phoneColumn, sortedColumn, isSortAscending, false, false);
      buildHeader(tr, phoneHeader, emailColumn, sortedColumn, isSortAscending, false, false);
      buildHeader(tr, buildingHeader, buildingColumn, sortedColumn, isSortAscending, false, false);
      buildHeader(tr, addressHeader, addressColumn, sortedColumn, isSortAscending, false, false);
      tr.endTR();

      return true;
    }

    /**
     * Renders the header of one column, with the given options.
     * 
     * @param out the table row to build into
     * @param header the {@link Header} to render
     * @param column the column to associate with the header
     * @param sortedColumn the column that is currently sorted
     * @param isSortAscending true if the sorted column is in ascending order
     * @param isFirst true if this the first column
     * @param isLast true if this the last column
     */
    private void buildHeader(TableRowBuilder out, Header<?> header, Column<TableMessage, ?> column,
        Column<?, ?> sortedColumn, boolean isSortAscending, boolean isFirst, boolean isLast) {
      // Choose the classes to include with the element.
      Style style = dataGrid.getResources().style();
      boolean isSorted = (sortedColumn == column);
      StringBuilder classesBuilder = new StringBuilder(style.header());
      if (isFirst) {
        classesBuilder.append(" " + style.firstColumnHeader());
      }
      if (isLast) {
        classesBuilder.append(" " + style.lastColumnHeader());
      }
      if (column.isSortable()) {
        classesBuilder.append(" " + style.sortableHeader());
      }
      if (isSorted) {
        classesBuilder.append(" "
            + (isSortAscending ? style.sortedHeaderAscending() : style.sortedHeaderDescending()));
      }

      // Create the table cell.
      TableCellBuilder th = out.startTH().className(classesBuilder.toString());

      // Associate the cell with the column to enable sorting of the column.
      enableColumnHandlers(th, column);

      // Render the header.
      Context context = new Context(0, 2, header.getKey());
      renderSortableHeader(th, context, header, isSorted, isSortAscending);

      // End the table cell.
      th.endTH();
    }
  }

  /**
   * Renders custom table footers that appear beneath the columns in the table.
   * This footer consists of a single cell containing the average age of all
   * contacts on the current page. This is an example of a dynamic footer that
   * changes with the row data in the table.
   */
  private class CustomFooterBuilder extends AbstractHeaderOrFooterBuilder<TableMessage> {

    public CustomFooterBuilder() {
      super(dataGrid, true);
    }

    @Override
    protected boolean buildHeaderOrFooterImpl() {
      String footerStyle = dataGrid.getResources().style().footer();

      // Calculate the age of all visible contacts.
      int itemCount = dataGrid.getVisibleItems().size();

      // Cells before age column.
      TableRowBuilder tr = startRow();
      tr.startTH().colSpan(4).className(footerStyle).endTH();

      // Show the average age of all contacts.
      TableCellBuilder th =
          tr.startTH().className(footerStyle).align(
              HasHorizontalAlignment.ALIGN_CENTER.getTextAlignString());
      th.text("Residents(" + itemCount + ")");
      th.endTH();

      // Cells after age column.
      tr.startTH().colSpan(4).className(footerStyle).endTH();
      tr.endTR();

      return true;
    }
  }

  /**
   * Renders the data rows that display each contact in the table.
   */
  private class CustomTableBuilder extends AbstractCellTableBuilder<TableMessage> {

    private final int todayMonth;

    private final String childCell = " " + resources.styles().childCell();
    private final String rowStyle;
    private final String selectedRowStyle;
    private final String cellStyle;
    private final String selectedCellStyle;

    @SuppressWarnings("deprecation")
    public CustomTableBuilder() {
      super(dataGrid);

      // Cache styles for faster access.
      Style style = dataGrid.getResources().style();
      rowStyle = style.evenRow();
      selectedRowStyle = " " + style.selectedRow();
      cellStyle = style.cell() + " " + style.evenRowCell();
      selectedCellStyle = " " + style.selectedRowCell();

      // Record today's date.
      Date today = new Date();
      todayMonth = today.getMonth();
    }

    @SuppressWarnings("deprecation")
    @Override
    public void buildRowImpl(final TableMessage rowValue, final int absRowIndex) {
      buildContactRow(rowValue, absRowIndex, false);

      List<TableMessage> friends = cachedRelations.get(rowValue.getMessageId());
      StringBuilder friendBirthdays = new StringBuilder();
      if(friends != null)
      {
    	  int count = 0;
    	  for(TableMessage m : friends)
    	  {
    		  Date dob = m.getDate(DTOConstants.TNT_DATE_OF_BIRTH_IDX);
    		  if(dob != null &&  dob.getMonth() == todayMonth)
    		  {
    			  if(friendBirthdays.length() > 0)
    				  friendBirthdays.append(", ");
    			  friendBirthdays.append(m.getText(DTOConstants.TNT_FNAME_IDX));
    		  }
    	  }
      }
      // Display information about the user in another row that spans the entire
      // table.
      Date dob = rowValue.getDate(DTOConstants.TNT_DATE_OF_BIRTH_IDX);
      if ((dob != null && dob.getMonth() == todayMonth) || friendBirthdays.length() > 0) {
        TableRowBuilder row = startRow();
        TableCellBuilder td = row.startTD().colSpan(8).className(cellStyle);
        td.style().trustedBackgroundColor("#f7b742").endStyle();
        StringBuilder bdayMsg = new StringBuilder();
        if((dob != null && dob.getMonth() == todayMonth))
        	bdayMsg.append(rowValue.getText(DTOConstants.TNT_FNAME_IDX)).append("'s birthday is this month!");
        if(friendBirthdays.length() > 0)
        {
        	if(bdayMsg.length() == 0)
        		bdayMsg.append(rowValue.getText(DTOConstants.TNT_FNAME_IDX)).append("'s");
        	bdayMsg.append(" relations with birthdays this month are: ").append(friendBirthdays.toString());
        }
        td.text(replaceNull(bdayMsg.toString())).endTD();
        row.endTR();
      }
      
      // Display list of friends.
      if (showingFriends.contains(rowValue.getMessageId())) {
    	if(friends != null)
    	{
	        for (TableMessage friend : friends) {
	            buildContactRow(friend, absRowIndex, true);
	          }
	        return;
    	}
      }
    }

    /**
     * Build a row.
     * 
     * @param rowValue the contact info
     * @param absRowIndex the absolute row index
     * @param isFriend true if this is a subrow, false if a top level row
     */
    private void buildContactRow(TableMessage rowValue, int absRowIndex, boolean isFriend) {
      // Calculate the row styles.
      SelectionModel<? super TableMessage> selectionModel = dataGrid.getSelectionModel();
      boolean isSelected =
          (selectionModel == null || rowValue == null) ? false : selectionModel
              .isSelected(rowValue);
      StringBuilder trClasses = new StringBuilder(rowStyle);
      if (isSelected) {
        trClasses.append(selectedRowStyle);
      }

      // Calculate the cell styles.
      String cellStyles = cellStyle;
      if (isSelected) {
        cellStyles += selectedCellStyle;
      }
      if (isFriend) {
        cellStyles += childCell;
      }

      TableRowBuilder row = startRow();
      row.className(trClasses.toString());

      /*
       * View friends column.
       * 
       * Displays a link to "show friends". When clicked, the list of friends is
       * displayed below the contact.
       */
      TableCellBuilder td = row.startTD();
      td.className(cellStyles);
      if (!isFriend) {
        td.style().outlineStyle(OutlineStyle.NONE).endStyle();
        renderCell(td, createContext(0), viewFriendsColumn, rowValue);
      }
      td.endTD();

      // First name column.
      td = row.startTD();
      td.className(cellStyles);
      td.style().outlineStyle(OutlineStyle.NONE).endStyle();
      if (isFriend) {
        td.text(replaceNull(rowValue.getText(DTOConstants.TNT_FNAME_IDX)));
      } else {
        renderCell(td, createContext(1), firstNameColumn, rowValue);
      }
      td.endTD();

      // Last name column.
      td = row.startTD();
      td.className(cellStyles);
      td.style().outlineStyle(OutlineStyle.NONE).endStyle();
      if (isFriend) {
        td.text(replaceNull(rowValue.getText(DTOConstants.TNT_LNAME_IDX)));
      } else {
    	  
        renderCell(td, createContext(2), lastNameColumn, rowValue);
      }
      td.endTD();

      //dob column
      td = row.startTD();
      td.className(cellStyles);
      td.style().outlineStyle(OutlineStyle.NONE).endStyle();
      if (isFriend) {
    	  Date d = rowValue.getDate(DTOConstants.TNT_DATE_OF_BIRTH_IDX);
        td.text(d == null?"":GUIConstants.DEFAULT_DATE_FORMAT.format(d));
      } else {
    	  
        renderCell(td, createContext(3), dobColumn, rowValue);
      }
      td.endTD();      
      
      // Category column.
      td = row.startTD();
      td.className(cellStyles);
      td.style().outlineStyle(OutlineStyle.NONE).endStyle();
      if (isFriend) {
        td.text(replaceNull(rowValue.getText(DTOConstants.TNT_PRIMARY_EMAIL_IDX)));
      } else {
        renderCell(td, createContext(4), emailColumn, rowValue);
      }
      td.endTD();
      
      // Category column.
      td = row.startTD();
      td.className(cellStyles);
      td.style().outlineStyle(OutlineStyle.NONE).endStyle();
      if (isFriend) {
        td.text(replaceNull(rowValue.getText(DTOConstants.TNT_PRIMARY_PHONE_IDX)));
      } else {
        renderCell(td, createContext(5), phoneColumn, rowValue);
      }
      td.endTD();
      
      // Category column.
      td = row.startTD();
      td.className(cellStyles);
      td.style().outlineStyle(OutlineStyle.NONE).endStyle();
      if (isFriend) {
        td.text(replaceNull(rowValue.getText(DTOConstants.RLT_TYPE_IDX)));
      } else {
        renderCell(td, createContext(6), buildingColumn, rowValue);
      }
      td.endTD();      

      // Address column.
      td = row.startTD();
      td.className(cellStyles);
      DivBuilder div = td.startDiv();
      div.style().outlineStyle(OutlineStyle.NONE).endStyle();
      div.text(replaceNull(rowValue.getText(DTOConstants.TNT_ADDRESS_IDX))).endDiv();
      td.endTD();
      
      row.endTR();
    }
  }

  /**
   * The main DataGrid.
   */
  @UiField(provided = true)
  DataGrid<TableMessage> dataGrid;

  /**
   * The pager used to change the range of data.
   */
  @UiField(provided = true)
  SimplePager pager;

  /**
   * The resources used by this example.
   */
  private Resources resources;

  /**
   * Contains the contact id for each row in the table where the friends list is
   * currently expanded.
   */
  private final Set<String> showingFriends = new HashSet<String>();

  /**
   * Column to expand friends list.
   */

  private Column<TableMessage, String> viewFriendsColumn;

  /**
   * Column displays first name.
   */
  private Column<TableMessage, String> firstNameColumn;

  /**
   * Column displays last name.
   */
  private Column<TableMessage, String> lastNameColumn;

  /**
   * Column displays age.
   */
  private Column<TableMessage, String> emailColumn;

  /**
   * Column displays category.
   */
  private Column<TableMessage, String> phoneColumn;


  private Column<TableMessage, Date> dobColumn;
  /**
   * Column displays address.
   */
  private Column<TableMessage, String> addressColumn;
  private Column<TableMessage, String> buildingColumn;

  /**
   * Constructor.
   * 
   * @param constants the constants
   */
  public CwCustomDataGrid() {
    super("View Resident Relationships", "Use this module to view bio info for contacts related to residents");
  }

  @Override
  public boolean hasMargins() {
    return false;
  }

  @Override
  public boolean hasScrollableContent() {
    return false;
  }

  /**
   * Initialize this example.
   */
  @Override
  public Widget onInitialize() {
    resources = GWT.create(Resources.class);
    resources.styles().ensureInjected();
    infoBox = new SimpleDialog("INFO");
    archive = new ArchiveButton();
    archive.enableBox(false);
    		
    // Create a DataGrid.

    /*
     * Set a key provider that provides a unique key for each contact. If key is
     * used to identify contacts when fields (such as the name and address)
     * change.
     */
    dataGrid = new DataGrid<TableMessage>(TableMessageKeyProvider.KEY_PROVIDER);
    dataGrid.setWidth("100%");

    /*
     * Do not refresh the headers every time the data is updated. The footer
     * depends on the current data, so we do not disable auto refresh on the
     * footer.
     */
    dataGrid.setAutoHeaderRefreshDisabled(true);

    // Set the message to display when the table is empty.
    dataGrid.setEmptyTableWidget(new Label("Loading ..."));

    //initialize dataprovider and add datagrid to its view
    bindTenantsToDisplay();    

    // Create a Pager to control the table.
    SimplePager.Resources pagerResources = GWT.create(SimplePager.Resources.class);
    pager = new SimplePager(TextLocation.CENTER, pagerResources, false, 0, true);
    pager.setDisplay(dataGrid);

    // Create the UiBinder.
    Binder uiBinder = GWT.create(Binder.class);
    return uiBinder.createAndBindUi(this);
  }
  
  private void buildTable()
  {
	    // Attach a column sort handler to the ListDataProvider to sort the list.
	    ListHandler<TableMessage> sortHandler =
	        new ListHandler<TableMessage>(dataProvider.getList());
	    dataGrid.addColumnSortHandler(sortHandler);	  
	    
	    // Initialize the columns.
	    initializeColumns(sortHandler);
	    
	    // Specify a custom table.
	    dataGrid.setTableBuilder(new CustomTableBuilder());
	    dataGrid.setHeaderBuilder(new CustomHeaderBuilder());
	    dataGrid.setFooterBuilder(new CustomFooterBuilder());
	    LocalDataProvider.TENANT_SERVICE.getBuildingTenants(null, true, archivedTenantListCallback);
  }

  @Override
  protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
    GWT.runAsync(CwCustomDataGrid.class, new RunAsyncCallback() {

      @Override
      public void onFailure(Throwable caught) {
        callback.onFailure(caught);
      }

      @Override
      public void onSuccess() {
        callback.onSuccess(onInitialize());
      }
    });
  }

  /**
   * Defines the columns in the custom table. Maps the data in the TableMessage
   * for each row into the appropriate column in the table, and defines handlers
   * for each column.
   */
  private void initializeColumns(ListHandler<TableMessage> sortHandler) {

    // View friends.
    SafeHtmlRenderer<String> anchorRenderer = new AbstractSafeHtmlRenderer<String>() {
      @Override
      public SafeHtml render(String object) {
        SafeHtmlBuilder sb = new SafeHtmlBuilder();
        if(object.equals("show"))
        	sb.appendHtmlConstant("(<a style='color:green' href=\"javascript:;\">");
        else if(object.equals("hide"))
        	sb.appendHtmlConstant("(<a style='color:red' href=\"javascript:;\">");
        else
        	sb.appendHtmlConstant("(<a href=\"javascript:;\">");
        
        sb.appendEscaped(object).appendHtmlConstant("</a>)");
        return sb.toSafeHtml();
      }
    };
    viewFriendsColumn = new Column<TableMessage, String>(new ClickableTextCell(anchorRenderer)) {
      @Override
      public String getValue(TableMessage object) {
        if (showingFriends.contains(object.getMessageId())) {
          return "hide";
        } else if(cachedRelations.containsKey(object.getMessageId())){
          return "show";
        }
      else
    	  return "none";
      }
    };
    viewFriendsColumn.setFieldUpdater(new FieldUpdater<TableMessage, String>() {
      @Override
      public void update(int index, TableMessage object, String value) {
        if (showingFriends.contains(object.getMessageId())) {
          showingFriends.remove(object.getMessageId());
        } else {
          showingFriends.add(object.getMessageId());
        }

        // Redraw the modified row.
        dataGrid.redrawRow(index);
      }
    });
    dataGrid.setColumnWidth(0, 5, Unit.PCT);

    // First name.
    firstNameColumn = new Column<TableMessage, String>(new TextCell()) {
      @Override
      public String getValue(TableMessage object) {
        return replaceNull(object.getText(DTOConstants.TNT_FNAME_IDX));
      }
    };
    firstNameColumn.setSortable(true);
    sortHandler.setComparator(firstNameColumn, new Comparator<TableMessage>() {
      @Override
      public int compare(TableMessage o1, TableMessage o2) {
        return replaceNull(o1.getText(DTOConstants.TNT_FNAME_IDX)).compareTo(replaceNull(o2.getText(DTOConstants.TNT_FNAME_IDX)));
      }
    });
    dataGrid.setColumnWidth(1, 12, Unit.PCT);

    // Last name.
    lastNameColumn = new Column<TableMessage, String>(new TextCell()) {
      @Override
      public String getValue(TableMessage object) {
        return replaceNull(object.getText(DTOConstants.TNT_LNAME_IDX));
      }
    };
    lastNameColumn.setSortable(true);
    sortHandler.setComparator(lastNameColumn, new Comparator<TableMessage>() {
      @Override
      public int compare(TableMessage o1, TableMessage o2) {
        return replaceNull(o1.getText(DTOConstants.TNT_LNAME_IDX)).compareTo(replaceNull(o2.getText(DTOConstants.TNT_LNAME_IDX)));
      }
    });
    dataGrid.setColumnWidth(2, 11, Unit.PCT);

    // Address.
    dobColumn = new Column<TableMessage, Date>(new DateCell()) {
      @Override
      public Date getValue(TableMessage object) {
        return object.getDate(DTOConstants.TNT_DATE_OF_BIRTH_IDX);
      }
    };
    dobColumn.setSortable(true);
    sortHandler.setComparator(dobColumn, new Comparator<TableMessage>() {
      @Override
      public int compare(TableMessage o1, TableMessage o2) {
    	  if(o1 == null) return -1;
    	  if(o2 == null) return +1;
          return o1.getDate(DTOConstants.TNT_DATE_OF_BIRTH_IDX).compareTo(o2.getDate(DTOConstants.TNT_DATE_OF_BIRTH_IDX));
      }
    });    
    dataGrid.setColumnWidth(3, 9, Unit.PCT);
    
    // Age.
    emailColumn = new Column<TableMessage, String>(new TextCell()) {
      @Override
      public String getValue(TableMessage object) {
        return replaceNull(object.getText(DTOConstants.TNT_PRIMARY_EMAIL_IDX));
      }
    };
    emailColumn.setSortable(true);
    sortHandler.setComparator(emailColumn, new Comparator<TableMessage>() {
      @Override
      public int compare(TableMessage o1, TableMessage o2) {
          return replaceNull(o1.getText(DTOConstants.TNT_PRIMARY_EMAIL_IDX)).compareTo(replaceNull(o2.getText(DTOConstants.TNT_PRIMARY_EMAIL_IDX)));
      }
    });
    dataGrid.setColumnWidth(4, 19, Unit.PCT);

    phoneColumn = new Column<TableMessage, String>(new TextCell()) {
      @Override
      public String getValue(TableMessage object) {
        return replaceNull(object.getText(DTOConstants.TNT_PRIMARY_PHONE_IDX));
      }
    };
    dataGrid.setColumnWidth(5, 14, Unit.PCT);

    buildingColumn = new Column<TableMessage, String>(new TextCell()) {
        @Override
        public String getValue(TableMessage object) {
        	
          return getLocationName(object);
        }
      };
      buildingColumn.setSortable(true);
      sortHandler.setComparator(buildingColumn, new Comparator<TableMessage>() {
        @Override
        public int compare(TableMessage o1, TableMessage o2) {
          return getLocationName(o1).compareTo(getLocationName(o2));
        }
      });
      
      dataGrid.setColumnWidth(6, 14, Unit.PCT);   
    
    // Address.
    addressColumn = new Column<TableMessage, String>(new TextCell()) {
      @Override
      public String getValue(TableMessage object) {
        return replaceNull(object.getText(DTOConstants.TNT_ADDRESS_IDX));
      }
    };
    addressColumn.setSortable(true);
    sortHandler.setComparator(addressColumn, new Comparator<TableMessage>() {
      @Override
      public int compare(TableMessage o1, TableMessage o2) {
        return replaceNull(o1.getText(DTOConstants.TNT_ADDRESS_IDX)).compareTo(replaceNull(o2.getText(DTOConstants.TNT_ADDRESS_IDX)));
      }
    });
    dataGrid.setColumnWidth(7, 17, Unit.PCT);

  }

	private void bindTenantsToDisplay() {
		dataProvider = new ListDataProvider<TableMessage>();
		LocalDataProvider.TENANT_SERVICE.getBuildingTenants(null, false, tenantListCallback);
		dataProvider.addDataDisplay(dataGrid);
	}  
	
	private String getLocationName(TableMessage object)
	{
		return replaceNull(object.getText(DTOConstants.TNT_BUILDING_IDX)) + " " + object.getText(DTOConstants.TNT_APT_IDX);
	}
  
	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_TENANTS_URL;
	}
	
	private String replaceNull(String val)
	{
		return (val == null?"":val);
	}
}
