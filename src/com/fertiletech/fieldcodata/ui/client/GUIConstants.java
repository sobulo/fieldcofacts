/**
 * 
 */
package com.fertiletech.fieldcodata.ui.client;

import com.fertiletech.fieldcodata.ui.client.gui.utils.CommentsPanel;
import com.fertiletech.fieldcodata.ui.client.gui.utils.ShowcaseTable;
import com.fertiletech.fieldcodata.ui.client.gui.utils.UserPanel;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.ui.DeckLayoutPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.StackLayoutPanel;
import com.google.gwt.user.client.ui.TabLayoutPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.user.datepicker.client.DateBox.DefaultFormat;

/**
 * @author Segun Razaq Sobulo
 *
 */
public final class GUIConstants {
	public final static String STYLE_PS_TABLE_SPACER = "scrollTableSpacer"; 
    public final static String STYLE_STND_TABLE_HEADER = "standardTableHeader";
    public final static String STYLE_STND_TABLE = "standardTable";	
    public final static String STYLE_CAPITALIZE = "capitalizeText";    
	public final static String ROOT_ID_REDIRECT = "contentAreaRedirect";
	public final static String ROOT_ID_PUBLIC = "contentAreaCalculator";
	public final static String ROOT_ID_ADMIN = "contentAreaAdmin";
	public final static String ROOT_ID_APPLY_PORTAL = "contentAreaPortal";

    public final static String DEFAULT_STACK_WIDTH = "100%";
    public final static String DEFAULT_STACK_HEIGHT = "100%";  
    public final static Double DEFAULT_STACK_HEADER = 30.0;
    
    public final static String DEFAULT_TAB_WIDTH = "100%";
    public final static String DEFAULT_TAB_HEIGHT = "600px";  
    public final static Double DEFAULT_TAB_HEADER = 40.0;
    public final static DefaultFormat DEFAULT_DATEBOX_FORMAT = new DateBox.DefaultFormat(DateTimeFormat.getFormat(PredefinedFormat.DATE_MEDIUM));
	public final static NumberFormat DEFAULT_NUMBER_FORMAT = NumberFormat.getFormat("#,###,###,##0.00");
	public final static NumberFormat DEFAULT_INTEGER_FORMAT = NumberFormat.getFormat("###");
	public final static DateTimeFormat DEFAULT_DATE_FORMAT = DateTimeFormat.getFormat(DateTimeFormat.PredefinedFormat.DATE_MEDIUM);
	public final static DateTimeFormat DEFAULT_DATE_TIME_FORMAT = DateTimeFormat.getFormat(DateTimeFormat.PredefinedFormat.DATE_TIME_SHORT);  
    public final static int MENU_SPACING = 20;
	public static String TICKET_ID_PREFIX = "ticket?";
	public static String getAcctDisplayName(String type, String ccy)
	{
		return type + " [" + ccy + "]";
	}    
    
    /**
     * urgh!!! different developers, different ports? equals commits containing code
     * server configuration. Hopefully everyone agrees to use same port configs
     */
    public final static String GWT_CODE_SERVER = "?gwt.codesvr=127.0.0.1:9997";
    
	static StackLayoutPanel getStackLayout(Widget[]widgets, String[] headers)
	{
		StackLayoutPanel result = new StackLayoutPanel(Unit.PX);
		//either this or add to a simple panel with size set
		result.setHeight(GUIConstants.DEFAULT_STACK_HEIGHT);
		result.setWidth(GUIConstants.DEFAULT_STACK_WIDTH);
		
		for(int i = 0; i < widgets.length; i++)
			result.add(new ScrollPanel(widgets[i]), 
					headers[i], GUIConstants.DEFAULT_STACK_HEADER);
		return result;
	}
	
	public static TabLayoutPanel getTabLayout(Widget[] widgets, String[]headers)
	{
		TabLayoutPanel result = new TabLayoutPanel(GUIConstants.DEFAULT_TAB_HEADER, Unit.PX);
		result.setHeight(GUIConstants.DEFAULT_TAB_HEIGHT);
		result.setWidth(GUIConstants.DEFAULT_TAB_WIDTH);		
		for(int i = 0; i < widgets.length; i++)
			result.add(new ScrollPanel(widgets[i]), headers[i]);
		return result;
	}
	
	public static void addContactInfoPopup(ShowcaseTable display)
	{
		addContactInfoPopup(display, true);
	}
	
	public static void addContactInfoPopup(ShowcaseTable display, final boolean useResidentCompany)
	{
		display.addValueChangeHandler(new ValueChangeHandler<TableMessage>() {
			UserPanel u = new UserPanel(useResidentCompany);
			CommentsPanel c = new CommentsPanel(false);
			PopupPanel p = new PopupPanel(true);
			DeckLayoutPanel container = new DeckLayoutPanel();
			{
				HorizontalPanel tabs = new HorizontalPanel();
				String grpName = "distntpop" ;
				final RadioButton cl = new RadioButton(grpName, "Comments");
				final RadioButton ul = new RadioButton(grpName, "Bio");
				tabs.add(cl);
				tabs.add(ul);
				tabs.setSpacing(20);
				ul.setValue(true);
				ValueChangeHandler<Boolean> buttonChangeManager = new ValueChangeHandler<Boolean>() {

					@Override
					public void onValueChange(ValueChangeEvent<Boolean> event) {
						if(ul.getValue())
							container.showWidget(0);
						else
							container.showWidget(1);
					}
				};
				
				cl.addValueChangeHandler(buttonChangeManager);
				ul.addValueChangeHandler(buttonChangeManager);
				p.setGlassEnabled(true);
				VerticalPanel buttonsAndContent = new VerticalPanel();
				buttonsAndContent.add(tabs);
				container.add(u);
				container.add(c);
				container.showWidget(0);
				//container.setAnimationDuration(0);
				buttonsAndContent.add(container);
				buttonsAndContent.setSize("650px", "400px");
				container.setSize("640px", "390px");
				p.add(buttonsAndContent);
			}
			@Override
			public void onValueChange(ValueChangeEvent<TableMessage> event) {
				if(event.getValue() == null)
				{
					u.clear();
					if(p.isShowing())
						p.hide();
					return;
				}
				
				u.setContactValues(event.getValue());
				u.enablePanel(false);
				c.setCommentID(event.getValue().getMessageId());
				//container.showWidget(0);
				p.center();
			}
		});			
	}
	
}
