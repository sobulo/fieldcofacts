/*
 * Copyright 2008 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.fertiletech.fieldcodata.ui.client;


import java.util.ArrayList;
import java.util.List;

import com.fertiletech.fieldcodata.ui.client.MainMenuTreeViewModel.Category;
import com.fertiletech.fieldcodata.ui.client.gui.HelpPageGenerator;
import com.fertiletech.fieldcodata.ui.client.gui.WelcomePanel;
import com.fertiletech.fieldcodata.ui.shared.LoginInfo;
import com.fertiletech.fieldcodata.ui.shared.LoginRoles;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.prefetch.Prefetcher;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.HeadElement;
import com.google.gwt.dom.client.LinkElement;
import com.google.gwt.event.logical.shared.OpenEvent;
import com.google.gwt.event.logical.shared.OpenHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.LocaleInfo;
import com.google.gwt.user.cellview.client.CellTree;
import com.google.gwt.user.cellview.client.TreeNode;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;


public class Showcase{
	
    private final static String ADMIN_PAGE_URL_PREFIX = GWT.getHostPageBaseURL();
    private final static String ADMIN_PAGE_URL;
    static
    {
    	if(!GWT.isProdMode()) //true if dev mode
    		ADMIN_PAGE_URL = ADMIN_PAGE_URL_PREFIX + GUIConstants.GWT_CODE_SERVER;
    	else
    		ADMIN_PAGE_URL = ADMIN_PAGE_URL_PREFIX;
    }
    


    //different views
    ContentWidget requestedPanel; //last panel requested as part of history token
    String requestArgs;
    LoginServiceAsync loginUtil = GWT.create(LoginService.class);

    final AsyncCallback<LoginInfo> ensureLoginCallback = new AsyncCallback<LoginInfo>() {

        @Override
        public void onSuccess(LoginInfo result) {
            if (!result.isLoggedIn() || requestedPanel instanceof WelcomePanel) {
            	WelcomePanel firstPanel = getFirstWidget(shell.getMainMenu());
            	updateFirstPanel(firstPanel, result);
            	requestedPanel = firstPanel; requestArgs = null;
            	loadNewPanel();
            }
            else if(result.getRole().equals(LoginRoles.ROLE_PUBLIC))
            {
            	if(result.getLoginID().toLowerCase().contains("@fieldcolimited.com"))
            	{
                	Window.alert("You do not have necessary privileges to view this page."  +
                			"Contact pilot-group@fieldcolimited.com to request access.");
            		
            	}
            	else
            	{
                	Window.alert("You do not have necessary privileges to view this page. Only accounts under the Fieldco domain" +
                			" may log in. If you have a fieldco account, please log out of all your Google accounts and try accessing the portal again");		
            	}
            	shell.showAdminScreen();
            }
            else
            {
            	if(requestedPanel.getHelpUrl().equals(HelpPageGenerator.HELP_ADMIN_URL) && !result.getPrivilleges()[0])
            	{
            		shell.showAdminScreen();
            	}
            	else
            		loadNewPanel();  //whew, finally show the screen requested.
            }
        }

        @Override
        public void onFailure(Throwable caught) {
            String displayMessage = "Unable to reach server, try a browser refresh." +
            " Error was: [" + caught.getMessage() + "]<br/>";
            //displayMessage += setupLink(ADMIN_PAGE_URL, "Click here to retry");
            Window.alert(displayMessage);
            //firstPanel.setLoginMessage(displayMessage);
        }
    };
    
    private void loadNewPanel()
    {
    	GWT.log("LOAD Arg: " + requestArgs);
        shell.setContent(requestedPanel);
    	if(requestedPanel instanceof CWArguments && requestArgs != null)
    	{
    		((CWArguments)	requestedPanel).setParameterValues(requestArgs);
    	}
        Window.setTitle("Fieldco F.A.C.T.S: " + requestedPanel.getName());    	
    }
    
    private String setupLink(String url, String display)
    {
    	return "<a href='" + url + "'>" + display + "</a>"; 
    }
    
    private void updateFirstPanel(WelcomePanel firstPanel, LoginInfo result)
    {
    	String displayMessage = null;
    	if(firstPanel == null) return;
    	firstPanel.refreshFeed(result.isLoggedIn());
    	if(result.isLoggedIn())
    	{
        	displayMessage = setupLink(result.getLogoutUrl(), "Click here to log out");
        	firstPanel.setUserName(result.getLoginName());
    	}
    	else
        {
        	//google apps login idx
        	int idx = LoginInfo.GOOGLE_PROVIDER_IDX;
            String url = result.getLoginUrl()[idx];
            displayMessage = setupLink(url, "Login with your Google Apps Account");
        }
        firstPanel.setLoginMessage(displayMessage);
    }
	
	
  /**
   * The type passed into the
   * {@link com.google.gwt.sample.showcase.generator.ShowcaseGenerator}.
   */
  private static final class GeneratorInfo {
  }

  /**
   * The static images used throughout the Showcase.
   */
  public static final ShowcaseResources images = GWT.create(
      ShowcaseResources.class);

  /**
   * The name of the style theme used in showcase.
   */
  public static final String THEME = "clean";

  /**
   * Get the token for a given content widget.
   *
   * @return the content widget token.
   */
  public static String getContentWidgetToken(ContentWidget content) {
    return getContentWidgetToken(content.getClass());
  }

  /**
   * Get the token for a given content widget.
   *
   * @return the content widget token.
   */
  public static <C extends ContentWidget> String getContentWidgetToken(
      Class<C> cwClass) {
    String className = cwClass.getName();
    className = className.substring(className.lastIndexOf('.') + 1);
    return "do" + className;
  }

  /**
   * The main application shell.
   */
  private ShowcaseShell shell;

  /**
   * This is the entry point method. At least it used to be, now we call it from PortalEntryPoint
   */
  public Showcase(RootPanel p) {
    // Generate the source code and css for the examples
    GWT.create(GeneratorInfo.class);

    // Inject global styles.
    injectThemeStyleSheet();
    images.css().ensureInjected();

    // Create the application shell.
    final SingleSelectionModel<ContentWidget> selectionModel = new SingleSelectionModel<ContentWidget>();
    final MainMenuTreeViewModel treeModel = new MainMenuTreeViewModel(selectionModel);
    //Set<ContentWidget> contentWidgets = treeModel.getAllContentWidgets();
    shell = new ShowcaseShell(treeModel);
    RootLayoutPanel.get().add(shell);

    // Prefetch examples when opening the Category tree nodes.
    final List<Category> prefetched = new ArrayList<Category>();
    final CellTree mainMenu = shell.getMainMenu();
    mainMenu.addOpenHandler(new OpenHandler<TreeNode>() {
      public void onOpen(OpenEvent<TreeNode> event) {
        Object value = event.getTarget().getValue();
        if (!(value instanceof Category)) {
          return;
        }

        Category category = (Category) value;
        if (!prefetched.contains(category)) {
          prefetched.add(category);
          Prefetcher.prefetch(category.getSplitPoints());
        }
      }
    });

    // Always prefetch.
    Prefetcher.start();

    // Change the history token when a main menu item is selected.
    selectionModel.addSelectionChangeHandler(
        new SelectionChangeEvent.Handler() {
          public void onSelectionChange(SelectionChangeEvent event) {
        	ContentWidget selected = selectionModel.getSelectedObject();
        	GWT.log("Menu Selection MODEL change detected: " + selected);
            if (selected != null) {
            	if(selected == requestedPanel)
            		return;            	
            	String token = getContentWidgetToken(selected);            	
            	GWT.log("Selection model adding token: " + token);
              History.newItem(token, true);
            }
          }
        });

    // Setup a history handler to reselect the associate menu item.
    final ValueChangeHandler<String> historyHandler = new ValueChangeHandler<
        String>() {
      public void onValueChange(ValueChangeEvent<String> event) {
    	  GWT.log("HISTORY: " + event.getValue());
    	  String[] urlParams = event.getValue().split("/");
    	  String token = urlParams[0];
    	  String args = null;
    	  if(urlParams.length == 2 && urlParams[1].trim().length() > 0)
    		  args = urlParams[1].trim();
    	  GWT.log("TOKEN: " + token + " Args: " + args);
    	  showRequestedHistory(treeModel, mainMenu, selectionModel, token, args);
      }
    };
    History.addValueChangeHandler(historyHandler);

    // Show the initial example.
    if (History.getToken().length() > 0) {
    	GWT.log("About to fire an initial history of: " + History.getToken());
      History.fireCurrentHistoryState();
    } else {
    	String requestUrl = Window.Location.getParameter("history");
    	if(requestUrl != null && requestUrl.length() > 0)
    	{
    		History.newItem(requestUrl);
    	}
    	else
    		selectionModel.setSelected(getFirstWidget(mainMenu), true);
    }
    p.addStyleName("removeStaffMessage");
  }
  
  public WelcomePanel getFirstWidget(CellTree mainMenu )
  {
      TreeNode root = mainMenu.getRootTreeNode();
      TreeNode category = root.setChildOpen(0, true);
      ContentWidget content = (ContentWidget) category.getChildValue(0);
      return (WelcomePanel) content; //first widget in mainmenutreemodel class def is a welcomepanel
	  
  }
  
  public void showRequestedHistory(MainMenuTreeViewModel treeModel, CellTree mainMenu, 
		  SingleSelectionModel<ContentWidget> selectionModel, String token, String args)
  {
      // Get the content widget associated with the history token.
      ContentWidget contentWidget = treeModel.getContentWidgetForToken(token);
      if (contentWidget == null) {
        return;
      }
      
      // Expand the tree node associated with the content.
      Category category = treeModel.getCategoryForContentWidget(
          contentWidget);
      TreeNode node = mainMenu.getRootTreeNode();
      int childCount = node.getChildCount();
      for (int i = 0; i < childCount; i++) {
        if (node.getChildValue(i) == category) {
          node.setChildOpen(i, true, true);
          break;
        }
      }
      
      //Window.alert("History Called");

      // Select the node in the tree. is below really necessary?
      selectionModel.setSelected(contentWidget, true);

      // Display the content widget.
      displayContentWidget(contentWidget, args);
	  
  }
  
  /**
   * Set the content to the {@link ContentWidget}.
   *
   * @param content the {@link ContentWidget} to display
   */
  private void displayContentWidget(ContentWidget content, String args) {
    if (content == null) {
    	GWT.log("Received a null panel request");
      return;
    }
    GWT.log("Received a panel request for: " + content.getName());
    //Window.alert("Display got Called: " + firstTime + " - " + content);
    requestedPanel = content;
    requestArgs = args;
    shell.showLoading();
    String redirectURL = GWT.getHostPageBaseURL() + "#" + getContentWidgetToken(requestedPanel);
    if(args!=null && args.length() > 0)
    	redirectURL += ("/" + args);
    GWT.log("URL REDIRECT:" + redirectURL);
    loginUtil.login(redirectURL, ensureLoginCallback);
  }

  /**
   * Convenience method for getting the document's head element.
   *
   * @return the document's head element
   */
  private native HeadElement getHeadElement() /*-{
    return $doc.getElementsByTagName("head")[0];
  }-*/;

  /**
   * Inject the GWT theme style sheet based on the RTL direction of the current
   * locale.
   */
  private void injectThemeStyleSheet() {
    // Choose the name style sheet based on the locale.
    String styleSheet = "gwt/" + THEME + "/" + THEME;
    styleSheet += LocaleInfo.getCurrentLocale().isRTL() ? "_rtl.css" : ".css";

    // Load the GWT theme style sheet
    String modulePath = GWT.getModuleBaseURL();
    LinkElement linkElem = Document.get().createLinkElement();
    linkElem.setRel("stylesheet");
    linkElem.setType("text/css");
    linkElem.setHref(modulePath + styleSheet);
    getHeadElement().appendChild(linkElem);
  }
}
