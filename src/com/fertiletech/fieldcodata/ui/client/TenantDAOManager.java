package com.fertiletech.fieldcodata.ui.client;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import com.fertiletech.fieldcodata.ui.shared.UtilConstants.ContactTypes;
import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.fertiletech.fieldcodata.ui.shared.exceptions.DuplicateEntitiesException;
import com.fertiletech.fieldcodata.ui.shared.exceptions.MissingEntitiesException;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("tenants")
public interface TenantDAOManager extends RemoteService {
	public HashMap<String, String> getBuildingNames();
	
	public TableMessage createNewTenant(HashMap<String, String> prospectiveData, TableMessage m) throws DuplicateEntitiesException;
	
	public TableMessage changeApartmentInfo(String tenantID, String apartmentID, String comments) throws MissingEntitiesException, DuplicateEntitiesException;
	
	public HashMap<String, String> getAllBuildingAndApartments();
	public HashMap<String, String> getBuildingApartments(String buildingID) throws MissingEntitiesException;
	List<TableMessage> getBuildingTenants(String buildingID, Boolean archive) throws MissingEntitiesException;
	
	public HashMap<String, String> getAllProspects();
	public HashMap<String, String> getAllTenants();
	public HashMap<String, String>[] getApartments(String buildingID);
	
	public HashMap<String, String> getProspectiveData(String prospectiveId);
	public String saveProspectiveData(String prospectId, HashMap<String, String> supplementary);
	
	public String[] createContact(ContactTypes t, String firstName, String lastName, Date dob, String primaryEmail, String primaryNumber,
			HashSet<String> secondaryNumbers, HashSet<String> secondaryEmails, String companyName, String tenantAddress, Boolean isMale, String title, String[] args);
	TableMessage updateContact(String contactID, String firstName,
			String lastName, Date dob, String primaryEmail,
			String primaryNumber, HashSet<String> secondaryNumbers,
			HashSet<String> secondaryEmails, String companyName,
			String tenantAddress, Boolean isMale, String title, String[] args) throws MissingEntitiesException;

	
	public TableMessage getContact(String contactKeyStr) throws MissingEntitiesException;
	
	List<TableMessage> getProspectsTable(Date start, Date end);

	List<TableMessage> getBuildingApartmentTable(String buildingID);
	List<TableMessage> getTenantRelations(String tenantID);
	HashMap<String, List<TableMessage>> getTenantContacts();
	TableMessage getResident(String locationID);
	List<TableMessage> getLeases(String tenantID) throws MissingEntitiesException;
	List<TableMessage> getLeases(Date startDate, Date endDate) throws MissingEntitiesException;
	TableMessage saveLease(TableMessage m, String comments);
	String removeLease(String leaseID, String comments) throws MissingEntitiesException;
	TableMessage archiveTenant(String tenantID, String comments) throws DuplicateEntitiesException, MissingEntitiesException;
	
}
