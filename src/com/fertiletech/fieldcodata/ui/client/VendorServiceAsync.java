package com.fertiletech.fieldcodata.ui.client;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import com.fertiletech.fieldcodata.ui.shared.dto.TableMessage;
import com.google.gwt.user.client.rpc.AsyncCallback;



/**
 * The async counterpart of <code>GreetingService</code>.
 */
public interface VendorServiceAsync {

	void getAllVendors(AsyncCallback<List<TableMessage>> callback);

	void createVendor(String category, String firstName, String lastName,
			Date dob, String primaryEmail, String primaryNumber,
			HashSet<String> secondaryNumbers, HashSet<String> secondaryEmails,
			String companyName, String address, Boolean isMale, String title,
			boolean throwDuplicateException,
			AsyncCallback<TableMessage> callback);

	void changeVendorCategory(String vendorID, String category,
			AsyncCallback<TableMessage> callback);

	void getAllVendorsIDs(AsyncCallback<HashMap<String, String>> callback);
	
	
}
