package com.fertiletech.fieldcodata.backend.entities;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.fertiletech.fieldcodata.ui.shared.dto.DTOConstants;
import com.fertiletech.fieldcodata.ui.shared.exceptions.DuplicateEntitiesException;
import com.fertiletech.fieldcodata.ui.shared.exceptions.MissingEntitiesException;
import com.fertiletech.testutils.Order;
import com.fertiletech.testutils.OrderedRunner;
import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;


@RunWith(OrderedRunner.class)
public class ReadDAOTest {
	private static final LocalServiceTestHelper helper = new LocalServiceTestHelper(
            new LocalDatastoreServiceTestConfig());
    private static Objectify ofy;
    

    private static final String TEST_KEY1 = "abc";
    private static final String TEST_KEY2 = "def";    
    private static final String TEST_VAL1 = "hello";
    private static final String TEST_VAL2 = "10";

    static
    {
    	WriteDAO.registerClassesWithObjectify();
    }
    
    private static Key<ApplicationParameters> PARAM_KEY = null;
    
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		helper.setUp();
		PARAM_KEY = new Key<ApplicationParameters>(ApplicationParameters.class, DTOConstants.APP_PARAM_COMPANY_LIST_KEY);
		ofy = ObjectifyService.begin();
	}
	
	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		helper.tearDown();
	}
	
	
	@Test(expected=MissingEntitiesException.class)
	@Order(order = 1)
	public void updateApplicationParameters() throws MissingEntitiesException
	{
		HashMap<String, String> params = new HashMap<String, String>();
		params.put(TEST_KEY1, TEST_VAL2);
		WriteDAO.updateApplicationParameters("", PARAM_KEY, params);
	}	
	
	@Test
	@Order(order = 2)
	public void testCreateApplicationParameters() throws DuplicateEntitiesException
	{
		HashMap<String, String> params = new HashMap<String, String>();
		params.put(TEST_KEY1, TEST_VAL1);
		params.put(TEST_KEY2, TEST_VAL2);
		//create and store a param obj in the datastore
		ApplicationParameters createdParamObj = WriteDAO.createApplicationParameters(DTOConstants.APP_PARAM_COMPANY_LIST_KEY, 
				params, null);
		
		assertTrue(createdParamObj.getKey() != null);
	}
	
	@Test
	@Order(order = 3)
	public void readCreatedApplicationParameters()
	{
		ApplicationParameters paramObj = ofy.get(PARAM_KEY); 
		assertTrue(paramObj != null);
		assertEquals(paramObj.getParams().get(TEST_KEY1), TEST_VAL1);
	}
	
	@Test(expected=DuplicateEntitiesException.class)
	@Order(order = 4)
	public void testDuplicateException() throws DuplicateEntitiesException
	{
		HashMap<String, String> params = new HashMap<String, String>();
		params.put(TEST_VAL1, TEST_VAL2);
		params.put(TEST_KEY2, TEST_VAL1);
		//create and store a param obj in the datastore
		ApplicationParameters createdParamObj = WriteDAO.createApplicationParameters(DTOConstants.APP_PARAM_COMPANY_LIST_KEY, 
				params, null);
	}
	
	@Test
	@Order(order = 5)
	public void readCreatedApplicationParametersAgain()
	{
		ApplicationParameters paramObj = ofy.get(PARAM_KEY); 
		assertTrue(paramObj != null);
		assertEquals(paramObj.getParams().get(TEST_KEY1), TEST_VAL1);
	}
	
	@Test
	@Order(order = 6)
	public void updateApplicationParametersAgain() throws MissingEntitiesException
	{
		HashMap<String, String> params = new HashMap<String, String>();
		params.put(TEST_KEY1, TEST_VAL2);
		WriteDAO.updateApplicationParameters("", PARAM_KEY, params);
	}
	
	@Test
	@Order(order = 7)
	public void readUpdatedApplicationParameters()
	{
		ApplicationParameters paramObj = ofy.get(new Key<ApplicationParameters>(ApplicationParameters.class, 
				DTOConstants.APP_PARAM_COMPANY_LIST_KEY)); 
		assertTrue(paramObj != null);
		assertEquals(paramObj.getParams().get(TEST_KEY1), TEST_VAL2);
	}	
	
	@Test
	@Order(order = 8)
	public void testCreateBuilding() throws DuplicateEntitiesException
	{
		Building b = WriteDAO.createBuilding(TEST_KEY1, TEST_VAL1, TEST_VAL2);
		assertTrue(b.getKey() != null);
		
		b = WriteDAO.createBuilding(TEST_KEY2, TEST_VAL2, TEST_VAL1);
		assertEquals(TEST_VAL1, b.getBuildingAddress().getValue());
	}
	
	@Test
	@Order(order = 9)
	public void readBuilding()
	{
		List<Key<Building>> buildingKeys = ReadDAO.getAllBuildings(ofy);
		assertEquals(2, buildingKeys.size());
		
		Building b = ofy.get(new Key<Building>(Building.class, TEST_KEY1));
		assertEquals(TEST_VAL1, b.getDisplayName());
	}
	
	@Test(expected=DuplicateEntitiesException.class)
	@Order(order = 10)
	public void testDuplicateBuilding() throws DuplicateEntitiesException
	{
		WriteDAO.createBuilding(TEST_KEY1, TEST_VAL2, TEST_VAL1);
	}
	
	@Test
	@Order(order = 11)
	public void testCreateApartment() throws DuplicateEntitiesException
	{
		Key<Building> buildingKey = new Key<Building>(Building.class, TEST_KEY2);
		Apartment a = WriteDAO.createApartment(buildingKey, TEST_KEY1);
		assertTrue(a.getKey() != null);		
	}
	
	@Test
	@Order(order = 12)
	public void readApartment()
	{
		Key<Building> buildingKey = new Key<Building>(Building.class, TEST_KEY2);
		List<Key<Apartment>> aptKeys = ReadDAO.getApartments(ofy, buildingKey);
		assertEquals(1, aptKeys.size());
	}
	
	@Test
	@Order(order = 13)
	public void testCreateTenant() throws DuplicateEntitiesException
	{
		Key<Building> buildingKey = new Key<Building>(Building.class, TEST_KEY2);
		Key<Apartment> aptKey = ReadDAO.getApartments(ofy, buildingKey).get(0);
		HashSet<String> nums = new HashSet<String>();
		nums.add(TEST_VAL1);
		Tenant t = WriteDAO.createTenant(TEST_KEY1, TEST_KEY1, null, TEST_KEY1, TEST_KEY2, null, nums, TEST_KEY1, TEST_VAL1, true, "Mr", aptKey, null);
		assertTrue(t.getKey() != null);		
	}
	
	@Test
	@Order(order = 14)
	public void testReadTenant()
	{
		Key<Building> buildingKey = new Key<Building>(Building.class, TEST_KEY2);
		Key<Apartment> aptKey = ReadDAO.getApartments(ofy, buildingKey).get(0);
		Key<Tenant> t = ReadDAO.getCurrentTenant(ofy, aptKey);
		assertTrue(t != null);		
	}
	
	@Test(expected=DuplicateEntitiesException.class)
	@Order(order = 15)
	public void testDuplicateTenant() throws DuplicateEntitiesException //actually it's a test to see if 2 primary tenants can be slotted in the same apt
	{
		Key<Building> buildingKey = new Key<Building>(Building.class, TEST_KEY2);
		Key<Apartment> aptKey = ReadDAO.getApartments(ofy, buildingKey).get(0);
		Tenant t = WriteDAO.createTenant(TEST_KEY2, TEST_KEY2, null, TEST_KEY2, TEST_KEY1, null, null, TEST_KEY1, TEST_VAL1, false, "Mrs", aptKey, null);
		assertTrue(t.getKey() != null);				
	}	
	
}